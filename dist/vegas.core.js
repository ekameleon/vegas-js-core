/**
 * A specialized set of basic functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc. - version: 1.0.45 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.core = factory());
}(this, (function () { 'use strict';

    var skip = false;
    var isChrome = navigator && navigator.userAgent && navigator.vendor && /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    function sayHello() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var link = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
      if (skip) {
        return;
      }
      try {
        if (isChrome) {
          var args = ["\n %c %c %c ".concat(name, " ").concat(version, " %c %c ").concat(link, " %c %c\n\n"), 'background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #000000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'color: #F7FF3C; background: #ff0000; padding:5px 0;', 'background: #AA0000; padding:5px 0;', 'background: #ff0000; padding:5px 0;'];
          window.console.log.apply(console, args);
        } else if (window.console) {
          window.console.log("".concat(name, " ").concat(version, " - ").concat(link));
        }
      } catch (error) {
      }
    }
    function skipHello() {
      skip = true;
    }

    var object;
    try {
      object = global$1 || null;
    } catch (ignore) {}
    if (!object) {
      try {
        object = window;
      } catch (e) {}
    }
    if (!object) {
      try {
        object = document;
      } catch (ignore) {}
    }
    if (!object) {
      object = {};
    }
    var global$1 = object;

    var performance = global$1.performance || {};
    Object.defineProperty(global$1, 'performance', {
      value: performance,
      configurable: true,
      writable: true
    });
    performance.now = performance.now || performance.mozNow || performance.msNow || performance.oNow || performance.webkitNow;
    if (!(global$1.performance && global$1.performance.now)) {
      var startTime = Date.now();
      global$1.performance.now = function () {
        return Date.now() - startTime;
      };
    }

    var canUseSymbol$1 = typeof Symbol === 'function' && Symbol["for"];

    function toUnicodeNotation(num) {
      var hex = num.toString(16);
      while (hex.length < 4) {
        hex = "0" + hex;
      }
      return hex;
    }

    function dump$1(value) {
      var prettyprint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var indent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var indentor = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "    ";
      indent = isNaN(indent) ? 0 : indent;
      prettyprint = Boolean(prettyprint);
      if (!indentor) {
        indentor = "    ";
      }
      if (value === undefined) {
        return "undefined";
      } else if (value === null) {
        return "null";
      } else if (typeof value === "string" || value instanceof String) {
        return dumpString(value);
      } else if (typeof value === "boolean" || value instanceof Boolean) {
        return value ? "true" : "false";
      } else if (typeof value === "number" || value instanceof Number) {
        return value.toString();
      } else if (value instanceof Date) {
        return dumpDate(value);
      } else if (value instanceof Array) {
        return dumpArray(value, prettyprint, indent, indentor);
      } else if (value.constructor && value.constructor === Object) {
        return dumpObject(value, prettyprint, indent, indentor);
      } else if ("toSource" in value) {
        return value.toSource(indent);
      } else {
        return '<unknown>';
      }
    }
    function dumpArray(value) {
      var prettyprint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var indent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var indentor = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "    ";
      indent = isNaN(indent) ? 0 : indent;
      prettyprint = Boolean(prettyprint);
      if (!indentor) {
        indentor = "    ";
      }
      var source = [];
      var i;
      var l = value.length;
      for (i = 0; i < l; i++) {
        if (value[i] === undefined) {
          source.push("undefined");
          continue;
        }
        if (value[i] === null) {
          source.push("null");
          continue;
        }
        if (prettyprint) {
          indent++;
        }
        source.push(dump$1(value[i], prettyprint, indent, indentor));
        if (prettyprint) {
          indent--;
        }
      }
      if (prettyprint) {
        var spaces = [];
        for (i = 0; i < indent; i++) {
          spaces.push(indentor);
        }
        var decal = '\n' + spaces.join('');
        return decal + "[" + decal + indentor + source.join("," + decal + indentor) + decal + "]";
      } else {
        return "[" + source.join(",") + "]";
      }
    }
    function dumpDate(date ) {
      var timestamp = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      timestamp = Boolean(timestamp);
      if (timestamp) {
        return "new Date(" + String(date.valueOf()) + ")";
      } else {
        var y = date.getFullYear();
        var m = date.getMonth();
        var d = date.getDate();
        var h = date.getHours();
        var mn = date.getMinutes();
        var s = date.getSeconds();
        var ms = date.getMilliseconds();
        var data = [y, m, d, h, mn, s, ms];
        data.reverse();
        while (data[0] === 0) {
          data.splice(0, 1);
        }
        data.reverse();
        return "new Date(" + data.join(",") + ")";
      }
    }
    function dumpObject(value) {
      var prettyprint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var indent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var indentor = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "    ";
      indent = isNaN(indent) ? 0 : indent;
      prettyprint = Boolean(prettyprint);
      if (!indentor) {
        indentor = "    ";
      }
      var source = [];
      for (var member in value) {
        if (value.hasOwnProperty(member)) {
          if (value[member] === undefined) {
            source.push(member + ":" + "undefined");
            continue;
          }
          if (value[member] === null) {
            source.push(member + ":" + "null");
            continue;
          }
          if (prettyprint) {
            indent++;
          }
          source.push(member + ":" + dump$1(value[member], prettyprint, indent, indentor));
          if (prettyprint) {
            indent--;
          }
        }
      }
      source = source.sort();
      if (prettyprint) {
        var spaces = [];
        for (var i = 0; i < indent; i++) {
          spaces.push(indentor);
        }
        var decal = '\n' + spaces.join('');
        return decal + '{' + decal + indentor + source.join(',' + decal + indentor) + decal + '}';
      } else {
        return '{' + source.join(',') + '}';
      }
    }
    function dumpString(value) {
      var code;
      var quote = "\"";
      var str = "";
      var ch = "";
      var pos = 0;
      var len = value.length;
      while (pos < len) {
        ch = value.charAt(pos);
        code = value.charCodeAt(pos);
        if (code > 0xFF) {
          str += "\\u" + toUnicodeNotation(code);
          pos++;
          continue;
        }
        switch (ch) {
          case "\b":
            {
              str += "\\b";
              break;
            }
          case "\t":
            {
              str += "\\t";
              break;
            }
          case "\n":
            {
              str += "\\n";
              break;
            }
          case "\x0B":
            {
              str += "\\v";
              break;
            }
          case "\f":
            {
              str += "\\f";
              break;
            }
          case "\r":
            {
              str += "\\r";
              break;
            }
          case "\"":
            {
              str += "\\\"";
              break;
            }
          case "'":
            {
              str += "\\\'";
              break;
            }
          case "\\":
            {
              str += "\\\\";
              break;
            }
          default:
            {
              str += ch;
            }
        }
        pos++;
      }
      return quote + str + quote;
    }

    var trace$1 = function trace(context) {
      if (console) {
        console.log(context);
      }
    };

    function _arrayLikeToArray(r, a) {
      (null == a || a > r.length) && (a = r.length);
      for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
      return n;
    }
    function _arrayWithHoles(r) {
      if (Array.isArray(r)) return r;
    }
    function _arrayWithoutHoles(r) {
      if (Array.isArray(r)) return _arrayLikeToArray(r);
    }
    function _checkPrivateRedeclaration(e, t) {
      if (t.has(e)) throw new TypeError("Cannot initialize the same private elements twice on an object");
    }
    function _classCallCheck(a, n) {
      if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function");
    }
    function _classPrivateFieldInitSpec(e, t, a) {
      _checkPrivateRedeclaration(e, t), t.set(e, a);
    }
    function _defineProperties(e, r) {
      for (var t = 0; t < r.length; t++) {
        var o = r[t];
        o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o);
      }
    }
    function _createClass(e, r, t) {
      return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", {
        writable: !1
      }), e;
    }
    function _createForOfIteratorHelper(r, e) {
      var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
      if (!t) {
        if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) {
          t && (r = t);
          var n = 0,
            F = function () {};
          return {
            s: F,
            n: function () {
              return n >= r.length ? {
                done: !0
              } : {
                done: !1,
                value: r[n++]
              };
            },
            e: function (r) {
              throw r;
            },
            f: F
          };
        }
        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
      }
      var o,
        a = !0,
        u = !1;
      return {
        s: function () {
          t = t.call(r);
        },
        n: function () {
          var r = t.next();
          return a = r.done, r;
        },
        e: function (r) {
          u = !0, o = r;
        },
        f: function () {
          try {
            a || null == t.return || t.return();
          } finally {
            if (u) throw o;
          }
        }
      };
    }
    function _defineProperty(e, r, t) {
      return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, {
        value: t,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }) : e[r] = t, e;
    }
    function _iterableToArray(r) {
      if ("undefined" != typeof Symbol && null != r[Symbol.iterator] || null != r["@@iterator"]) return Array.from(r);
    }
    function _iterableToArrayLimit(r, l) {
      var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
      if (null != t) {
        var e,
          n,
          i,
          u,
          a = [],
          f = !0,
          o = !1;
        try {
          if (i = (t = t.call(r)).next, 0 === l) {
            if (Object(t) !== t) return;
            f = !1;
          } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
        } catch (r) {
          o = !0, n = r;
        } finally {
          try {
            if (!f && null != t.return && (u = t.return(), Object(u) !== u)) return;
          } finally {
            if (o) throw n;
          }
        }
        return a;
      }
    }
    function _nonIterableRest() {
      throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    function _nonIterableSpread() {
      throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    function ownKeys(e, r) {
      var t = Object.keys(e);
      if (Object.getOwnPropertySymbols) {
        var o = Object.getOwnPropertySymbols(e);
        r && (o = o.filter(function (r) {
          return Object.getOwnPropertyDescriptor(e, r).enumerable;
        })), t.push.apply(t, o);
      }
      return t;
    }
    function _objectSpread2(e) {
      for (var r = 1; r < arguments.length; r++) {
        var t = null != arguments[r] ? arguments[r] : {};
        r % 2 ? ownKeys(Object(t), !0).forEach(function (r) {
          _defineProperty(e, r, t[r]);
        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) {
          Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
        });
      }
      return e;
    }
    function _objectWithoutProperties(e, t) {
      if (null == e) return {};
      var o,
        r,
        i = _objectWithoutPropertiesLoose(e, t);
      if (Object.getOwnPropertySymbols) {
        var s = Object.getOwnPropertySymbols(e);
        for (r = 0; r < s.length; r++) o = s[r], t.includes(o) || {}.propertyIsEnumerable.call(e, o) && (i[o] = e[o]);
      }
      return i;
    }
    function _objectWithoutPropertiesLoose(r, e) {
      if (null == r) return {};
      var t = {};
      for (var n in r) if ({}.hasOwnProperty.call(r, n)) {
        if (e.includes(n)) continue;
        t[n] = r[n];
      }
      return t;
    }
    function _slicedToArray(r, e) {
      return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray(r, e) || _nonIterableRest();
    }
    function _toArray(r) {
      return _arrayWithHoles(r) || _iterableToArray(r) || _unsupportedIterableToArray(r) || _nonIterableRest();
    }
    function _toConsumableArray(r) {
      return _arrayWithoutHoles(r) || _iterableToArray(r) || _unsupportedIterableToArray(r) || _nonIterableSpread();
    }
    function _toPrimitive(t, r) {
      if ("object" != typeof t || !t) return t;
      var e = t[Symbol.toPrimitive];
      if (void 0 !== e) {
        var i = e.call(t, r || "default");
        if ("object" != typeof i) return i;
        throw new TypeError("@@toPrimitive must return a primitive value.");
      }
      return ("string" === r ? String : Number)(t);
    }
    function _toPropertyKey(t) {
      var i = _toPrimitive(t, "string");
      return "symbol" == typeof i ? i : i + "";
    }
    function _typeof(o) {
      "@babel/helpers - typeof";

      return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
        return typeof o;
      } : function (o) {
        return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
      }, _typeof(o);
    }
    function _unsupportedIterableToArray(r, a) {
      if (r) {
        if ("string" == typeof r) return _arrayLikeToArray(r, a);
        var t = {}.toString.call(r).slice(8, -1);
        return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0;
      }
    }

    var _value = new WeakMap();
    var version$1 = function () {
      function version() {
        var major = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var minor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var build = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
        var revision = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
        _classCallCheck(this, version);
        _classPrivateFieldInitSpec(this, _value, void 0);
        this._value = major << 28 | minor << 24 | build << 16 | revision;
      }
      return _createClass(version, [{
        key: "build",
        get:
        function get() {
          return (this._value & 0x00FF0000) >>> 16;
        }
        ,
        set: function set(value) {
          this._value = this._value & 0xFF00FFFF | value << 16;
        }
      }, {
        key: "major",
        get: function get() {
          return this._value >>> 28;
        }
        ,
        set: function set(value) {
          this._value = this._value & 0x0FFFFFFF | value << 28;
        }
      }, {
        key: "minor",
        get: function get() {
          return (this._value & 0x0F000000) >>> 24;
        }
        ,
        set: function set(value) {
          this._value = this._value & 0xF0FFFFFF | value << 24;
        }
      }, {
        key: "revision",
        get: function get() {
          return this._value & 0x0000FFFF;
        }
        ,
        set: function set(value) {
          this._value = this._value & 0xFFFF0000 | value;
        }
      }, {
        key: "toString",
        value:
        function toString() {
          var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
          var separator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ".";
          var data = [this.major, this.minor, this.build, this.revision];
          if (fields > 0 && fields < 5) {
            data = data.slice(0, fields);
          } else {
            var i;
            var l = data.length;
            for (i = l - 1; i > 0; i--) {
              if (data[i] === 0) {
                data.pop();
              } else {
                break;
              }
            }
          }
          return data.join(separator);
        }
      }, {
        key: "valueOf",
        value: function valueOf() {
          return this._value;
        }
      }], [{
        key: "fromNumber",
        value: function fromNumber() {
          var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
          var v = new version();
          if (isNaN(value) || value === 0 || value < 0 || value === Number.MAX_VALUE || value === Number.POSITIVE_INFINITY || value === Number.NEGATIVE_INFINITY) {
            return v;
          }
          v.major = value >>> 28;
          v.minor = (value & 0x0f000000) >>> 24;
          v.build = (value & 0x00ff0000) >>> 16;
          v.revision = value & 0x0000ffff;
          return v;
        }
      }, {
        key: "fromString",
        value: function fromString() {
          var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
          var separator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ".";
          var v = new version();
          if (value === null || !(value instanceof String || typeof value === 'string') || value === "") {
            return v;
          }
          if (value.indexOf(separator) > -1) {
            var values = value.split(separator);
            var len = values.length;
            if (len > 0) {
              v.major = parseInt(values[0]);
            }
            if (len > 1) {
              v.minor = parseInt(values[1]);
            }
            if (len > 2) {
              v.build = parseInt(values[2]);
            }
            if (len > 3) {
              v.revision = parseInt(values[3]);
            }
          } else {
            v.major = parseInt(value);
          }
          return v;
        }
      }]);
    }();

    var ONE_FRAME_TIME = 16;
    var lastTime = Date.now();
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    var len = vendors.length;
    for (var x = 0; x < len && !global$1.requestAnimationFrame; ++x) {
      var p = vendors[x];
      global$1.requestAnimationFrame = global$1["".concat(p, "RequestAnimationFrame")];
      global$1.cancelAnimationFrame = global$1["".concat(p, "CancelAnimationFrame")] || global$1["".concat(p, "CancelRequestAnimationFrame")];
    }
    if (!global$1.requestAnimationFrame) {
      global$1.requestAnimationFrame = function (callback) {
        if (typeof callback !== 'function') {
          throw new TypeError("".concat(callback, "is not a function"));
        }
        var currentTime = Date.now();
        var delay = ONE_FRAME_TIME + lastTime - currentTime;
        if (delay < 0) {
          delay = 0;
        }
        lastTime = currentTime;
        return setTimeout(function () {
          lastTime = Date.now();
          callback(performance.now());
        }, delay);
      };
    }
    if (!global$1.cancelAnimationFrame) {
      global$1.cancelAnimationFrame = function (id) {
        return clearTimeout(id);
      };
    }
    global$1.cancelAnimationFrame;
    var requestAnimationFrame = global$1.requestAnimationFrame;

    var eq$1 = function eq(object1, object2) {
      return object1 === object2 || object1 !== object1 && object2 !== object2;
    };

    var assocIndexOf = function assocIndexOf(array, key) {
      var length = array.length;
      while (length--) {
        if (eq$1(array[length][0], key)) {
          return length;
        }
      }
      return -1;
    };

    var ListCache = function () {
      function ListCache(entries) {
        var _entries$length;
        _classCallCheck(this, ListCache);
        this.clear();
        var index = -1;
        var length = (_entries$length = entries === null || entries === void 0 ? void 0 : entries.length) !== null && _entries$length !== void 0 ? _entries$length : 0;
        while (++index < length) {
          var entry = entries[index];
          this.set(entry[0], entry[1]);
        }
      }
      return _createClass(ListCache, [{
        key: "clear",
        value: function clear() {
          this.__data__ = [];
          this.size = 0;
        }
      }, {
        key: "delete",
        value: function _delete(key) {
          var data = this.__data__;
          var index = assocIndexOf(data, key);
          if (index < 0) {
            return false;
          }
          var lastIndex = data.length - 1;
          if (index === lastIndex) {
            data.pop();
          } else {
            Array.prototype.splice.call(data, index, 1);
          }
          --this.size;
          return true;
        }
      }, {
        key: "has",
        value: function has(key) {
          return assocIndexOf(this.__data__, key) > -1;
        }
      }, {
        key: "get",
        value: function get(key) {
          var data = this.__data__;
          var index = assocIndexOf(data, key);
          return index < 0 ? undefined : data[index][1];
        }
      }, {
        key: "set",
        value: function set(key, value) {
          var data = this.__data__;
          var index = assocIndexOf(data, key);
          if (index < 0) {
            ++this.size;
            data.push([key, value]);
          } else {
            data[index][1] = value;
          }
          return this;
        }
      }]);
    }();

    var freeGlobal = (typeof global === "undefined" ? "undefined" : _typeof(global)) === 'object' && global && global.Object === Object && global;

    var _ref;
    var freeSelf = (typeof self === "undefined" ? "undefined" : _typeof(self)) === 'object' && self && self.Object === Object && self;
    var root = (_ref = freeGlobal !== null && freeGlobal !== void 0 ? freeGlobal : freeSelf) !== null && _ref !== void 0 ? _ref : Function('return this')();

    var _Symbol = root.Symbol;

    var _Symbol$toStringTag$1;
    var symToStringTag$1 = (_Symbol$toStringTag$1 = _Symbol === null || _Symbol === void 0 ? void 0 : _Symbol.toStringTag) !== null && _Symbol$toStringTag$1 !== void 0 ? _Symbol$toStringTag$1 : undefined;
    var getRawTag = function getRawTag(value) {
      var isOwn = Object.prototype.hasOwnProperty.call(value, symToStringTag$1);
      var tag = value[symToStringTag$1];
      var unmasked;
      try {
        value[symToStringTag$1] = undefined;
        unmasked = true;
      } catch (doNothing) {}
      var result = Object.prototype.toString.call(value);
      if (unmasked) {
        if (isOwn) {
          value[symToStringTag$1] = tag;
        } else {
          delete value[symToStringTag$1];
        }
      }
      return result;
    };

    var toString$1 = function toString(value) {
      return Object.prototype.toString.call(value);
    };

    var _cloneableTags;
    var nullTag = '[object Null]';
    var undefinedTag = '[object Undefined]';
    var argsTag = '[object Arguments]';
    var arrayTag = '[object Array]';
    var asyncTag = '[object AsyncFunction]';
    var boolTag = '[object Boolean]';
    var dateTag = '[object Date]';
    var errorTag = '[object Error]';
    var funcTag = '[object Function]';
    var genTag = '[object GeneratorFunction]';
    var mapTag = '[object Map]';
    var numberTag = '[object Number]';
    var objectTag = '[object Object]';
    var proxyTag = '[object Proxy]';
    var regexpTag = '[object RegExp]';
    var setTag = '[object Set]';
    var stringTag = '[object String]';
    var symbolTag = '[object Symbol]';
    var weakSetTag = '[object WeakSet]';
    var weakMapTag = '[object WeakMap]';
    var arrayBufferTag = '[object ArrayBuffer]';
    var dataViewTag = '[object DataView]';
    var float32Tag = '[object Float32Array]';
    var float64Tag = '[object Float64Array]';
    var int8Tag = '[object Int8Array]';
    var int16Tag = '[object Int16Array]';
    var int32Tag = '[object Int32Array]';
    var uint8Tag = '[object Uint8Array]';
    var uint8ClampedTag = '[object Uint8ClampedArray]';
    var uint16Tag = '[object Uint16Array]';
    var uint32Tag = '[object Uint32Array]';
    var cloneableTags = (_cloneableTags = {}, _defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_cloneableTags, argsTag, true), arrayTag, true), arrayBufferTag, true), dataViewTag, true), boolTag, true), dateTag, true), float32Tag, true), float64Tag, true), int8Tag, true), int16Tag, true), _defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_cloneableTags, int32Tag, true), mapTag, true), numberTag, true), objectTag, true), regexpTag, true), setTag, true), stringTag, true), symbolTag, true), uint8Tag, true), uint8ClampedTag, true), _defineProperty(_defineProperty(_defineProperty(_defineProperty(_defineProperty(_cloneableTags, uint16Tag, true), uint32Tag, true), errorTag, false), funcTag, false), weakMapTag, false));
    var typedArrayTags = {
      float32Tag: true,
      float64Tag: true,
      int8Tag: true,
      int16Tag: true,
      int32Tag: true,
      uint8Tag: true,
      uint8ClampedTag: true,
      uint16Tag: true,
      uint32Tag: true,
      argsTag: false,
      arrayTag: false,
      arrayBufferTag: false,
      boolTag: false,
      dataViewTag: false,
      dateTag: false,
      errorTag: false,
      funcTag: false,
      mapTag: false,
      numberTag: false,
      objectTag: false,
      regexpTag: false,
      setTag: false,
      stringTag: false,
      weakMapTag: false
    };

    var _Symbol$toStringTag;
    var symToStringTag = (_Symbol$toStringTag = _Symbol === null || _Symbol === void 0 ? void 0 : _Symbol.toStringTag) !== null && _Symbol$toStringTag !== void 0 ? _Symbol$toStringTag : undefined;
    var baseGetTag = function baseGetTag(value) {
      if (value === null) {
        return nullTag;
      } else if (value === undefined) {
        return undefinedTag;
      }
      return symToStringTag && symToStringTag in Object(value) ? getRawTag(value) : toString$1(value);
    };

    var isObject$1 = function isObject(value) {
      var type = _typeof(value);
      return value !== null && value !== undefined && (type === 'object' || type === 'function');
    };

    var isFunction$1 = function isFunction(value) {
      if (!isObject$1(value)) {
        return false;
      }
      var tag = baseGetTag(value);
      return tag === funcTag || tag === genTag || tag === asyncTag || tag === proxyTag;
    };

    var coreJsData = root['__core-js_shared__'];

    var apply = function apply() {
      var _coreJsData$keys;
      var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && ((_coreJsData$keys = coreJsData.keys) === null || _coreJsData$keys === void 0 ? void 0 : _coreJsData$keys.IE_PROTO) || '');
      return uid ? 'Symbol(src)_1.' + uid : '';
    };
    var maskSrcKey = apply();
    var isMasked = function isMasked(func) {
      return !!maskSrcKey && maskSrcKey in func;
    };

    var toSource = function toSource(func) {
      if (func !== null) {
        try {
          return Function.prototype.toString.call(func);
        } catch (doNothing) {}
        try {
          return func + '';
        } catch (doNothing) {}
      }
      return '';
    };

    var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
    var reIsHostCtor = /^\[object .+?Constructor\]$/;
    var reIsNative = RegExp('^' + Function.prototype.toString.call(Object.prototype.hasOwnProperty).replace(reRegExpChar, '\\$&').replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$');
    var baseIsNative = function baseIsNative(value) {
      if (!isObject$1(value) || isMasked(value)) {
        return false;
      }
      var pattern = isFunction$1(value) ? reIsNative : reIsHostCtor;
      return pattern.test(toSource(value));
    };

    var getValue = function getValue(object, key) {
      return object === null || object === undefined ? undefined : object[key];
    };

    var getNative = function getNative(object, key) {
      var value = getValue(object, key);
      return baseIsNative(value) ? value : undefined;
    };

    var Map$1 = getNative(root, 'Map');

    var isKeyable = function isKeyable(value) {
      var type = _typeof(value);
      return type === 'string' || type === 'number' || type === 'symbol' || type === 'boolean' ? value !== '__proto__' : value === null;
    };

    var getMapData = function getMapData(map, key) {
      var data = map.__data__;
      return isKeyable(key) ? data[typeof key === 'string' ? 'string' : 'hash'] : data.map;
    };

    var nativeCreate = getNative(Object, 'create');

    var HASH_UNDEFINED$1 = '__hash_undefined__';
    var Hash = function () {
      function Hash(entries) {
        var _entries$length;
        _classCallCheck(this, Hash);
        this.clear();
        var index = -1;
        var length = (_entries$length = entries === null || entries === void 0 ? void 0 : entries.length) !== null && _entries$length !== void 0 ? _entries$length : 0;
        while (++index < length) {
          var entry = entries[index];
          this.set(entry[0], entry[1]);
        }
      }
      return _createClass(Hash, [{
        key: "clear",
        value: function clear() {
          this.__data__ = nativeCreate ? nativeCreate(null) : {};
          this.size = 0;
        }
      }, {
        key: "delete",
        value: function _delete(key) {
          var result = this.has(key) && delete this.__data__[key];
          this.size -= result ? 1 : 0;
          return result;
        }
      }, {
        key: "has",
        value: function has(key) {
          var data = this.__data__;
          return nativeCreate ? data[key] !== undefined : Object.prototype.hasOwnProperty.call(data, key);
        }
      }, {
        key: "get",
        value: function get(key) {
          var data = this.__data__;
          if (nativeCreate) {
            var result = data[key];
            return result === HASH_UNDEFINED$1 ? undefined : result;
          }
          return Object.prototype.hasOwnProperty.call(data, key) ? data[key] : undefined;
        }
      }, {
        key: "set",
        value: function set(key, value) {
          var data = this.__data__;
          this.size += this.has(key) ? 0 : 1;
          data[key] = nativeCreate && value === undefined ? HASH_UNDEFINED$1 : value;
          return this;
        }
      }]);
    }();

    var MapCache = function () {
      function MapCache(entries) {
        _classCallCheck(this, MapCache);
        this.clear();
        var index = -1;
        var length = entries == null ? 0 : entries.length;
        while (++index < length) {
          var entry = entries[index];
          this.set(entry[0], entry[1]);
        }
      }
      return _createClass(MapCache, [{
        key: "clear",
        value: function clear() {
          this.size = 0;
          var MapConstructor = Map$1 !== null && Map$1 !== void 0 ? Map$1 : ListCache;
          this.__data__ = {
            'hash': new Hash(),
            'map': new MapConstructor(),
            'string': new Hash()
          };
        }
      }, {
        key: "delete",
        value: function _delete(key) {
          var result = getMapData(this, key)['delete'](key);
          this.size -= result ? 1 : 0;
          return result;
        }
      }, {
        key: "has",
        value: function has(key) {
          return getMapData(this, key).has(key);
        }
      }, {
        key: "get",
        value: function get(key) {
          return getMapData(this, key).get(key);
        }
      }, {
        key: "set",
        value: function set(key, value) {
          var data = getMapData(this, key);
          var size = data.size;
          data.set(key, value);
          this.size += data.size === size ? 0 : 1;
          return this;
        }
      }]);
    }();

    var LARGE_ARRAY_SIZE = 200;
    var Stack = function () {
      function Stack(entries) {
        _classCallCheck(this, Stack);
        var data = this.__data__ = new ListCache(entries);
        this.size = data.size;
      }
      return _createClass(Stack, [{
        key: "clear",
        value: function clear() {
          this.__data__ = new ListCache();
          this.size = 0;
        }
      }, {
        key: "delete",
        value: function _delete(key) {
          var data = this.__data__;
          var result = data['delete'](key);
          this.size = data.size;
          return result;
        }
      }, {
        key: "get",
        value: function get(key) {
          return this.__data__.get(key);
        }
      }, {
        key: "has",
        value: function has(key) {
          return this.__data__.has(key);
        }
      }, {
        key: "set",
        value: function set(key, value) {
          var data = this.__data__;
          if (data instanceof ListCache) {
            var pairs = data.__data__;
            if (!Map$1 || pairs.length < LARGE_ARRAY_SIZE - 1) {
              pairs.push([key, value]);
              this.size = ++data.size;
              return this;
            }
            data = this.__data__ = new MapCache(pairs);
          }
          data.set(key, value);
          this.size = data.size;
          return this;
        }
      }]);
    }();

    var each = function each(array, callback) {
      var _array$length;
      var length = (_array$length = array === null || array === void 0 ? void 0 : array.length) !== null && _array$length !== void 0 ? _array$length : 0;
      if (length > 0 && callback instanceof Function) {
        var index = -1;
        while (++index < length) {
          if (callback(array[index], index, array) === false) {
            break;
          }
        }
      }
      return array;
    };

    var baseAssignValue = function baseAssignValue(object, key, value) {
      if (key === '__proto__') {
        Object.defineProperty(object, key, {
          configurable: true,
          enumerable: true,
          value: value,
          writable: true
        });
      } else {
        object[key] = value;
      }
    };

    var assignValue = function assignValue(object, key, value) {
      var objValue = object[key];
      if (!(Object.prototype.hasOwnProperty.call(object, key) && eq$1(objValue, value)) || value === undefined && !(key in object)) {
        baseAssignValue(object, key, value);
      }
    };

    var copy$1 = function copy(source, props) {
      var _object, _props$length;
      var object = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var customizer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      var isNew = !object;
      object = (_object = object) !== null && _object !== void 0 ? _object : {};
      var length = (_props$length = props === null || props === void 0 ? void 0 : props.length) !== null && _props$length !== void 0 ? _props$length : 0;
      var index = -1;
      while (++index < length) {
        var key = props[index];
        var newValue = customizer ? customizer(object[key], source[key], key, object, source) : undefined;
        if (newValue === undefined) {
          newValue = source[key];
        }
        if (isNew) {
          baseAssignValue(object, key, newValue);
        } else {
          assignValue(object, key, newValue);
        }
      }
      return object;
    };

    var baseTimes = function baseTimes(count, callback) {
      var result = Array(count);
      var index = -1;
      while (++index < count) {
        result[index] = callback(index);
      }
      return result;
    };

    var getTag = function getTag(object) {
      if (object === null) {
        return nullTag;
      } else if (object === undefined) {
        return undefinedTag;
      }
      return Object.prototype.toString.call(object);
    };

    var isObjectLike$1 = function isObjectLike(value) {
      return _typeof(value) === 'object' && value !== null && value !== undefined;
    };

    var isArguments$1 = function isArguments(value) {
      return isObjectLike$1(value) && getTag(value) === argsTag;
    };

    var isArray$1 = Array.isArray;

    var _exports$1, _module$1, _Buffer$isBuffer;
    var freeExports$2 = (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && exports && !((_exports$1 = exports) !== null && _exports$1 !== void 0 && _exports$1.nodeType) && exports;
    var freeModule$2 = freeExports$2 && (typeof module === "undefined" ? "undefined" : _typeof(module)) === 'object' && module && !((_module$1 = module) !== null && _module$1 !== void 0 && _module$1.nodeType) && module;
    var moduleExports$2 = freeModule$2 && freeModule$2.exports === freeExports$2;
    var Buffer$1 = moduleExports$2 ? root === null || root === void 0 ? void 0 : root.Buffer : undefined;
    var isBuffer$1 = (_Buffer$isBuffer = Buffer$1 === null || Buffer$1 === void 0 ? void 0 : Buffer$1.isBuffer) !== null && _Buffer$isBuffer !== void 0 ? _Buffer$isBuffer : function () {
      return false;
    };

    var MAX_SAFE_INTEGER = 9007199254740991;

    var reIsUint = /^(?:0|[1-9]\d*)$/;
    var isIndex$1 = function isIndex(value) {
      var length = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : MAX_SAFE_INTEGER;
      var type = _typeof(value);
      return !!length && (type === 'number' || type !== 'symbol' && reIsUint.test(value)) && value > -1 && value % 1 === 0 && value < length;
    };

    var isLength$1 = function isLength(value) {
      return typeof value === 'number' && value > -1 && value % 1 === 0 && value <= MAX_SAFE_INTEGER;
    };

    var baseIsTypedArray = function baseIsTypedArray(value) {
      return isObjectLike$1(value) && isLength$1(value.length) && !!typedArrayTags[baseGetTag(value)];
    };

    var baseUnary = function baseUnary(func) {
      return function (value) {
        return func(value);
      };
    };

    var _exports, _module;
    var freeExports$1 = (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && exports && !((_exports = exports) !== null && _exports !== void 0 && _exports.nodeType) && exports;
    var freeModule$1 = freeExports$1 && (typeof module === "undefined" ? "undefined" : _typeof(module)) === 'object' && module && !((_module = module) !== null && _module !== void 0 && _module.nodeType) && module;
    var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;
    var freeProcess = moduleExports$1 && freeGlobal.process;
    var process = function process() {
      try {
        var types = freeModule$1 && freeModule$1.require && freeModule$1.require('util').types;
        if (types) {
          return types;
        }
        return freeProcess && (freeProcess === null || freeProcess === void 0 ? void 0 : freeProcess.binding) && freeProcess.binding('util');
      } catch (doNothing) {}
    };
    var nodeUtil = process();

    var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;
    var isTypedArray$1 = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

    var arrayLikeKeys = function arrayLikeKeys(value) {
      var inherited = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var isArr = isArray$1(value);
      var isArg = !isArr && isArguments$1(value);
      var isBuff = !isArr && !isArg && isBuffer$1(value);
      var isType = !isArr && !isArg && !isBuff && isTypedArray$1(value);
      var skipIndexes = isArr || isArg || isBuff || isType;
      var result = skipIndexes ? baseTimes(value.length, String) : [];
      var length = result.length;
      for (var key in value) {
        if ((inherited || Object.prototype.hasOwnProperty.call(value, key)) && !(skipIndexes && (
        key === 'length' ||
        isBuff && (key === 'offset' || key === 'parent') ||
        isType && (key === 'buffer' || key === 'byteLength' || key === 'byteOffset') ||
        isIndex$1(key, length)))) {
          result.push(key);
        }
      }
      return result;
    };

    var isPrototype = function isPrototype(value) {
      var Constructor = value && value.constructor;
      var proto = typeof Constructor === 'function' && Constructor.prototype || Object.prototype;
      return value === proto;
    };

    var overArg = function overArg(func, transform) {
      return function (arg) {
        return func(transform(arg));
      };
    };

    var nativeKeys = overArg(Object.keys, Object);

    var baseKeys = function baseKeys(object) {
      if (!isPrototype(object)) {
        return nativeKeys(object);
      }
      var result = [];
      for (var key in Object(object)) {
        if (Object.prototype.hasOwnProperty.call(object, key) && key !== 'constructor') {
          result.push(key);
        }
      }
      return result;
    };

    var isArrayLike$1 = function isArrayLike(value) {
      return value !== null && isLength$1(value.length) && !isFunction$1(value);
    };

    var keys = function keys(object) {
      return isArrayLike$1(object) ? arrayLikeKeys(object) : baseKeys(object);
    };

    var baseAssign = function baseAssign(object, source) {
      return object && copy$1(source, keys(source), object);
    };

    var nativeKeysIn = function nativeKeysIn(object) {
      var result = [];
      if (object !== null) {
        for (var key in Object(object)) {
          result.push(key);
        }
      }
      return result;
    };

    var baseKeysIn = function baseKeysIn(object) {
      if (!isObject$1(object)) {
        return nativeKeysIn(object);
      }
      var isProto = isPrototype(object);
      var result = [];
      for (var key in object) {
        if (!(key === 'constructor' && (isProto || !Object.prototype.hasOwnProperty.call(object, key)))) {
          result.push(key);
        }
      }
      return result;
    };

    var keysIn = function keysIn(object) {
      return isArrayLike$1(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
    };

    var baseAssignIn = function baseAssignIn(object, source) {
      return object && copy$1(source, keysIn(source), object);
    };

    var freeExports = (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && exports && !exports.nodeType && exports;

    var freeModule = freeExports && (typeof module === "undefined" ? "undefined" : _typeof(module)) === 'object' && module && !module.nodeType && module;

    var moduleExports = freeModule && freeModule.exports === freeExports;

    var Buffer = moduleExports ? root.Buffer : undefined;
    var allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined;

    var cloneBuffer = function cloneBuffer(buffer, isDeep) {
      if (isDeep) {
        return buffer.subarray();
      }
      var length = buffer.length;
      var result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);
      buffer.copy(result);
      return result;
    };

    var copy = function copy(source, array) {
      var _array;
      var length = source.length;
      var index = -1;
      array = (_array = array) !== null && _array !== void 0 ? _array : Array(length);
      while (++index < length) {
        array[index] = source[index];
      }
      return array;
    };

    var arrayFilter = function arrayFilter(array, predicate) {
      var _array$length;
      var index = -1;
      var resIndex = 0;
      var length = (_array$length = array === null || array === void 0 ? void 0 : array.length) !== null && _array$length !== void 0 ? _array$length : 0;
      var result = [];
      while (++index < length) {
        var value = array[index];
        if (predicate(value, index, array)) {
          result[resIndex++] = value;
        }
      }
      return result;
    };

    var stub$2 = function stub() {
      return [];
    };

    var propertyIsEnumerable = Object.prototype.propertyIsEnumerable;
    var nativeGetSymbols$1 = Object.getOwnPropertySymbols;
    var getSymbols = !nativeGetSymbols$1 ? stub$2 : function (object) {
      if (object === null || object === undefined) {
        return [];
      }
      object = Object(object);
      return arrayFilter(nativeGetSymbols$1(object), function (symbol) {
        return propertyIsEnumerable.call(object, symbol);
      });
    };

    var copySymbols = function copySymbols(source, object) {
      return copy$1(source, getSymbols(source), object);
    };

    var arrayPush = function arrayPush(array, values) {
      var index = -1;
      var length = values.length;
      var offset = array.length;
      while (++index < length) {
        array[offset + index] = values[index];
      }
      return array;
    };

    var getPrototype = overArg(Object.getPrototypeOf, Object);

    var nativeGetSymbols = Object.getOwnPropertySymbols;
    var getSymbolsIn = !nativeGetSymbols ? stub$2 : function (object) {
      var result = [];
      while (object) {
        arrayPush(result, getSymbols(object));
        object = getPrototype(object);
      }
      return result;
    };

    var copySymbolsIn = function copySymbolsIn(source, object) {
      return copy$1(source, getSymbolsIn(source), object);
    };

    var baseGetAllKeys = function baseGetAllKeys(object, keysFunc, symbolsFunc) {
      var result = keysFunc(object);
      return isArray$1(object) ? result : arrayPush(result, symbolsFunc(object));
    };

    var getAllKeys = function getAllKeys(object) {
      return baseGetAllKeys(object, keys, getSymbols);
    };

    var getAllKeysIn = function getAllKeysIn(object) {
      return baseGetAllKeys(object, keysIn, getSymbolsIn);
    };

    var initCloneArray = function initCloneArray(array) {
      var length = array.length;
      var result = new array.constructor(length);
      if (length && typeof array[0] === 'string' && Object.prototype.hasOwnProperty.call(array, 'index')) {
        result.index = array.index;
        result.input = array.input;
      }
      return result;
    };

    var Uint8Array$1 = root.Uint8Array;

    var cloneArrayBuffer = function cloneArrayBuffer(arrayBuffer) {
      var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
      new Uint8Array$1(result).set(new Uint8Array$1(arrayBuffer));
      return result;
    };

    var cloneDataView = function cloneDataView(dataView, isDeep) {
      var buffer = isDeep ? cloneArrayBuffer(dataView.buffer) : dataView.buffer;
      return new dataView.constructor(buffer, dataView.byteOffset, dataView.byteLength);
    };

    var reFlags = /\w*$/;
    var cloneRegExp = function cloneRegExp(regexp) {
      var result = new regexp.constructor(regexp.source, reFlags.exec(regexp));
      result.lastIndex = regexp.lastIndex;
      return result;
    };

    var symbolProto = _Symbol ? _Symbol.prototype : undefined;
    var symbolValueOf$1 = symbolProto ? symbolProto.valueOf : undefined;
    var cloneSymbol = function cloneSymbol(symbol) {
      return symbolValueOf$1 ? Object(symbolValueOf$1.call(symbol)) : {};
    };

    var cloneTypedArray = function cloneTypedArray(typedArray, isDeep) {
      var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
      return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
    };

    var initCloneByTag = function initCloneByTag(object, tag, isDeep) {
      var Ctor = object.constructor;
      switch (tag) {
        case arrayBufferTag:
          {
            return cloneArrayBuffer(object);
          }
        case boolTag:
        case dateTag:
          {
            return new Ctor(+object);
          }
        case dataViewTag:
          {
            return cloneDataView(object, isDeep);
          }
        case float32Tag:
        case float64Tag:
        case int8Tag:
        case int16Tag:
        case int32Tag:
        case uint8Tag:
        case uint8ClampedTag:
        case uint16Tag:
        case uint32Tag:
          {
            return cloneTypedArray(object, isDeep);
          }
        case setTag:
        case mapTag:
          {
            return new Ctor();
          }
        case numberTag:
        case stringTag:
          {
            return new Ctor(object);
          }
        case regexpTag:
          {
            return cloneRegExp(object);
          }
        case symbolTag:
          {
            return cloneSymbol(object);
          }
      }
    };

    var create = Object === null || Object === void 0 ? void 0 : Object.create;
    var build = function build() {
      var Constructor = function Constructor() {};
      return function (proto) {
        if (!isObject$1(proto)) {
          return {};
        }
        if (create) {
          return create(proto);
        }
        Constructor.prototype = proto;
        var result = new Constructor();
        Constructor.prototype = undefined;
        return result;
      };
    };
    var baseCreate = build();

    var initCloneObject = function initCloneObject(object) {
      return typeof object.constructor === 'function' && !isPrototype(object) ? baseCreate(getPrototype(object)) : {};
    };

    var baseIsMap = function baseIsMap(value) {
      return isObjectLike$1(value) && getTag(value) === mapTag;
    };

    var nodeIsMap = nodeUtil && nodeUtil.isMap;
    var isMap$1 = nodeIsMap ? baseUnary(nodeIsMap) : baseIsMap;

    var baseIsSet = function baseIsSet(value) {
      return isObjectLike$1(value) && getTag(value) === setTag;
    };

    var nodeIsSet = nodeUtil && nodeUtil.isSet;
    var isSet$1 = nodeIsSet ? baseUnary(nodeIsSet) : baseIsSet;

    var CLONE_DEEP_FLAG$2 = 1;
    var CLONE_FLAT_FLAG = 2;
    var CLONE_SYMBOLS_FLAG$2 = 4;
    var _baseClone = function baseClone(value, bitmask, customizer, key, object, stack) {
      var _stack;
      var result;
      var isDeep = bitmask & CLONE_DEEP_FLAG$2;
      var isFlat = bitmask & CLONE_FLAT_FLAG;
      var isFull = bitmask & CLONE_SYMBOLS_FLAG$2;
      if (customizer) {
        result = object ? customizer(value, key, object, stack) : customizer(value);
      }
      if (result !== undefined) {
        return result;
      }
      if (!isObject$1(value)) {
        return value;
      }
      var isArr = isArray$1(value);
      if (isArr) {
        result = initCloneArray(value);
        if (!isDeep) {
          return copy(value, result);
        }
      } else {
        var tag = getTag(value);
        var isFunc = tag === funcTag || tag === genTag;
        if (isBuffer$1(value)) {
          return cloneBuffer(value, isDeep);
        }
        if (tag === objectTag || tag === argsTag || isFunc && !object) {
          result = isFlat || isFunc ? {} : initCloneObject(value);
          if (!isDeep) {
            return isFlat ? copySymbolsIn(value, baseAssignIn(result, value)) : copySymbols(value, baseAssign(result, value));
          }
        } else {
          if (!cloneableTags[tag]) {
            return object ? value : {};
          }
          result = initCloneByTag(value, tag, isDeep);
        }
      }
      stack = (_stack = stack) !== null && _stack !== void 0 ? _stack : new Stack();
      var stacked = stack.get(value);
      if (stacked) {
        return stacked;
      }
      stack.set(value, result);
      if (isSet$1(value)) {
        value.forEach(function (subValue) {
          result.add(_baseClone(subValue, bitmask, customizer, subValue, value, stack));
        });
      } else if (isMap$1(value)) {
        value.forEach(function (subValue, key) {
          result.set(key, _baseClone(subValue, bitmask, customizer, key, value, stack));
        });
      }
      var keysFunc = isFull ? isFlat ? getAllKeysIn : getAllKeys : isFlat ? keysIn : keys;
      var props = isArr ? undefined : keysFunc(value);
      each(props !== null && props !== void 0 ? props : value, function (subValue, key) {
        if (props) {
          key = subValue;
          subValue = value[key];
        }
        assignValue(result, key, _baseClone(subValue, bitmask, customizer, key, value, stack));
      });
      return result;
    };

    var CLONE_DEEP_FLAG$1 = 1;
    var CLONE_SYMBOLS_FLAG$1 = 4;
    var cloneDeep$1 = function cloneDeep(value) {
      return _baseClone(value, CLONE_DEEP_FLAG$1 | CLONE_SYMBOLS_FLAG$1);
    };

    var CLONE_DEEP_FLAG = 1;
    var CLONE_SYMBOLS_FLAG = 4;
    var cloneDeepWith$1 = function cloneDeepWith(value) {
      var customizer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return _baseClone(value, CLONE_DEEP_FLAG | CLONE_SYMBOLS_FLAG, typeof customizer === 'function' ? customizer : undefined);
    };

    var isNotNullObject$1 = function isNotNullObject(value) {
      return _typeof(value) === 'object' && value !== null;
    };

    var REACT_ELEMENT_TYPE = canUseSymbol$1 ? Symbol["for"]('react.element') : 0xeac7;
    var isReactElement$1 = function isReactElement(value) {
      return !!value && value.$$typeof === REACT_ELEMENT_TYPE;
    };

    var isSpecial = function isSpecial(value) {
      value = Object.prototype.toString.call(value);
      return value === regexpTag || value === dateTag || isReactElement$1(value);
    };
    var isMergeableObject$1 = function isMergeableObject(value) {
      return isNotNullObject$1(value) && !isSpecial(value);
    };

    var defaultMergeable = isMergeableObject$1;
    var cloneUnlessOtherwiseSpecified = function cloneUnlessOtherwiseSpecified(value, options) {
      return options && options.clone !== false && options.isMergeableObject(value) ? deepMerge$1(Array.isArray(value) ? [] : {}, value, options) : value;
    };
    var defaultArrayMerge = function defaultArrayMerge(target, source, options) {
      return target.concat(source).map(function (element) {
        return cloneUnlessOtherwiseSpecified(element, options);
      });
    };
    var getMergeFunction = function getMergeFunction(key, options) {
      if (options) {
        var customMerge = options.customMerge;
        if (typeof customMerge === 'function') {
          customMerge = customMerge(key);
          if (typeof customMerge === 'function') {
            return customMerge;
          }
        }
      }
      return deepMerge$1;
    };
    var mergeObject = function mergeObject(target, source, options) {
      var destination = {};
      if (options) {
        var _isMergeableObject = options.isMergeableObject;
        if (typeof _isMergeableObject === 'function') {
          if (_isMergeableObject(target)) {
            Object.keys(target).forEach(function (key) {
              destination[key] = cloneUnlessOtherwiseSpecified(target[key], options);
            });
          }
        }
      }
      Object.keys(source).forEach(function (key) {
        if (!options.isMergeableObject(source[key]) || !target[key]) {
          destination[key] = cloneUnlessOtherwiseSpecified(source[key], options);
        } else {
          destination[key] = getMergeFunction(key, options)(target[key], source[key], options);
        }
      });
      return destination;
    };
    var deepMerge$1 = function deepMerge(target, source) {
      var _options;
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      options = (_options = options) !== null && _options !== void 0 ? _options : {};
      var _options2 = options,
        arrayMerge = _options2.arrayMerge,
        isMergeableObject = _options2.isMergeableObject;
      options.arrayMerge = arrayMerge || defaultArrayMerge;
      options.isMergeableObject = isMergeableObject || defaultMergeable;
      var sourceIsArray = Array.isArray(source);
      var targetIsArray = Array.isArray(target);
      var sourceAndTargetTypesMatch = sourceIsArray === targetIsArray;
      if (!sourceAndTargetTypesMatch) {
        return cloneUnlessOtherwiseSpecified(source, options);
      } else if (sourceIsArray) {
        return options.arrayMerge(target, source, options);
      } else {
        return mergeObject(target, source, options);
      }
    };

    var deepMergeAll$1 = function deepMergeAll(array) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!Array.isArray(array)) {
        throw new Error('The first argument should be an array');
      }
      return array.reduce(function (prev, next) {
        return deepMerge$1(prev, next, options);
      }, {});
    };

    var fastFormat = function fastFormat(pattern) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      if (pattern === null || !(pattern instanceof String || typeof pattern === 'string')) {
        return "";
      }
      if (args.length > 0) {
        args = [].concat.apply([], args);
        var len = args.length;
        for (var i = 0; i < len; i++) {
          pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "g"), args[i]);
        }
      }
      return pattern;
    };

    var defaultMessage = 'Invariant failed';
    var invariant$1 = function invariant(condition, message) {
      if (condition) {
        return;
      }
      for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        args[_key - 2] = arguments[_key];
      }
      var provided = typeof message === 'function' ? message.apply(void 0, args) : fastFormat.apply(void 0, [message].concat(args));
      throw new Error(provided !== null && provided !== void 0 ? provided : defaultMessage);
    };

    var isArrayLikeObject$1 = function isArrayLikeObject(value) {
      return isObjectLike$1(value) && isArrayLike$1(value);
    };

    var isAsyncFunction$1 = function isAsyncFunction(value) {
      if (!isObject$1(value)) {
        return false;
      }
      var tag = baseGetTag(value);
      return tag === asyncTag;
    };

    var isBoolean$1 = function isBoolean(object) {
      return typeof object === 'boolean' || object instanceof Boolean;
    };

    var isEmpty$2 = function isEmpty(value) {
      if (value instanceof Array) {
        return value.length === 0;
      } else if (typeof value === 'string' || value instanceof String) {
        return value === '';
      } else if (value instanceof Object) {
        for (var prop in value) {
          return false;
        }
        return true;
      }
      return false;
    };

    var HASH_UNDEFINED = '__hash_undefined__';
    var SetCache = function () {
      function SetCache(values) {
        _classCallCheck(this, SetCache);
        var index = -1;
        var length = values === null || values === undefined ? 0 : values === null || values === void 0 ? void 0 : values.length;
        this.__data__ = new MapCache();
        while (++index < length) {
          this.add(values[index]);
        }
      }
      return _createClass(SetCache, [{
        key: "add",
        value: function add(value) {
          this.__data__.set(value, HASH_UNDEFINED);
          return this;
        }
      }, {
        key: "has",
        value: function has(value) {
          return this.__data__.has(value);
        }
      }, {
        key: "push",
        value: function push(value) {
          this.add(value);
          return this;
        }
      }]);
    }();

    var some = function some(array, callback) {
      if (Array.isArray(array)) {
        var index = -1;
        var length = array === null || array === undefined ? 0 : array.length;
        while (++index < length) {
          if (callback(array[index], index, array)) {
            return true;
          }
        }
      }
      return false;
    };

    var COMPARE_PARTIAL_FLAG = 1;

    var COMPARE_UNORDERED_FLAG = 2;

    var equalArrays = function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
      if (!Array.isArray(array) || !Array.isArray(other)) {
        return false;
      }
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      var arrLength = array.length;
      var othLength = other.length;
      if (arrLength !== othLength && !(isPartial && othLength > arrLength)) {
        return false;
      }
      var stacked = stack.get(array);
      if (stacked && stack.get(other)) {
        return stacked === other;
      }
      var index = -1;
      var result = true;
      var seen = bitmask & COMPARE_UNORDERED_FLAG ? new SetCache() : undefined;
      stack.set(array, other);
      stack.set(other, array);
      var compareArrays = function compareArrays(arrValue) {
        return function (othValue, othIndex) {
          if (!seen.has(othIndex) && (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
            return seen.push(othIndex);
          }
        };
      };
      while (++index < arrLength)
      {
        var compared = void 0;
        var arrValue = array[index];
        var othValue = other[index];
        if (customizer) {
          compared = isPartial ? customizer(othValue, arrValue, index, other, array, stack) : customizer(arrValue, othValue, index, array, other, stack);
        }
        if (compared !== undefined) {
          if (compared) {
            continue;
          }
          result = false;
          break;
        }
        if (seen) {
          if (!some(other, compareArrays(arrValue))) {
            result = false;
            break;
          }
        } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
          result = false;
          break;
        }
      }
      stack['delete'](array);
      stack['delete'](other);
      return result;
    };

    var mapToArray = function mapToArray(map) {
      var index = -1;
      var result = Array(map.size);
      map.forEach(function (value, key) {
        result[++index] = [key, value];
      });
      return result;
    };

    var setToArray = function setToArray(set) {
      var index = -1;
      var result = Array(set.size);
      set.forEach(function (value) {
        result[++index] = value;
      });
      return result;
    };

    var symbolValueOf = Symbol.prototype.valueOf;
    var equalByTag = function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
      switch (tag) {
        case dataViewTag:
        case arrayBufferTag:
          {
            if (tag === dataViewTag) {
              if (object.byteLength !== other.byteLength || object.byteOffset !== other.byteOffset) {
                return false;
              }
              object = object.buffer;
              other = other.buffer;
            }
            if (object.byteLength !== other.byteLength || !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
              return false;
            }
            return true;
          }
        case boolTag:
        case dateTag:
        case numberTag:
          {
            return eq$1(+object, +other);
          }
        case errorTag:
          {
            return object.name === other.name && object.message === other.message;
          }
        case regexpTag:
        case stringTag:
          {
            return object === "".concat(other);
          }
        case mapTag:
        case setTag:
          {
            var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
            var convert = tag === mapTag ? mapToArray : setToArray;
            if (object.size !== other.size && !isPartial) {
              return false;
            }
            var stacked = stack.get(object);
            if (stacked) {
              return stacked === other;
            }
            bitmask |= COMPARE_UNORDERED_FLAG;
            stack.set(object, other);
            var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
            stack['delete'](object);
            return result;
          }
        case symbolTag:
          {
            if (symbolValueOf) {
              return symbolValueOf.call(object) === symbolValueOf.call(other);
            }
          }
      }
      return false;
    };

    var equalObjects = function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      var objProps = getAllKeys(object);
      var objLength = objProps.length;
      var othProps = getAllKeys(other);
      var othLength = othProps.length;
      if (objLength !== othLength && !isPartial) {
        return false;
      }
      var key;
      var index = objLength;
      while (index--) {
        key = objProps[index];
        if (!(isPartial ? key in other : Object.prototype.hasOwnProperty.call(other, key))) {
          return false;
        }
      }
      var stacked = stack.get(object);
      if (stacked && stack.get(other)) {
        return stacked === other;
      }
      var result = true;
      stack.set(object, other);
      stack.set(other, object);
      var compared;
      var skipCtor = isPartial;
      while (++index < objLength) {
        var _skipCtor;
        key = objProps[index];
        var objValue = object[key];
        var othValue = other[key];
        if (customizer) {
          compared = isPartial ? customizer(othValue, objValue, key, other, object, stack) : customizer(objValue, othValue, key, object, other, stack);
        }
        if (!(compared === undefined ? objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack) : compared)) {
          result = false;
          break;
        }
        skipCtor = (_skipCtor = skipCtor) !== null && _skipCtor !== void 0 ? _skipCtor : key === 'constructor';
      }
      if (result && !skipCtor) {
        var objCtor = object.constructor;
        var othCtor = other.constructor;
        if (objCtor !== othCtor && 'constructor' in object && 'constructor' in other && !(typeof objCtor === 'function' && objCtor instanceof objCtor && typeof othCtor === 'function' && othCtor instanceof othCtor)) {
          result = false;
        }
      }
      stack['delete'](object);
      stack['delete'](other);
      return result;
    };

    var baseIsEqualDeep = function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
      var _stack3;
      var objIsArr = Array.isArray(object);
      var othIsArr = Array.isArray(other);
      var objTag = objIsArr ? arrayTag : getTag(object);
      var othTag = othIsArr ? arrayTag : getTag(other);
      objTag = objTag === argsTag ? objectTag : objTag;
      othTag = othTag === argsTag ? objectTag : othTag;
      var objIsObj = objTag === objectTag;
      var othIsObj = othTag === objectTag;
      var isSameTag = objTag === othTag;
      if (isSameTag && isBuffer$1(object)) {
        if (!isBuffer$1(other)) {
          return false;
        }
        objIsArr = true;
        objIsObj = false;
      }
      if (isSameTag && !objIsObj) {
        var _stack;
        stack = (_stack = stack) !== null && _stack !== void 0 ? _stack : new Stack();
        return objIsArr || isTypedArray$1(object) ? equalArrays(object, other, bitmask, customizer, equalFunc, stack) : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
      }
      if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
        var objIsWrapped = objIsObj && Object.prototype.hasOwnProperty.call(object, '__wrapped__');
        var othIsWrapped = othIsObj && Object.prototype.hasOwnProperty.call(other, '__wrapped__');
        if (objIsWrapped || othIsWrapped) {
          var _stack2;
          var objUnwrapped = objIsWrapped ? object.value() : object;
          var othUnwrapped = othIsWrapped ? other.value() : other;
          stack = (_stack2 = stack) !== null && _stack2 !== void 0 ? _stack2 : new Stack();
          return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
        }
      }
      if (!isSameTag) {
        return false;
      }
      stack = (_stack3 = stack) !== null && _stack3 !== void 0 ? _stack3 : new Stack();
      return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
    };

    var _baseIsEqual = function baseIsEqual(value, other) {
      var bitmask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : -1;
      var customizer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
      var stack = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
      if (value === other) {
        return true;
      }
      if (value === null || value === undefined || other === null || other === undefined || !isObjectLike$1(value) && !isObjectLike$1(other)) {
        return value !== value && other !== other;
      }
      return baseIsEqualDeep(value, other, bitmask, customizer, _baseIsEqual, stack);
    };

    var isEqual$1 = function isEqual(value, other) {
      return _baseIsEqual(value, other);
    };

    var isEqualWith$1 = function isEqualWith(value, other, customizer) {
      customizer = typeof customizer === 'function' ? customizer : undefined;
      var result = customizer ? customizer(value, other) : undefined;
      return result === undefined ? _baseIsEqual(value, other, undefined, customizer) : !!result;
    };

    var isFloat$1 = function isFloat(value) {
      return Number(value) === value && value % 1 !== 0;
    };

    var isInt$1 = function isInt(value) {
      return Number(value) === value && value % 1 === 0;
    };

    var isNil$1 = function isNil(value) {
      return value === undefined || value === null;
    };

    var isNotEmpty$1 = function isNotEmpty(value) {
      if (value instanceof Array) {
        return value.length > 0;
      } else if (typeof value === 'string' || value instanceof String) {
        return value !== '';
      } else if (value instanceof Object) {
        for (var prop in value) {
          return true;
        }
        return false;
      }
      return true;
    };

    var isNotNull$1 = function isNotNull(value) {
      return value !== null;
    };

    var isNumber$1 = function isNumber(object) {
      return typeof object === 'number' || object instanceof Number;
    };

    var isPureObject = function isPureObject(value) {
      return isObject$1(value) === true && Object.prototype.toString.call(value) === objectTag;
    };
    var isPlainObject$1 = function isPlainObject(value) {
      var ctor, proto;
      if (isPureObject(value) === false) {
        return false;
      }
      ctor = value.constructor;
      if (typeof ctor !== 'function') {
        return false;
      }
      proto = ctor.prototype;
      if (isPureObject(proto) === false || proto.hasOwnProperty('isPrototypeOf') === false) {
        return false;
      }
      return true;
    };

    var isPrimitive$1 = function isPrimitive(value) {
      return value === null || _typeof(value) !== 'object' && typeof value !== 'function';
    };

    var isString$1 = function isString(object) {
      return typeof object === 'string' || object instanceof String;
    };

    var isSymbol$2 = function isSymbol(object) {
      var type = _typeof(object);
      return type === 'symbol' || type === 'object' && object !== null && getTag(object) === symbolTag;
    };

    var isUint$1 = function isUint(value) {
      return Number(value) === value && value % 1 === 0 && value >= 0;
    };

    var isWeakMap$1 = function isWeakMap(value) {
      return isObjectLike$1(value) && getTag(value) === weakMapTag;
    };

    var isWeakSet$1 = function isWeakSet(value) {
      return isObjectLike$1(value) && getTag(value) === weakSetTag;
    };

    var stubFalse$1 = function stubFalse() {
      return false;
    };

    var stubTrue$1 = function stubTrue() {
      return true;
    };

    var toPlainObject$1 = function toPlainObject(value) {
      value = Object(value);
      var result = {};
      for (var key in value) {
        result[key] = value[key];
      }
      return result;
    };

    var contains = function contains(array, value) {
      return Array.isArray(array) ? array.includes(value) : false;
    };

    var distinct = function distinct(value, index, array) {
      return array instanceof Array ? array.indexOf(value) === index : false;
    };

    var empty$2 = function empty(array) {
      return array instanceof Array && array.length === 0;
    };

    var head = function head(array) {
      return Array.isArray(array) && array.length ? array[0] : undefined;
    };

    var inBetween = function inBetween(source) {
      var element = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!Array.isArray(source)) {
        throw new TypeError('inBetween failed, the passed-in source parameter must be an Array.');
      }
      var len = source.length;
      if (len > 1) {
        return source.flatMap(function (value, index) {
          return index === 0 || index === len ? [value] : [element, value];
        });
      }
      return source;
    };

    function initialize() {
      var elements = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var ar = [];
      elements = elements > 0 ? Math.abs(elements) : 0;
      if (elements > 0) {
        for (var i = 0; i < elements; i++) {
          ar[i] = value;
        }
      }
      return ar;
    }

    var last = function last(array) {
      var length = array === null ? 0 : array.length;
      return length ? array[length - 1] : undefined;
    };

    var map$2 = function map(array, callback) {
      if ((array === null || array === void 0 ? void 0 : array.length) > 0) {
        var length = array.length;
        var result = new Array(length);
        var index = -1;
        while (++index < length) {
          result[index] = callback(array[index], index, array);
        }
      }
      return [];
    };

    var move = function move(array, from, to) {
      var clone = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      if (array instanceof Array) {
        if (clone) {
          array = _toConsumableArray(array);
        }
        if (array.length > 1) {
          if (from < 0) {
            from += array.length;
            if (from < 0) {
              from = 0;
            }
          }
          if (to < 0) {
            to += array.length;
            if (to < 0) {
              to = 0;
            }
          }
          array.splice(to, 0, array.splice(from, 1)[0]);
        }
        return array;
      } else {
        throw new TypeError('move failed, the passed-in array argument is not an Array.');
      }
    };

    var notEmpty$1 = function notEmpty(array) {
      return array instanceof Array && array.length > 0;
    };

    var nullify$1 = function nullify(array) {
      return array instanceof Array && array.length > 0 ? array : null;
    };

    var partition$1 = function partition(collection, predicate) {
      if (!(collection instanceof Array)) {
        throw new TypeError('partition() failed, the passed-in `collection` argument must be a valid Array reference.');
      }
      if (!(predicate instanceof Function)) {
        throw new TypeError('partition() failed, the passed-in `predicate` argument must be a valid function reference.');
      }
      var result = [[], []];
      collection.forEach(function (value) {
        result[predicate(value) ? 0 : 1].push(value);
      });
      return result;
    };

    function pierce(ar, index , flag) {
      index = index > 0 ? Math.abs(index) : 0;
      flag = Boolean(flag);
      var item = ar[index];
      ar.splice(index, 1);
      return flag ? ar : item;
    }

    var range = function range(start) {
      var stop = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var step = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
      if (isNil$1(stop)) {
        var _start;
        stop = (_start = start) !== null && _start !== void 0 ? _start : 0;
        start = 0;
      }
      if (!step) {
        step = stop < start ? -1 : 1;
      }
      var length = Math.max(Math.ceil((stop - start) / step), 0);
      var range = Array(length);
      for (var id = 0; id < length; id++, start += step) {
        range[id] = start;
      }
      return range;
    };

    function repeat$1(ar, count ) {
      var result = null;
      if (ar instanceof Array) {
        count = count > 0 ? count : 0;
        if (count > 0) {
          result = [];
          for (var i = 0; i < count; i++) {
            result = result.concat(ar);
          }
        } else {
          result = [].concat(ar);
        }
      }
      return result;
    }

    function rotate(ar) {
      var amount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      if (ar instanceof Array && ar.length > 0) {
        amount %= ar.length;
        if (amount > 0) {
          ar.unshift.apply(ar, ar.splice(-amount, amount));
        } else if (amount < 0) {
          ar.push.apply(ar, ar.splice(0, -amount));
        }
      } else {
        ar = null;
      }
      return ar;
    }

    function shuffle(ar) {
      if (ar instanceof Array) {
        var item;
        var rdm;
        var tmp = [];
        var len = ar.length;
        var index = len - 1;
        for (var i = 0; i < len; i++) {
          rdm = Math.round(Math.random() * index);
          item = ar[rdm];
          ar.splice(rdm, 1);
          tmp[tmp.length] = item;
          index--;
        }
        while (--len > -1) {
          ar[len] = tmp[len];
        }
      } else {
        ar = null;
      }
      return ar;
    }

    function sortOn(ar, propName, options) {
      var sort = function sort(o1, o2) {
        var v1 = propName in o1 ? o1[propName] : '';
        var v2 = propName in o2 ? o2[propName] : '';
        switch (options) {
          case Array.CASEINSENSITIVE:
          case Array.CASEINSENSITIVE | Array.RETURNINDEXEDARRAY:
            {
              v1 = typeof v1 === "string" || v1 instanceof String ? v1.toLowerCase() : v1;
              v2 = typeof v2 === "string" || v2 instanceof String ? v2.toLowerCase() : v2;
              break;
            }
          case Array.NUMERIC:
          case Array.NUMERIC | Array.RETURNINDEXEDARRAY:
            {
              v1 = Number(v1);
              v2 = Number(v2);
              v1 = isNaN(v1) ? 0 : v1;
              v2 = isNaN(v2) ? 0 : v2;
              break;
            }
          case Array.DESCENDING:
          case Array.DESCENDING | Array.RETURNINDEXEDARRAY:
            {
              var _ref = [v2, v1];
              v1 = _ref[0];
              v2 = _ref[1];
              break;
            }
          case Array.CASEINSENSITIVE | Array.DESCENDING:
          case Array.CASEINSENSITIVE | Array.DESCENDING | Array.RETURNINDEXEDARRAY:
            {
              v1 = typeof v1 === "string" || v1 instanceof String ? v1.toLowerCase() : v1;
              v2 = typeof v2 === "string" || v2 instanceof String ? v2.toLowerCase() : v2;
              var _ref2 = [v2, v1];
              v1 = _ref2[0];
              v2 = _ref2[1];
              break;
            }
          case Array.NUMERIC | Array.DESCENDING:
          case Array.NUMERIC | Array.DESCENDING | Array.RETURNINDEXEDARRAY:
            {
              v1 = Number(v1);
              v2 = Number(v2);
              v1 = isNaN(v1) ? 0 : v1;
              v2 = isNaN(v2) ? 0 : v2;
              var _ref3 = [v2, v1];
              v1 = _ref3[0];
              v2 = _ref3[1];
              break;
            }
          case Array.UNIQUESORT:
            {
              if (v1 === v2) {
                return;
              }
              break;
            }
        }
        if (v1 < v2) {
          return -1;
        } else if (v1 > v2) {
          return 1;
        } else {
          return 0;
        }
      };
      switch (options) {
        case Array.RETURNINDEXEDARRAY:
        case Array.RETURNINDEXEDARRAY | Array.NUMERIC:
        case Array.RETURNINDEXEDARRAY | Array.CASEINSENSITIVE:
        case Array.RETURNINDEXEDARRAY | Array.NUMERIC | Array.DESCENDING:
        case Array.RETURNINDEXEDARRAY | Array.CASEINSENSITIVE | Array.DESCENDING:
          {
            var tmp = [].concat(ar);
            tmp.sort(sort);
            var result = [];
            var l = ar.length;
            for (var i = 0; i < l; i++) {
              result.push(tmp.indexOf(ar[i]));
            }
            return result;
          }
        default:
          {
            return ar.sort(sort);
          }
      }
    }
    Array.CASEINSENSITIVE = 1;
    Array.DESCENDING = 2;
    Array.NUMERIC = 16;
    Array.RETURNINDEXEDARRAY = 8;
    Array.UNIQUESORT = 4;

    function spliceInto(inserted, container, position, count) {
      inserted.unshift(position, count);
      try {
        container.splice.apply(container, inserted);
      } finally {
        inserted.splice(0, 2);
      }
    }

    function swap(ar) {
      var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var to = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var clone = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      if (ar instanceof Array) {
        if (clone) {
          ar = _toConsumableArray(ar);
        }
        var _ref = [ar[from], ar[to]];
        ar[to] = _ref[0];
        ar[from] = _ref[1];
        return ar;
      }
      return null;
    }

    var tail = function tail(array) {
      var num = array instanceof Array ? array.length : 0;
      if (!num) {
        return [];
      }
      var _array = _toArray(array),
        rest = _array.slice(1);
      return rest;
    };

    var take = function take(array) {
      var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      if (array instanceof Array && array.length > 0) {
        return array.slice(0, count < 0 ? 0 : count);
      }
      return [];
    };

    var takeRight = function takeRight(array) {
      var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      if (array instanceof Array && array.length > 0) {
        var len = array.length;
        count = len - count;
        return array.slice(count < 0 ? 0 : count, len);
      }
      return [];
    };

    var baseClamp = function baseClamp(number, lower, upper) {
      if (number === number) {
        if (upper !== undefined) {
          number = number <= upper ? number : upper;
        }
        if (lower !== undefined) {
          number = number >= lower ? number : lower;
        }
      }
      return number;
    };

    var MAX_INTEGER = 1.7976931348623157e+308;

    var INFINITY$2 = 1 / 0;

    var whitespace = /\s/;
    var trimmedEndIndex = function trimmedEndIndex(string) {
      var index = string.length;
      while (index-- && whitespace.test(string.charAt(index))) {
      }
      return index;
    };

    var reTrimStart = /^\s+/;
    var baseTrim = function baseTrim(string) {
      return string ? string.slice(0, trimmedEndIndex(string) + 1).replace(reTrimStart, '') : string;
    };

    var NAN = 0 / 0;
    var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
    var reIsBinary = /^0b[01]+$/i;
    var reIsOctal = /^0o[0-7]+$/i;
    var freeParseInt = parseInt;
    var toNumber = function toNumber(value) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (value === null || value === undefined) {
        return defaultValue;
      }
      if (typeof value === 'number') {
        return value;
      }
      if (isSymbol$2(value)) {
        return NAN;
      }
      if (isObject$1(value)) {
        var _value;
        var other = typeof ((_value = value) === null || _value === void 0 ? void 0 : _value.valueOf) === 'function' ? value.valueOf() : value;
        value = isObject$1(other) ? other + '' : other;
      }
      if (typeof value !== 'string') {
        return value === 0 ? value : +value;
      }
      value = baseTrim(value);
      var isBinary = reIsBinary.test(value);
      return isBinary || reIsOctal.test(value) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : reIsBadHex.test(value) ? NAN : +value;
    };

    var toFinite = function toFinite(value) {
      if (!value) {
        return value === 0 ? value : 0;
      }
      value = toNumber(value);
      if (value === INFINITY$2 || value === -INFINITY$2) {
        var sign = value < 0 ? -1 : 1;
        return sign * MAX_INTEGER;
      }
      return value === value ? value : 0;
    };

    var toInteger = function toInteger(value) {
      var result = toFinite(value);
      var remainder = result % 1;
      return result === result ? remainder ? result - remainder : result : 0;
    };

    var MAX_ARRAY_LENGTH = 4294967295;
    var toLength = function toLength(value) {
      return value ? baseClamp(toInteger(value), 0, MAX_ARRAY_LENGTH) : 0;
    };

    var unique = function unique(array) {
      var last = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (array instanceof Array) {
        if (last === true) {
          array = _toConsumableArray(array);
          array.reverse();
        }
        array = array.filter(distinct);
        if (last === true) {
          array.reverse();
        }
        return array;
      }
      return [];
    };

    /**
     * The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
     * @summary The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.arrays
     * @memberof core
     * @since 1.0.0
     */
    var arrays$1 = {
      contains: contains,
      distinct: distinct,
      each: each,
      empty: empty$2,
      head: head,
      inBetween: inBetween,
      initialize: initialize,
      last: last,
      map: map$2,
      move: move,
      notEmpty: notEmpty$1,
      nullify: nullify$1,
      partition: partition$1,
      pierce: pierce,
      range: range,
      repeat: repeat$1,
      rotate: rotate,
      shuffle: shuffle,
      sortOn: sortOn,
      spliceInto: spliceInto,
      some: some,
      stub: stub$2,
      swap: swap,
      tail: tail,
      take: take,
      takeRight: takeRight,
      toLength: toLength,
      unique: unique
    };

    function compare$1(charA, charB) {
      var a = charA.charAt(0);
      var b = charB.charAt(0);
      if (caseValue(a) < caseValue(b)) {
        return -1;
      }
      if (caseValue(a) > caseValue(b)) {
        return 1;
      }
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
      return 0;
    }
    var caseValue = function caseValue(str) {
      return str.toLowerCase().valueOf() === str.valueOf() ? 0 : 1;
    };

    function isAlpha(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "A" <= c && c <= "Z" || "a" <= c && c <= "z";
    }

    function isAlphaOrDigit(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "A" <= c && c <= "Z" || "a" <= c && c <= "z" || "0" <= c && c <= "9";
    }

    function isASCII(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return c.charCodeAt(0) <= 255;
    }

    function isContained(c, charset) {
      var index = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      var l = charset.length;
      for (var i = 0; i < l; i++) {
        if (c === charset.charAt(i)) {
          return true;
        }
      }
      return false;
    }

    function isDigit(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "0" <= c && c <= "9";
    }

    function isHexDigit(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "0" <= c && c <= "9" || "A" <= c && c <= "F" || "a" <= c && c <= "f";
    }

    function isIdentifierStart(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "A" <= c && c <= "Z" || "a" <= c && c <= "z" || c === "_" || c === "$";
    }

    var lineTerminators = ["\n",
    "\r",
    "\u2028",
    "\u2929"
    ];

    function isLineTerminator(c) {
      var index  = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      var l = lineTerminators.length;
      while (--l > -1) {
        if (c === lineTerminators[l]) {
          return true;
        }
      }
      return false;
    }

    function isLower(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "a" <= c && c <= "z";
    }

    function isOctalDigit(c) {
      var index  = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "0" <= c && c <= "7";
    }

    var operators = ["*", "/", "%", "+", "-", "«", "»", ">", "<", "›", "&", "^", "|"];

    function isOperator(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return operators.indexOf(c) > -1;
    }

    var symbols = [" ",
    "!",
    "\"",
    "#",
    "$",
    "%",
    "&",
    "\'",
    "(",
    ")",
    "*",
    "+",
    ",",
    "-",
    ".",
    "/",
    ":",
    ";",
    "<",
    "=",
    ">",
    "?",
    "@",
    "[",
    "\\",
    "]",
    "^",
    "_",
    "`",
    "{",
    "|",
    "}",
    "~"
    ];

    function isSymbol$1(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return symbols.indexOf(c) > -1;
    }

    function isUnicode(c) {
      var index  = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return c.charCodeAt(0) > 255;
    }

    function isUpper(c) {
      var index  = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return "A" <= c && c <= "Z";
    }

    var whiteSpaces = ["\t",
    "\n",
    "\x0B",
    "\f",
    "\r",
    " ",
    "\xA0",
    "\u1680",
    "\u180E",
    "\u2000",
    "\u2001",
    "\u2002",
    "\u2003",
    "\u2004",
    "\u2005",
    "\u2006",
    "\u2007",
    "\u2008",
    "\u2009",
    "\u200A",
    "\u200B",
    "\u2028",
    "\u2029",
    "\u202F",
    "\u205F",
    "\u3000"
    ];

    function isWhiteSpace(c) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (index > 0) {
        c = c.charAt(index);
      }
      return whiteSpaces.indexOf(c) > -1;
    }

    /**
     * The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform the basic characters.
     * <p>You can use this library for example to parse a string (JSON, csv, etc.).</p>
     * @summary The {@link core.chars} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods to validate and transform a basic character.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.chars
     * @memberof core
     */
    var chars$1 = {
      compare: compare$1,
      isAlpha: isAlpha,
      isAlphaOrDigit: isAlphaOrDigit,
      isASCII: isASCII,
      isContained: isContained,
      isDigit: isDigit,
      isHexDigit: isHexDigit,
      isIdentifierStart: isIdentifierStart,
      isLineTerminator: isLineTerminator,
      isLower: isLower,
      isOctalDigit: isOctalDigit,
      isOperator: isOperator,
      isSymbol: isSymbol$1,
      isUnicode: isUnicode,
      isUpper: isUpper,
      isWhiteSpace: isWhiteSpace,
      lineTerminators: lineTerminators,
      operators: operators,
      symbols: symbols,
      whiteSpaces: whiteSpaces
    };

    var distance$1 = function distance(color1, color2) {
      return Math.pow((color1 >> 16 & 0xFF) - (color2 >> 16 & 0xFF), 2) + Math.pow((color1 >> 8 & 0xFF) - (color2 >> 8 & 0xFF), 2) + Math.pow((color1 & 0xFF) - (color2 & 0xFF), 2);
    };

    var equals$1 = function equals(color1, color2) {
      var tolerance = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.01;
      return distance$1(color1, color2) <= tolerance * (255 * 255 * 3) << 0;
    };

    var fade = function fade() {
      var from = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var to = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0xFFFFFF;
      var ratio = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      if (ratio <= 0) {
        return from;
      } else if (ratio >= 1) {
        return to;
      }
      var r = from >> 16;
      var g = from >> 8 & 0xFF;
      var b = from & 0xFF;
      r += ((to >> 16) - r) * ratio;
      g += ((to >> 8 & 0xFF) - g) * ratio;
      b += ((to & 0xFF) - b) * ratio;
      return r << 16 | g << 8 | b;
    };

    var isLittleEndian = function isLittleEndian() {
      var a = new ArrayBuffer(4);
      var b = new Uint8Array(a);
      var c = new Uint32Array(a);
      b[0] = 0xa1;
      b[1] = 0xb2;
      b[2] = 0xc3;
      b[3] = 0xd4;
      if (c[0] === 0xd4c3b2a1) {
        return true;
      }
      if (c[0] === 0xa1b2c3d4) {
        return false;
      } else {
        return null;
      }
    };
    var littleEndian = isLittleEndian();

    var max = 0xFF;
    var fromARGB = function fromARGB(a, r, g, b) {
      r = Math.min(Math.max(r, 0), max);
      g = Math.min(Math.max(g, 0), max);
      b = Math.min(Math.max(b, 0), max);
      a = isNaN(a) ? 0 : a;
      a = 0xFF * Math.max(Math.min(a, 1), 0);
      return littleEndian ? (a << 24 | b << 16 | g << 8 | r) >>> 0 : (r << 24 | g << 16 | b << 8 | a) >>> 0;
    };

    var getAlpha = function getAlpha(color) {
      return color >> 24 & 0xFF;
    };

    var getBlue = function getBlue(color) {
      return color & 0xFF;
    };

    var getGreen = function getGreen(color) {
      return color >> 8 & 0xFF;
    };

    var getRed = function getRed(color) {
      return color >> 16 & 0xFF;
    };

    function hex(value) {
      var upper = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      var hex = Number(value).toString(16);
      hex = hex.length % 2 === 0 ? hex : "0" + hex;
      return upper ? hex.toUpperCase() : hex;
    }

    var hexToRgb = function hexToRgb(hex) {
      return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (m, r, g, b) {
        return '#' + r + r + g + g + b + b;
      }).substring(1).match(/.{2}/g).map(function (x) {
        return parseInt(x, 16);
      });
    };

    var hexIsDark = function hexIsDark(hex) {
      var _hexToRgb = hexToRgb(hex),
        _hexToRgb2 = _slicedToArray(_hexToRgb, 3),
        r = _hexToRgb2[0],
        g = _hexToRgb2[1],
        b = _hexToRgb2[2];
      return (r * 299 + g * 587 + b * 114) / 1000 < 128;
    };

    var hexIsLight = function hexIsLight(hex) {
      var _hexToRgb = hexToRgb(hex),
        _hexToRgb2 = _slicedToArray(_hexToRgb, 3),
        r = _hexToRgb2[0],
        g = _hexToRgb2[1],
        b = _hexToRgb2[2];
      return (r * 299 + g * 587 + b * 114) / 1000 >= 128;
    };

    var hexToInt = function hexToInt(hex) {
      return parseInt(hex.replace(/^#/, ''), 16);
    };

    var hexToYIQ = function hexToYIQ(hex) {
      var _hexToRgb = hexToRgb(hex),
        _hexToRgb2 = _slicedToArray(_hexToRgb, 3),
        r = _hexToRgb2[0],
        g = _hexToRgb2[1],
        b = _hexToRgb2[2];
      return (r * 299 + g * 587 + b * 114) / 1000;
    };

    var htmlColors = {
      aliceblue: [240, 248, 255],
      antiquewhite: [250, 235, 215],
      aqua: [0, 255, 255],
      aquamarine: [127, 255, 212],
      azure: [240, 255, 255],
      beige: [245, 245, 220],
      bisque: [255, 228, 196],
      black: [0, 0, 0],
      blanchedalmond: [255, 235, 205],
      blue: [0, 0, 255],
      blueviolet: [138, 43, 226],
      brown: [165, 42, 42],
      burlywood: [222, 184, 135],
      cadetblue: [95, 158, 160],
      chartreuse: [127, 255, 0],
      chocolate: [210, 105, 30],
      coral: [255, 127, 80],
      cornflowerblue: [100, 149, 237],
      cornsilk: [255, 248, 220],
      crimson: [220, 20, 60],
      cyan: [0, 255, 255],
      darkblue: [0, 0, 139],
      darkcyan: [0, 139, 139],
      darkgoldenrod: [184, 134, 11],
      darkgray: [169, 169, 169],
      darkgreen: [0, 100, 0],
      darkgrey: [169, 169, 169],
      darkkhaki: [189, 183, 107],
      darkmagenta: [139, 0, 139],
      darkolivegreen: [85, 107, 47],
      darkorange: [255, 140, 0],
      darkorchid: [153, 50, 204],
      darkred: [139, 0, 0],
      darksalmon: [233, 150, 122],
      darkseagreen: [143, 188, 143],
      darkslateblue: [72, 61, 139],
      darkslategray: [47, 79, 79],
      darkslategrey: [47, 79, 79],
      darkturquoise: [0, 206, 209],
      darkviolet: [148, 0, 211],
      deeppink: [255, 20, 147],
      deepskyblue: [0, 191, 255],
      dimgray: [105, 105, 105],
      dimgrey: [105, 105, 105],
      dodgerblue: [30, 144, 255],
      firebrick: [178, 34, 34],
      floralwhite: [255, 250, 240],
      forestgreen: [34, 139, 34],
      fuchsia: [255, 0, 255],
      gainsboro: [220, 220, 220],
      ghostwhite: [248, 248, 255],
      gold: [255, 215, 0],
      goldenrod: [218, 165, 32],
      gray: [128, 128, 128],
      green: [0, 128, 0],
      greenyellow: [173, 255, 47],
      grey: [128, 128, 128],
      honeydew: [240, 255, 240],
      hotpink: [255, 105, 180],
      indianred: [205, 92, 92],
      indigo: [75, 0, 130],
      ivory: [255, 255, 240],
      khaki: [240, 230, 140],
      lavender: [230, 230, 250],
      lavenderblush: [255, 240, 245],
      lawngreen: [124, 252, 0],
      lemonchiffon: [255, 250, 205],
      lightblue: [173, 216, 230],
      lightcoral: [240, 128, 128],
      lightcyan: [224, 255, 255],
      lightgoldenrodyellow: [250, 250, 210],
      lightgray: [211, 211, 211],
      lightgreen: [144, 238, 144],
      lightgrey: [211, 211, 211],
      lightpink: [255, 182, 193],
      lightsalmon: [255, 160, 122],
      lightseagreen: [32, 178, 170],
      lightskyblue: [135, 206, 250],
      lightslategray: [119, 136, 153],
      lightslategrey: [119, 136, 153],
      lightsteelblue: [176, 196, 222],
      lightyellow: [255, 255, 224],
      lime: [0, 255, 0],
      limegreen: [50, 205, 50],
      linen: [250, 240, 230],
      magenta: [255, 0, 255],
      maroon: [128, 0, 0],
      mediumaquamarine: [102, 205, 170],
      mediumblue: [0, 0, 205],
      mediumorchid: [186, 85, 211],
      mediumpurple: [147, 112, 219],
      mediumseagreen: [60, 179, 113],
      mediumslateblue: [123, 104, 238],
      mediumspringgreen: [0, 250, 154],
      mediumturquoise: [72, 209, 204],
      mediumvioletred: [199, 21, 133],
      midnightblue: [25, 25, 112],
      mintcream: [245, 255, 250],
      mistyrose: [255, 228, 225],
      moccasin: [255, 228, 181],
      navajowhite: [255, 222, 173],
      navy: [0, 0, 128],
      oldlace: [253, 245, 230],
      olive: [128, 128, 0],
      olivedrab: [107, 142, 35],
      orange: [255, 165, 0],
      orangered: [255, 69, 0],
      orchid: [218, 112, 214],
      palegoldenrod: [238, 232, 170],
      palegreen: [152, 251, 152],
      paleturquoise: [175, 238, 238],
      palevioletred: [219, 112, 147],
      papayawhip: [255, 239, 213],
      peachpuff: [255, 218, 185],
      peru: [205, 133, 63],
      pink: [255, 192, 203],
      plum: [221, 160, 221],
      powderblue: [176, 224, 230],
      purple: [128, 0, 128],
      rebeccapurple: [102, 51, 153],
      red: [255, 0, 0],
      rosybrown: [188, 143, 143],
      royalblue: [65, 105, 225],
      saddlebrown: [139, 69, 19],
      salmon: [250, 128, 114],
      sandybrown: [244, 164, 96],
      seagreen: [46, 139, 87],
      seashell: [255, 245, 238],
      sienna: [160, 82, 45],
      silver: [192, 192, 192],
      skyblue: [135, 206, 235],
      slateblue: [106, 90, 205],
      slategray: [112, 128, 144],
      slategrey: [112, 128, 144],
      snow: [255, 250, 250],
      springgreen: [0, 255, 127],
      steelblue: [70, 130, 180],
      tan: [210, 180, 140],
      teal: [0, 128, 128],
      thistle: [216, 191, 216],
      tomato: [255, 99, 71],
      turquoise: [64, 224, 208],
      violet: [238, 130, 238],
      wheat: [245, 222, 179],
      white: [255, 255, 255],
      whitesmoke: [245, 245, 245],
      yellow: [255, 255, 0],
      yellowgreen: [154, 205, 50]
    };

    var CMYK = 'cmyk';
    var COLOR = 'color';
    var DEG = 'deg';
    var GRAD = 'grad';
    var GRAY = 'gray';
    var NONE = 'none';
    var RAD = 'rad';
    var RGB = 'rgb';
    var TURN = 'turn';
    var baseHues = {
      red: 0,
      orange: 60,
      yellow: 120,
      green: 180,
      blue: 240,
      purple: 300
    };
    var parse = function parse(color) {
      var names = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var m,
        alpha = 1,
        parts = [],
        space;
      if (isNumber$1(color)) {
        return {
          space: RGB,
          values: [color >>> 16, (color & 0x00ff00) >>> 8, color & 0x0000ff],
          alpha: 1
        };
      }
      color = (color + '').toLowerCase();
      if (names !== null && names !== void 0 && names[color])
        {
          parts = names[color].slice();
          space = RGB;
        } else if (color === 'transparent')
        {
          alpha = 0;
          space = 'rgb';
          parts = [0, 0, 0];
        } else if (color[0] === '#')
        {
          var _parts$, _parts, _parts$2, _parts2, _parts$3, _parts3;
          var base = color.slice(1);
          var size = base.length;
          var isShort = size <= 4;
          alpha = 1;
          if (isShort) {
            parts = [parseInt(base[0] + base[0], 16), parseInt(base[1] + base[1], 16), parseInt(base[2] + base[2], 16)];
            if (size === 4) {
              alpha = parseInt(base[3] + base[3], 16) / 255;
            }
          } else {
            parts = [parseInt(base[0] + base[1], 16), parseInt(base[2] + base[3], 16), parseInt(base[4] + base[5], 16)];
            if (size === 8) {
              alpha = parseInt(base[6] + base[7], 16) / 255;
            }
          }
          parts[0] = (_parts$ = (_parts = parts) === null || _parts === void 0 ? void 0 : _parts[0]) !== null && _parts$ !== void 0 ? _parts$ : 0;
          parts[1] = (_parts$2 = (_parts2 = parts) === null || _parts2 === void 0 ? void 0 : _parts2[1]) !== null && _parts$2 !== void 0 ? _parts$2 : 0;
          parts[2] = (_parts$3 = (_parts3 = parts) === null || _parts3 === void 0 ? void 0 : _parts3[2]) !== null && _parts$3 !== void 0 ? _parts$3 : 0;
          space = RGB;
        } else if (m = /^((?:rgba?|hs[lvb]a?|hwba?|cmyk?|xy[zy]|gray|lab|lchu?v?|[ly]uv|lms|oklch|oklab|color))\s*\(([^\)]*)\)/.exec(color))
        {
          var name = m[1];
          space = name.replace(/a$/, '');
          var dims = space === CMYK ? 4 : space === GRAY ? 1 : 3;
          parts = m[2].trim().split(/\s*[,\/]\s*|\s+/);
          if (space === COLOR) {
            space = parts.shift();
          }
          parts = parts.map(function (x, i) {
            if (x[x.length - 1] === '%') {
              x = parseFloat(x) / 100;
              if (i === 3) {
                return x;
              }
              if (space === RGB) {
                return x * 255;
              }
              if (space[0] === 'h') {
                return x * 100;
              }
              if (space[0] === 'l' && !i) {
                return x * 100;
              }
              if (space === 'lab') {
                return x * 125;
              }
              if (space === 'lch') {
                return i < 2 ? x * 150 : x * 360;
              }
              if (space[0] === 'o' && !i) {
                return x;
              }
              if (space === 'oklab') {
                return x * 0.4;
              }
              if (space === 'oklch') {
                return i < 2 ? x * 0.4 : x * 360;
              }
              return x;
            }
            if (space[i] === 'h' || i === 2 && space[space.length - 1] === 'h') {
              if ((baseHues === null || baseHues === void 0 ? void 0 : baseHues[x]) !== undefined)
                {
                  return baseHues[x];
                }
              if (x.endsWith(DEG)) {
                return parseFloat(x);
              }
              if (x.endsWith(TURN)) {
                return parseFloat(x) * 360;
              }
              if (x.endsWith(GRAD)) {
                return parseFloat(x) * 360 / 400;
              }
              if (x.endsWith(RAD)) {
                return parseFloat(x) * 180 / Math.PI;
              }
            }
            if (x === NONE) {
              return 0;
            }
            return parseFloat(x);
          });
          alpha = parts.length > dims ? parts.pop() : 1;
        } else if (/[0-9](?:\s|\/|,)/.test(color))
        {
          var _color$match, _color$match$join;
          parts = color.match(/([0-9]+)/g).map(function (value) {
            return parseFloat(value);
          });
          space = ((_color$match = color.match(/([a-z])/ig)) === null || _color$match === void 0 ? void 0 : (_color$match$join = _color$match.join('')) === null || _color$match$join === void 0 ? void 0 : _color$match$join.toLowerCase()) || RGB;
        }
      return {
        space: space,
        values: parts,
        alpha: alpha
      };
    };

    var leading = function leading(value) {
      var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
      var _char = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '0';
      var isNegative = Number(value) < 0;
      count = count > 0 ? count : 2;
      var buffer = value.toString();
      if (isNegative) {
        buffer = buffer.slice(1);
      }
      var size = count - buffer.length + 1;
      buffer = new Array(size).join(_char).concat(buffer);
      return (isNegative ? '-' : '') + buffer;
    };

    var percentToHex = function percentToHex(value) {
      var upperCase = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var result = leading(Math.round(value / 100 * 255).toString(16));
      return upperCase ? result.toUpperCase() : result;
    };

    var rgbIsDark = function rgbIsDark(r, g, b) {
      return (r * 299 + g * 587 + b * 114) / 1000 < 128;
    };

    var rgbIsLight = function rgbIsLight(r, g, b) {
      return (r * 299 + g * 587 + b * 114) / 1000 >= 128;
    };

    var rgbToHex = function rgbToHex() {
      var r = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var g = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var b = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      if (Array.isArray(r) && r.length === 3) {
        g = r[1];
        b = r[2];
        r = r[0];
      }
      return '#' + [r, g, b].map(function (x) {
        return Number(x).toString(16).padStart(2, '0');
      }).join('');
    };

    var rgbToYIQ = function rgbToYIQ(r, g, b) {
      return (r * 299 + g * 587 + b * 114) / 1000;
    };

    var isUnique = function isUnique(color, colors) {
      var tolerance = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.01;
      if (!(colors instanceof Array) || colors.length === 0) {
        return true;
      }
      tolerance = tolerance * (255 * 255 * 3) << 0;
      var cur;
      var distance;
      var len = colors.length;
      for (var i = 0; i < len; i++) {
        cur = colors[i];
        distance = Math.pow((color >> 16 & 0xFF) - (cur >> 16 & 0xFF), 2) + Math.pow((color >> 8 & 0xFF) - (cur >> 8 & 0xFF), 2) + Math.pow((color & 0xFF) - (cur & 0xFF), 2);
        if (distance <= tolerance) {
          return false;
        }
      }
      return true;
    };

    var toHex = function toHex(value) {
      var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#';
      var upper = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      if (!(Number(value) === value && value % 1 === 0 && value >= 0)) {
        throw new TypeError('toHex failed, the value parameter must be an upper integer.');
      }
      prefix = typeof prefix === 'string' || prefix instanceof String ? prefix : '#';
      upper = upper === true;
      var gb;
      var r = value >> 16;
      gb = value ^ r << 16;
      var g = gb >> 8;
      var b = gb ^ g << 8;
      return prefix + hex(r, upper) + hex(g, upper) + hex(b, upper);
    };

    var uniques = function uniques(colors) {
      var maximum = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0xFFFFFF;
      var tolerance = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.01;
      var result = [];
      for (var i = 0; i < colors.length && result.length < maximum; i++) {
        if (isUnique(colors[i], result, tolerance)) {
          result.push(colors[i]);
        }
      }
      return result;
    };

    /**
     * The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
     * @summary The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.colors
     * @memberof core
     */
    var colors$1 = {
      distance: distance$1,
      equals: equals$1,
      fade: fade,
      fromARGB: fromARGB,
      getAlpha: getAlpha,
      getBlue: getBlue,
      getGreen: getGreen,
      getRed: getRed,
      hex: hex,
      hexIsDark: hexIsDark,
      hexToInt: hexToInt,
      hexIsLight: hexIsLight,
      hexToRgb: hexToRgb,
      hexToYIQ: hexToYIQ,
      htmlColors: htmlColors,
      isUnique: isUnique,
      parse: parse,
      percentToHex: percentToHex,
      rgbIsDark: rgbIsDark,
      rgbIsLight: rgbIsLight,
      rgbToHex: rgbToHex,
      rgbToYIQ: rgbToYIQ,
      toHex: toHex,
      uniques: uniques
    };

    var arrayReduce = function arrayReduce(array, iteratee, accumulator, initAccum) {
      var index = -1;
      var length = array == null ? 0 : array.length;
      if (initAccum && length) {
        accumulator = array[++index];
      }
      while (++index < length) {
        accumulator = iteratee(accumulator, array[index], index, array);
      }
      return accumulator;
    };

    var baseFor = function baseFor(object, iteratee, keysFunc) {
      var iterable = Object(object);
      var props = keysFunc(object);
      var index = -1;
      var length = props.length;
      while (length--) {
        var key = props[++index];
        if (iteratee(iterable[key], key, iterable) === false) {
          break;
        }
      }
      return object;
    };

    var baseForOwn = function baseForOwn(object, iteratee) {
      return object && baseFor(object, iteratee, keys);
    };

    var baseEach = function baseEach(collection, iteratee) {
      if (collection === null || collection === undefined) {
        return collection;
      }
      if (!isArrayLike$1(collection)) {
        return baseForOwn(collection, iteratee);
      }
      var length = collection.length;
      var iterable = Object(collection);
      var index = -1;
      while (++index < length) {
        if (iteratee(iterable[index], index, iterable) === false) {
          break;
        }
      }
      return collection;
    };

    var baseReduce = function baseReduce(collection, iteratee, accumulator, initAccum, eachFunc) {
      eachFunc(collection, function (value, index, collection) {
        accumulator = initAccum ? (initAccum = false, value) : iteratee(accumulator, value, index, collection);
      });
      return accumulator;
    };

    var _arguments = typeof arguments === "undefined" ? void 0 : arguments;
    var reduce = function reduce(collection, iteratee, accumulator) {
      var func = Array.isArray(collection) ? arrayReduce : baseReduce;
      var initAccum = _arguments.length < 3;
      return func(collection, iteratee, accumulator, initAccum, baseEach);
    };

    var partition = function partition(collection, predicate) {
      return reduce(collection, function (result, value) {
        result[predicate(value) ? 0 : 1].push(value);
        return result;
      }, [[], []]);
    };

    /**
     * The {@link core.collections} package is a modular <b>JavaScript</b> library that provides extra <code>collections</code> methods. A collection is an array of objects.
     * @summary The {@link core.collections} package is a modular <b>JavaScript</b> library that provides extra <code>collection</code> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.collections
     * @memberof core
     * @since 1.0.37
     */
    var collections$1 = {
      partition: partition,
      reduce: reduce
    };

    var ONE_DAY_MS = 86400000;

    var MS_PER_HOUR = 60 * 60 * 1000;

    var MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR;

    var MS_PER_DAY = 24 * 60 * 60 * 1000;

    var MS_PER_MINUTE = 60 * 1000;

    var MS_PER_SECOND = 1000;

    var MS_PER_WEEK = 7 * 24 * 60 * 60 * 1000;

    var MONTH_PER_CENTURY = 12 * 100;

    var MONTH_PER_DECADE = 12 * 10;

    var MONTH_PER_YEAR = 12;

    var solveDST = function solveDST(current, next) {
      var currentOffset = current.getTimezoneOffset();
      var nextOffset = next.getTimezoneOffset();
      var diffMinutes = nextOffset - currentOffset;
      return new Date(+next + diffMinutes * MS_PER_MINUTE);
    };

    var addMS = function addMS(date, value) {
      if (!(date instanceof Date)) {
        date = new Date();
      }
      return solveDST(date, new Date(+date + value));
    };

    var addDays = function addDays(date, value) {
      return addMS(date, value * MS_PER_DAY);
    };

    var addHours = function addHours(date, value) {
      return addMS(date, value * MS_PER_HOUR);
    };

    var addMinutes = function addMinutes(date, value) {
      return addMS(date, value * MS_PER_MINUTE);
    };

    var leapYear = function leapYear(date) {
      var year;
      if (date instanceof Date) {
        year = date.getFullYear();
      } else if (typeof date === 'number' || date instanceof Number) {
        year = date;
      } else {
        throw new TypeError('leapYear failed, the passed-in date argument must be a valid Date object or an integer representing the year to evaluates.');
      }
      return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    };

    var daysInFebruary = function daysInFebruary(year) {
      return leapYear(year) ? 29 : 28;
    };

    var daysOf = function daysOf(year) {
      return [31, daysInFebruary(year), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    };

    var addMonths = function addMonths(date, value) {
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth();
      var totalMonths = year * 12 + month + value;
      var nextYear = Math.trunc(totalMonths / 12);
      var nextMonth = totalMonths % 12;
      var nextDay = Math.min(day, daysOf(nextYear)[nextMonth]);
      var next = new Date(date);
      next.setFullYear(nextYear);
      next.setDate(1);
      next.setMonth(nextMonth);
      next.setDate(nextDay);
      return next;
    };

    var addSeconds = function addSeconds(date, seconds) {
      return new Date(date.getTime() + seconds * MS_PER_SECOND);
    };

    var addWeeks = function addWeeks(date, value) {
      return addMS(date, value * MS_PER_WEEK);
    };

    var addYears = function addYears(date, value) {
      return addMonths(date, value * MONTH_PER_YEAR);
    };

    var after = function after(date1, date2) {
      if (!(date1 instanceof Date && date2 instanceof Date)) {
        throw new TypeError('after() failed, the passed-in date arguments must be valid Date objects.');
      }
      return date1.valueOf() > date2.valueOf();
    };

    var before = function before(date1, date2) {
      if (!(date1 instanceof Date && date2 instanceof Date)) {
        throw new TypeError('after() failed, the passed-in date arguments must be valid Date objects.');
      }
      return date1.valueOf() < date2.valueOf();
    };

    var createDate = function createDate(year) {
      var month = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var day = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
      var hours = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
      var minutes = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
      var seconds = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
      var ms = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 0;
      var date;
      if (year < 100 && year >= 0)
        {
          date = new Date(year + 400, month, day, hours, minutes, seconds, ms);
          if (isFinite(date.getFullYear())) {
            date.setFullYear(year);
          }
        } else {
        date = new Date(year, month, day, hours, minutes, seconds, ms);
      }
      return date;
    };

    var createUTCDate = function createUTCDate(year) {
      var date;
      for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        rest[_key - 1] = arguments[_key];
      }
      var args = [year].concat(rest);
      if (year < 100 && year >= 0)
        {
          args[0] = year + 400;
          date = new Date(Date.UTC.apply(null, args));
          if (isFinite(date.getUTCFullYear())) {
            date.setUTCFullYear(year);
          }
        } else {
        date = new Date(Date.UTC.apply(null, args));
      }
      return date;
    };

    var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var daysInMonth = function daysInMonth() {
      var year = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var month = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var y, m;
      if (year instanceof Date) {
        y = year.getFullYear();
        m = year.getMonth();
      } else if ((typeof year === 'number' || year instanceof Number) && (typeof month === 'number' || month instanceof Number)) {
        y = year;
        m = month;
      } else {
        var now = new Date();
        y = now.getFullYear();
        m = now.getMonth();
      }
      if (m === 1) {
        return y % 4 === 0 && y % 100 !== 0 || y % 400 === 0 ? 29 : 28;
      }
      return monthDays[m];
    };

    var daysInYear = function daysInYear(year) {
      return leapYear(year) ? 366 : 365;
    };

    var CONSTANT$1 = 4800 / 146097;
    var daysToMonths = function daysToMonths(days) {
      return days * CONSTANT$1;
    };

    var equals = function equals(date1, date2) {
      if (!(date1 instanceof Date && date2 instanceof Date)) {
        throw new TypeError('equals() failed, the passed-in date arguments must be valid Date objects.');
      }
      return date1.valueOf() === date2.valueOf();
    };

    var endOfDay = function endOfDay() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      date = new Date(date);
      date.setHours(23, 59, 59, 999);
      return date;
    };

    var localStartOfDate = function localStartOfDate(y, m, d) {
      if (y < 100 && y >= 0) {
        return new Date(y + 400, m, d) - MS_PER_400_YEARS;
      } else {
        return new Date(y, m, d).valueOf();
      }
    };

    var utcStartOfDate = function utcStartOfDate(y, m, d) {
      if (y < 100 && y >= 0) {
        return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
      } else {
        return Date.UTC(y, m, d);
      }
    };

    var endOfMonth = function endOfMonth() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var isUTC = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
      return new Date(startOfDate(date.getFullYear(), date.getMonth() + 1, 1) - 1);
    };

    var getLength = function getLength(method) {
      switch (method) {
        case 'Milliseconds':
          {
            return 3600000;
          }
        case 'Seconds':
          {
            return 3600;
          }
        case 'Minutes':
          {
            return 60;
          }
        case 'Hours':
          {
            return 1;
          }
        default:
          {
            return null;
          }
      }
    };
    var createAccessors = function createAccessors(method) {
      var length = getLength(method);
      return function (date, value) {
        if (value === undefined) {
          return date['get' + method]();
        }
        var dateOut = new Date(date);
        var getter = dateOut['get' + method];
        var setter = dateOut['set' + method];
        if (setter instanceof Function && getter instanceof Function) {
          dateOut['set' + method](value);
          if (length && dateOut['get' + method]() !== value && (method === 'Hours' || value >= length && dateOut.getHours() - date.getHours() < Math.floor(value / length))) {
            dateOut['set' + method](value + length);
          }
        }
        return dateOut;
      };
    };
    var date$2 = createAccessors('Date');
    var day$1 = createAccessors('Day');
    var hours$2 = createAccessors('Hours');
    var milliseconds$1 = createAccessors('Milliseconds');
    var minutes$1 = createAccessors('Minutes');
    var seconds$1 = createAccessors('Seconds');
    var year = createAccessors('FullYear');
    var accessors = {
      date: date$2,
      day: day$1,
      hours: hours$2,
      milliseconds: milliseconds$1,
      minutes: minutes$1,
      seconds: seconds$1,
      year: year
    };

    var day = accessors.day,
      hours$1 = accessors.hours;
    var weekday = function weekday(date, value, firstDay) {
      var w = (day(date) + 7 - (firstDay || 0)) % 7;
      return value === undefined ? w : addDays(date, value - w);
    };
    var startOfWeek = function startOfWeek() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var firstOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      date = new Date(date);
      date = hours$1(date, 0);
      date = weekday(date, 0, firstOfWeek);
      return date;
    };

    var subtractDays = function subtractDays(date, value) {
      return addDays(date, -value);
    };

    var endOfWeek = function endOfWeek() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var firstOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      date = new Date(date);
      date = startOfWeek(date, firstOfWeek);
      date = addWeeks(date, 1);
      date = subtractDays(date, 1);
      date.setHours(23, 59, 59, 999);
      return date;
    };

    var endOfYear = function endOfYear() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var isUTC = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
      return new Date(startOfDate(date.getFullYear() + 1, 0, 1) - 1);
    };

    var startOfMonth = function startOfMonth() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var isUTC = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
      return new Date(startOfDate(date.getFullYear(), date.getMonth(), 1));
    };

    var firstVisibleDay = function firstVisibleDay(date) {
      var firstOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var isUTC = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return startOfWeek(startOfMonth(date, isUTC), firstOfWeek);
    };

    var lastVisibleDay = function lastVisibleDay(date) {
      var firstOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var isUTC = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return endOfWeek(endOfMonth(date, isUTC), firstOfWeek);
    };

    var CONSTANT = 146097 / 4800;
    var monthsToDays = function monthsToDays(months) {
      return months * CONSTANT;
    };

    var milliseconds = accessors.milliseconds,
      minutes = accessors.minutes,
      hours = accessors.hours,
      seconds = accessors.seconds;
    var startOfDay = function startOfDay() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      date = new Date(date);
      date = hours(date, 0);
      date = minutes(date, 0);
      date = seconds(date, 0);
      date = milliseconds(date, 0);
      return date;
    };

    var startOfYear = function startOfYear() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var isUTC = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
      return new Date(startOfDate(date.getFullYear(), 0, 1));
    };

    var subtractHours = function subtractHours(date, value) {
      return addHours(date, -value);
    };

    var subtractMinutes = function subtractMinutes(date, value) {
      return addMinutes(date, -value);
    };

    var subtractMonths = function subtractMonths(date, value) {
      return addMonths(date, -value);
    };

    var subtractMS = function subtractMS(date, value) {
      return addMS(date, -value);
    };

    var subtractSeconds = function subtractSeconds(date, value) {
      return addSeconds(date, -value);
    };

    var tomorrow = function tomorrow() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var d = date instanceof Date ? date : new Date();
      return new Date(d.valueOf() + ONE_DAY_MS);
    };

    var createComparer = function createComparer(operator) {
      return function (a, b) {
        return operator(+startOfDay(a), +startOfDay(b));
      };
    };
    var lte = createComparer(function (a, b) {
      return a <= b;
    });

    var visibleDays = function visibleDays(date) {
      var firstOfWeek = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var isUTC = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      date = new Date(date);
      var current = firstVisibleDay(date, firstOfWeek, isUTC);
      var last = lastVisibleDay(date, firstOfWeek, isUTC);
      var days = [];
      while (lte(current, last)) {
        days.push(current);
        current = addDays(current, 1);
      }
      return days;
    };

    var yesterday = function yesterday() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var d = date instanceof Date ? date : new Date();
      return new Date(d.valueOf() - ONE_DAY_MS);
    };

    /**
     * The {@link core.date} package is a modular <b>JavaScript</b> library that provides extra <code>Date</code> methods.
     * @summary The {@link core.date} package is a modular <b>JavaScript</b> library that provides extra <code>Date</code> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.date
     * @memberof core
     */
    var date$1 = {
      ONE_DAY_MS: ONE_DAY_MS,
      MONTH_PER_CENTURY: MONTH_PER_CENTURY,
      MONTH_PER_DECADE: MONTH_PER_DECADE,
      MONTH_PER_YEAR: MONTH_PER_YEAR,
      MS_PER_400_YEARS: MS_PER_400_YEARS,
      MS_PER_DAY: MS_PER_DAY,
      MS_PER_HOUR: MS_PER_HOUR,
      MS_PER_MINUTE: MS_PER_MINUTE,
      MS_PER_SECOND: MS_PER_SECOND,
      MS_PER_WEEK: MS_PER_WEEK,
      addDays: addDays,
      addHours: addHours,
      addMinutes: addMinutes,
      addMonths: addMonths,
      addMS: addMS,
      addSeconds: addSeconds,
      addWeeks: addWeeks,
      addYears: addYears,
      after: after,
      before: before,
      createUTCDate: createUTCDate,
      daysInFebruary: daysInFebruary,
      daysInMonth: daysInMonth,
      daysInYear: daysInYear,
      daysToMonths: daysToMonths,
      daysOf: daysOf,
      createDate: createDate,
      equals: equals,
      endOfDay: endOfDay,
      endOfMonth: endOfMonth,
      endOfWeek: endOfWeek,
      endOfYear: endOfYear,
      firstVisibleDay: firstVisibleDay,
      lastVisibleDay: lastVisibleDay,
      leapYear: leapYear,
      monthsToDays: monthsToDays,
      startOfDay: startOfDay,
      startOfMonth: startOfMonth,
      startOfWeek: startOfWeek,
      startOfYear: startOfYear,
      subtractDays: subtractDays,
      subtractHours: subtractHours,
      subtractMinutes: subtractMinutes,
      subtractMonths: subtractMonths,
      subtractMS: subtractMS,
      subtractSeconds: subtractSeconds,
      tomorrow: tomorrow,
      visibleDays: visibleDays,
      yesterday: yesterday
    };

    var isDOMElement = function isDOMElement(value) {
      if (!value) {
        return false;
      }
      try {
        return value instanceof Node;
      } catch (e) {
        return false;
      }
    };

    var isDOMObject = function isDOMObject(value) {
      if (!value) {
        return false;
      }
      try {
        return value instanceof EventTarget;
      } catch (e) {
        return false;
      }
    };

    var isElement = function isElement(value) {
      if (!value) {
        return false;
      }
      try {
        return value instanceof Element;
      } catch (e) {
        return !!(value && _typeof(value) === "object" && 'nodeType' in value && value.nodeType === 1 && value.nodeName);
      }
    };

    var isHTMLElement = function isHTMLElement(value) {
      if (!value) {
        return false;
      }
      if ("HTMLElement" in window) {
        return value && value instanceof HTMLElement;
      }
      return !!(value && _typeof(value) === "object" && 'nodeType' in value && value.nodeType === 1 && value.nodeName);
    };

    var isListObject = function isListObject(value) {
      if (!value) {
        return false;
      }
      try {
        return value instanceof NodeList;
      } catch (e) {
        return false;
      }
    };

    var isSVGElement = function isSVGElement(value) {
      if (!value) {
        return false;
      }
      if ("SVGElement" in window) {
        return value && value instanceof SVGElement;
      }
      return !!(value && _typeof(value) === "object" && 'nodeType' in value && value.nodeType === 1 && value.nodeName && value.xmlbase);
    };

    var isTouchDevice = function isTouchDevice() {
      return !!(typeof window !== 'undefined' && ('ontouchstart' in window || window.DocumentTouch && typeof document !== 'undefined' && document instanceof window.DocumentTouch)) || !!(typeof navigator !== 'undefined' && (navigator.maxTouchPoints || navigator.msMaxTouchPoints));
    };

    var supported = false;
    try {
      window.addEventListener('check', null, Object.defineProperty({}, 'passive', {
        get: function get() {
          supported = {
            passive: true
          };
        }
      }));
    } catch (error) {
    }
    var passiveIfSupported = supported;

    /**
     * The {@link core.dom} package is a modular <b>JavaScript</b> library that provides extra <code>W3C DOM</code> methods.
     * @summary The {@link core.dom} package is a modular <b>JavaScript</b> library that provides extra <code>W3C DOM</code> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.dom
     * @memberof core
     */
    var dom$1 = {
      isDOMElement: isDOMElement,
      isDOMObject: isDOMObject,
      isElement: isElement,
      isHTMLElement: isHTMLElement,
      isListObject: isListObject,
      isSVGElement: isSVGElement,
      isTouchDevice: isTouchDevice,
      passiveIfSupported: passiveIfSupported
    };

    var backIn = function backIn(t, b, c, d) {
      var s = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1.70158;
      if (isNaN(s)) {
        s = 1.70158;
      }
      return c * (t /= d) * t * ((s + 1) * t - s) + b;
    };

    var backInOut = function backInOut(t, b, c, d) {
      var s = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1.70158;
      if (isNaN(s)) {
        s = 1.70158;
      }
      if ((t /= d / 2) < 1) {
        return c / 2 * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
      }
      return c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
    };

    var backOut = function backOut(t, b, c, d) {
      var s = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1.70158;
      if (isNaN(s)) {
        s = 1.70158;
      }
      return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    };

    var bounceOut = function bounceOut(t, b, c, d) {
      if ((t /= d) < 1 / 2.75) {
        return c * (7.5625 * t * t) + b;
      } else if (t < 2 / 2.75) {
        return c * (7.5625 * (t -= 1.5 / 2.75) * t + 0.75) + b;
      } else if (t < 2.5 / 2.75) {
        return c * (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375) + b;
      } else {
        return c * (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375) + b;
      }
    };

    var bounceIn = function bounceIn(t, b, c, d) {
      return c - bounceOut(d - t, 0, c, d) + b;
    };

    var bounceInOut = function bounceInOut(t, b, c, d) {
      return t < d / 2 ? bounceIn(t * 2, 0, c, d) * 0.5 + b : bounceOut(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b;
    };

    var circularIn = function circularIn(t, b, c, d) {
      return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    };

    var circularInOut = function circularInOut(t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
      }
      return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    };

    var circularOut = function circularOut(t, b, c, d) {
      return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    };

    var cubicIn = function cubicIn(t, b, c, d) {
      return c * (t /= d) * t * t + b;
    };

    var cubicInOut = function cubicInOut(t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return c / 2 * t * t * t + b;
      }
      return c / 2 * ((t -= 2) * t * t + 2) + b;
    };

    var cubicOut = function cubicOut(t, b, c, d) {
      return c * ((t = t / d - 1) * t * t + 1) + b;
    };

    var elasticIn = function elasticIn(t, b, c, d) {
      var a = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
      var p = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
      var s;
      if (t === 0) {
        return b;
      }
      if ((t /= d) === 1) {
        return b + c;
      }
      if (!p) {
        p = d * 0.3;
      }
      if (!a || a < Math.abs(c)) {
        a = c;
        s = p / 4;
      } else {
        s = p / (2 * Math.PI) * Math.asin(c / a);
      }
      return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    };

    var elasticInOut = function elasticInOut(t, b, c, d) {
      var a = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
      var p = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
      var s;
      if (t === 0) {
        return b;
      }
      if ((t /= d / 2) === 2) {
        return b + c;
      }
      if (!p) {
        p = d * (0.3 * 1.5);
      }
      if (!a || a < Math.abs(c)) {
        a = c;
        s = p / 4;
      } else {
        s = p / (2 * Math.PI) * Math.asin(c / a);
      }
      if (t < 1) {
        return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
      }
      return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * 0.5 + c + b;
    };

    var elasticOut = function elasticOut(t, b, c, d) {
      var a = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
      var p = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
      var s;
      if (t === 0) {
        return b;
      }
      if ((t /= d) === 1) {
        return b + c;
      }
      if (!p) {
        p = d * 0.3;
      }
      if (!a || a < Math.abs(c)) {
        a = c;
        s = p / 4;
      } else {
        s = p / (2 * Math.PI) * Math.asin(c / a);
      }
      return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    };

    var expoIn = function expoIn(t, b, c, d) {
      return t === 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    };

    var expoInOut = function expoInOut(t, b, c, d) {
      if (t === 0) {
        return b;
      }
      if (t === d) {
        return b + c;
      }
      if ((t /= d / 2) < 1) {
        return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      }
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    };

    var expoOut = function expoOut(t, b, c, d) {
      return t === d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    };

    var linear = function linear(t, b, c, d) {
      return c * t / d + b;
    };

    var quarticIn = function quarticIn(t, b, c, d) {
      return c * (t /= d) * t * t * t + b;
    };

    var quarticInOut = function quarticInOut(t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return c / 2 * t * t * t * t + b;
      }
      return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    };

    var quarticOut = function quarticOut(t, b, c, d) {
      return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    };

    var quinticIn = function quinticIn(t, b, c, d) {
      return c * (t /= d) * t * t * t * t + b;
    };

    var quinticInOut = function quinticInOut(t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return c / 2 * t * t * t * t * t + b;
      }
      return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    };

    var quinticOut = function quinticOut(t, b, c, d) {
      return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    };

    var regularIn = function regularIn(t, b, c, d) {
      return c * (t /= d) * t + b;
    };

    var regularInOut = function regularInOut(t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return c / 2 * t * t + b;
      }
      return -c / 2 * (--t * (t - 2) - 1) + b;
    };

    var regularOut = function regularOut(t, b, c, d) {
      return -c * (t /= d) * (t - 2) + b;
    };

    var sineIn = function sineIn(t, b, c, d) {
      return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    };

    var sineInOut = function sineInOut(t, b, c, d) {
      return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    };

    var sineOut = function sineOut(t, b, c, d) {
      return c * Math.sin(t / d * (Math.PI / 2)) + b;
    };

    /**
     * The {@link system.transitions} package use the {@link core.easings} library who contains all the easing functions to create the specific <b>tweening</b> effects.
     * <p>These easings functions provide different flavors of math-based motion under a consistent API.</p>
     *
     * |  easing   |                         description                         |  in  | out  | inout  |
     * |:--------: |:----------------------------------------------------------: |:---: |:---: |:-----: |
     * |  linear   | simple linear tweening : no easing, no acceleration         |  -   |  -   |   -    |
     * |   back    | back easing : overshooting cubic easing: (s+1)*t^3 - s*t^2  | yes  | yes  |  yes   |
     * |  bounce   | bounce easing : exponentially decaying parabolic bounce     | yes  | yes  |  yes   |
     * | circular  | circular easing : sqrt(1-t^2)                               | yes  | yes  |  yes   |
     * |   cubic   | cubic easing : t^3                                          | yes  | yes  |  yes   |
     * |  elastic  | elastic easing : exponentially decaying sine wave           | yes  | yes  |  yes   |
     * |   expo    | exponential easing : 2^t                                    | yes  | yes  |  yes   |
     * |   quad    | quadratic easing : t^2                                      | yes  | yes  | yes    |
     * |  quartic  | quartic easing : t^4                                        | yes  | yes  |  yes   |
     * |  quintic  | quintic easing : t^5                                        | yes  | yes  |  yes   |
     * |  regular  | regular easing                                              | yes  | yes  |  yes   |
     * |   sine    | sinusoidal easing : sin(t)                                  | yes  | yes  |  yes   |
     * @summary The {@link core.easings} library contains all the easing functions to create the specific tweening effects.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @namespace core.easings
     * @memberof core
     * @since 1.0.0
     */
    var easings$1 = {
      backIn: backIn,
      backInOut: backInOut,
      backOut: backOut,
      bounceIn: bounceIn,
      bounceInOut: bounceInOut,
      bounceOut: bounceOut,
      circularIn: circularIn,
      circularInOut: circularInOut,
      circularOut: circularOut,
      cubicIn: cubicIn,
      cubicInOut: cubicInOut,
      cubicOut: cubicOut,
      elasticIn: elasticIn,
      elasticInOut: elasticInOut,
      elasticOut: elasticOut,
      expoIn: expoIn,
      expoInOut: expoInOut,
      expoOut: expoOut,
      linear: linear,
      quarticIn: quarticIn,
      quarticInOut: quarticInOut,
      quarticOut: quarticOut,
      quinticIn: quinticIn,
      quinticInOut: quinticInOut,
      quinticOut: quinticOut,
      regularIn: regularIn,
      regularInOut: regularInOut,
      regularOut: regularOut,
      sineIn: sineIn,
      sineInOut: sineInOut,
      sineOut: sineOut
    };

    var aop = function aop(func) {
      var begin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var end = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var scope = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      return function () {
        try {
          if (begin instanceof Function) {
            begin();
          }
          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }
          return func.apply(scope, args);
        } finally {
          if (end instanceof Function) {
            end();
          }
        }
      };
    };

    var callOrReturn = function callOrReturn(func) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      return isFunction$1(func) ? func.apply(void 0, args) : func;
    };

    var compose = function compose() {
      for (var _len = arguments.length, functions = new Array(_len), _key = 0; _key < _len; _key++) {
        functions[_key] = arguments[_key];
      }
      if (functions.length === 0) {
        return function (arg) {
          return arg;
        };
      }
      functions = functions.filter(function (func) {
        return func instanceof Function;
      });
      if (functions.length === 1) {
        return functions[0];
      }
      return functions.reduce(function (a, b) {
        return function () {
          return a(b.apply(void 0, arguments));
        };
      });
    };

    var debounce = function debounce(func, wait, options) {
      var lastArgs, lastCallTime, lastThis, maxWait, result, timerId;
      var lastInvokeTime = 0;
      var leading = false;
      var maxing = false;
      var trailing = true;
      var framed = !wait && wait !== 0 && typeof requestAnimationFrame === 'function';
      if (typeof func !== 'function') {
        throw new TypeError('Expected a function');
      }
      wait = +wait || 0;
      if (isObject$1(options)) {
        leading = options.leading;
        maxing = options.hasOwnProperty('maxWait');
        maxWait = maxing ? Math.max(+options.maxWait || 0, wait) : maxWait;
        trailing = options.hasOwnProperty('trailing') ? options.trailing : trailing;
      }
      var invokeFunc = function invokeFunc(time) {
        var args = lastArgs;
        var thisArg = lastThis;
        lastArgs = lastThis = undefined;
        lastInvokeTime = time;
        result = func.apply(thisArg, args);
        return result;
      };
      var startTimer = function startTimer(pendingFunc, wait) {
        if (framed) {
          return requestAnimationFrame(pendingFunc);
        }
        return setTimeout(pendingFunc, wait);
      };
      var stopTimer = function stopTimer(id) {
        if (framed) {
          return requestAnimationFrame(id);
        }
        clearTimeout(id);
      };
      var leadingEdge = function leadingEdge(time) {
        lastInvokeTime = time;
        timerId = startTimer(_timerExpired, wait);
        return leading ? invokeFunc(time) : result;
      };
      var remainingWait = function remainingWait(time) {
        var timeSinceLastCall = time - lastCallTime;
        var timeSinceLastInvoke = time - lastInvokeTime;
        var timeWaiting = wait - timeSinceLastCall;
        return maxing ? Math.min(timeWaiting, maxWait - timeSinceLastInvoke) : timeWaiting;
      };
      var shouldInvoke = function shouldInvoke(time) {
        var timeSinceLastCall = time - lastCallTime;
        var timeSinceLastInvoke = time - lastInvokeTime;
        return lastCallTime === undefined || timeSinceLastCall >= wait || timeSinceLastCall < 0 || maxing && timeSinceLastInvoke >= maxWait;
      };
      var _timerExpired = function timerExpired() {
        var time = Date.now();
        if (shouldInvoke(time)) {
          return trailingEdge(time);
        }
        timerId = startTimer(_timerExpired, remainingWait(time));
      };
      var trailingEdge = function trailingEdge(time) {
        timerId = undefined;
        if (trailing && lastArgs) {
          return invokeFunc(time);
        }
        lastArgs = lastThis = undefined;
        return result;
      };
      var cancel = function cancel() {
        if (timerId !== undefined) {
          stopTimer(timerId);
        }
        lastInvokeTime = 0;
        lastArgs = lastCallTime = lastThis = timerId = undefined;
      };
      var flush = function flush() {
        return timerId === undefined ? result : trailingEdge(Date.now());
      };
      var pending = function pending() {
        return timerId !== undefined;
      };
      function debounced() {
        var time = Date.now();
        var isInvoking = shouldInvoke(time);
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }
        lastArgs = args;
        lastThis = this;
        lastCallTime = time;
        if (isInvoking) {
          if (timerId === undefined) {
            return leadingEdge(lastCallTime);
          }
          if (maxing) {
            timerId = startTimer(_timerExpired, wait);
            return invokeFunc(lastCallTime);
          }
        }
        if (timerId === undefined) {
          timerId = startTimer(_timerExpired, wait);
        }
        return result;
      }
      debounced.cancel = cancel;
      debounced.flush = flush;
      debounced.pending = pending;
      return debounced;
    };

    var empty$1 = function empty() {};

    var maybeCall = function maybeCall(func) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      return isFunction$1(func) && func.apply(void 0, args);
    };

    var _memoize = function memoize(func) {
      var resolver = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (typeof func !== 'function' || resolver != null && typeof resolver !== 'function') {
        throw new TypeError('memoize, expected a function');
      }
      var _memoized = function memoized() {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }
        var key = resolver ? resolver.apply(null, args) : args[0];
        var cache = _memoized.cache;
        if (cache.has(key)) {
          return cache.get(key);
        }
        var result = func.apply(null, args);
        _memoized.cache = cache.set(key, result) || cache;
        return result;
      };
      var Constructor = _memoize.Cache || Map;
      _memoized.cache = new Constructor();
      return _memoized;
    };
    _memoize.Cache = Map;

    var memoizeBounded = function memoizeBounded(func) {
      var result = _memoize(func, function (key) {
        var cache = result.cache;
        if (cache.size === MAX_MEMOIZE_SIZE) {
          cache.clear();
        }
        return key;
      });
      return result;
    };
    var MAX_MEMOIZE_SIZE = 500;

    var negate = function negate(func) {
      if (typeof func !== 'function') {
        throw new TypeError('negate failed, expected a function');
      }
      return function () {
        return !func.apply(void 0, arguments);
      };
    };

    var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/;
    var reIsPlainProp = /^\w*$/;
    var isKey = function isKey(value, object) {
      if (Array.isArray(value)) {
        return false;
      }
      var type = _typeof(value);
      if (type === 'number' || type === 'boolean' || value === null || isSymbol$2(value)) {
        return true;
      }
      return reIsPlainProp.test(value) || !reIsDeepProp.test(value) || object !== null && value in Object(object);
    };

    var charCodeOfDot = '.'.charCodeAt(0);
    var escapeChar = /\\(\\)?/g;
    var propName = RegExp('[^.[\\]]+' + '|'
    + '\\[(?:'
    + '([^"\'][^[]*)' + '|'
    + '(["\'])((?:(?!\\2)[^\\\\]|\\\\.)*?)\\2'
    + ')\\]' + '|'
    + '(?=(?:\\.|\\[\\])(?:\\.|\\[\\]|$))', 'g');
    var stringToPath = memoizeBounded(function (source) {
      var result = [];
      if (source.charCodeAt(0) === charCodeOfDot) {
        result.push('');
      }
      source.replace(propName, function (match, expression, quote, subString) {
        var key = match;
        if (quote) {
          key = subString.replace(escapeChar, '$1');
        } else if (expression) {
          key = expression.trim();
        }
        result.push(key);
      });
      return result;
    });

    var castPath = function castPath(value, object) {
      if (value instanceof Array) {
        return value;
      }
      return isKey(value, object) ? [value] : stringToPath(value);
    };

    var INFINITY$1 = 1 / 0;
    var toKey = function toKey(object) {
      if (typeof object === 'string' || isSymbol$2(object)) {
        return object;
      }
      var result = "".concat(object);
      return result === '0' && 1 / object === -INFINITY$1 ? '-0' : result;
    };

    var get = function get(object, path) {
      var defaultValue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var result = object === null ? undefined : baseGet(object, path);
      return result === undefined ? defaultValue : result;
    };
    var baseGet = function baseGet(object, path) {
      path = castPath(path, object);
      var index = 0;
      var length = path.length;
      while (object !== null && object !== undefined && index < length) {
        object = object[toKey(path[index++])];
      }
      return index && index === length ? object : undefined;
    };

    var has = function has(object, key) {
      return object !== null && object !== undefined && Object.prototype.hasOwnProperty.call(object, key);
    };

    var factory = function factory() {
      var validator = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : has;
      return function (object, path) {
        path = castPath(path, object);
        var key;
        var _path = path,
          length = _path.length;
        var result = false;
        var index = -1;
        while (++index < length) {
          key = toKey(path[index]);
          result = validator(object, key);
          if (!result) {
            break;
          }
          object = object[key];
        }
        if (result || ++index !== length) {
          return result;
        }
        length = object === null || object === undefined ? 0 : object.length;
        return !!length && isLength$1(length) && isIndex$1(key, length) && (Array.isArray(object) || isArguments$1(object));
      };
    };
    var hasPath = factory();

    function sortBy(definition) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var x = typeof this === "function" && !this['firstBy'] ? this : false;
      var y = makeCompare(definition, options);
      var func = x ? function (a, b) {
        return x(a, b) || y(a, b);
      } : y;
      Object.defineProperty(func, 'thenBy', {
        enumerable: true,
        value: sortBy
      });
      return func;
    }
    var identity = function identity(v) {
      return v;
    };
    var ignoreCase = function ignoreCase(value) {
      return isString$1(value) ? value.toLowerCase() : value;
    };
    var defaultCompare = function defaultCompare(v1, v2) {
      if (v1 > v2) {
        return v1 < v2 ? -1 : 1;
      } else {
        return v1 < v2 ? -1 : 0;
      }
    };
    var makeCompare = function makeCompare(definition, options) {
      options = _typeof(options) === "object" ? options : {
        direction: options
      };
      var _ref = options || {},
        direction = _ref.direction,
        compare = _ref.compare,
        ic = _ref.ignoreCase;
      if (isString$1(definition)) {
        var prop = definition;
        definition = function definition(object) {
          return hasPath(object, prop) ? get(object, prop) : '';
        };
      }
      compare = compare instanceof Function ? compare : defaultCompare;
      if (definition.length === 1) {
        var uf = definition;
        var pr = ic ? ignoreCase : identity;
        definition = function definition(v1, v2) {
          return compare(pr(uf(v1)), pr(uf(v2)));
        };
      }
      return direction === -1 || direction === 'desc' ? function (v1, v2) {
        return -definition(v1, v2);
      } : definition;
    };

    var throttle = function throttle(func, wait) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      if (!isFunction$1(func)) {
        throw new TypeError('Expected a function');
      }
      var _ref = options || {},
        _ref$leading = _ref.leading,
        leading = _ref$leading === void 0 ? true : _ref$leading,
        _ref$trailing = _ref.trailing,
        trailing = _ref$trailing === void 0 ? true : _ref$trailing;
      return debounce(func, wait, {
        leading: leading,
        maxWait: wait,
        trailing: trailing
      });
    };

    /**
     * The {@link core.functors} package is a modular <b>JavaScript</b> library that provides extra <code>Function</code> methods.
     * @summary The {@link core.functors} package is a modular <b>JavaScript</b> library that provides extra <code>Function</code> methods.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.functors
     * @memberof core
     */
    var functors$1 = {
      aop: aop,
      callOrReturn: callOrReturn,
      compose: compose,
      debounce: debounce,
      empty: empty$1,
      maybeCall: maybeCall,
      memoize: _memoize,
      memoizeBounded: memoizeBounded,
      negate: negate,
      sortBy: sortBy,
      throttle: throttle
    };

    var gcd = function gcd(i1, i2) {
      if (i2 === 0) {
        return i1;
      } else if (i1 === i2) {
        return i1;
      } else {
        var t;
        while (i2 !== 0) {
          t = i2;
          i2 = i1 % i2;
          i1 = t;
        }
        return i1;
      }
    };

    var floor = function floor(n) {
      var floatCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (isNaN(n)) {
        return NaN;
      }
      var r = 1;
      var i = -1;
      while (++i < floatCount) {
        r *= 10;
      }
      return Math.floor(n * r) / r;
    };

    var resizeHeightAndKeepAspectRatio = function resizeHeightAndKeepAspectRatio(height, originalWidth, originalHeight) {
      var _gcd = gcd(originalWidth, originalHeight);
      var _aspW = floor(originalWidth / _gcd, 0);
      var _aspH = floor(originalHeight / _gcd, 0);
      if (isNaN(_aspW)) {
        _aspW = 0;
      }
      if (isNaN(_aspH)) {
        _aspH = 0;
      }
      return {
        width: floor(height * _aspW / _aspH, 0),
        height: floor(height, 0)
      };
    };

    var resizeWidthAndKeepAspectRatio = function resizeWidthAndKeepAspectRatio(width, originalWidth, originalHeight) {
      var _gcd = gcd(originalWidth, originalHeight);
      var _aspW = floor(originalWidth / _gcd, 0);
      var _aspH = floor(originalHeight / _gcd, 0);
      if (isNaN(_aspW)) {
        _aspW = 0;
      }
      if (isNaN(_aspH)) {
        _aspH = 0;
      }
      return {
        width: floor(width, 0),
        height: floor(width * _aspH / _aspW, 0)
      };
    };

    /**
     * The {@link core.graphics} package is a modular <b>JavaScript</b> library that provides extra <code>graphics</code> methods and implementations.
     * @summary The {@link core.graphics} package is a modular <b>JavaScript</b> library that provides extra <code>graphics</code> methods and implementations.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.graphics
     * @memberof core
     */
    var numbers$2 = {
      resizeHeightAndKeepAspectRatio: resizeHeightAndKeepAspectRatio,
      resizeWidthAndKeepAspectRatio: resizeWidthAndKeepAspectRatio
    };

    var RAD2DEG = 180 / Math.PI;

    var acosD = function acosD(ratio) {
      return Math.acos(ratio) * RAD2DEG;
    };

    var acosHm = function acosHm(x) {
      return Math.log(x - Math.sqrt(x * x - 1));
    };

    var acosHp = function acosHp(x) {
      return Math.log(x + Math.sqrt(x * x - 1));
    };

    var angleOfLine = function angleOfLine(x1, y1, x2, y2) {
      return Math.atan2(y2 - y1, x2 - x1) * RAD2DEG;
    };

    var asinD = function asinD(ratio) {
      return Math.asin(ratio) * RAD2DEG;
    };

    var asinH = function asinH(x) {
      return Math.log(x + Math.sqrt(x * x + 1));
    };

    var atan2D = function atan2D(y, x) {
      return Math.atan2(y, x) * RAD2DEG;
    };

    var atanD = function atanD(angle) {
      return Math.atan(angle) * RAD2DEG;
    };

    var atanH = function atanH(x) {
      return Math.log((1 + x) / (1 - x)) / 2;
    };

    var DEG2RAD = Math.PI / 180;

    var bearing = function bearing(latitude1, longitude1, latitude2, longitude2) {
      latitude1 = latitude1 * DEG2RAD;
      latitude2 = latitude2 * DEG2RAD;
      var dLng = (longitude2 - longitude1) * DEG2RAD;
      var y = Math.sin(dLng) * Math.cos(latitude2);
      var x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(dLng);
      return (Math.atan2(y, x) * RAD2DEG + 360) % 360;
    };

    var clamp = function clamp(value, min, max) {
      if (isNaN(value)) {
        return NaN;
      }
      if (isNaN(min)) {
        min = value;
      }
      if (isNaN(max)) {
        max = value;
      }
      return Math.max(Math.min(value, max), min);
    };

    var berp = function berp(start, end, amount) {
      if (start === end) {
        return start;
      }
      amount = clamp(amount, 0, 1);
      amount = (Math.sin(amount * Math.PI * (0.2 + 2.5 * amount * amount * amount)) * Math.pow(1 - amount, 2.2) + amount) * (1 + 1.2 * (1 - amount));
      return start + (end - start) * amount;
    };

    var bounce = function bounce(amount) {
      return Math.abs(Math.sin(6.28 * (amount + 1) * (amount + 1)) * (1 - amount));
    };

    var cartesianToPolar = function cartesianToPolar(vector) {
      var degrees = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return {
        angle: Math.atan2(vector.y, vector.x) * (degrees ? RAD2DEG : 1),
        radius: Math.sqrt(vector.x * vector.x + vector.y * vector.y)
      };
    };

    var ceil = function ceil(n) {
      var floatCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (isNaN(n)) {
        return NaN;
      }
      var r = 1;
      var i = -1;
      while (++i < floatCount) {
        r *= 10;
      }
      return Math.ceil(n * r) / r;
    };

    var clerp = function clerp(start, end, amount) {
      var max = 360;
      var half = 180;
      var diff = end - start;
      if (diff < -half) {
        return start + (max - start + end) * amount;
      } else if (diff > half) {
        return start - (max - end + start) * amount;
      } else {
        return start + (end - start) * amount;
      }
    };

    var cosD = function cosD(angle) {
      return Math.cos(angle * DEG2RAD);
    };

    var coserp = function coserp(start, end, amount) {
      if (start === end) {
        return start;
      }
      amount = 1 - Math.cos(amount * Math.PI * 0.5);
      return (1 - amount) * start + amount * end;
    };

    var cosH = function cosH(x) {
      return (Math.exp(x) + Math.exp(-x)) / 2;
    };

    var degreesToRadians = function degreesToRadians(angle) {
      return angle * DEG2RAD;
    };

    var distance = function distance(x1, y1, x2, y2) {
      var dx = x2 - x1;
      var dy = y2 - y1;
      return Math.sqrt(dx * dx + dy * dy);
    };

    var distanceByObject = function distanceByObject(p1, p2) {
      var dx = p2.x - p1.x;
      var dy = p2.y - p1.y;
      return Math.sqrt(dx * dx + dy * dy);
    };

    var EARTH_RADIUS_IN_METERS = 6371000;

    var EPSILON = 0.000000001;

    var factorial = function factorial(value) {
      if (value === 0) {
        return 1;
      }
      var result = value;
      while (--value) {
        result *= value;
      }
      return result;
    };

    var fibonacci = function fibonacci(value) {
      var i = 1;
      var j = 0;
      for (var k = 1; k <= value; k++) {
        var _ref = [j, i + j];
        i = _ref[0];
        j = _ref[1];
      }
      return j;
    };

    var finalBearing = function finalBearing(latitude1, longitude1, latitude2, longitude2) {
      latitude1 = latitude1 * DEG2RAD;
      latitude2 = latitude2 * DEG2RAD;
      var dLng = (longitude2 - longitude1) * DEG2RAD;
      var y = Math.sin(dLng) * Math.cos(latitude2);
      var x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(dLng);
      return (Math.atan2(y, x) * RAD2DEG + 180) % 360;
    };

    var fixAngle = function fixAngle(angle) {
      if (isNaN(angle)) {
        angle = 0;
      }
      angle %= 360;
      return angle < 0 ? angle + 360 : angle;
    };

    var floorMod = function floorMod(x, n) {
      return x - n * Math.floor(x / n);
    };

    var haversine = function haversine(latitude1, longitude1, latitude2, longitude2) {
      var radius = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : EARTH_RADIUS_IN_METERS;
      if (isNaN(radius)) {
        radius = EARTH_RADIUS_IN_METERS;
      }
      var dLat = (latitude2 - latitude1) * DEG2RAD;
      var dLng = (longitude2 - longitude1) * DEG2RAD;
      var a = Math.sin(dLat * 0.5) * Math.sin(dLat * 0.5) + Math.cos(latitude1 * DEG2RAD) * Math.cos(latitude2 * DEG2RAD) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
      var c = Number((2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * radius).toFixed(3));
      return c === c ? c : 0;
    };

    var hermite = function hermite(start, end, amount) {
      if (start === end) {
        return start;
      }
      amount = amount * amount * (3 - 2 * amount);
      return (1 - amount) * start + amount * end;
    };

    var hypothenuse = function hypothenuse(x, y) {
      return Math.sqrt(x * x + y * y);
    };

    var interpolate = function interpolate(value, min, max) {
      return min + (max - min) * value;
    };

    var isEven$1 = function isEven(value) {
      return value % 2 === 0;
    };

    var isOdd$1 = function isOdd(value) {
      return value % 2 !== 0;
    };

    var LAMBDA = 0.57721566490143;

    var lerp = function lerp(start, end, amount) {
      if (start === end) {
        return start;
      }
      return (1 - amount) * start + amount * end;
    };

    var log10 = function log10(value) {
      return Math.log(value) / Math.LN10;
    };

    var logN = function logN(value, base) {
      return Math.log(value) / Math.log(base);
    };

    var normalize$1 = function normalize(value, minimum, maximum) {
      return (value - minimum) / (maximum - minimum);
    };

    var map$1 = function map(value, min1, max1, min2, max2) {
      return interpolate(normalize$1(value, min1, max1), min2, max2);
    };

    var midPoint = function midPoint(latitude1, longitude1, latitude2, longitude2) {
      var dLng = (longitude2 - longitude1) * DEG2RAD;
      latitude1 = latitude1 * DEG2RAD;
      longitude1 = longitude1 * DEG2RAD;
      latitude2 = latitude2 * DEG2RAD;
      var bx = Math.cos(latitude2) * Math.cos(dLng);
      var by = Math.cos(latitude2) * Math.sin(dLng);
      var point = {
        x: Math.atan2(Math.sin(latitude1) + Math.sin(latitude2), Math.sqrt((Math.cos(latitude1) + bx) * (Math.cos(latitude1) + bx) + by * by)) * RAD2DEG,
        y: (longitude1 + Math.atan2(by, Math.cos(latitude1) + bx)) * RAD2DEG
      };
      return point;
    };

    var MILE_TO_METER = 1609;

    var modulo = function modulo(a, b) {
      var r = a % b;
      return r * b < 0 ? r + b : r;
    };

    var nearlyEquals = function nearlyEquals(value1, value2) {
      var tolerance = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.000001;
      if (isNaN(tolerance)) {
        tolerance = 0.000001;
      } else if (tolerance < 0) {
        tolerance = 0;
      }
      return Math.abs(value1 - value2) <= tolerance;
    };

    var percentage = function percentage(value, maximum) {
      var p = value / maximum * 100;
      return isNaN(p) || !isFinite(p) ? NaN : p;
    };

    var PHI = 1.61803398874989;

    var PI2 = Math.PI * 2;

    var polarToCartesian = function polarToCartesian(vector, degrees) {
      var angle = vector.angle;
      var radius = vector.radius;
      if (degrees) {
        angle *= DEG2RAD;
      }
      return {
        x: radius * Math.cos(angle),
        y: radius * Math.sin(angle)
      };
    };

    var replaceNaN = function replaceNaN(value) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      return isNaN(value) ? defaultValue : value;
    };

    var round = function round(n) {
      var floatCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      if (isNaN(n)) {
        return NaN;
      }
      var r = 1;
      var i = -1;
      while (++i < floatCount) {
        r *= 10;
      }
      return Math.round(n * r) / r;
    };

    var sharpen = function sharpen(value) {
      var _split$;
      var to = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0.5;
      var rounded = Math.round(value / to) * to;
      var precision = ((_split$ = "".concat(to).split('.')[1]) === null || _split$ === void 0 ? void 0 : _split$.length) || 0;
      return Number(rounded.toFixed(precision));
    };

    var sign = function sign(n) {
      if (isNaN(n)) {
        throw new TypeError("sign failed, the passed-in value not must be NaN.");
      }
      return n < 0 ? -1 : 1;
    };

    var sinD = function sinD(angle) {
      return Math.sin(angle * DEG2RAD);
    };

    var sinerp = function sinerp(start, end, amount) {
      if (start === end) {
        return start;
      }
      amount = Math.sin(amount * Math.PI * 0.5);
      return (1 - amount) * start + amount * end;
    };

    var sinH = function sinH(x) {
      return (Math.exp(x) - Math.exp(-x)) * 0.5;
    };

    var tanD = function tanD(angle) {
      return Math.tan(angle * DEG2RAD);
    };

    var tanH = function tanH(x) {
      return sinH(x) / cosH(x);
    };

    function vincenty(latitude1, longitude1, latitude2, longitude2)
    {
      var a = 6378137;
      var b = 6356752.3142;
      var f = 1 / 298.257223563;
      var L = (longitude2 - longitude1) * DEG2RAD;
      var U1 = Math.atan((1 - f) * Math.tan(latitude1 * DEG2RAD));
      var U2 = Math.atan((1 - f) * Math.tan(latitude2 * DEG2RAD));
      var sinU1 = Math.sin(U1),
        cosU1 = Math.cos(U1);
      var sinU2 = Math.sin(U2),
        cosU2 = Math.cos(U2);
      var lambda = L;
      var lambdaP = 2 * Math.PI;
      var iterLimit = 20;
      var cosLambda;
      var sinLambda;
      var cosSigma;
      var sinSigma;
      var sigma;
      var sinAlpha;
      var cosSqAlpha;
      var cos2SigmaM;
      var C;
      do {
        sinLambda = Math.sin(lambda);
        cosLambda = Math.cos(lambda);
        sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
        if (sinSigma === 0) {
          return 0;
        }
        cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
        sigma = Math.atan2(sinSigma, cosSigma);
        sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
        cosSqAlpha = 1 - sinAlpha * sinAlpha;
        cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
        if (isNaN(cos2SigmaM)) {
          cos2SigmaM = 0;
        }
        C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
        lambdaP = lambda;
        lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
      } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);
      if (iterLimit === 0) {
        return NaN;
      }
      var uSq = cosSqAlpha * (a * a - b * b) / (b * b);
      var A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
      var B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
      var deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
      var s = b * A * (sigma - deltaSigma);
      s = Number(s.toFixed(3));
      return s;
    }

    function wrap(angle) {
      var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 360;
      var range = max - min;
      if (range <= 0) {
        return 0;
      }
      var result = (angle - min) % range;
      if (result < 0) {
        result += range;
      }
      return result + min;
    }

    /**
     * The {@link core.maths} package is a modular <b>JavaScript</b> library that provides extra <code>mathematics</code> methods and implementations.
     * @summary The {@link core.maths} package is a modular <b>JavaScript</b> library that provides extra <code>mathematics</code> methods and implementations.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.maths
     * @memberof core
     */
    var maths$1 = {
      acosD: acosD,
      acosHm: acosHm,
      acosHp: acosHp,
      angleOfLine: angleOfLine,
      asinD: asinD,
      asinH: asinH,
      atan2D: atan2D,
      atanD: atanD,
      atanH: atanH,
      bearing: bearing,
      berp: berp,
      bounce: bounce,
      cartesianToPolar: cartesianToPolar,
      ceil: ceil,
      clamp: clamp,
      clerp: clerp,
      cosD: cosD,
      coserp: coserp,
      cosH: cosH,
      DEG2RAD: DEG2RAD,
      degreesToRadians: degreesToRadians,
      distance: distance,
      distanceByObject: distanceByObject,
      EARTH_RADIUS_IN_METERS: EARTH_RADIUS_IN_METERS,
      EPSILON: EPSILON,
      factorial: factorial,
      fibonacci: fibonacci,
      finalBearing: finalBearing,
      fixAngle: fixAngle,
      floor: floor,
      floorMod: floorMod,
      gcd: gcd,
      haversine: haversine,
      hermite: hermite,
      hypothenuse: hypothenuse,
      interpolate: interpolate,
      isEven: isEven$1,
      isOdd: isOdd$1,
      LAMBDA: LAMBDA,
      lerp: lerp,
      littleEndian: littleEndian,
      log10: log10,
      logN: logN,
      map: map$1,
      midPoint: midPoint,
      MILE_TO_METER: MILE_TO_METER,
      modulo: modulo,
      nearlyEquals: nearlyEquals,
      normalize: normalize$1,
      percentage: percentage,
      PHI: PHI,
      PI2: PI2,
      polarToCartesian: polarToCartesian,
      RAD2DEG: RAD2DEG,
      replaceNaN: replaceNaN,
      round: round,
      sharpen: sharpen,
      sign: sign,
      sinD: sinD,
      sinerp: sinerp,
      sinH: sinH,
      tanD: tanD,
      tanH: tanH,
      vincenty: vincenty,
      wrap: wrap
    };

    var sum = function sum(array) {
      if (!Array.isArray(array)) {
        throw new TypeError('sum failed, expected an array');
      }
      var len = array.length;
      var num = 0;
      while (len--) {
        num += Number(array[len]);
      }
      return num;
    };

    var avg = function avg(array, index, range) {
      return sum(array.slice(index - range, index)) / range;
    };

    var clip = function clip(value, min, max) {
      if (value <= min) {
        return min;
      } else if (value >= max) {
        return max;
      }
      return value;
    };

    var isEven = function isEven(value) {
      if (!isNumber$1(value)) {
        throw new TypeError('isEven failed, expected a number');
      }
      if (!Number.isInteger(value)) {
        throw new Error('isEven failed, expected an integer');
      }
      if (!Number.isSafeInteger(value)) {
        throw new Error('isEven failed, value exceeds maximum safe integer');
      }
      return value % 2 === 0;
    };

    var isOdd = function isOdd(value) {
      if (!isNumber$1(value)) {
        throw new TypeError('isOdd failed, expected a number');
      }
      if (!Number.isInteger(value)) {
        throw new Error('isOdd failed, expected an integer');
      }
      if (!Number.isSafeInteger(value)) {
        throw new Error('isOdd failed, value exceeds maximum safe integer');
      }
      return value % 2 === 1;
    };

    var nullify = function nullify(value) {
      return isNaN(value) ? null : value;
    };

    var nullifyZeroAndNaN = function nullifyZeroAndNaN(value) {
      return value === 0 ? undefined : nullify(value);
    };

    var padTo2Digits = function padTo2Digits(number) {
      return "".concat(number).padStart(2, 0);
    };

    var padTo3Digits = function padTo3Digits(number) {
      return "".concat(number).padStart(3, '0');
    };

    var toFixed$1 = function toFixed(n) {
      return n.toFixed(2);
    };
    var sma = function sma(array, range) {
      var format = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      if (!Array.isArray(array)) {
        throw TypeError('sma failed, expected first argument to be an array');
      }
      var fn = typeof format === 'function' ? format : toFixed$1;
      var result = [];
      var num = range !== null && range !== void 0 ? range : array.length;
      var len = array.length + 1;
      var index = num - 1;
      while (++index < len) {
        result.push(fn(avg(array, index, num)));
      }
      return result;
    };

    var toFixed = function toFixed(number) {
      var digits = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      return number.toFixed(digits);
    };

    var toInt = function toInt(num) {
      return num - num % 1;
    };

    var toUint = function toUint(num) {
      num -= num % 1;
      return num < 0 ? -num : num;
    };

    var toIntegerOrInfinity = function toIntegerOrInfinity(value) {
      var number = +value;
      return number !== number || number === 0 ? 0 : Math.trunc(number);
    };

    /**
     * The {@link core.numbers} package is a modular <b>JavaScript</b> library that provides extra <code>Number</code> methods and implementations.
     * @summary The {@link core.numbers} package is a modular <b>JavaScript</b> library that provides extra <code>Number</code> methods and implementations.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.numbers
     * @memberof core
     */
    var numbers$1 = {
      avg: avg,
      clip: clip,
      isEven: isEven,
      isOdd: isOdd,
      leading: leading,
      nullify: nullify,
      nullifyZeroAndNaN: nullifyZeroAndNaN,
      padTo2Digits: padTo2Digits,
      padTo3Digits: padTo3Digits,
      sma: sma,
      sum: sum,
      toFinite: toFinite,
      toFixed: toFixed,
      toInt: toInt,
      toInteger: toInteger,
      toNumber: toNumber,
      toUint: toUint,
      toIntegerOrInfinity: toIntegerOrInfinity,
      toUnicodeNotation: toUnicodeNotation
    };

    var baseAt = function baseAt(object, paths) {
      var index = -1;
      var length = paths.length;
      var result = new Array(length);
      var skip = object === null || object === undefined;
      while (++index < length) {
        result[index] = skip ? undefined : get(object, paths[index]);
      }
      return result;
    };

    var spreadableSymbol = Symbol.isConcatSpreadable;
    var isFlattenable = function isFlattenable(value) {
      return Array.isArray(value) || isArguments$1(value) || !!(value && value[spreadableSymbol]);
    };

    var _baseFlatten = function baseFlatten(array, depth) {
      var _predicate, _result;
      var predicate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : isFlattenable;
      var isStrict = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      var result = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
      predicate = (_predicate = predicate) !== null && _predicate !== void 0 ? _predicate : isFlattenable;
      result = (_result = result) !== null && _result !== void 0 ? _result : [];
      if (array === null || array === undefined) {
        return result;
      }
      var _iterator = _createForOfIteratorHelper(array),
        _step;
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var value = _step.value;
          if (depth > 0 && predicate(value)) {
            if (depth > 1)
              {
                _baseFlatten(value, depth - 1, predicate, isStrict, result);
              } else {
              var _result2, _result3;
              (_result2 = result) === null || _result2 === void 0 ? void 0 : (_result3 = _result2).push.apply(_result3, _toConsumableArray(value));
            }
          } else if (!isStrict) {
            result[result.length] = value;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
      return result;
    };

    var at = function at(object) {
      for (var _len = arguments.length, paths = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        paths[_key - 1] = arguments[_key];
      }
      return baseAt(object, _baseFlatten(paths, 1));
    };

    var replaceUndefinedOrNull = function replaceUndefinedOrNull(key, value) {
      if (value === null || value === undefined) {
        return undefined;
      }
      return value;
    };
    function clean$1(object) {
      var replacer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (object) {
        return JSON.parse(JSON.stringify(object, replacer !== null && replacer !== void 0 ? replacer : replaceUndefinedOrNull));
      }
      return null;
    }

    var forEach = function forEach(object, callback) {
      var context = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var breaker = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      if (!object) {
        return;
      }
      if ('forEach' in object && object.forEach instanceof Function) {
        object.forEach(callback, context);
      } else {
        for (var key in object) {
          if (key in object) {
            if (breaker !== null) {
              if (callback.call(context, object[key], key, object) === breaker) {
                return;
              }
            } else {
              callback.call(context, object[key], key, object);
            }
          }
        }
      }
    };

    function fuse(src, srcPos, dest, destPos, length) {
      if (!src) {
        throw new ReferenceError("fuse failed, if either src is null.");
      }
      if (!dest) {
        dest = src;
      }
      if (destPos < 0) {
        destPos = dest.length;
      }
      while (length > 0) {
        dest[destPos] = src[srcPos];
        srcPos++;
        destPos++;
        length--;
      }
    }

    var hasIn = function hasIn(object, key) {
      return object !== null && object !== undefined && key in Object(object);
    };

    var hasPathIn = factory(hasIn);

    var isEmpty$1 = function isEmpty(object) {
      var checkAll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      return checkAll ? Object.getOwnPropertyNames(object).length === 0 : Object.keys(object).length === 0;
    };

    var map = function map(object, callback) {
      return Object.fromEntries(Object.entries(object).map(function (_ref, i) {
        var _ref2 = _slicedToArray(_ref, 2),
          k = _ref2[0],
          v = _ref2[1];
        return [k, callback(v, k, object, i)];
      }));
    };

    var members = function members(object) {
      var byValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return byValue ? Object.values(object) : Object.keys(object);
    };

    function merge(target, source) {
      var overwrite = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      if (overwrite === null || overwrite === undefined) {
        overwrite = true;
      }
      if (source === null || source === undefined) {
        source = {};
      }
      for (var prop in source) {
        if (!(prop in target) || overwrite) {
          target[prop] = source[prop];
        }
      }
      return target;
    }

    var assignMergeValue = function assignMergeValue(object, key, value) {
      if (value !== undefined && !eq$1(object[key], value) || value === undefined && !(key in object)) {
        baseAssignValue(object, key, value);
      }
    };

    var copyArray = function copyArray(source, array) {
      var _array;
      var length = source.length;
      array = (_array = array) !== null && _array !== void 0 ? _array : new Array(length);
      var index = -1;
      while (++index < length) {
        array[index] = source[index];
      }
      return array;
    };

    var baseMergeDeep = function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
      var objValue = object[key];
      var srcValue = source[key];
      var stacked = stack.get(srcValue);
      if (stacked) {
        assignMergeValue(object, key, stacked);
        return;
      }
      var newValue = customizer ? customizer(objValue, srcValue, "".concat(key), object, source, stack) : undefined;
      var isCommon = newValue === undefined;
      if (isCommon) {
        var isArr = Array.isArray(srcValue);
        var isBuff = !isArr && isBuffer$1(srcValue);
        var isTyped = !isArr && !isBuff && isTypedArray$1(srcValue);
        newValue = srcValue;
        if (isArr || isBuff || isTyped) {
          if (Array.isArray(objValue)) {
            newValue = objValue;
          } else if (isArrayLikeObject$1(objValue)) {
            newValue = copyArray(objValue);
          } else if (isBuff) {
            isCommon = false;
            newValue = cloneBuffer(srcValue, true);
          } else if (isTyped) {
            isCommon = false;
            newValue = cloneTypedArray(srcValue, true);
          } else {
            newValue = [];
          }
        } else if (isPlainObject$1(srcValue) || isArguments$1(srcValue)) {
          newValue = objValue;
          if (isArguments$1(objValue)) {
            newValue = toPlainObject$1(objValue);
          } else if (typeof objValue === 'function' || !isObject$1(objValue)) {
            newValue = initCloneObject(srcValue);
          }
        } else {
          isCommon = false;
        }
      }
      if (isCommon) {
        stack.set(srcValue, newValue);
        mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
        stack['delete'](srcValue);
      }
      assignMergeValue(object, key, newValue);
    };

    var _baseMerge = function baseMerge(object, source, srcIndex, customizer, stack) {
      if (object === source) {
        return;
      }
      baseFor(source, function (srcValue, key) {
        if (isObject$1(srcValue)) {
          var _stack;
          stack = (_stack = stack) !== null && _stack !== void 0 ? _stack : new Stack();
          baseMergeDeep(object, source, key, srcIndex, _baseMerge, customizer, stack);
        } else {
          var newValue = customizer ? customizer(object[key], srcValue, "".concat(key), object, source, stack) : undefined;
          if (newValue === undefined) {
            newValue = srcValue;
          }
          assignMergeValue(object, key, newValue);
        }
      }, keysIn);
    };

    var isIterateeCall = function isIterateeCall(value, index, object) {
      if (!isObject$1(object)) {
        return false;
      }
      var type = _typeof(index);
      if (type === 'number' ? isArrayLike$1(object) && isIndex$1(index, object.length) : type === 'string' && index in object) {
        return eq$1(object[index], value);
      }
      return false;
    };

    var createAssigner = function createAssigner(assigner) {
      return function (object) {
        var length = arguments.length <= 1 ? 0 : arguments.length - 1;
        var customizer = length > 1 ? length - 1 + 1 < 1 || arguments.length <= length - 1 + 1 ? undefined : arguments[length - 1 + 1] : undefined;
        var guard = length > 2 ? arguments.length <= 3 ? undefined : arguments[3] : undefined;
        customizer = assigner.length > 3 && typeof customizer === 'function' ? (length--, customizer) : undefined;
        if (guard && isIterateeCall(arguments.length <= 1 ? undefined : arguments[1], arguments.length <= 2 ? undefined : arguments[2], guard)) {
          customizer = length < 3 ? undefined : customizer;
          length = 1;
        }
        object = Object(object);
        var index = -1;
        while (++index < length) {
          var source = index + 1 < 1 || arguments.length <= index + 1 ? undefined : arguments[index + 1];
          if (source) {
            assigner(object, source, index, customizer);
          }
        }
        return object;
      };
    };

    var mergeDeep = createAssigner(function (object, source, srcIndex) {
      _baseMerge(object, source, srcIndex);
    });

    var omit = function omit(object, keys) {
      var result = _objectSpread2({}, object);
      var length = keys.length;
      for (var i = 0; i < length; i++) {
        delete result[keys[i]];
      }
      return result;
    };

    var pairs = function pairs(object) {
      var _keys = keys(object);
      var length = _keys.length;
      var pairs = Array(length);
      for (var i = 0; i < length; i++) {
        pairs[i] = [_keys[i], object[_keys[i]]];
      }
      return pairs;
    };

    var set = function set(object, path, value) {
      return object === null || object === undefined ? object : baseSet(object, path, value);
    };
    var baseSet = function baseSet(object, path, value) {
      var customizer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      if (!isObject$1(object)) {
        return object;
      }
      path = castPath(path, object);
      var length = path.length;
      var lastIndex = length - 1;
      var index = -1;
      var nested = object;
      while (nested !== null && ++index < length) {
        var key = toKey(path[index]);
        var newValue = value;
        if (index !== lastIndex) {
          var objValue = nested[key];
          newValue = customizer ? customizer(objValue, key, nested) : undefined;
          if (newValue === undefined) {
            if (isObject$1(objValue)) {
              newValue = objValue;
            } else {
              newValue = isIndex$1(path[index + 1]) ? [] : {};
            }
          }
        }
        assignValue(nested, key, newValue);
        nested = nested[key];
      }
      return object;
    };

    var basePickBy = function basePickBy(object, paths, predicate) {
      var result = {};
      var length = paths.length;
      var index = -1;
      while (++index < length) {
        var path = paths[index];
        var value = baseGet(object, path);
        if (predicate(value, path)) {
          baseSet(result, castPath(path, object), value);
        }
      }
      return result;
    };

    var basePick = function basePick(object, paths) {
      return basePickBy(object, paths, function (value, path) {
        return hasIn(object, path);
      });
    };

    var pick = function pick(object, paths) {
      return object === null || object === undefined ? {} : basePick(object, paths);
    };

    var pickBy = function pickBy(object, callback) {
      if (object === null || object === undefined) {
        return {};
      }
      var props = getAllKeysIn(object).map(function (prop) {
        return [prop];
      });
      return basePickBy(object, props, function (value, path) {
        return callback(value, path[0]);
      });
    };

    var removeProperty = function removeProperty(prop) {
      return function (_ref) {
        _ref[prop];
          var rest = _objectWithoutProperties(_ref, [prop].map(_toPropertyKey));
        return rest;
      };
    };

    var renameProperty = function renameProperty(prop, name) {
      return function (_ref) {
        var value = _ref[prop],
          rest = _objectWithoutProperties(_ref, [prop].map(_toPropertyKey));
        return _objectSpread2(_defineProperty({}, name, value), rest);
      };
    };

    var stub$1 = function stub() {
      return {};
    };

    var func = function func(object, path) {
      return path.length < 2 ? object : baseGet(object, path.slice(0, -1));
    };

    var unset = function unset(object, path) {
      return object === null ? true : baseUnset(object, path);
    };
    var baseUnset = function baseUnset(object, path) {
      path = castPath(path, object);
      object = func(object, path);
      return object === null || delete object[toKey(last(path))];
    };

    /**
     * The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
     * @summary The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.objects
     * @memberof core
     */
    var objects$1 = {
      at: at,
      castPath: castPath,
      clean: clean$1,
      forEach: forEach,
      fuse: fuse,
      get: get,
      getTag: getTag,
      has: has,
      hasIn: hasIn,
      hasPath: hasPath,
      hasPathIn: hasPathIn,
      isEmpty: isEmpty$1,
      isKey: isKey,
      keys: keys,
      keysIn: keysIn,
      map: map,
      members: members,
      merge: merge,
      mergeDeep: mergeDeep,
      omit: omit,
      pairs: pairs,
      pick: pick,
      pickBy: pickBy,
      removeProperty: removeProperty,
      renameProperty: renameProperty,
      set: set,
      stub: stub$1,
      toKey: toKey,
      unset: unset
    };

    var pattern$1 = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
    var generateUUID = function generateUUID() {
      var d = new Date().getTime();
      return pattern$1.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : r & 0x3 | 0x8).toString(16);
      });
    };

    /**
     * The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra methods to generates a random number.
     * @summary The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra methods to generates a random number.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.random
     * @memberof core
     */
    var random$1 = {
      generateUUID: generateUUID
    };

    function getDefinitionByName(name) {
      var domain = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (name instanceof String || typeof name === 'string') {
        name = name.split('.');
        if (name.length > 0) {
          try {
            var o = domain || global$1;
            name.forEach(function (element) {
              if (o.hasOwnProperty(element)) {
                o = o[element];
              } else {
                return undefined;
              }
            });
            return o;
          } catch (e) {
          }
        }
      }
      return undefined;
    }

    function invoke(c) {
      var a = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!(c instanceof Function)) {
        return null;
      }
      if (a === null || !(a instanceof Array) || a.length === 0) {
        return new c();
      }
      switch (a === null || a === void 0 ? void 0 : a.length) {
        case 0:
          return new c();
        case 1:
          return new c(a[0]);
        case 2:
          return new c(a[0], a[1]);
        case 3:
          return new c(a[0], a[1], a[2]);
        case 4:
          return new c(a[0], a[1], a[2], a[3]);
        case 5:
          return new c(a[0], a[1], a[2], a[3], a[4]);
        case 6:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5]);
        case 7:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6]);
        case 8:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
        case 9:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]);
        case 10:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
        case 11:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10]);
        case 12:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11]);
        case 13:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12]);
        case 14:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13]);
        case 15:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14]);
        case 16:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);
        case 17:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16]);
        case 18:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17]);
        case 19:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18]);
        case 20:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19]);
        case 21:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20]);
        case 22:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21]);
        case 23:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22]);
        case 24:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23]);
        case 25:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24]);
        case 26:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25]);
        case 27:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26]);
        case 28:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27]);
        case 29:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27], a[28]);
        case 30:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27], a[28], a[29]);
        case 31:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27], a[28], a[29], a[30]);
        case 32:
          return new c(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19], a[20], a[21], a[22], a[23], a[24], a[25], a[26], a[27], a[28], a[29], a[30], a[31]);
        default:
          return null;
      }
    }

    /**
     * The {@link core.reflect} package is a modular <b>JavaScript</b> library that provides extra methods to to obtain information about loaded objects or generate it.
     * @summary The {@link core.reflect} package is a modular <b>JavaScript</b> library that provides extra methods to to obtain information about loaded objects or generate it.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core.reflect
     * @memberof core
     */
    var reflect$1 = {
      getDefinitionByName: getDefinitionByName,
      invoke: invoke,
      isPrototype: isPrototype,
      toString: toString$1
    };

    function between(source, left, right) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      if (!(left instanceof String || typeof left === 'string') || left === "") {
        return source;
      }
      var start = source.indexOf(left);
      var end = source.indexOf(right, start + left.length);
      if (end < 0 || !(right instanceof String || typeof right === 'string') || right === "") {
        return source.substring(start + left.length);
      }
      return source.slice(start + left.length, end);
    }

    var nGram = function nGram(n) {
      if (typeof n !== 'number' || Number.isNaN(n) || n < 1 || n === Number.POSITIVE_INFINITY) {
        throw new Error('`' + n + '` is not a valid argument for `n-gram`');
      }
      return function (value) {
        var nGrams = [];
        if (value === null || value === undefined) {
          return nGrams;
        }
        var source = typeof value.slice === 'function' ? value : String(value);
        var index = source.length - n + 1;
        if (index < 1) {
          return nGrams;
        }
        while (index--) {
          nGrams[index] = source.slice(index, index + n);
        }
        return nGrams;
      };
    };

    var bigram = nGram(2);

    var bin2hex = function bin2hex(value) {
      var encoder = new TextEncoder();
      var bytes = encoder.encode(value);
      var hex = '';
      var _iterator = _createForOfIteratorHelper(bytes),
        _step;
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var _byte = _step.value;
          hex += _byte.toString(16).padStart(2, '0');
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
      return hex;
    };

    function camelCase(source) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      return source.replace(/-\D/g, function (match) {
        return match.charAt(1).toUpperCase();
      });
    }

    function capitalize(source) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      return source.replace(/\b[a-z]/g, function (match) {
        return match.toUpperCase();
      });
    }

    function center(source) {
      var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var separator = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : " ";
      if (source === null || !(source instanceof String || typeof source === 'string')) {
        return "";
      }
      if (separator === null || !(separator instanceof String || typeof separator === 'string')) {
        separator = " ";
      }
      var len = source.length;
      if (len <= size) {
        len = size - len;
        var remain = len % 2 === 0 ? "" : separator;
        var pad = "";
        var count = Math.floor(len / 2);
        if (count > 0) {
          for (var i = 0; i < count; i++) {
            pad = pad.concat(separator);
          }
        } else {
          pad = separator;
        }
        return pad + source + pad + remain;
      } else {
        return source;
      }
    }

    function trim(source) {
      var chars = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      if (!chars || !(chars instanceof Array)) {
        chars = whiteSpaces;
      }
      var i;
      var l;
      l = source.length;
      for (i = 0; i < l && chars.indexOf(source.charAt(i)) > -1; i++) {}
      source = source.substring(i);
      for (i = source.length - 1; i >= 0 && chars.indexOf(source.charAt(i)) > -1; i--) {}
      source = source.substring(0, i + 1);
      return source;
    }

    function clean(source) {
      if (source === null || !(source instanceof String || typeof source === 'string')) {
        return "";
      }
      return trim(source.replace(/\s+/g, ' '));
    }

    function compare(str1, str2) {
      var strict = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      if (!(typeof str1 === 'string' || str1 instanceof String)) {
        throw new TypeError('Bad arguments, the compare function failed, the first argument must be a string value.');
      }
      if (!(typeof str2 === 'string' || str2 instanceof String)) {
        throw new TypeError('Bad arguments, the compare function failed, the second argument must be a string value.');
      }
      if (str1 === str2) {
        return 0;
      }
      strict = Boolean(strict);
      if (!strict) {
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
      }
      if (str1.length === str2.length) {
        var local = str1.localeCompare(str2);
        if (local === 0) {
          return 0;
        } else if (local < 0) {
          return -1;
        }
        return 1;
      } else if (str1.length > str2.length) {
        return 1;
      } else {
        return -1;
      }
    }

    var deburr = function deburr(source) {
      return source && source.replace(reLatin, deburrLetter).replace(reComboMark, '');
    };
    var reLatin = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g;
    var rsComboMarksRange$1 = "\\u0300-\\u036f\\ufe20-\\ufe23",
      rsComboSymbolsRange$1 = "\\u20d0-\\u20f0";
    var rsCombo$1 = '[' + rsComboMarksRange$1 + rsComboSymbolsRange$1 + ']';
    var reComboMark = RegExp(rsCombo$1, 'g');
    var deburredLetters = {
      '\xc0': 'A',
      '\xc1': 'A',
      '\xc2': 'A',
      '\xc3': 'A',
      '\xc4': 'A',
      '\xc5': 'A',
      '\xe0': 'a',
      '\xe1': 'a',
      '\xe2': 'a',
      '\xe3': 'a',
      '\xe4': 'a',
      '\xe5': 'a',
      '\xc7': 'C',
      '\xe7': 'c',
      '\xd0': 'D',
      '\xf0': 'd',
      '\xc8': 'E',
      '\xc9': 'E',
      '\xca': 'E',
      '\xcb': 'E',
      '\xe8': 'e',
      '\xe9': 'e',
      '\xea': 'e',
      '\xeb': 'e',
      '\xcc': 'I',
      '\xcd': 'I',
      '\xce': 'I',
      '\xcf': 'I',
      '\xec': 'i',
      '\xed': 'i',
      '\xee': 'i',
      '\xef': 'i',
      '\xd1': 'N',
      '\xf1': 'n',
      '\xd2': 'O',
      '\xd3': 'O',
      '\xd4': 'O',
      '\xd5': 'O',
      '\xd6': 'O',
      '\xd8': 'O',
      '\xf2': 'o',
      '\xf3': 'o',
      '\xf4': 'o',
      '\xf5': 'o',
      '\xf6': 'o',
      '\xf8': 'o',
      '\xd9': 'U',
      '\xda': 'U',
      '\xdb': 'U',
      '\xdc': 'U',
      '\xf9': 'u',
      '\xfa': 'u',
      '\xfb': 'u',
      '\xfc': 'u',
      '\xdd': 'Y',
      '\xfd': 'y',
      '\xff': 'y',
      '\xc6': 'Ae',
      '\xe6': 'ae',
      '\xde': 'Th',
      '\xfe': 'th',
      '\xdf': 'ss',
      "\u0100": 'A',
      "\u0102": 'A',
      "\u0104": 'A',
      "\u0101": 'a',
      "\u0103": 'a',
      "\u0105": 'a',
      "\u0106": 'C',
      "\u0108": 'C',
      "\u010A": 'C',
      "\u010C": 'C',
      "\u0107": 'c',
      "\u0109": 'c',
      "\u010B": 'c',
      "\u010D": 'c',
      "\u010E": 'D',
      "\u0110": 'D',
      "\u010F": 'd',
      "\u0111": 'd',
      "\u0112": 'E',
      "\u0114": 'E',
      "\u0116": 'E',
      "\u0118": 'E',
      "\u011A": 'E',
      "\u0113": 'e',
      "\u0115": 'e',
      "\u0117": 'e',
      "\u0119": 'e',
      "\u011B": 'e',
      "\u011C": 'G',
      "\u011E": 'G',
      "\u0120": 'G',
      "\u0122": 'G',
      "\u011D": 'g',
      "\u011F": 'g',
      "\u0121": 'g',
      "\u0123": 'g',
      "\u0124": 'H',
      "\u0126": 'H',
      "\u0125": 'h',
      "\u0127": 'h',
      "\u0128": 'I',
      "\u012A": 'I',
      "\u012C": 'I',
      "\u012E": 'I',
      "\u0130": 'I',
      "\u0129": 'i',
      "\u012B": 'i',
      "\u012D": 'i',
      "\u012F": 'i',
      "\u0131": 'i',
      "\u0134": 'J',
      "\u0135": 'j',
      "\u0136": 'K',
      "\u0137": 'k',
      "\u0138": 'k',
      "\u0139": 'L',
      "\u013B": 'L',
      "\u013D": 'L',
      "\u013F": 'L',
      "\u0141": 'L',
      "\u013A": 'l',
      "\u013C": 'l',
      "\u013E": 'l',
      "\u0140": 'l',
      "\u0142": 'l',
      "\u0143": 'N',
      "\u0145": 'N',
      "\u0147": 'N',
      "\u014A": 'N',
      "\u0144": 'n',
      "\u0146": 'n',
      "\u0148": 'n',
      "\u014B": 'n',
      "\u014C": 'O',
      "\u014E": 'O',
      "\u0150": 'O',
      "\u014D": 'o',
      "\u014F": 'o',
      "\u0151": 'o',
      "\u0154": 'R',
      "\u0156": 'R',
      "\u0158": 'R',
      "\u0155": 'r',
      "\u0157": 'r',
      "\u0159": 'r',
      "\u015A": 'S',
      "\u015C": 'S',
      "\u015E": 'S',
      "\u0160": 'S',
      "\u015B": 's',
      "\u015D": 's',
      "\u015F": 's',
      "\u0161": 's',
      "\u0162": 'T',
      "\u0164": 'T',
      "\u0166": 'T',
      "\u0163": 't',
      "\u0165": 't',
      "\u0167": 't',
      "\u0168": 'U',
      "\u016A": 'U',
      "\u016C": 'U',
      "\u016E": 'U',
      "\u0170": 'U',
      "\u0172": 'U',
      "\u0169": 'u',
      "\u016B": 'u',
      "\u016D": 'u',
      "\u016F": 'u',
      "\u0171": 'u',
      "\u0173": 'u',
      "\u0174": 'W',
      "\u0175": 'w',
      "\u0176": 'Y',
      "\u0177": 'y',
      "\u0178": 'Y',
      "\u0179": 'Z',
      "\u017B": 'Z',
      "\u017D": 'Z',
      "\u017A": 'z',
      "\u017C": 'z',
      "\u017E": 'z',
      "\u0132": 'IJ',
      "\u0133": 'ij',
      "\u0152": 'Oe',
      "\u0153": 'oe',
      "\u0149": "'n",
      "\u017F": 'ss'
    };
    var basePropertyOf = function basePropertyOf(object) {
      return function (key) {
        return object === null || object === undefined ? undefined : object[key];
      };
    };
    var deburrLetter = basePropertyOf(deburredLetters);

    var diceCoefficient = function diceCoefficient(value, other) {
      var left = toPairs(value);
      var right = toPairs(other);
      var index = -1;
      var intersections = 0;
      while (++index < left.length) {
        var leftPair = left[index];
        var offset = -1;
        while (++offset < right.length) {
          var rightPair = right[offset];
          if (leftPair === rightPair) {
            intersections++;
            right[offset] = '';
            break;
          }
        }
      }
      return 2 * intersections / (left.length + right.length);
    };
    var toPairs = function toPairs(value) {
      if (Array.isArray(value)) {
        return value.map(function (bigram) {
          return normalize(bigram);
        });
      }
      var normal = normalize(value);
      return normal.length === 1 ? [normal] : bigram(normal);
    };
    var normalize = function normalize(value) {
      return String(value).replace(/\s+/g, '').toLowerCase();
    };

    var empty = function empty(value) {
      return (typeof value === 'string' || value instanceof String) && value === '';
    };

    function endsWith(source, value) {
      if (!(source instanceof String || typeof source === 'string') || !(value instanceof String || typeof value === 'string') || source.length < value.length) {
        return false;
      }
      if (value === "") {
        return true;
      }
      return source.lastIndexOf(value) === source.length - value.length;
    }

    var fastFormatDate = function fastFormatDate() {
      var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var separator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '-';
      if (!(date instanceof Date)) {
        date = new Date();
      }
      var month = date.getMonth() + 1;
      var day = date.getDate();
      var exp = date.getFullYear() + separator;
      if (month < 10) {
        exp += "0";
      }
      exp += month + separator;
      if (day < 10) {
        exp += "0";
      }
      exp += day;
      return exp;
    };

    function pad(source) {
      var amount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var expression = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : " ";
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      var left = amount >= 0;
      var width = amount > 0 ? amount : -amount;
      if (width < source.length || width === 0) {
        return source;
      }
      if (expression === null) {
        expression = " ";
      } else if (expression.length > 1) {
        expression = expression.charAt(0);
      }
      while (source.length !== width) {
        if (left) {
          source = expression + source;
        } else {
          source += expression;
        }
      }
      return source;
    }

    function format(pattern) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      if (pattern === null || !(pattern instanceof String || typeof pattern === 'string')) {
        return "";
      }
      var formatted = pattern;
      var len = args.length;
      var words = {};
      if (len === 1 && args[0] instanceof Array) {
        args = args[0];
      } else if (args[0] instanceof Array) {
        var a = args[0];
        args.shift();
        args = a.concat(args);
      } else if (args[0] instanceof Object && String(args[0]) === "[object Object]") {
        words = args[0];
        if (len > 1) {
          args.shift();
        }
      }
      var search = new RegExp("{([a-z0-9,:\\-]*)}", "m");
      var result = search.exec(formatted);
      var part;
      var token;
      var c;
      var pos;
      var dirty = false;
      var padding = 0;
      var buffer = [];
      while (result !== null) {
        part = result[0];
        token = result[1];
        pos = token.indexOf(",");
        if (pos > 0) {
          padding = Number(token.substr(pos + 1));
          token = token.substring(0, pos);
        }
        c = token.charAt(0);
        if ("0" <= c && c <= "9") {
          formatted = formatted.replace(part, pad(String(args[token]), padding));
        } else if (token === "" || token.indexOf(":") > -1) {
          buffer.push(part);
          formatted = formatted.replace(new RegExp(part, "g"), "\uFFFC" + (buffer.length - 1));
          dirty = true;
        } else if ("a" <= c && c <= "z") {
          if (token in words || words.hasOwnProperty(token)) {
            formatted = formatted.replace(new RegExp(part, "g"), pad(String(words[token]), padding));
          }
        } else {
          throw new Error("core.strings.format failed, malformed token \"" + part + "\", can not start with \"" + c + "\"");
        }
        result = search.exec(formatted);
      }
      if (dirty) {
        var i;
        var bl = buffer.length;
        for (i = 0; i < bl; i++) {
          formatted = formatted.replace(new RegExp("\uFFFC" + i, "g"), buffer[i]);
        }
      }
      return formatted;
    }

    var substr = function substr(string, start, length) {
      var size = string.length;
      start = toIntegerOrInfinity(start);
      if (start === Infinity) {
        start = 0;
      } else if (start < 0) {
        start = Math.max(size + start, 0);
      }
      length = length === undefined ? size : toIntegerOrInfinity(length);
      if (length <= 0 || length === Infinity) {
        return '';
      }
      var end = Math.min(start + length, size);
      return start >= end ? '' : string.slice(start, end);
    };

    var hex2Bin = function hex2Bin(value) {
      var i = 0;
      var l;
      value += '';
      var ret = [];
      for (l = value.length; i < l; i += 2) {
        var c = parseInt(substr(value, i, 1), 16);
        var k = parseInt(substr(value, i + 1, 1), 16);
        if (isNaN(c) || isNaN(k)) {
          return false;
        }
        ret.push(c << 4 | k);
      }
      return String.fromCharCode.apply(String, ret);
    };

    var hyphenate = function hyphenate(source) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      return source.replace(/[A-Z]/g, function (match) {
        return '-' + match.charAt(0).toLowerCase();
      });
    };

    function indexOfAny(source, anyOf) {
      var startIndex = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var count = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : -1;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return -1;
      }
      if (!(anyOf instanceof Array)) {
        return -1;
      }
      startIndex = startIndex > 0 ? 0 : startIndex;
      count = count < 0 ? -1 : count;
      var l = source.length;
      var endIndex;
      if (count < 0 || count > l - startIndex) {
        endIndex = l - 1;
      } else {
        endIndex = startIndex + count - 1;
      }
      for (var i = startIndex; i <= endIndex; i++) {
        if (anyOf.indexOf(source[i]) > -1) {
          return i;
        }
      }
      return -1;
    }

    function insert(source) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      if (!(value instanceof String || typeof value === 'string') || value === "") {
        return source;
      }
      if (index < 0) {
        index = source.length - Math.abs(index) + 1;
      } else if (index === 0) {
        return value + source;
      } else if (index >= source.length) {
        return source + value;
      }
      return source.substr(0, index) + value + source.substr(index);
    }

    var rsAstralRange = "\\ud800-\\udfff";
    var rsComboMarksRange = "\\u0300-\\u036f";
    var reComboHalfMarksRange = "\\ufe20-\\ufe2f";
    var rsComboSymbolsRange = "\\u20d0-\\u20ff";
    var rsComboMarksExtendedRange = "\\u1ab0-\\u1aff";
    var rsComboMarksSupplementRange = "\\u1dc0-\\u1dff";
    var rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange + rsComboMarksExtendedRange + rsComboMarksSupplementRange;
    var rsDingbatRange = "\\u2700-\\u27bf";
    var rsLowerRange = 'a-z\\xdf-\\xf6\\xf8-\\xff';
    var rsMathOpRange = '\\xac\\xb1\\xd7\\xf7';
    var rsNonCharRange = '\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf';
    var rsPunctuationRange = "\\u2000-\\u206f";
    var rsSpaceRange = " \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000";
    var rsUpperRange = 'A-Z\\xc0-\\xd6\\xd8-\\xde';
    var rsVarRange = "\\ufe0e\\ufe0f";
    var rsBreakRange = rsMathOpRange + rsNonCharRange + rsPunctuationRange + rsSpaceRange;
    var rsApos$1 = "['\u2019]";
    var rsBreak = "[".concat(rsBreakRange, "]");
    var rsCombo = "[".concat(rsComboRange, "]");
    var rsDigits = '\\d';
    var rsDingbat = "[".concat(rsDingbatRange, "]");
    var rsLower = "[".concat(rsLowerRange, "]");
    var rsMisc = "[^".concat(rsAstralRange).concat(rsBreakRange + rsDigits + rsDingbatRange + rsLowerRange + rsUpperRange, "]");
    var rsFitz = "\\ud83c[\\udffb-\\udfff]";
    var rsModifier = "(?:".concat(rsCombo, "|").concat(rsFitz, ")");
    var rsNonAstral = "[^".concat(rsAstralRange, "]");
    var rsRegional = "(?:\\ud83c[\\udde6-\\uddff]){2}";
    var rsSurrPair = "[\\ud800-\\udbff][\\udc00-\\udfff]";
    var rsUpper = "[".concat(rsUpperRange, "]");
    var rsZWJ = "\\u200d";
    var rsMiscLower = "(?:".concat(rsLower, "|").concat(rsMisc, ")");
    var rsMiscUpper = "(?:".concat(rsUpper, "|").concat(rsMisc, ")");
    var rsOptContrLower = "(?:".concat(rsApos$1, "(?:d|ll|m|re|s|t|ve))?");
    var rsOptContrUpper = "(?:".concat(rsApos$1, "(?:D|LL|M|RE|S|T|VE))?");
    var reOptMod = "".concat(rsModifier, "?");
    var rsOptVar = "[".concat(rsVarRange, "]?");
    var rsOptJoin = "(?:".concat(rsZWJ, "(?:").concat([rsNonAstral, rsRegional, rsSurrPair].join('|'), ")").concat(rsOptVar + reOptMod, ")*");
    var rsOrdLower = '\\d*(?:1st|2nd|3rd|(?![123])\\dth)(?=\\b|[A-Z_])';
    var rsOrdUpper = '\\d*(?:1ST|2ND|3RD|(?![123])\\dTH)(?=\\b|[a-z_])';
    var rsSeq = rsOptVar + reOptMod + rsOptJoin;
    var rsEmoji = "(?:".concat([rsDingbat, rsRegional, rsSurrPair].join('|'), ")").concat(rsSeq);
    var reUnicodeWord = RegExp([rsUpper + '?' + rsLower + '+' + rsOptContrLower + '(?=' + [rsBreak, rsUpper, '$'].join('|') + ')', rsMiscUpper + '+' + rsOptContrUpper + '(?=' + [rsBreak, rsUpper + rsMiscLower, '$'].join('|') + ')', rsUpper + '?' + rsMiscLower + '+' + rsOptContrLower, rsUpper + '+' + rsOptContrUpper, rsOrdUpper, rsOrdLower, rsDigits, rsEmoji].join('|'), 'g');
    var unicodeWords = function unicodeWords(source) {
      return source.match(reUnicodeWord) || [];
    };

    var reAsciiWord = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g;
    var reHasUnicodeWord = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;
    var asciiWords = function asciiWords(string) {
      return string.match(reAsciiWord) || [];
    };
    var hasUnicodeWord = function hasUnicodeWord(string) {
      return reHasUnicodeWord.test(string);
    };
    var words = function words(source) {
      var pattern = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
      var guard = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      pattern = guard ? undefined : pattern;
      if (pattern === undefined) {
        return hasUnicodeWord(source) ? unicodeWords(source) : asciiWords(source);
      }
      return source.match(pattern) || [];
    };

    var INFINITY = 1 / 0;
    var _toString = function toString(value) {
      if (value === null || value === undefined) {
        return '';
      }
      if (typeof value === 'string' || value instanceof String) {
        return value;
      }
      if (Array.isArray(value)) {
        return "".concat(value.map(function (other) {
          return other === null ? other : _toString(other);
        }));
      }
      if (isSymbol$2(value)) {
        return value.toString();
      }
      var result = "".concat(value);
      return result === '0' && 1 / value === -INFINITY ? '-0' : result;
    };

    var kebabCase = function kebabCase(source) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      return words(_toString(source).replace(/['\u2019]/g, '')).reduce(function (result, word, index) {
        return result + (index ? '-' : '') + word.toLowerCase();
      }, '');
    };

    function lastIndexOfAny(source, anyOf) {
      var startIndex = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Infinity;
      var count = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : Infinity;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return -1;
      }
      if (!(anyOf instanceof Array) || anyOf.length === 0) {
        return -1;
      }
      if (startIndex < 0) {
        return -1;
      } else if (isNaN(startIndex) || startIndex > source.length) {
        startIndex = source.length;
      }
      count = count > 0 ? count : 0;
      var endIndex = Math.max(startIndex - count, 0);
      source = source.slice(endIndex, startIndex);
      var len = anyOf.length;
      for (var i = 0; i < len; i++) {
        var index = source.lastIndexOf(anyOf[i], startIndex);
        if (index > -1) {
          return endIndex + index;
        }
      }
      return -1;
    }

    var notEmpty = function notEmpty(value) {
      return (typeof value === 'string' || value instanceof String) && value !== '';
    };

    var pascalCase = function pascalCase(source) {
      if (!(source instanceof String || typeof source === 'string') || source === '') {
        return '';
      }
      return words(source).map(function (word) {
        return capitalize(word);
      }).join('');
    };

    var defaultOptions = {
      format: null,
      singular: '',
      plural: 's'
    };
    var pluralize = function pluralize(expression, count) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      options = _objectSpread2(_objectSpread2({}, defaultOptions), options);
      var _options = options,
        format = _options.format,
        plural = _options.plural,
        singular = _options.singular;
      if (format instanceof Function) {
        return format({
          expression: expression,
          count: count,
          plural: plural,
          singular: singular
        });
      }
      return expression + (count > 1 ? plural : singular);
    };

    function repeat(source) {
      var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      count = isNaN(count) ? 0 : count;
      if (count < 0) {
        throw new RangeError('repeat count must be non-negative');
      }
      if (count === Infinity) {
        throw new RangeError('repeat count must be less than infinity');
      }
      count = Math.floor(count);
      if (source.length * count >= 1 << 28) {
        throw new RangeError('repeat count must not overflow maximum string size');
      }
      if (count === 0) {
        return "";
      }
      var result = '';
      for (;;) {
        if ((count & 1) === 1) {
          result += source;
        }
        count >>>= 1;
        if (count === 0) {
          break;
        }
        source += source;
      }
      return result;
    }

    var transform = function transform(result, word, index) {
      return result + (index ? '_' : '') + word.toLowerCase();
    };
    var rsApos = "['\u2019]";
    var reApos = RegExp(rsApos, 'g');
    function snakeCase() {
      var source = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!(source instanceof String || typeof source === 'string')) {
        return null;
      }
      return words(deburr(source).replace(reApos, '')).reduce(transform, '');
    }

    function startsWith(source, value) {
      if (!(source instanceof String || typeof source === 'string') || !(value instanceof String || typeof value === 'string') || source.length < value.length) {
        return false;
      }
      if (value === "") {
        return true;
      }
      if (source.charAt(0) !== value.charAt(0)) {
        return false;
      }
      return source.indexOf(value) === 0;
    }

    var stub = function stub() {
      return '';
    };

    var toString = function toString(value) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      if (value === null || value === undefined) {
        return defaultValue;
      }
      if (value instanceof String || typeof value === 'string') {
        return value;
      }
      return String(value);
    };

    var trigram = nGram(3);

    function trimEnd(source) {
      var chars = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      if (!chars || !(chars instanceof Array)) {
        chars = whiteSpaces;
      }
      var i;
      for (i = source.length - 1; i >= 0 && chars.indexOf(source.charAt(i)) > -1; i--) {}
      return source.substring(0, i + 1);
    }

    function trimStart(source) {
      var chars = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      if (!chars || !(chars instanceof Array)) {
        chars = whiteSpaces;
      }
      var i;
      var l = source.length;
      for (i = 0; i < l && chars.indexOf(source.charAt(i)) > -1; i++) {}
      return source.substring(i);
    }

    var truncate = function truncate(source) {
      var length = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var prune = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "...";
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      length = length > 0 ? length : 0;
      if (!(prune instanceof String || typeof prune === 'string')) {
        prune = '...';
      }
      if (source.length <= length) {
        return source;
      }
      var template = source.slice(0, length + 1).replace(/.(?=\W*\w*$)/g, function (c) {
        return c.toUpperCase() !== c.toLowerCase() ? 'A' : ' ';
      });
      if (template.slice(template.length - 2).match(/\w\w/)) {
        template = template.replace(/\s*\S+$/, '');
      } else {
        template = trimEnd(template.slice(0, template.length - 1));
      }
      return (template + prune).length > source.length ? source : source.slice(0, template.length) + prune;
    };

    function ucFirst(source) {
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      return source.charAt(0).toUpperCase() + source.substring(1);
    }

    function ucWords(source) {
      var separator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : " ";
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return '';
      }
      var ar = source.split(separator);
      var l = ar.length;
      while (--l > -1) {
        ar[l] = ar[l].charAt(0).toUpperCase() + ar[l].substring(1);
      }
      return ar.join(separator);
    }

    var versionUUID = function versionUUID(uuid) {
      return uuid.charAt(14) | 0;
    };

    var pattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-4][0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}$/i;
    var validateUUID = function validateUUID(source) {
      var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (!(source instanceof String || typeof source === 'string') || source === "") {
        return false;
      }
      source = source.toLowerCase();
      if (!pattern.test(source)) {
        return false;
      }
      if (!version) {
        version = versionUUID(source);
      } else if (versionUUID(source) !== version) {
        return false;
      }
      switch (version) {
        case 1:
        case 2:
          {
            return true;
          }
        case 3:
        case 4:
          {
            return ['8', '9', 'a', 'b'].indexOf(source.charAt(19)) !== -1;
          }
        default:
          {
            throw new Error('Invalid version provided.');
          }
      }
    };

    /**
     * The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
     * @summary The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
     * @namespace core.strings
     * @memberof core
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @since 1.0.0
     */
    var strings$1 = {
      between: between,
      bigram: bigram,
      bin2hex: bin2hex,
      camelCase: camelCase,
      capitalize: capitalize,
      center: center,
      clean: clean,
      compare: compare,
      deburr: deburr,
      diceCoefficient: diceCoefficient,
      empty: empty,
      endsWith: endsWith,
      fastformat: fastFormat,
      fastformatDate: fastFormatDate,
      format: format,
      hex2bin: hex2Bin,
      hyphenate: hyphenate,
      indexOfAny: indexOfAny,
      insert: insert,
      kebabCase: kebabCase,
      lastIndexOfAny: lastIndexOfAny,
      nGram: nGram,
      notEmpty: notEmpty,
      pad: pad,
      pascalCase: pascalCase,
      pluralize: pluralize,
      repeat: repeat,
      snakeCase: snakeCase,
      startsWith: startsWith,
      stringToPath: stringToPath,
      stub: stub,
      substr: substr,
      toString: toString,
      trigram: trigram,
      trim: trim,
      trimEnd: trimEnd,
      trimStart: trimStart,
      truncate: truncate,
      ucFirst: ucFirst,
      ucWords: ucWords,
      validateUUID: validateUUID,
      versionUUID: versionUUID,
      words: words
    };

    /**
     * The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.
     * <p>You can consider a library as a set of functions organized into classes, here with a <strong>"core"</strong> library in some cases we organize the functions in the package definitions without assembling them into a class.</p>
     * <p>Those functions are allowed to reuse the builtin types (Object, Array, Date, etc.), the Javascript API classes and packages, but nothing else.</p>
     * @summary The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies.
     * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
     * @author Marc Alcaraz <ekameleon@gmail.com>
     * @namespace core
     * @version 1.0.3
     * @since 1.0.0
     */
    var core = {
      global: global$1,
      canUseSymbol: canUseSymbol$1,
      dump: dump$1,
      trace: trace$1,
      version: version$1,
      cancelAnimationFrame: requestAnimationFrame,
      requestAnimationFrame: requestAnimationFrame,
      cloneDeep: cloneDeep$1,
      cloneDeepWith: cloneDeepWith$1,
      deepMerge: deepMerge$1,
      deepMergeAll: deepMergeAll$1,
      eq: eq$1,
      invariant: invariant$1,
      isArray: isArray$1,
      isArrayLike: isArrayLike$1,
      isArrayLikeObject: isArrayLikeObject$1,
      isArguments: isArguments$1,
      isAsyncFunction: isAsyncFunction$1,
      isBoolean: isBoolean$1,
      isBuffer: isBuffer$1,
      isEmpty: isEmpty$2,
      isEqual: isEqual$1,
      isEqualWith: isEqualWith$1,
      isFloat: isFloat$1,
      isFunction: isFunction$1,
      isIndex: isIndex$1,
      isInt: isInt$1,
      isLength: isLength$1,
      isMap: isMap$1,
      isMergeableObject: isMergeableObject$1,
      isNil: isNil$1,
      isNotEmpty: isNotEmpty$1,
      isNotNull: isNotNull$1,
      isNotNullObject: isNotNullObject$1,
      isNumber: isNumber$1,
      isObject: isObject$1,
      isObjectLike: isObjectLike$1,
      isPlainObject: isPlainObject$1,
      isPrimitive: isPrimitive$1,
      isReactElement: isReactElement$1,
      isSet: isSet$1,
      isString: isString$1,
      isSymbol: isSymbol$2,
      isTypedArray: isTypedArray$1,
      isUint: isUint$1,
      isWeakMap: isWeakMap$1,
      isWeakSet: isWeakSet$1,
      stubFalse: stubFalse$1,
      stubTrue: stubTrue$1,
      toPlainObject: toPlainObject$1,
      arrays: arrays$1,
      chars: chars$1,
      collections: collections$1,
      colors: colors$1,
      date: date$1,
      dom: dom$1,
      easings: easings$1,
      functors: functors$1,
      graphics: numbers$2,
      maths: maths$1,
      numbers: numbers$1,
      objects: objects$1,
      random: random$1,
      reflect: reflect$1,
      strings: strings$1
    };

    var globalFromProps = core.global,
      canUseSymbol = core.canUseSymbol,
      dump = core.dump,
      trace = core.trace,
      version = core.version,
      cancelAnimationFrameProp = core.cancelAnimationFrame,
      requestAnimationFrameFromProp = core.requestAnimationFrame,
      cloneDeep = core.cloneDeep,
      cloneDeepWith = core.cloneDeepWith,
      deepMerge = core.deepMerge,
      deepMergeAll = core.deepMergeAll,
      eq = core.eq,
      invariant = core.invariant,
      isArray = core.isArray,
      isArrayLike = core.isArrayLike,
      isArrayLikeObject = core.isArrayLikeObject,
      isArguments = core.isArguments,
      isAsyncFunction = core.isAsyncFunction,
      isBoolean = core.isBoolean,
      isBuffer = core.isBuffer,
      isEmpty = core.isEmpty,
      isEqual = core.isEqual,
      isEqualWith = core.isEqualWith,
      isFloat = core.isFloat,
      isFunction = core.isFunction,
      isIndex = core.isIndex,
      isInt = core.isInt,
      isLength = core.isLength,
      isMap = core.isMap,
      isMergeableObject = core.isMergeableObject,
      isNil = core.isNil,
      isNotEmpty = core.isNotEmpty,
      isNotNull = core.isNotNull,
      isNotNullObject = core.isNotNullObject,
      isNumber = core.isNumber,
      isObject = core.isObject,
      isObjectLike = core.isObjectLike,
      isPlainObject = core.isPlainObject,
      isPrimitive = core.isPrimitive,
      isReactElement = core.isReactElement,
      isSet = core.isSet,
      isString = core.isString,
      isSymbol = core.isSymbol,
      isTypedArray = core.isTypedArray,
      isUint = core.isUint,
      isWeakMap = core.isWeakMap,
      isWeakSet = core.isWeakSet,
      stubFalse = core.stubFalse,
      stubTrue = core.stubTrue,
      toPlainObject = core.toPlainObject,
      arrays = core.arrays,
      chars = core.chars,
      colors = core.colors,
      collections = core.collections,
      date = core.date,
      dom = core.dom,
      easings = core.easings,
      functors = core.functors,
      graphics = core.graphics,
      maths = core.maths,
      numbers = core.numbers,
      objects = core.objects,
      random = core.random,
      reflect = core.reflect,
      strings = core.strings;
    var metas = Object.defineProperties({}, {
      name: {
        enumerable: true,
        value: 'vegas-js-core'
      },
      description: {
        enumerable: true,
        value: "A specialized set of basic functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc."
      },
      version: {
        enumerable: true,
        value: '1.0.45'
      },
      license: {
        enumerable: true,
        value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
      },
      url: {
        enumerable: true,
        value: 'https://bitbucket.org/ekameleon/vegas-js-core'
      }
    });
    try {
      if (window) {
        var _load = function load() {
          var name = metas.name,
            version = metas.version,
            url = metas.url;
          window.removeEventListener('load', _load, false);
          sayHello(name, version, url);
        };
        window.addEventListener('load', _load, false);
      }
    } catch (ignored) {}
    var bundle = {
      metas: metas,
      sayHello: sayHello,
      skipHello: skipHello,
      canUseSymbol: canUseSymbol,
      dump: dump,
      trace: trace,
      version: version,
      performance: performance,
      global: globalFromProps,
      cancelAnimationFrame: cancelAnimationFrameProp,
      requestAnimationFrame: requestAnimationFrameFromProp,
      cloneDeep: cloneDeep,
      cloneDeepWith: cloneDeepWith,
      deepMerge: deepMerge,
      deepMergeAll: deepMergeAll,
      eq: eq,
      invariant: invariant,
      isArray: isArray,
      isArrayLike: isArrayLike,
      isArrayLikeObject: isArrayLikeObject,
      isArguments: isArguments,
      isAsyncFunction: isAsyncFunction,
      isBoolean: isBoolean,
      isBuffer: isBuffer,
      isEqual: isEqual,
      isEqualWith: isEqualWith,
      isEmpty: isEmpty,
      isFloat: isFloat,
      isFunction: isFunction,
      isIndex: isIndex,
      isInt: isInt,
      isLength: isLength,
      isMap: isMap,
      isMergeableObject: isMergeableObject,
      isNil: isNil,
      isNotNull: isNotNull,
      isNotNullObject: isNotNullObject,
      isNumber: isNumber,
      isNotEmpty: isNotEmpty,
      isObject: isObject,
      isObjectLike: isObjectLike,
      isPlainObject: isPlainObject,
      isPrimitive: isPrimitive,
      isReactElement: isReactElement,
      isSet: isSet,
      isString: isString,
      isSymbol: isSymbol,
      isTypedArray: isTypedArray,
      isUint: isUint,
      isWeakMap: isWeakMap,
      isWeakSet: isWeakSet,
      stubFalse: stubFalse,
      stubTrue: stubTrue,
      toPlainObject: toPlainObject,
      arrays: arrays,
      chars: chars,
      colors: colors,
      collections: collections,
      date: date,
      dom: dom,
      easings: easings,
      functors: functors,
      graphics: graphics,
      maths: maths,
      numbers: numbers,
      objects: objects,
      random: random,
      reflect: reflect,
      strings: strings
    };

    return bundle;

})));
//# sourceMappingURL=vegas.core.js.map
