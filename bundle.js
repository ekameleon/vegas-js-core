'use strict'

/**
 * The vegas-core JS library.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */

import sayHello , { skipHello } from './src/hello'

import performance  from './src/performance'

import core from './index.js' ;

const
{
    global : globalFromProps ,

    canUseSymbol,
    dump ,
    trace ,
    version ,

    cancelAnimationFrame  : cancelAnimationFrameProp ,
    requestAnimationFrame : requestAnimationFrameFromProp ,

    cloneDeep ,
    cloneDeepWith ,
    deepMerge,
    deepMergeAll,
    eq,
    invariant,
    isArray,
    isArrayLike,
    isArrayLikeObject,
    isArguments,
    isAsyncFunction,
    isBoolean,
    isBuffer,
    isEmpty,
    isEqual,
    isEqualWith,
    isFloat,
    isFunction,
    isIndex,
    isInt,
    isLength,
    isMap,
    isMergeableObject,
    isNil,
    isNotEmpty,
    isNotNull,
    isNotNullObject,
    isNumber,
    isObject,
    isObjectLike,
    isPlainObject,
    isPrimitive,
    isReactElement,
    isSet ,
    isString,
    isSymbol,
    isTypedArray,
    isUint,
    isWeakMap ,
    isWeakSet ,
    stubFalse,
    stubTrue,
    toPlainObject,

    arrays   ,
    chars    ,
    colors   ,
    collections ,
    date     ,
    dom      ,
    easings  ,
    functors ,
    graphics ,
    maths    ,
    numbers  ,
    objects  ,
    random   ,
    reflect  ,
    strings
}
= core ;

const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : '<@NAME@>'        },
    description : { enumerable : true , value : "<@DESCRIPTION@>" },
    version     : { enumerable : true , value : '<@VERSION@>'     } ,
    license     : { enumerable : true , value : "<@LICENSE@>"     } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>'    }
});

try
{
    if ( window )
    {
        const load = () =>
        {
            const { name , version , url } = metas ;
            window.removeEventListener( 'load' , load , false ) ;
            sayHello( name , version , url ) ;
        };
        window.addEventListener( 'load' , load , false );
    }
}
catch( ignored ) {}

export default
{
    metas     ,
    sayHello  ,
    skipHello ,

    canUseSymbol,
    dump ,
    trace ,
    version ,

    performance,
    global                : globalFromProps ,
    cancelAnimationFrame  : cancelAnimationFrameProp ,
    requestAnimationFrame : requestAnimationFrameFromProp ,

    cloneDeep ,
    cloneDeepWith ,
    deepMerge,
    deepMergeAll,
    eq,
    invariant,
    isArray ,
    isArrayLike ,
    isArrayLikeObject ,
    isArguments,
    isAsyncFunction,
    isBoolean,
    isBuffer,
    isEqual ,
    isEqualWith ,
    isEmpty,
    isFloat,
    isFunction,
    isIndex,
    isInt,
    isLength,
    isMap,
    isMergeableObject,
    isNil,
    isNotNull,
    isNotNullObject,
    isNumber,
    isNotEmpty,
    isObject,
    isObjectLike,
    isPlainObject,
    isPrimitive,
    isReactElement,
    isSet,
    isString,
    isSymbol,
    isTypedArray,
    isUint,
    isWeakMap,
    isWeakSet,
    stubFalse,
    stubTrue,
    toPlainObject,

    arrays,
    chars,
    colors,
    collections,
    date,
    dom,
    easings,
    functors,
    graphics,
    maths,
    numbers,
    objects,
    random,
    reflect,
    strings
}
