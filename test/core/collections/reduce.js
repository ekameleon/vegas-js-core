'use strict'

import reduce from '../../../src/collections/reduce'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.collections.reduce' , () =>
{
    it('reduce( [1, 2], (sum, n) => sum + n, 0 ) === 3', () =>
    {
        const result = reduce( [1, 2], (sum, n) => sum + n, 0 ) ;
        assert.equal( result , 3 );
    }) ;

    it('reduce({ \'a\': 1, \'b\': 2, \'c\': 1 }, ( result = [] , value , key ) => ...', () =>
    {
        const iteratee = ( result , value , key ) =>
        {
            result[value] = result[value] ?? [] ;
            result[value].push( key ) ;
            return result ;
        }

        const accumulator = {} ;

        const result = reduce({ 'a': 1, 'b': 2, 'c': 1 }, iteratee , accumulator  ) ;

        // { '1': ['a', 'c'], '2': ['b'] }

        assert.equal( result['1'][0] , 'a' );
        assert.equal( result['1'][1] , 'c' );
        assert.equal( result['2'][0] , 'b' );
    }) ;
});
