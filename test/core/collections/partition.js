'use strict'

import partition from '../../../src/collections/partition'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.collections.partition' , () =>
{
    it('partition( developers , ( { active } = {} ) => active )', () =>
    {
        const developers =
        [
            { name : 'eka'  , age: 41 , active : false } ,
            { name : 'faya' , age: 40 , active : true  } ,
            { name : 'bob'  , age: 24 , active : false }
        ] ;

        const result = partition( developers , ( { active } = {} ) => active ) ;

        assert.isArray( result );
        assert.equal( result.length , 2 );

        assert.isArray( result[0] );
        assert.equal( result[0].length, 1 );

        assert.isObject( result[0][0] );
        assert.equal( result[0][0].name , 'faya' );
        assert.equal( result[0][0].age  , 40 );
        assert.isTrue( result[0][0].active );

        assert.isArray( result[1] );
        assert.equal( result[1].length, 2 );

        assert.isObject( result[1][0] );
        assert.equal( result[1][0].name , 'eka' );
        assert.equal( result[1][0].age  , 41 );
        assert.isFalse( result[1][0].active );

        assert.isObject( result[1][1] );
        assert.equal( result[1][1].name , 'bob' );
        assert.equal( result[1][1].age  , 24 );
        assert.isFalse( result[1][1].active );
    }) ;
});
