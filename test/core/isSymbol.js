/*jshint -W053 */
'use strict'

import isSymbol from '../../src/isSymbol'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.isSymbol' , () =>
{
    it('isSymbol(Symbol.iterator) === true' , () => { assert.isTrue( isSymbol(Symbol.iterator) ); });
    it('isSymbol("foo") === false' , () => { assert.isFalse( isSymbol('foo') ); });
});
