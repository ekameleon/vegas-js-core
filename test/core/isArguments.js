/*jshint -W053 */
'use strict'

import isArguments from '../../src/isArguments'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.isArguments' , () =>
{
    it('isArguments( function() { return arguments }() ) === true' , () =>
    {
        assert.isTrue( isArguments( function() { return arguments }() ) );
    });

    it('isArguments( [ 1 , 2 , 3 ] ) === false' , () =>
    {
        assert.isFalse( isArguments( [ 1 , 2 , 3 ] ) );
    });
});
