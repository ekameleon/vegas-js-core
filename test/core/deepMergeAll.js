'use strict'

import deepMergeAll from "../../src/deepMergeAll"

import chai from 'chai' ;

const { assert } = chai ;

const { equal } = assert ;


let a = { foo : { bar : 3 } };
let b = { foo : { boo : 4 } };
let c = { bar : 'hello' };

describe( 'core.deepMergeAll' , () =>
{
    const result = deepMergeAll( [ a , b , c ] ) ;
    // { foo: { bar: 3, boo: 4 }, bar: 'hello' }
    
    it("result.foo.bar === 3" , () => { equal(result.foo.bar , 3 ) });
    it("result.foo.boo === 4" , () => { equal(result.foo.boo , 4 ) });
    it("result.bar === 'hello'" , () => { equal(result.bar, 'hello' ) });
});
