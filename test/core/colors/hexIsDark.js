'use strict'

import hexIsDark from '../../../src/colors/hexIsDark'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isFalse,
    isTrue
}
= assert ;

describe( 'core.colors.hexIsDark' , () =>
{
    it('hexIsDark("#D1534D") === false', () => { isTrue(hexIsDark("#D1534D")); });
    it('hexIsDark("#D1534D") === true' , () => { isFalse(hexIsDark("#E6A74B")); });
    it('hexIsDark("#D1534D") === true' , () => { isFalse(hexIsDark("#FAF05E")); });
});
