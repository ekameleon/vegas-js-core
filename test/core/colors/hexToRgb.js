'use strict'

import hexToRgb from '../../../src/colors/hexToRgb'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isArray,
    lengthOf,
    sameOrderedMembers
}
= assert ;

describe( 'core.colors.hexToRgb' , () =>
{
    describe( 'hexToRgb("#ff0000")' , () =>
    {
        const rgb = hexToRgb("#ff0000") ;
        it('hexToRgb("#ff0000") is Array', () => { isArray(rgb); });
        it('hexToRgb("#ff0000").length === 3', () => { lengthOf(rgb, 3); });
        it('hexToRgb("#ff0000") === [255,0,0]', () => { sameOrderedMembers(rgb, [255,0,0]); });
    });

    describe( 'hexToRgb("#00ff00")' , () =>
    {
        const rgb = hexToRgb("#00ff00") ;
        it('hexToRgb("#00ff00") is Array', () => { isArray(rgb); });
        it('hexToRgb("#00ff00").length === 3', () => { lengthOf(rgb, 3); });
        it('hexToRgb("#00ff00") === [0,255,0]', () => { sameOrderedMembers(rgb, [0,255,0]); });
    });

    describe( 'hexToRgb("#0000ff")' , () =>
    {
        const rgb = hexToRgb("#0000ff") ;
        it('hexToRgb("#0000ff") is Array', () => { isArray(rgb); });
        it('hexToRgb("#0000ff").length === 3', () => { lengthOf(rgb, 3); });
        it('hexToRgb("#0000ff") === [0,255,0]', () => { sameOrderedMembers(rgb, [0,0,255]); });
    });

});
