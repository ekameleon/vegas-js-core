'use strict'

import rgbIsLight from '../../../src/colors/rgbIsLight'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isFalse,
    isTrue
}
= assert ;

describe( 'core.colors.rgbIsLight' , () =>
{
    it('rgbIsLight(209,83,77) === false', () => { isFalse(rgbIsLight(209,83,77)); });
    it('rgbIsLight(230,167,75) === true' , () => { isTrue(rgbIsLight(230,167,75)); });
    it('rgbIsLight(250,240,94) === true' , () => { isTrue(rgbIsLight(250,240,94)); });
});
