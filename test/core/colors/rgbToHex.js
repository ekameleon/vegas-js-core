'use strict'

import rgbToHex from '../../../src/colors/rgbToHex'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.colors.rgbToHex' , () =>
{
    it( 'rgbToHex( 0xFF ,    0 ,    0 ) === #ff0000' , () => { assert.equal( rgbToHex( 0xFF ,    0 ,    0 ) , '#ff0000' ); }) ;
    it( 'rgbToHex(    0 , 0xFF ,    0 ) === #00ff00' , () => { assert.equal( rgbToHex(    0 , 0xFF ,    0 ) , '#00ff00' ); }) ;
    it( 'rgbToHex(    0 ,    0 , 0xFF ) === #0000ff' , () => { assert.equal( rgbToHex(    0 ,    0 , 0xFF ) , '#0000ff' ); }) ;
});
