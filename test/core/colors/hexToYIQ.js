'use strict'

import hexToYIQ from '../../../src/colors/hexToYIQ'

import chai from 'chai' ;

const { assert } = chai ;

const {
    equal
}
= assert ;

describe( 'core.colors.hexToYIQ' , () =>
{
    describe( 'hexToYIQ("#D1534D")' , () =>
    {
        const yiq = hexToYIQ("#D1534D") ;
        it('hexToYIQ("#D1534D") === 119.99', () => { equal(yiq,119.99); });
    });

    describe( 'hexToYIQ("#E6A74B")' , () =>
    {
        const yiq = hexToYIQ("#E6A74B") ;
        it('hexToYIQ("#E6A74B") === 175.349', () => { equal(yiq,175.349); });
    });

    describe( 'hexToYIQ("#FAF05E")' , () =>
    {
        const yiq = hexToYIQ("#FAF05E") ;
        it('hexToYIQ("#FAF05E") === 226.346', () => { equal(yiq,226.346); });
    });
});
