'use strict'

import rgbToYIQ from '../../../src/colors/rgbToYIQ'

import chai from 'chai' ;

const { assert } = chai ;
const { equal  } = assert ;

describe( 'core.colors.rgbToYIQ' , () =>
{
    describe( 'rgbToYIQ(209,83,77)' , () =>
    {
        const yiq = rgbToYIQ(209,83,77) ;
        it('rgbToYIQ(209,83,77) === 119.99', () => { equal(yiq,119.99); });
    });

    describe( 'rgbToYIQ(230,167,75)' , () =>
    {
        const yiq = rgbToYIQ(230,167,75) ;
        it('rgbToYIQ(230,167,75) === 175.349', () => { equal(yiq,175.349); });
    });

    describe( 'rgbToYIQ(250,240,94)' , () =>
    {
        const yiq = rgbToYIQ(250,240,94) ;
        it('rgbToYIQ(250,240,94) === 226.346', () => { equal(yiq,226.346); });
    });
});
