'use strict'

import hexToInt from '../../../src/colors/hexToInt'

import chai from 'chai' ;

const { assert } = chai ;

const { equal } = assert ;

describe( 'core.colors.hexToInt' , () =>
{
    it('hexToInt("#ffffff") ===', () => { equal(hexToInt("#ffffff"), 16777215); });
    it('hexToInt("#FFFFFF") ===', () => { equal(hexToInt("#FFFFFF"), 16777215); });
    it('hexToInt("#000000") ===', () => { equal(hexToInt("#000000"), 0); });
    it('hexToInt("#ff0000") ===', () => { equal(hexToInt("#ff0000"), 16711680); });
    it('hexToInt("#FF0000") ===', () => { equal(hexToInt("#FF0000"), 16711680); });
});
