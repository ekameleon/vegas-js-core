'use strict'

import rgbIsDark from '../../../src/colors/rgbIsDark'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isFalse,
    isTrue
}
= assert ;

describe( 'core.colors.rgbIsDark' , () =>
{
    it('rgbIsDark(209,83,77)  === true'  , () => { isTrue(rgbIsDark(209,83,77)); });
    it('rgbIsDark(230,167,75) === false' , () => { isFalse(rgbIsDark(230,167,75)); });
    it('rgbIsDark(250,240,94) === false' , () => { isFalse(rgbIsDark(250,240,94)); });
});
