'use strict'

import hexIsLight from '../../../src/colors/hexIsLight'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isFalse,
    isTrue
}
= assert ;

describe( 'core.colors.hexIsLight' , () =>
{
    it('hexIsLight("#D1534D") === false', () => { isFalse(hexIsLight("#D1534D")); });
    it('hexIsLight("#D1534D") === true' , () => { isTrue(hexIsLight("#E6A74B")); });
    it('hexIsLight("#D1534D") === true' , () => { isTrue(hexIsLight("#FAF05E")); });
});
