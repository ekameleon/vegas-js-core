'use strict'

import parse from '../../../src/colors/parse'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.colors.parse' , () =>
{
    it("parse('hsla(12 10% 50% / .3') === { space: 'hsl', values: [12, 10, 50], alpha: 0.3 }"  , () =>
    {
        const { space , values , alpha } = parse('hsla(12 10% 50% / .3)' ) ;
        assert.equal( space , 'hsl' ) ;
        assert.isArray( values ) ;
        assert.equal( values.length , 3 ) ;
        assert.equal( values[0] , 12 ) ;
        assert.equal( values[1] , 10 ) ;
        assert.equal( values[2] , 50 ) ;
        assert.equal( alpha , 0.3 ) ;
    });

    it("parse('oklch(74.7039% .158278 39.947') === { space: 'oklch', values: [ 0.747039, 0.158278, 39.947 ], alpha: 0.3 }"  , () =>
    {
        const { space , values , alpha } = parse('oklch(74.7039% .158278 39.947)' ) ; // color space
        assert.equal( space , 'oklch' ) ;
        assert.isArray( values ) ;
        assert.equal( values.length , 3 ) ;
        assert.equal( values[0] , 0.747039 ) ;
        assert.equal( values[1] , 0.158278 ) ;
        assert.equal( values[2] , 39.947 ) ;
        assert.equal( alpha , 1 ) ;
    });

    it("parse('R:10 G:20 B:30') === { space: 'rgb', values: [ 10 , 20 , 30 ], alpha: 1 }"  , () =>
    {
        const { space , values , alpha } = parse('R:10 G:20 B:30' ) ; // // named channels case
        assert.equal( space , 'rgb' ) ;
        assert.isArray( values ) ;
        assert.equal( values.length , 3 ) ;
        assert.equal( values[0] , 10 ) ;
        assert.equal( values[1] , 20 ) ;
        assert.equal( values[2] , 30 ) ;
        assert.equal( alpha , 1 ) ;
    });

    it("parse( 0x00ff00 ) === { space: 'rgb', values: [ 0 , 255 , 0 ], alpha: 1 }"  , () =>
    {
        const { space , values , alpha } = parse(0x00ff00 ) ; // // number
        assert.equal( space , 'rgb' ) ;
        assert.isArray( values ) ;
        assert.equal( values.length , 3 ) ;
        assert.equal( values[0] , 0 ) ;
        assert.equal( values[1] , 255 ) ;
        assert.equal( values[2] , 0 ) ;
        assert.equal( alpha , 1 ) ;
    });

    it("parse('#00ff00' === { space: 'rgb', values: [ 0 , 255 , 0 ], alpha: 1 }"  , () =>
    {
        const { space , values , alpha } = parse("#00ff00" ) ; // // number
        assert.equal( space , 'rgb' ) ;
        assert.isArray( values ) ;
        assert.equal( values.length , 3 ) ;
        assert.equal( values[0] , 0 ) ;
        assert.equal( values[1] , 255 ) ;
        assert.equal( values[2] , 0 ) ;
        assert.equal( alpha , 1 ) ;
    });
});

//oklch(74.7039% .158278 39.947)
