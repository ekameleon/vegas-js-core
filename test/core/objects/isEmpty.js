'use strict'

import isEmpty from '../../../src/objects/isEmpty'

import chai from 'chai' ;

const { assert } = chai ;

const {
    isFalse,
    isTrue,
    throws
} = assert ;

describe( 'core.objects.isEmpty' , () =>
{
    it('isEmpty({}) === true' , () =>
    {
        isTrue( isEmpty({}) );
    });

    it('isEmpty({ nonEmumerableProperty : 3 }) === false' , () =>
    {
        const object = {} ;
        Object.defineProperty( object, 'property', { value: 3 , enumerable : false }  );
        isFalse( isEmpty( object ) );
    });

    it('isEmpty({ nonEmumerableProperty : 3 } , false ) === false' , () =>
    {
        const object = {} ;
        Object.defineProperty( object, 'property', { value: 3 , enumerable : false }  );
        isTrue( isEmpty( object , false ) );
    });

    it('isEmpty({}) === false' , () =>
    {
        isFalse( isEmpty({ name:'john'} ));
    });

    it('isEmpty("string") === false' , () =>
    {
        isFalse( isEmpty("string") );
    });

    it('isEmpty(null) throws TypeError' , () =>
    {
        throws( () => isEmpty(null) , TypeError ) ;
    });

    it('isEmpty(undefined) throws TypeError' , () =>
    {
        throws( () => isEmpty(undefined) , TypeError ) ;
    });
});
