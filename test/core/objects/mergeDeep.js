'use strict'

import dump from '../../../src/dump'
import mergeDeep from '../../../src/objects/mergeDeep'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.mergeDeep' , () =>
{
    it('mergeDeep({ \'a\': [{ \'b\': { \'c\': 3 } }, 4] }, "a[1]") === [3, 4]' , () =>
    {
        const object = { a: [ { b: { c: 3 } }, 4] }  ;
        const other  = { a: [ { c: 3 } , { e: 5 } ] } ;

        mergeDeep( object , other ) ;

        assert.equal( dump(object) , '{a:[{b:{c:3},c:3},{e:5}]}' ) ; //  { a: [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
    });
});
