'use strict'

import pick from '../../../src/objects/pick'

import chai from 'chai' ;

const { assert } = chai ;

const { deepEqual } = assert ;

describe( 'core.objects.pick' , () =>
{
    let source = { a : 1 , b : 2 , c: 3 } ;
    it('pick({a:1,b:2,c:3},[\'a\',\'c\'])' , () =>
    {
        let result = pick( source, ['a', 'c']) ;
        deepEqual( result , { a : 1 , c: 3 });
    });

});
