'use strict'

import map from '../../../src/objects/map'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.map' , () =>
{
    it('map({ a : 2 , b : 4 }), n => n * n )' , () =>
    {
        const object = { a : 2 , b : 4 }  ;
        const result = map( object, n => n * n) ;
        assert.isObject( result ) ;
        assert.equal( result.a , 4 ) ;
        assert.equal( result.b , 16 ) ;
    });
});
