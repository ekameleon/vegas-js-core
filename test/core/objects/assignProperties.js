'use strict'

import assignProperties from '../../../src/objects/assignProperties'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.assignProperties' , () =>
{
    it('assignProperties({ a:1, b:2, c:3 }, ["a"] ) === { a:1 } ' , () =>
    {
        let source = { a : 1 , b : 2 , c: 3 } ;
        assert.equal( JSON.stringify( assignProperties( source , ["a"] )) , '{"a":1}' );
    });

    it('assignProperties({ a:1, b:2, c:3 }, ["a","c"] ) === { a:1 , c:3 } ' , () =>
    {
        let source = { a : 1 , b : 2 , c: 3 } ;
        assert.equal( JSON.stringify( assignProperties( source , ["a","c"] )) , '{"a":1,"c":3}' );
    });

    it('assignProperties({ a:1, b:2, c:null }, ["a","c"] , true ) === { a:1 } ' , () =>
    {
        let source = { a : 1 , b : 2 , c: null } ;
        assert.equal( JSON.stringify( assignProperties( source , ["a","c"] , true )) , '{"a":1}' );
    });
});
