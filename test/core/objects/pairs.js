'use strict'

import pairs from '../../../src/objects/pairs'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.pairs' , () =>
{
    it('pairs( { one: 1, two: 2, three: 3 } ) === [["one", 1], ["two", 2], ["three", 3]]' , () =>
    {
        const array = pairs( { one: 1, two: 2, three: 3 } ) ;

        assert.isArray( array );
        assert.equal( array.length , 3 );

        assert.isArray( array[0] );
        assert.equal( array[0].length , 2  );
        assert.equal( array[0][0] , 'one' );
        assert.equal( array[0][1] , 1 );

        assert.isArray( array[1] );
        assert.equal( array[1].length , 2  );
        assert.equal( array[1][0] , 'two' );
        assert.equal( array[1][1] , 2 );

        assert.isArray( array[2] );
        assert.equal( array[2].length , 2  );
        assert.equal( array[2][0] , 'three' );
        assert.equal( array[2][1] , 3 );
    });

});
