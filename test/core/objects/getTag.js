'use strict'

import getTag from '../../../src/objects/getTag'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.getTag' , () =>
{
    it('getTag([2,3]) === [object Array]' , () =>
    {
        assert.equal( getTag([2,3]) , '[object Array]' );
    });
});
