'use strict'

import keysIn from '../../../src/objects/keysIn'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.keysIn' , () =>
{
    it('keysIn( new Foo() ) === [\'a\', \'b\', \'c\]' , () =>
    {
        function Foo()
        {
            this.a = 1;
            this.b = 2;
        }
        Foo.prototype.c = 3;

        const array = keysIn(new Foo()) ;

        assert.isArray( array );
        assert.equal( array.length , 3 );
        assert.equal( array[0] , 'a' );
        assert.equal( array[1] , 'b' );
        assert.equal( array[2] , 'c' );
    });
});
