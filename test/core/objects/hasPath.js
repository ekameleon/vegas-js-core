"use strict"

import hasPath from '../../../src/objects/hasPath'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.hasPath' , () =>
{
    let object ;

    beforeEach( () =>
    {
        object = { 'a': { 'b': 2 } } ;
    });

    it('hasPath(object, \'a.b\') === true' , () =>
    {
        assert.isTrue( hasPath(object, 'a.b') );
    });

    it('hasPath(object, [\'a\', \'b\']) === true' , () =>
    {
        assert.isTrue( hasPath(object, ['a' , 'b' ] ) );
    });
});
