'use strict'

import isEmpty from '../../../src/objects/isEmpty'
import stub from '../../../src/objects/stub'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.objects.stub' , () =>
{
    it('stub() === {}', () =>
    {
        const object = stub() ;
        assert.isObject( object ) ;
        assert.isTrue( isEmpty(object) ) ;
    });
});
