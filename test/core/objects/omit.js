'use strict'

import omit from '../../../src/objects/omit'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.omit' , () =>
{
    it('omit( { a:1 , b:2 , c:3 } , ["a","c"] ) === { b:2 }' , () =>
    {
        const result = omit( { a:1 , b:2 , c:3 } , ['a','c'] ) ;
        assert.isObject( result );
        assert.deepInclude( result , { b : 2 });
        assert.notDeepEqual( result , { a:1 , c:3 } );
    });
});
