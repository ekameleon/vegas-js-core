'use strict'

import update from '../../../src/objects/update'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.update' , () =>
{
    let object ;

    beforeEach( () =>
    {
        object = { 'a': [ { 'b': { 'c': 3 } } ] } ;
    });

    it("update( object, 'a[0].b.c', n => n * n )" , () =>
    {
        object = update( object , 'a[0].b.c', n => n * n ) ;
        assert.equal( object.a[0].b.c , 9 );
    });

    it("update(object, 'x[0].y.z', n => n ? n + 1 : 0 )" , () =>
    {
        object = update(object, 'x[0].y.z', n =>
        {
            return n ? ( n + 1 ) : 0 ;
        } );
        assert.equal( object.x[0].y.z , 0 );
    });
});
