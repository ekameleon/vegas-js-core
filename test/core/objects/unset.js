'use strict'

import unset from '../../../src/objects/unset'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.unset' , () =>
{
    let object ;

    beforeEach( () =>
    {
        object = { 'a': [ { 'b': { 'c': 3 } } ] } ;
    });

    it("unset(object, 'a[0].b.c')" , () =>
    {
        const flag = unset( object , 'a[0].b.c' ) ;
        assert.isTrue( flag );
        assert.equal( JSON.stringify( object ) , '{"a":[{"b":{}}]}' );
    });
});
