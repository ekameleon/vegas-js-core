'use strict'

import at from '../../../src/objects/at'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.at' , () =>
{
    it('at({ \'a\': [{ \'b\': { \'c\': 3 } }, 4] }, "a[1]") === [3, 4]' , () =>
    {
        const object = { 'a': [{ 'b': { 'c': 3 } }, 4] }  ;
        const result = at( object , 'a[1]' ) ;
        assert.isArray( result ) ;
        assert.equal( result.length , 1 ) ;
        assert.equal( result[0] , 4 ) ;
    });

    it('at({ \'a\': [{ \'b\': { \'c\': 3 } }, 4] }, "a[1]") === [3, 4]' , () =>
    {
        const object = { 'a': [{ 'b': { 'c': 3 } }, 4] }  ;
        const result = at( object , 'a[0].b.c' ,'a[1]' ) ;
        assert.isArray( result ) ;
        assert.equal( result.length , 2 ) ;
        assert.equal( result[0] , 3 ) ;
        assert.equal( result[1] , 4 ) ;
    });

});
