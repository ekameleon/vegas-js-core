'use strict'

import renameProperty from '../../../src/objects/renameProperty'

import chai from 'chai' ;

const { assert } = chai ;

const {
    deepEqual
} = assert ;

describe( 'core.objects.renameProperty' , () =>
{
    const user = { id:1, name: 'John Doe', password: 'PWD' } ;
    
    const renameID = renameProperty('id','ID');
    
    const object = renameID( user ) ;
    
    //console.log( user ) ;
    //console.log( object ) ;
    
    it('renameID = renameProperty("id","ID")' , () =>
    {
        deepEqual( object , { ID:1, name: 'John Doe', password: 'PWD' } );
    });
});
