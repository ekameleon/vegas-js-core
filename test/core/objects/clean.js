'use strict'

import clean from '../../../src/objects/clean'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.clean' , () =>
{
    it('clean( { a:1 , b:null , c:3 } ) === { a:1 , c:3 }' , () =>
    {
        let source = { a : 1 , b : null , c: 3 } ;
        assert.equal( JSON.stringify( clean( source ) ) , '{"a":1,"c":3}' );
    });

    it('clean( { a:1 , b:null , c:3 } , replacer ) === { a:1 }' , () =>
    {
        const replacer = ( key , value ) =>
        {
            if ( key === 'c' || value === null || value === undefined )
            {
                return undefined;
            }
            return value ;
        };

        let source = { a : 1 , b : null , c: 3 } ;
        assert.equal( JSON.stringify( clean( source , replacer ) ) , '{"a":1}' );
    });
});
