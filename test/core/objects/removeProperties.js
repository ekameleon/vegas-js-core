'use strict'

import removeProperties from '../../../src/objects/removeProperties'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.removeProperties' , () =>
{
    it('removeProperties({ a:1, b:2, c:3 }, ["a"] ) === { b:2 , c:3 } ' , () =>
    {
        let source = { a : 1 , b : 2 , c: 3 } ;
        assert.equal( JSON.stringify( removeProperties( source , ["a"] )) , '{"b":2,"c":3}' );
    });

    it('removeProperties({ a:1, b:2, c:3 }, ["a","b"] ) === { c:3 } ' , () =>
    {
        let source = { a : 1 , b : 2 , c: 3 } ;
        assert.equal( JSON.stringify( removeProperties( source , ["a","b"] )) , '{"c":3}' );
    });

    it('removeProperties({ a:1, b:2, c:3 }, ["a","b","c"] ) === {} ' , () =>
    {
        let source = { a : 1 , b : 2 , c: 3 } ;
        assert.equal( JSON.stringify( removeProperties( source , ["a","b","c"] )) , '{}' );
    });
});
