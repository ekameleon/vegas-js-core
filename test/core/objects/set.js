'use strict'

import set from '../../../src/objects/set'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.set' , () =>
{
    let object ;

    beforeEach( () =>
    {
        object = { 'a': [ { 'b': { 'c': 3 } } ] } ;
    });

    it("set(object, 'a[0].b.c', 4)" , () =>
    {
        object = set( object , 'a[0].b.c', 4 ) ;
        assert.equal( object.a[0].b.c , 4 );
    });

    it("set(object, ['x', '0', 'y', 'z'], 5)" , () =>
    {
        object = set(object, [ 'x' , '0' , 'y' , 'z' ] , 5 ) ;
        assert.equal( object.x[0].y.z , 5 );
    });

    it("set(object, 'd.e', 9)" , () =>
    {
        object = set( object, 'd.e', 9 ) ;
        assert.equal( object.d.e , 9 );
    });

    it("set(object, 'a[0].test[0].h', 15)" , () =>
    {
        object = set( object, 'a[0].test[0].h', 15 ) ;
        object = set( object, 'a[0].test[1].h', 25 ) ;
        assert.equal( object.a[0].test[0].h , 15 );
        assert.equal( object.a[0].test[1].h , 25 );
    });
});
