'use strict'

import isNumber from '../../../src/isNumber'
import pick from '../../../src/objects/pickBy'

import chai from 'chai' ;

const { assert } = chai ;

const { deepEqual } = assert ;

describe( 'core.objects.pickBy' , () =>
{
    it('pickBy({a:1,b:"hello",c:3},isNumber) === { a : 1 , c : 3}' , () =>
    {
        let source = { a : 1 , b : 'hello' , c : 3 } ;
        let result = pick( source, isNumber) ;
        deepEqual( result , { a : 1 , c: 3 });
    });
});
