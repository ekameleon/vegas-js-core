'use strict'

import keys from '../../../src/objects/keys'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.keys' , () =>
{
    it('keys( new Foo() ) === [\'a\', \'b\']' , () =>
    {
        function Foo()
        {
            this.a = 1;
            this.b = 2;
        }
        Foo.prototype.c = 3;

        const array = keys(new Foo()) ;

        assert.isArray( array );
        assert.equal( array.length , 2 );
        assert.equal( array[0] , 'a' );
        assert.equal( array[1] , 'b' );
    });

    it('keys( "hi") === [\'0\', \'1\']' , () =>
    {
        const array = keys('hi') ;

        assert.isArray( array );
        assert.equal( array.length , 2 );
        assert.equal( array[0] , '0' );
        assert.equal( array[1] , '1' );
    });
});
