'use strict'

import get from '../../../src/objects/get'

import chai from 'chai' ;

const { assert } = chai ;

const object = { 'a': [{ 'b': { 'c': 3 } }] }

describe( 'core.objects.get' , () =>
{
    it('get(object, "a[0].b.c") === 3' , () =>
    {
        assert.equal( get(object, 'a[0].b.c') , 3 );
    });
    
    it("get(object, ['a', '0', 'b', 'c']) === 3" , () =>
    {
        assert.equal( get(object, ['a', '0', 'b', 'c']) , 3 );
    });
    
    it('get(object, "a[0].b.d") === null' , () =>
    {
        assert.isNull( get(object, 'a[0].b.d') );
    });
    
    it('get(object, "a[0].b.d", 5) === 5' , () =>
    {
        assert.equal( get(object, 'a[0].b.d' , 5 ) , 5 );
    });
});
