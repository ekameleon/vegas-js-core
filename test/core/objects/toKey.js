'use strict'

import toKey from '../../../src/objects/toKey'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.objects.toKey' , () =>
{
    it('toKey("foo") === "foo"' , () =>
    {
        assert.equal( toKey('foo') , 'foo' );
    });
    it('toKey([2,3]) === "2,3"' , () =>
    {
        assert.equal( toKey([2,3]) , '2,3' );
    });
});
