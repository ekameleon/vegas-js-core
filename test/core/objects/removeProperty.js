'use strict'

import removeProperty from '../../../src/objects/removeProperty'

import chai from 'chai' ;

const { assert } = chai ;

const {
    deepEqual,
    notInclude,
    notDeepEqual
} = assert ;

describe( 'core.objects.removeProperty' , () =>
{
    const user1 = { id:1, name: 'John Doe' , password: 'PWD' } ;
    const user2 = { id:1, name: 'John Doe' , test:'hello' } ;
    
    const removePassword = removeProperty('password');
    
    const object1 = removePassword( user1 ) ;
    const object2 = removePassword( user2 ) ;
    
    it('removeProperty("password") not deep equal' , () =>
    {
        notDeepEqual( user1 , object1 );
        deepEqual( user2 , object2 );
    });
    
    it('removeProperty("password") not include' , () =>
    {
        notInclude( object1 , { password: 'PWD' } );
        notInclude( object2 , { password: 'PWD' } );
    });
});
