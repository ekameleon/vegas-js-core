/*jshint -W053 */
'use strict'

import isEmpty from '../../src/isEmpty'

import chai from 'chai'

const { assert } = chai ;

const {
    isTrue,
    isFalse
} = assert ;

describe( 'core.isEmpty' , () =>
{
    it('isEmpty([]) === true'       , () => { isTrue( isEmpty([]) ); });
    it('isEmpty([1,2,3]) === false' , () => { isFalse( isEmpty([1,2,3]) ); });
    
    it('isEmpty("") === true'       , () => { isTrue( isEmpty('') ); });
    it('isEmpty("hello") === false' , () => { isFalse( isEmpty('hello') ); });
    
    it('isEmpty({}) === true'       , () => { isTrue( isEmpty({}) ); });
    it('isEmpty({a:0}) === false'   , () => { isFalse( isEmpty({a:0}) ); });
    
    it('isEmpty(true) === false'    , () => { isFalse( isEmpty(true) ); });
    it('isEmpty(false) === false'   , () => { isFalse( isEmpty(false) ); });
    it('isEmpty(null) === false'    , () => { isFalse( isEmpty(null) ); });
});
