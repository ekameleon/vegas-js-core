/*jshint -W053 */

'use strict'

import isPrimitive from '../../src/isPrimitive'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.isPrimitive' , () =>
{
    it('isPrimitive(null) === true' , () => { assert.isTrue( isPrimitive(null) ) } );
    it('isPrimitive(undefined) === true' , () => { assert.isTrue( isPrimitive(undefined) ) } );
    it('isPrimitive(123) === true' , () => { assert.isTrue( isPrimitive(123) ) } );
    it('isPrimitive("string") === true' , () => { assert.isTrue( isPrimitive("string") ) } );
    it('isPrimitive(true) === true' , () => { assert.isTrue( isPrimitive(true) ) } );
    it('isPrimitive(false) === true' , () => { assert.isTrue( isPrimitive(false) ) } );
    it('isPrimitive(Symbol(\'a\')) === true' , () => { assert.isTrue( isPrimitive(Symbol('a')) ) } );
    it('isPrimitive(Symbol(123n)) === true' , () => { assert.isTrue( isPrimitive(123n ) ) } );

    it('isPrimitive({}) === false' , () => { assert.isFalse( isPrimitive({} ) ) } );
    it('isPrimitive(new Date()) === false' , () => { assert.isFalse( isPrimitive( new Date() ) ) } );
    it('isPrimitive(new Map()) === false' , () => { assert.isFalse( isPrimitive( new Map() ) ) } );
    it('isPrimitive(new Set()) === false' , () => { assert.isFalse( isPrimitive( new Set() ) ) } );
    it('isPrimitive(["foo","bar"]) === false' , () => { assert.isFalse( isPrimitive( ["foo","bar"] ) ) } );
});
