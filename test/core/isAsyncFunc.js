/*jshint -W053 */
'use strict'

import isAsyncFunction from '../../src/isAsyncFunction'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.isAsyncFunc' , () =>
{
    it('isAsyncFunction(0)     === false' , () => { assert.isFalse( isAsyncFunction(0) ); });
    it('isAsyncFunction(true)  === false' , () => { assert.isFalse( isAsyncFunction(true) ); });
    it('isAsyncFunction("foo") === false' , () => { assert.isFalse( isAsyncFunction("foo") ); });
    it('isAsyncFunction(null)  === false' , () => { assert.isFalse( isAsyncFunction(null) ); });
    it('isAsyncFunction(NaN)   === false' , () => { assert.isFalse( isAsyncFunction(NaN) ); });
    
    it('isFunction(()=>{}) === false' , () => { assert.isFalse( isAsyncFunction(() => {}) ); });
    it('isFunction(async ()=>{}) === true' , () => { assert.isFalse( isAsyncFunction(async () => {}) ); });
});
