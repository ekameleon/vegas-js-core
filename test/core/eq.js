/*jshint -W053 */
'use strict'

import eq from '../../src/eq'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

const object = { 'a': 1 } ;
const other  = { 'a': 1 } ;

describe( 'core.eq' , () =>
{
    it("eq('a','a') === true"           , () => {  isTrue( eq('a', 'a') ); });
    it('eq(object, object) === true'    , () => {  isTrue( eq(object, object) ); });
    it('eq(object, other)  === false'   , () => { isFalse( eq(object, other) ); });
    it("eq('a', Object('a'))  === true" , () => { isFalse( eq('a', Object('a')) ); });
    it('eq(object, other)  === false'   , () => { isFalse( eq(object, other) ); });
});
