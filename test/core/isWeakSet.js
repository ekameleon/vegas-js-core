'use strict'

import isWeakSet from '../../src/isWeakSet'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isWeakSet' , () =>
{
    it('isWeakSet(new Set()) === false' , () => { isFalse( isWeakSet(new Set()) ); });
    it('isWeakSet(new WeakSet()) === trie' , () => { isTrue( isWeakSet(new WeakSet()) ); });
});
