'use strict'

import resizeWidthAndKeepAspectRatio from '../../../src/graphics/resizeWidthAndKeepAspectRatio'

import chai from 'chai' ;

const { assert : { deepEqual } } = chai ;

describe( 'core.graphics.resizeWidthAndKeepAspectRatio' , () =>
{
    it('resizeWidthAndKeepAspectRatio(480,320,200) === { width : 480 , height : 300 }' , () =>
    {
        deepEqual( resizeWidthAndKeepAspectRatio(480,320,200)  , { width : 480 , height : 300 } );
    });

    it('resizeWidthAndKeepAspectRatio(720,320,200) === { width : 720 , height : 450 }' , () =>
    {
        deepEqual( resizeWidthAndKeepAspectRatio(720,320,200)  , { width : 720 , height : 450 } );
    });
});
