'use strict'

import resizeHeightAndKeepAspectRatio from '../../../src/graphics/resizeHeightAndKeepAspectRatio'

import chai from 'chai' ;

const { assert : { deepEqual } } = chai ;

describe( 'core.graphics.resizeHeightAndKeepAspectRatio' , () =>
{
    it('resizeHeightAndKeepAspectRatio(300,320,200) === { width : 480 , height : 300 }' , () =>
    {
        deepEqual( resizeHeightAndKeepAspectRatio(300,320,200)  , { width : 480 , height : 300 } );
    });

    it('resizeHeightAndKeepAspectRatio(450,320,200) === { width : 720 , height : 450 }' , () =>
    {
        deepEqual( resizeHeightAndKeepAspectRatio(450,320,200)  , { width : 720 , height : 450 } );
    });
});
