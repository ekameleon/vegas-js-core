/*jshint -W053 */
'use strict'

import isLength from '../../src/isLength'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.isLength' , () =>
{
    it('isLength( 3 ) === true' , () =>
    {
        assert.isTrue( isLength( 3 ) );
    });

    it('isLength( Number.MIN_VALUE ) === false' , () =>
    {
        assert.isFalse( isLength( Number.MIN_VALUE ) );
    });

    it('isLength( Infinity ) === false' , () =>
    {
        assert.isFalse( isLength( Infinity ) );
    });

    it('isLength( \'3\' ) === false' , () =>
    {
        assert.isFalse( isLength( '3' ) );
    });
});
