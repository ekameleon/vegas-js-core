/*jshint -W053 */
'use strict'

import isMap from '../../src/isMap'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isMap' , () =>
{
    it('isMap(new Map()) === true' , () => { isTrue( isMap(new Map()) ); });
    it('isMap(new WeakMap()) === false' , () => { isFalse( isMap(new WeakMap()) ); });
});
