'use strict'

import dump from "../../src/dump"
import deepMerge from "../../src/deepMerge"

import chai from 'chai' ;

const { assert } = chai ;

const { equal } = assert ;

const verbose = false ;

let user1 =
{
    name :
    {
        first : 'John',
        last  : 'Doe'
    },
    pets: ['Cat', 'Parrot'],
    info  : { name : 'ekameleon' },
    roles : [ { name  : 'admin', value : [ 1, 2, 3 ] }]
};

let user2 =
{
    name :
    {
        first : 'Jeanne',
        last  : 'Doe'
    },
    pets    : ['Dog'] ,
    info    : { age : 42 },
	roles   : [ { name  : 'superadmin', value : [ 4, 5, 6 ] }] ,
	options : { isAdmin : 'yes' }
};

if( verbose )
{
    // Overwrite Array
    const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray ;
    console.log( dump(deepMerge( user1, user2, { arrayMerge: overwriteMerge } )) ) ;

    // Combine Array
    const emptyTarget = value => Array.isArray(value) ? [] : {} ;
    const clone = (value, options) => deepMerge(emptyTarget(value), value, options) ;

    const combineMerge = (target, source, options) =>
    {
        const destination = [ ...target ] ;
        source.forEach( (e, i) =>
        {
            if (typeof destination[i] === 'undefined')
            {
                const cloneRequested = options.clone !== false;
                const shouldClone = cloneRequested && options.isMergeableObject(e);
                destination[i] = shouldClone ? clone(e, options) : e;
            }
            else if (options.isMergeableObject(e))
            {
                destination[i] = deepMerge(target[i], e, options);
            }
            else if (target.indexOf(e) === -1)
            {
                destination.push(e);
            }
        });
        return destination ;
    };

    console.log( deepMerge( [{ a: true }], [{ b: true }, 'hello'], { arrayMerge: combineMerge } ) ) ;
    // [{ a: true, b: true }, 'hello']

    // Custom merge

    const mergeNames = (nameA, nameB) => `${nameA.first} and ${nameB.first}` ;
    const options =
    {
        customMerge : (key) =>
        {
            if (key === 'name')
            {
                return mergeNames ;
            }
        }
    };

    const user = deepMerge(user1, user2, options);

    console.log(user.name) ; // John and Jeanne
    console.log(user.pets) ; // ['Cat', 'Parrot', 'Dog']
}

describe( 'core.deepMerge' , () =>
{
    const user = deepMerge( user1, user2 ) ;

    it("user.info.name       === 'ekameleon'"  , () => { equal(user.info.name , 'ekameleon' ) });
    it("user.info.age        === 42"           , () => { equal(user.info.age , 42 ) });
    it("user.roles[0].name   === 'admin'"      , () => { equal(user.roles[0].name , 'admin') });
    it("user.roles[1].name   === 'superadmin'" , () => { equal(user.roles[1].name , 'superadmin') });
    it("user.options.isAdmin === 'yes'"        , () => { equal(user.options.isAdmin , 'yes' ) });
});
