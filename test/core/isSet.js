/*jshint -W053 */
'use strict'

import isSet from '../../src/isSet'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isSet' , () =>
{
    it('isSet(new Set()) === true' , () => { isTrue( isSet(new Set()) ); });
    it('isSet(new WeakSet()) === false' , () => { isFalse( isSet(new WeakSet()) ); });
});
