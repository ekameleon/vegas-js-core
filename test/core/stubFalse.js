/*jshint -W053 */
'use strict'

import stubFalse from '../../src/stubFalse'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse } = assert ;

describe( 'core.stubFalse' , () =>
{
    it('stubFalse() === false' , () =>
    {
        isFalse( stubFalse() );
    });
});
