/*jshint -W053 */
'use strict'

import stubTrue from '../../src/stubTrue'

import chai from 'chai' ;

const { assert } = chai ;

const { isTrue } = assert ;

describe( 'core.stubTrue' , () =>
{
    it('stubTrue() === true' , () =>
    {
        isTrue( stubTrue() );
    });
});
