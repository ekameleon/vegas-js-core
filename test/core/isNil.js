/*jshint -W053 */
'use strict'

import isNil from '../../src/isNil'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isNil' , () =>
{
    it('isNil(null) === true' , () => { isTrue( isNil(null) ); });
    it('isNil(undefined) === true' , () => { isTrue( isNil(undefined) ); });
    
    let value1 ;
    let value2 = null ;
    let value3 = undefined ;
    let value4 = 'hello' ;
    
    it('isNil(value1) === true' , () => { isTrue( isNil(value1) ); });
    it('isNil(value2) === true' , () => { isTrue( isNil(value2) ); });
    it('isNil(value3) === true' , () => { isTrue( isNil(value3) ); });
    it('isNil(value4) === false' , () => { isFalse( isNil(value4) ); });
});
