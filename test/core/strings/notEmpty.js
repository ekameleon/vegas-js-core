'use strict'

import notEmpty from '../../../src/strings/notEmpty'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.notEmpty' , () =>
{
    let value1 ;
    let value2 = "hello" ;
    let value3 = '' ;
    
    it('notEmpty(value1) === false', () =>
    {
        assert.isFalse( notEmpty(value1) ) ;
    });
    
    it('notEmpty(value2) === true', () =>
    {
        assert.isTrue( notEmpty(value2) ) ;
    });
    
    it('notEmpty(value3) === false', () =>
    {
        assert.isFalse( notEmpty(value3) ) ;
    });
});
