'use strict'

import hex2bin from '../../../src/strings/hex2bin'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.hex2bin' , () =>
{
    it("hex2bin('44696d61') === 'Dima'", () =>
    {
        assert.equal( hex2bin('44696d61') , 'Dima' ) ;
    });

    it("hex2bin('00') === '\x00'", () =>
    {
        assert.equal( hex2bin('00') , '\x00' ) ;
    });

    it("hex2bin('2f1q') === false", () =>
    {
        assert.isFalse( hex2bin('2f1q') ) ;
    });
});
