'use strict'

import trigram from '../../../src/strings/trigram'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.trigram' , () =>
{
    it('trigram(\'n-gram\') === [\'n-g\', \'-gr\', \'gra\', \'ram\']', () =>
    {
        const result = trigram('n-gram') ;
        assert.isArray( result ) ;
        assert.equal( result.length , 4 ) ;
        assert.equal( result[0] , 'n-g' ) ; // 'n-g', '-gr', 'gra', 'ram'
        assert.equal( result[1] , '-gr' ) ;
        assert.equal( result[2] , 'gra' ) ;
        assert.equal( result[3] , 'ram' ) ;
    });
});
