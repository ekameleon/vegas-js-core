'use strict'

import empty from '../../../src/strings/empty'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.empty' , () =>
{
    let value1 ;
    let value2 = "hello" ;
    let value3 = '' ;
    
    it('empty(value1) === false', () =>
    {
        assert.isFalse( empty(value1) ) ;
    });
    
    it('empty(value2) === false', () =>
    {
        assert.isFalse( empty(value2) ) ;
    });
    
    it('empty(value3) === true', () =>
    {
        assert.isTrue( empty(value3) ) ;
    });
});
