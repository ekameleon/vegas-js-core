'use strict'

import snakeCase from '../../../src/strings/snakeCase'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.snakeCase' , () =>
{
    it('snakeCase() === ""', () =>
    {
        assert.equal( snakeCase("") , "" ) ;
    });
    
    it('snakeCase(null) === ""', () =>
    {
        assert.isNull( snakeCase() ) ;
        assert.isNull( snakeCase(null) ) ;
    });
    
    it('snakeCase(Foo Bar) === foo_bar', () =>
    {
        assert.equal( snakeCase("Foo Bar") , "foo_bar" ) ;
    });
});
