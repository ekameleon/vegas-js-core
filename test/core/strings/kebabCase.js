'use strict'

import kebabCase from '../../../src/strings/kebabCase'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.kebabCase' , () =>
{
    it('kebabCase() === ""', () =>
    {
        assert.equal( kebabCase() , "" ) ;
    });

    it('kebabCase(null) === ""', () =>
    {
        assert.equal( kebabCase(null) , "" ) ;
    });

    it('kebabCase(1) === ""', () =>
    {
        assert.equal( kebabCase(1) , "" ) ;
    });

    it('kebabCase("foo") === "foo"', () =>
    {
        assert.equal( kebabCase("foo") , "foo" ) ;
    });

    it('kebabCase("helloWorld") == "hello-world"', () =>
    {
        assert.equal( kebabCase("helloWorld") , "hello-world" ) ;
    });


    it('kebabCase("Hello World") == "hello-world"', () =>
    {
        assert.equal( kebabCase("Hello World") , "hello-world" ) ;
    });

    it('kebabCase("__HELLO__WORLD__") == "hello-world"', () =>
    {
        assert.equal( kebabCase("__HELLO__WORLD__") , "hello-world" ) ;
    });
});
