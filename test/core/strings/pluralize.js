'use strict'

import fastformat from '../../../src/strings/fastformat'
import pluralize  from '../../../src/strings/pluralize'

import chai from 'chai' ;

const { assert } = chai ;

const options =
{
    format : ( { expression , count , plural , singular } = {} ) =>
    {
       const params = count > 1 ? plural : singular ;
       const { one , two } = params || {} ;
       return fastformat( expression , one , two ) ;
    },
    plural : { one:'mes' , two : 'x' } ,
    singular : { one : 'mon' , two : '' }
};

describe( 'core.strings.pluralize' , () =>
{
    it('pluralize( "cat" , 0 )  === "cat"', () =>
    {
        assert.equal( pluralize( "cat" , 0 ) , "cat" ) ;
    });

    it('pluralize( "cat" , 1 )  === "cat"', () =>
    {
        assert.equal( pluralize( "cat" , 1 ) , "cat" ) ;
    });

    it('pluralize( "cat" , 2 )  === "cat"', () =>
    {
        assert.equal( pluralize( "cat" , 2 ) , "cats" ) ;
    });

    it('pluralize( "bijou" , 2 , { plurial:"x" } )  === "bijoux"', () =>
    {
        assert.equal( pluralize( "bijou" , 2 , { plural:"x" } ) , "bijoux" ) ;
    });

    it( 'pluralize( \'{0} bijou{1}\' , 1 , options ) === "mon bijou"' , () =>
    {
        assert.equal( pluralize( '{0} bijou{1}' , 1 , options ) , "mon bijou" ) ;
    });

    it( 'pluralize( \'{0} bijou{1}\' , 2 , options ) === "mes bijoux"' , () =>
    {
        assert.equal( pluralize( '{0} bijou{1}' , 2 , options ) , "mes bijoux" ) ;
    });
});
