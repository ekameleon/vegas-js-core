'use strict'

import pascalCase from '../../../src/strings/pascalCase'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.pascalCase' , () =>
{
    it('pascalCase() === ""', () =>
    {
        assert.equal( pascalCase() , "" ) ;
    });

    it('pascalCase("pascalCase") === "PascalCase"', () =>
    {
        assert.equal( pascalCase("pascalCase") , "PascalCase" ) ;
    });

    it('pascalCase("some whitespace") === "SomeWhitespace"', () =>
    {
        assert.equal( pascalCase("some whitespace") , "SomeWhitespace" ) ;
    });

    it('pascalCase("hyphen-text") === "HyphenText"', () =>
    {
        assert.equal( pascalCase("hyphen-text") , "HyphenText" ) ;
    });
});
