'use strict'

import isEmpty from '../../../src/strings/empty'
import stub from '../../../src/strings/stub'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.stub' , () =>
{
    it('stub() === ""', () =>
    {
        const string = stub() ;
        assert.isString( string ) ;
        assert.isTrue( isEmpty(string) ) ;
        assert.equal( string , '' ) ;
    });
});
