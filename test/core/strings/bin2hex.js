'use strict'

import bin2hex from '../../../src/strings/bin2hex'
import hex2bin from '../../../src/strings/hex2bin'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.bin2hex' , () =>
{
    it("bin2hex('Kev') === '4b6576'", () =>
    {
        assert.equal( bin2hex('Kev' ) , '4b6576' ) ;
        assert.equal( hex2bin(bin2hex('Kev' )) , 'Kev' ) ;
    });

    it("bin2hex(String.fromCharCode(0x00)) === '00'", () =>
    {
        assert.equal( bin2hex(String.fromCharCode(0x00)) , '00' ) ;
    });

    it("bin2hex('æ') === 'c3a6'", () =>
    {
        assert.equal( bin2hex('æ' ) , 'c3a6' ) ;
    });
});
