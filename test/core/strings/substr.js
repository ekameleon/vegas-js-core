'use strict'

import substr from '../../../src/strings/substr'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.substr' , () =>
{
    const string = "Mozilla";

    it(`substr( "${string}" , 0 , 1 ) === 'M'`, () =>
    {
        assert.equal( substr( string , 0 , 1 ) , 'M' ) ;
    });

    it(`substr( "${string}" , 1 , 0 ) === ''`, () =>
    {
        assert.equal( substr( string , 1 , 0 ) , '' ) ;
    });

    it(`substr( "${string}" , -1 , 1 ) === 'a'`, () =>
    {
        assert.equal( substr( string , -1 , 1 ) , 'a' ) ;
    });

    it(`substr( "${string}" , 1 , -1 ) === ''`, () =>
    {
        assert.equal( substr( string , 1 , -1 ) , '' ) ;
    });

    it(`substr( "${string}" , -3 ) === 'lla'`, () =>
    {
        assert.equal( substr( string , -3 ) , 'lla' ) ;
    });

    it(`substr( "${string}" , 1 ) === 'ozilla'`, () =>
    {
        assert.equal( substr( string , 1 ) , 'ozilla' ) ;
    });

    it(`substr( "${string}" , -20 , 2 ) === 'Mo'`, () =>
    {
        assert.equal( substr( string , -20 , 2 ) , 'Mo' ) ;
    });

    it(`substr( "${string}" , 20 , 2 ) === ''`, () =>
    {
        assert.equal( substr( string , 20 , 2 ) , '' ) ;
    });
});
