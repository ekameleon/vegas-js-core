'use strict'

import toString from '../../../src/strings/toString'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.toString' , () =>
{
    let value1 ;
    let value2 = "hello" ;
    let value3 = 12 ;
    
    it('toString(value1) === ""', () =>
    {
        assert.equal( toString(value1) , "" ) ;
    });

    it('toString(value1,"unknow") === "unknow"', () =>
    {
        assert.equal( toString(value1,"unknow") , "unknow" ) ;
    });

    it('toString(value2) === "hello"', () =>
    {
        assert.equal( toString(value2) , "hello" ) ;
    });

    it('toString(value3) === "12"', () =>
    {
        assert.equal( toString(value3) , "12" ) ;
    });
});
