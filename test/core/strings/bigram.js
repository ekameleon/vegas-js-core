'use strict'

import bigram from '../../../src/strings/bigram'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.bigram' , () =>
{
    it('bigram(\'n-gram\') === "[\'n-\', \'-g\', \'gr\', \'ra\', \'am\']"', () =>
    {
        const result = bigram('n-gram') ;
        assert.isArray( result ) ;
        assert.equal( result.length , 5 ) ;
        assert.equal( result[0] , 'n-' ) ;
        assert.equal( result[1] , '-g' ) ;
        assert.equal( result[2] , 'gr' ) ;
        assert.equal( result[3] , 'ra' ) ;
        assert.equal( result[4] , 'am' ) ;
    });

    it('bigram([\'alpha\', \'bravo\', \'charlie\']) === "[[\'alpha\', \'bravo\'], [\'bravo\', \'charlie\']]"', () =>
    {
        const result = bigram(['alpha', 'bravo', 'charlie']) ;
        assert.isArray( result ) ;
        assert.equal( result.length , 2 ) ;

        assert.isArray( result[0] ) ;
        assert.equal( result[0][0] , 'alpha' ) ;
        assert.equal( result[0][1] , 'bravo' ) ;

        assert.isArray( result[1] ) ;
        assert.equal( result[1][0] , 'bravo' ) ;
        assert.equal( result[1][1] , 'charlie' ) ;
    });
});
