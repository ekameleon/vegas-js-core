'use strict'

import diceCoefficient from '../../../src/strings/diceCoefficient'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.diceCoefficient' , () =>
{
    it('diceCoefficient(\'abc\', \'abc\') === 1', () =>
    {
        assert.equal( diceCoefficient('abc', 'abc') , 1 ) ;
    });

    it('diceCoefficient(\'abc\', \'xyz\') === 0', () =>
    {
        assert.equal( diceCoefficient('abc', 'xyz') , 0 ) ;
    });

    it('diceCoefficient(\'night\', \'nacht\') === 0.25', () =>
    {
        assert.equal( diceCoefficient('night', 'nacht') , 0.25 ) ;
    });

    it('diceCoefficient(\'night\', \'nacht\') === diceCoefficient(\'NiGhT\', \'NACHT\')', () =>
    {
        assert.equal( diceCoefficient('night', 'nacht') , diceCoefficient('NiGhT', 'NACHT')  ) ;
    });
});
