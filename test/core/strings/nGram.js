'use strict'

import nGram from '../../../src/strings/nGram'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.strings.nGram' , () =>
{
    it('nGram(2)(\'n-gram\') === "[\'n-\', \'-g\', \'gr\', \'ra\', \'am\']"', () =>
    {
        const result = nGram(2)('n-gram') ;
        assert.isArray( result ) ;
        assert.equal( result.length , 5 ) ;
        assert.equal( result[0] , 'n-' ) ;
        assert.equal( result[1] , '-g' ) ;
        assert.equal( result[2] , 'gr' ) ;
        assert.equal( result[3] , 'ra' ) ;
        assert.equal( result[4] , 'am' ) ;
    });

    it('nGram(6)(\'n-gram\') === "[\'n-gram\']"', () =>
    {
        const result = nGram(6)('n-gram') ;
        assert.isArray( result ) ;
        assert.equal( result.length , 1 ) ;
        assert.equal( result[0] , 'n-gram' ) ;
    });

    it('nGram(7)(\'n-gram\') === "[]"', () =>
    {
        const result = nGram(7)('n-gram') ;
        assert.isArray( result ) ;
        assert.equal( result.length , 0 ) ;
    });
});
