'use strict'

import toFinite from '../../../src/numbers/toFinite'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.toFinite' , () =>
{
    it('toFinite( 3.2 ) === 3.2', () =>
    {
        assert.equal( toFinite( 3.2 ) , 3.2 ) ;
    });

    it('toFinite( Number.MIN_VALUE  ) === 5e-324', () =>
    {
        assert.equal( toFinite( Number.MIN_VALUE ) , 5e-324 ) ;
    });

    it('toFinite( Infinity  ) === 1.7976931348623157e+308', () =>
    {
        assert.equal( toFinite( Infinity ) , 1.7976931348623157e+308 ) ;
    });

    it('toFinite( \'3.2\' ) === 3.2', () =>
    {
        assert.equal( toFinite( '3.2' ) , 3.2 ) ;
    });
});
