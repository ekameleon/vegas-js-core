'use strict'

import toFixed from '../../../src/numbers/toFixed'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.toFixed' , () =>
{
    it('toFixed( 12345.6789 ) === 12346' , () =>
    {
        assert.equal( toFixed( 12345.6789 ) ,  12346 );
    });
});
