'use strict'

import toInteger from '../../../src/numbers/toInteger'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.toInteger' , () =>
{
    it('toInteger( 3.2 ) === 3', () =>
    {
        assert.equal( toInteger( 3.2 ) , 3 ) ;
    });

    it('toInteger( Number.MIN_VALUE  ) === 0', () =>
    {
        assert.equal( toInteger( Number.MIN_VALUE ) , 0 ) ;
    });

    it('toInteger( Infinity  ) === 1.7976931348623157e+308', () =>
    {
        assert.equal( toInteger( Infinity ) , 1.7976931348623157e+308 ) ;
    });

    it('toInteger( \'3.2\' ) === 3', () =>
    {
        assert.equal( toInteger( '3.2' ) , 3 ) ;
    });
});
