'use strict'

import isOdd from '../../../src/numbers/isOdd'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.isOdd' , () =>
{
    it('isOdd( 0 ) === false' , () =>
    {
        assert.isFalse( isOdd(0) );
    });

    it('isOdd( 1 ) === true' , () =>
    {
        assert.isTrue( isOdd(1) );
    });

    it('isOdd( 2 ) === false' , () =>
    {
        assert.isFalse( isOdd(2) );
    });
});
