'use strict'

import toNumber from '../../../src/numbers/toNumber'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.toNumber' , () =>
{
    let value1 ;
    let value2 = 3 ;
    let value3 = "12" ;
    
    it('toNumber(value1) === 0', () =>
    {
        assert.equal( toNumber(value1) , 0 ) ;
    });

    it('toNumber(value1,4) === 4', () =>
    {
        assert.equal( toNumber(value1,4) , 4 ) ;
    });

    it('toNumber(value2) === 3', () =>
    {
        assert.equal( toNumber(value2) , 3 ) ;
    });

    it('toNumber(value3) === 12', () =>
    {
        assert.equal( toNumber(value3) , 12 ) ;
    });
});
