'use strict'

import sma from '../../../src/numbers/sma'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.sma' , () =>
{
    it('sma( [1, 2, 3, 4, 5, 6, 7, 8, 9] , 4 ) === [ \'2.50\', \'3.50\', \'4.50\', \'5.50\', \'6.50\', \'7.50\' ]' , () =>
    {
        const result = sma( [1, 2, 3, 4, 5, 6, 7, 8, 9] , 4 ) ;
        assert.isArray( result );
        assert.equal( result.length , 6 );
        assert.equal( result[0] , '2.50' );
        assert.equal( result[1] , '3.50' );
        assert.equal( result[2] , '4.50' );
        assert.equal( result[3] , '5.50' );
        assert.equal( result[4] , '6.50' );
        assert.equal( result[5] , '7.50' );
    });
});
