'use strict'

import leading from '../../../src/numbers/leading'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.numbers.leading' , () =>
{
    it('leading( 0 ) === 00' , () => { assert.equal( leading( 0 ) , '00' ); });
    it('leading( 0 , 0 ) === 00' , () => { assert.equal( leading( 0 , 0 ) , '00' ); });
    it('leading( 0 , 1 ) === 0' , () => { assert.equal( leading( 0 , 1 ) , '0' ); });
    it('leading( 0 , 2 ) === 00' , () => { assert.equal( leading( 0 , 2 ) , '00' ); });
    it('leading( 0 , 3 ) === 000' , () => { assert.equal( leading( 0 , 3 ) , '000' ); });
    it('leading( 0 , 4 ) === 0000' , () => { assert.equal( leading( 0 , 4 ) , '0000' ); });
    
    it('leading( 1 , 0 ) === 01' , () => { assert.equal( leading( 1 , 0 ) , '01' ); });
    it('leading( 1 , 1 ) === 1' , () => { assert.equal( leading( 1 , 1 ) , '1' ); });
    it('leading( 1 , 2 ) === 01' , () => { assert.equal( leading( 1 , 2 ) , '01' ); });
    it('leading( 1 , 3 ) === 001' , () => { assert.equal( leading( 1 , 3 ) , '001' ); });
    it('leading( 1 , 4 ) === 0001' , () => { assert.equal( leading( 1 , 4 ) , '0001' ); });
    
    it('leading( 12 , 0 ) === 12' , () => { assert.equal( leading( 12 , 0 ) , '12' ); });
    it('leading( 12 , 1 ) === 12' , () => { assert.equal( leading( 12 , 1 ) , '12' ); });
    it('leading( 12 , 2 ) === 12' , () => { assert.equal( leading( 12 , 2 ) , '12' ); });
    it('leading( 12 , 3 ) === 012' , () => { assert.equal( leading( 12 , 3 ) , '012' ); });
    it('leading( 12 , 4 ) === 0012' , () => { assert.equal( leading( 12 , 4 ) , '0012' ); });
    
    it('leading( -12 , 0 ) === -12' , () => { assert.equal( leading( -12 , 0 ) , '-12' ); });
    it('leading( -12 , 1 ) === -12' , () => { assert.equal( leading( -12 , 1 ) , '-12' ); });
    it('leading( -12 , 2 ) === -12' , () => { assert.equal( leading( -12 , 2 ) , '-12' ); });
    it('leading( -12 , 3 ) === -012' , () => { assert.equal( leading( -12 , 3 ) , '-012' ); });
    it('leading( -12 , 4 ) === -0012' , () => { assert.equal( leading( -12 , 4 ) , '-0012' ); });
    
    it('leading( 12 , 0 , "x") === 12' , () => { assert.equal( leading( 12 , 0 , "x") , '12' ); });
    it('leading( 12 , 1 , "x") === 2' , () => { assert.equal( leading( 12 , 1 , "x") , '12' ); });
    it('leading( 12 , 2 , "x") === 12' , () => { assert.equal( leading( 12 , 2 , "x") , '12' ); });
    it('leading( 12 , 3 , "x") === x12' , () => { assert.equal( leading( 12 , 3 , "x") , 'x12' ); });
    it('leading( 12 , 4 , "x") === xx12' , () => { assert.equal( leading( 12 , 4 , "x") , 'xx12' ); });
});
