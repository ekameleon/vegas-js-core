'use strict'

import avg from '../../../src/numbers/avg'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.avg' , () =>
{
    it('avg( [1, 2, 3, 4, 5, 6, 7, 8, 9] , 5, 4 ) === 3.5' , () =>
    {
        assert.equal( avg( [1, 2, 3, 4, 5, 6, 7, 8, 9] , 5, 4 ) ,  3.5 );
    });
});
