'use strict'

import sum from '../../../src/numbers/sum'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.sum' , () =>
{
    it('sum( [ 1 , 2 , 3 ] ) === 6' , () =>
    {
        assert.equal( sum( [ 1 , 2 , 3 ] ) ,  6 );
    });
});
