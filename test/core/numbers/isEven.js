'use strict'

import isEven from '../../../src/numbers/isEven'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.isEven' , () =>
{
    it('isEven( 0 ) === true' , () =>
    {
        assert.isTrue( isEven(0) );
    });

    it('isEven( 1 ) === false' , () =>
    {
        assert.isFalse( isEven(1) );
    });

    it('isEven( 2 ) === true' , () =>
    {
        assert.isTrue( isEven(2) );
    });
});
