'use strict'

import clip from '../../../src/numbers/clip'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.numbers.clip' , () =>
{
    it('clip( 4, 5, 10) ===  5' , () => { assert.equal( clip( 4, 5, 10) ,  5 ); });
    it('clip(11, 5, 10) === 10' , () => { assert.equal( clip(11, 5, 10) , 10 ); });
    it('clip( 6, 5, 10) ===  6' , () => { assert.equal( clip( 6, 5, 10) ,  6 ); });
});
