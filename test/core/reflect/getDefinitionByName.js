'use strict'

import getDefinitionByName from '../../../src/reflect/getDefinitionByName'

import global from '../../../src/global'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.reflect.getDefinitionByName' , () =>
{
    const MyClass = () => {}
    MyClass.prototype.toString = () => "MyClass" ;
    
    global.MyClass = MyClass ;
    
    
    it('getDefinitionByName() === undefined', () =>
    {
        assert.instanceOf( getDefinitionByName( "MyClass" ) , global.MyClass ) ;
    });

    // FIXME test the function
});
