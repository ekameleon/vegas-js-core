'use strict'

import notEmpty from '../../../src/arrays/notEmpty'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.arrays.notEmpty' , () =>
{
    it('notEmpty([1,2,3]) === true', () => { assert.isTrue( notEmpty( [1,2,3] ) ) ; });
    
    it('notEmpty([])      === false', () => { assert.isFalse( notEmpty( [] ) ) ; });
    it('notEmpty(true)    === false', () => { assert.isFalse( notEmpty( true    ) ) ; });
    it('notEmpty("hello") === false', () => { assert.isFalse( notEmpty( "hello" ) ) ; });
    it('notEmpty(null)    === false', () => { assert.isFalse( notEmpty( null    ) ) ; });

});
