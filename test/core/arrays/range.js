'use strict'

import range from '../../../src/arrays/range'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.range' , () =>
{
    it('range( 10 ) === [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', () =>
    {
        const result = range( 10 );
        assert.isArray( result );
        assert.equal( result.length , 10 );
        assert.equal( result[0], 0 ) ;
        assert.equal( result[1], 1 ) ;
        assert.equal( result[2], 2 ) ;
        assert.equal( result[3], 3 ) ;
        assert.equal( result[4], 4 ) ;
        assert.equal( result[5], 5 ) ;
        assert.equal( result[6], 6 ) ;
        assert.equal( result[7], 7 ) ;
        assert.equal( result[8], 8 ) ;
        assert.equal( result[9], 9 ) ;
    });

    it('range( 1 , 11 ) === [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]', () =>
    {
        const result = range( 1 , 11 ) ;
        assert.isArray( result );
        assert.equal( result.length , 10 );
        assert.equal( result[0], 1 ) ;
        assert.equal( result[1], 2 ) ;
        assert.equal( result[2], 3 ) ;
        assert.equal( result[3], 4 ) ;
        assert.equal( result[4], 5 ) ;
        assert.equal( result[5], 6 ) ;
        assert.equal( result[6], 7 ) ;
        assert.equal( result[7], 8 ) ;
        assert.equal( result[8], 9 ) ;
        assert.equal( result[9], 10 ) ;
    });

    it('range( 0 , 30 , 5  ) === [0, 5, 10, 15, 20, 25]', () =>
    {
        const result = range( 0 , 30 , 5 ) ;
        assert.isArray( result );
        assert.equal( result.length , 6 );
        assert.equal( result[0], 0 ) ;
        assert.equal( result[1], 5 ) ;
        assert.equal( result[2], 10 ) ;
        assert.equal( result[3], 15 ) ;
        assert.equal( result[4], 20 ) ;
        assert.equal( result[5], 25 ) ;
    });

    it('range( 0 , 30 , 5  ) === [0, 5, 10, 15, 20, 25]', () =>
    {
        const result = range( 0, -10, -1 ) ;
        assert.isArray( result );
        assert.equal( result.length , 10 );
        assert.equal( result[0], 0 ) ;
        assert.equal( result[1], -1 ) ;
        assert.equal( result[2], -2 ) ;
        assert.equal( result[3], -3 ) ;
        assert.equal( result[4], -4 ) ;
        assert.equal( result[5], -5 ) ;
        assert.equal( result[6], -6 ) ;
        assert.equal( result[7], -7 ) ;
        assert.equal( result[8], -8 ) ;
        assert.equal( result[9], -9 ) ;
    });

    it('range( -2 , 3 ) === [-2,-1,0,1,2]', () =>
    {
        const result = range( -2 , 3 );
        assert.isArray( result );
        assert.equal( result.length , 5 );
        assert.equal( result[0], -2 ) ;
        assert.equal( result[1], -1 ) ;
        assert.equal( result[2], 0 ) ;
        assert.equal( result[3], 1 ) ;
        assert.equal( result[4], 2 ) ;
    });
});
