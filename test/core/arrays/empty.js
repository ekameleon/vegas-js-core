'use strict'

import empty from '../../../src/arrays/empty'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.arrays.empty' , () =>
{
    it('empty([]) === true', () => { assert.isTrue( empty( [] ) ) ; });
    
    it('empty([1,2,3]) === false', () => { assert.isFalse( empty( [1,2,3] ) ) ; });
    it('empty(true)    === false', () => { assert.isFalse( empty( true    ) ) ; });
    it('empty("hello") === false', () => { assert.isFalse( empty( "hello" ) ) ; });
    it('empty(null)    === false', () => { assert.isFalse( empty( null    ) ) ; });

});
