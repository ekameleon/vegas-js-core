'use strict'

import move from '../../../src/arrays/move'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.move' , () =>
{
    it('move([1,2,3,4],1,3) == [1,3,4,2]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , 3 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([1,2,3,4]).length ) ;
        assert.sameMembers( array , [1,2,3,4] ) ;
        assert.equal( array.toString() , '1,3,4,2' ) ;
    });

    it('move([1,2,3,4],3,3) == [1,2,3,4]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 3 , 3 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([1,2,3,4]).length ) ;
        assert.sameMembers( array , [1,2,3,4] ) ;
        assert.equal( array.toString() , '1,2,3,4' ) ;
    });

    it('move([1,2,3,4],1,-1) == [1,3,4,2]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , -1 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([1,3,4,2]).length ) ;
        assert.sameMembers( array , [1,3,4,2] ) ;
        assert.equal( array.toString() , '1,3,4,2' ) ;
    });

    it('move([1,2,3,4],1,-2) == [1,3,2,4]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , -2 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([1,3,2,4]).length ) ;
        assert.sameMembers( array , [1,3,2,4] ) ;
        assert.equal( array.toString() , '1,3,2,4' ) ;
    });

    it('move([1,2,3,4],1,-3) == [1,2,3,4]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , -3 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([1,2,3,4]).length ) ;
        assert.sameMembers( array , [1,2,3,4] ) ;
        assert.equal( array.toString() , '1,2,3,4' ) ;
    });

    it('move([1,2,3,4],1,-4) == [2,1,3,4]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , -4 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([2,1,3,4]).length ) ;
        assert.sameMembers( array , [2,1,3,4] ) ;
        assert.equal( array.toString() , '2,1,3,4' ) ;
    });

    it('move([1,2,3,4],1,-5) == [2,1,3,4]', () =>
    {
        let array = [1,2,3,4] ;
        move( array , 1 , -5 ) ;
        assert.equal( array , array ) ;
        assert.equal( array.length , ([2,1,3,4]).length ) ;
        assert.sameMembers( array , [2,1,3,4] ) ;
        assert.equal( array.toString() , '2,1,3,4' ) ;
    });

    it('move([1,2,3,4],1,-6) == [2,1,3,4]', () =>
    {
        let array1 = [1,2,3,4] ;
        let array2 = move( array1 , 1 , -6 ) ;
        assert.strictEqual( array1 , array2 ) ;
        assert.equal( array2.toString() , '2,1,3,4' ) ;
    });

    it('clone = move([1,2,3,4],1,3,true) ==> clone == [1,3,4,2]', () =>
    {
        let array = [1,2,3,4] ;
        let clone = move( array , 1 , 3 , true ) ;
        assert.notStrictEqual( clone , array ) ;
        assert.equal( clone.toString() , '1,3,4,2' ) ;
    });
});
