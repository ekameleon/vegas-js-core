'use strict'

import toLength from '../../../src/arrays/toLength'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.arrays.toLength' , () =>
{
    it('toInteger( 3.2 ) === 3', () =>
    {
        assert.equal( toLength( 3.2 ) , 3 ) ;
    });

    it('toLength( Number.MIN_VALUE  ) === 0', () =>
    {
        assert.equal( toLength( Number.MIN_VALUE ) , 0 ) ;
    });

    it('toLength( Infinity  ) === 4294967295', () =>
    {
        assert.equal( toLength( Infinity ) , 4294967295 ) ;
    });

    it('toLength( \'3.2\' ) === 3', () =>
    {
        assert.equal( toLength( '3.2' ) , 3 ) ;
    });
});
