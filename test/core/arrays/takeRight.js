'use strict'

import takeRight from '../../../src/arrays/takeRight'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.takeRight' , () =>
{
    it('takeRight([1,2,3]) === [1]', () =>
    {
        const slice = takeRight( [1,2,3] ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 1 ) ;
        assert.equal( slice.toString() , '3' );
    });

    it('takeRight([1,2,3],2) === [1,2]', () =>
    {
        const slice = takeRight( [1,2,3] , 2 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 2 ) ;
        assert.equal( slice.toString() , '2,3' );
    });

    it('takeRight([1,2,3],3) === [1,2,3]', () =>
    {
        const slice = takeRight( [1,2,3] , 3 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 3 ) ;
        assert.equal( slice.toString() , '1,2,3' );
    });

    it('takeRight([1,2,3],8) === [1,2,3]', () =>
    {
        const slice = takeRight( [1,2,3] , 8 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 3 ) ;
        assert.equal( slice.toString() , '1,2,3' );
    });

    it('takeRight([1,2,3],0) === []', () =>
    {
        const slice = takeRight( [1,2,3] , 0 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });

    it('takeRight(null) === []', () =>
    {
        const slice = takeRight(null) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });

    it('takeRight("string") === []', () =>
    {
        const slice = takeRight("string") ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });
});
