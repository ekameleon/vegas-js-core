'use strict'

import nullify from '../../../src/arrays/nullify'

import chai from 'chai'

const { assert : { equal , isNull } = {} } = chai ;

describe( 'core.arrays.nullify' , () =>
{
    it('nullify([1,2,3]) === [1,2,3]', () =>
    {
        const array = [1,2,3] ;
        equal( nullify(array)  , array );
    });

    it('nullify([]) === null', () =>
    {
        const array = [] ;
        isNull( nullify( array ) ) ;
    });

    it('nullify(null) === null', () =>
    {
        isNull( nullify( null ) ) ;
    });

    it('nullify("string") === null', () =>
    {
        isNull( nullify( "string" ) ) ;
    });

    it('nullify(10) === null', () =>
    {
        isNull( nullify( 10 ) ) ;
    });
});
