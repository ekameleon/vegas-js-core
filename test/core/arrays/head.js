'use strict'

import head from '../../../src/arrays/head'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.head' , () =>
{
    it('head([1,2,3]) === 1', () =>
    {
        assert.equal( head( [1,2,3] ), 1 );
    });
    
    it('head([]) === 1', () =>
    {
        assert.isUndefined( head([]) );
    });
});
