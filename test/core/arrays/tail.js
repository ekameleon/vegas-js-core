'use strict'

import tail from '../../../src/arrays/tail'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.tail' , () =>
{
    it('tail([]) is an empty Array', () =>
    {
        assert.isArray( tail([]) , 'not an Array' );
        assert.equal( tail([]).length , 0 , 'not empty' );
    });
    
    it('tail([1,2,3]) === [2,3]', () =>
    {
        assert.sameMembers( tail( [1,2,3] ), [2,3] );
    });
});
