'use strict'

import partition from '../../../src/arrays/partition'

import chai from 'chai'

const { assert } = chai ;

const {
    deepEqual,
    lengthOf
}
= assert ;

describe( 'core.arrays.partition' , () =>
{
    const users =
    [
        { name : 'Lancelot' , active : false },
        { name : 'Perceval' , active : true  },
        { name : 'Arthur'   , active : false }
    ];
    
    it('partition( users , ({ active }) => active )', () =>
    {
        const result = partition( users , ({ active }) => active ) ;
        
        // console.log( result ) ;
        
        lengthOf( result , 2 ) ;
        
        lengthOf( result[0] , 1 ) ;
        deepEqual( result[0][0] , { name : 'Perceval' , active : true  } ) ;
        
        lengthOf( result[1] , 2 ) ;
        deepEqual( result[1][0] , { name : 'Lancelot' , active : false } ) ;
        deepEqual( result[1][1] , { name : 'Arthur'   , active : false } ) ;
    });

});
