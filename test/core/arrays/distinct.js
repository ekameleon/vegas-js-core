'use strict'

import distinct from '../../../src/arrays/distinct'

import chai from 'chai'

const { assert } = chai ;

const {
    isTrue,
    isFalse
} = assert ;

describe( 'core.arrays.distinct' , () =>
{
    it('distinct( 3 , 0 , [1,2,3] ) === true'  , () => { isFalse( distinct( 3 , 0 , [1,2,3] ) ); });
    it('distinct( 3 , 1 , [1,2,3] ) === true'  , () => { isFalse( distinct( 3 , 1 , [1,2,3] ) ); });
    it('distinct( 3 , 2 , [1,2,3] ) === true'  , () => {  isTrue( distinct( 3 , 2 , [1,2,3] ) ); });
});
