'use strict'

import some from '../../../src/arrays/some'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.some' , () =>
{
    it('some( [null, 0, \'yes\', false], Boolean] , value => value === "yes" ) === true' , () =>
    {
        assert.isTrue( some( [null, 0, 'yes', false] , value => value ) , "yes" );
    });
});
