'use strict'

import each from '../../../src/arrays/each'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.each' , () =>
{
    it('each([1,2,3], ( value , index , array ) => ...', () =>
    {
        let input = [1,2,3] ;
        let output = [] ;

        each( input , ( value , index , array ) =>
        {
            output.push( value + '-' + index + '-[' + array + ']' ) ;
        } ) ;

        assert.equal( output.length , input.length );
        assert.equal( output[0] , '1-0-[1,2,3]' );
        assert.equal( output[1] , '2-1-[1,2,3]' );
        assert.equal( output[2] , '3-2-[1,2,3]' );
    });

    it('each([1,2,3], ( value , index , array ) => false', () =>
    {
        let input = [1,2,3] ;
        let output = [] ;

        each( input , ( value , index , array ) =>
        {
            output.push( value + '-' + index + '-[' + array + ']' ) ;
            return false ;
        } ) ;

        assert.equal( output.length , 1 );
        assert.equal( output[0] , '1-0-[1,2,3]' );
    });
});
