'use strict'

import unique from '../../../src/arrays/unique'

import chai from 'chai'

const { assert } = chai ;

const {
    equal,
    isUndefined,
    lengthOf
} = assert ;

describe( 'core.arrays.unique' , () =>
{
    describe( 'core.arrays.unique(ar)' , () =>
    {
        let ar = unique( [1,1,2,3,3,2,4] ) ;
        it('unique( [1,1,2,3,3,2,4] ).length === 4' , () => { lengthOf( ar , 4 ); });
        it('unique( [1,1,2,3,3,2,4] )[0] === 1' , () => { equal( ar[0] , 1 ); });
        it('unique( [1,1,2,3,3,2,4] )[1] === 2' , () => { equal( ar[1] , 2 ); });
        it('unique( [1,1,2,3,3,2,4] )[2] === 3' , () => { equal( ar[2] , 3 ); });
        it('unique( [1,1,2,3,3,2,4] )[2] === 4' , () => { equal( ar[3] , 4 ); });
        it('unique( [1,1,2,3,3,2,4] )[4] === undefined' , () => { isUndefined( ar[4] ); });
    });
    
    describe( 'core.arrays.unique(ar,true)' , () =>
    {
        let ar = unique( [1,1,2,3,3,2,4] , true ) ;
        it('unique( [1,1,2,3,3,2,4] , true ).length === 4' , () => { lengthOf( ar , 4 ); });
        it('unique( [1,1,2,3,3,2,4] , true )[0] === 1' , () => { equal( ar[0] , 1 ); });
        it('unique( [1,1,2,3,3,2,4] , true )[1] === 3' , () => { equal( ar[1] , 3 ); });
        it('unique( [1,1,2,3,3,2,4] , true )[2] === 2' , () => { equal( ar[2] , 2 ); });
        it('unique( [1,1,2,3,3,2,4] , true )[3] === 4' , () => { equal( ar[3] , 4 ); });
        it('unique( [1,1,2,3,3,2,4] , true )[4] === undefined' , () => { isUndefined( ar[4] ); });
    });
});
