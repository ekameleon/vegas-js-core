"use strict";

import inBetween from '../../../src/arrays/inBetween'

import chai from 'chai'

const { assert } = chai;

describe('core.arrays.inBetween', () => 
{
    it("inBetween( ['a', 'b', 'c'] , ' /')", () => { assert.deepEqual( inBetween( ['a', 'b', 'c'], '/') , ['a','/','b','/','c'] ) ; });
    it("inBetween( ['a', 'b'] , ' /')", () => { assert.deepEqual(inBetween(['a', 'b'], '/'), ['a', '/', 'b']); });
    it("inBetween( ['a'] , ' /')", () => { assert.deepEqual(inBetween(['a'], '/'), ['a']); });
    it("inBetween( [] , ' /')", () => { assert.deepEqual(inBetween([], '/'), []); });
    
    it("inBetween('foo') throws TypeError", () => { assert.throws(() => { inBetween("foo"); }, TypeError ) } );
});
