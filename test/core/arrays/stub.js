'use strict'

import stub from '../../../src/arrays/stub'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.arrays.stub' , () =>
{
    it('stub() === []', () =>
    {
        const array = stub() ;
        assert.isArray( array ) ;
        assert.equal( array.length , 0 ) ;
    });
});
