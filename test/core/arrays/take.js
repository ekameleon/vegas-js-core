'use strict'

import take from '../../../src/arrays/take'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.take' , () =>
{
    it('take([1,2,3]) === [1]', () =>
    {
        const slice = take( [1,2,3] ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 1 ) ;
        assert.equal( slice.toString() , '1' );
    });

    it('take([1,2,3],2) === [1,2]', () =>
    {
        const slice = take( [1,2,3] , 2 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 2 ) ;
        assert.equal( slice.toString() , '1,2' );
    });

    it('take([1,2,3],3) === [1,2,3]', () =>
    {
        const slice = take( [1,2,3] , 3 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 3 ) ;
        assert.equal( slice.toString() , '1,2,3' );
    });

    it('take([1,2,3],8) === [1,2,3]', () =>
    {
        const slice = take( [1,2,3] , 8 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 3 ) ;
        assert.equal( slice.toString() , '1,2,3' );
    });

    it('take([1,2,3],0) === []', () =>
    {
        const slice = take( [1,2,3] , 0 ) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });

    it('take(null) === []', () =>
    {
        const slice = take(null) ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });

    it('take("string") === []', () =>
    {
        const slice = take("string") ;
        assert.isArray( slice , 'not an Array' );
        assert.equal( slice.length , 0 ) ;
    });
});
