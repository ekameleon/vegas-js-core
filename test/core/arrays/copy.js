'use strict'

import copy from '../../../src/arrays/copy'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.arrays.copy' , () =>
{
    it('copy([1,2,3], []) ==> [1,2,3]', () =>
    {
        let output = copy( [1,2,3] , [] );

        assert.equal( output.length ,3 );
        assert.equal( output[0] , 1 );
        assert.equal( output[1] , 2 );
        assert.equal( output[2] , 3 );
    });
});
