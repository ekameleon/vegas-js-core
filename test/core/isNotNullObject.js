'use strict'

import isNotNullObject from '../../src/isNotNullObject'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isNotNullObject' , () =>
{
    it('isNotNullObject(null) === false' , () => { isFalse( isNotNullObject(null) ); });
    
    it('isNotNullObject([]) === true' , () => { isTrue( isNotNullObject([]) ); });
    it('isNotNullObject({}) === true' , () => { isTrue( isNotNullObject({}) ); });
    
    it('isNotNullObject(0) === false'     , () => { isFalse( isNotNullObject(0) ); });
    it('isNotNullObject(1) === false'     , () => { isFalse( isNotNullObject(1) ); });
    it('isNotNullObject("") === false'    , () => { isFalse( isNotNullObject("") ); });
    it('isNotNullObject(true) === false'  , () => { isFalse( isNotNullObject(true) ); });
    it('isNotNullObject(false) === false' , () => { isFalse( isNotNullObject(false) ); });
});
