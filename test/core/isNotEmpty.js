/*jshint -W053 */
'use strict'

import isNotEmpty from '../../src/isNotEmpty'

import chai from 'chai'

const { assert } = chai ;

const {
    isTrue,
    isFalse
} = assert ;

describe( 'core.isNotEmpty' , () =>
{
    it('isNotEmpty([]) === false'     , () => { isFalse( isNotEmpty([]) ); });
    it('isNotEmpty([1,2,3]) === true' , () => { isTrue( isNotEmpty([1,2,3]) ); });
    
    it('isNotEmpty("") === false'     , () => { isFalse( isNotEmpty('') ); });
    it('isNotEmpty("hello") === true' , () => { isTrue( isNotEmpty('hello') ); });
    
    it('isNotEmpty({}) === false'   , () => { isFalse( isNotEmpty({}) ); });
    it('isNotEmpty({a:0}) === true' , () => { isTrue( isNotEmpty({a:0}) ); });
    
    it('isNotEmpty(true) === true'  , () => { isTrue( isNotEmpty(true) ); });
    it('isNotEmpty(false) === true' , () => { isTrue( isNotEmpty(false) ); });
    it('isNotEmpty(null) === true'  , () => { isTrue( isNotEmpty(null) ); });
});
