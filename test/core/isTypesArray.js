/*jshint -W053 */
'use strict'

import isTypedArray from '../../src/isTypedArray'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.isTypedArray' , () =>
{
    it('isTypedArray(new Uint8Array()) === true' , () => { assert.isTrue( isTypedArray(new Uint8Array())); });
    it('isTypedArray([])) === false' , () => { assert.isFalse( isTypedArray([])); });
});
