/*jshint -W053 */
'use strict'

import isFunction from '../../src/isFunction'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.isFunction' , () =>
{
    it('isFunction(0)     === false' , () => { assert.isFalse( isFunction(0) ); });
    it('isFunction(true)  === false' , () => { assert.isFalse( isFunction(true) ); });
    it('isFunction("foo") === false' , () => { assert.isFalse( isFunction("foo") ); });
    it('isFunction(null)  === false' , () => { assert.isFalse( isFunction(null) ); });
    it('isFunction(NaN)   === false' , () => { assert.isFalse( isFunction(NaN) ); });
    
    it('isFunction(()=>{}) === true' , () => { assert.isTrue( isFunction(() => {}) ); });
});
