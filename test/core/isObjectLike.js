'use strict'

import isObjectLike from '../../src/isObjectLike'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.isObjectLike' , () =>
{

    it('isObjectLike([])) === true' , () => { assert.isTrue( isObjectLike([]) ); });
    it('isObjectLike({})) === true' , () => { assert.isTrue( isObjectLike({}) ); });

    it('isObjectLike(()=>{})) === false' , () => { assert.isFalse( isObjectLike(()=>{}) ); });
    it('isObjectLike(0)     === false' , () => { assert.isFalse( isObjectLike(0) ); });
    it('isObjectLike(true)  === false' , () => { assert.isFalse( isObjectLike(true) ); });
    it('isObjectLike("foo") === false' , () => { assert.isFalse( isObjectLike("foo") ); });
    it('isObjectLike(null)  === false' , () => { assert.isFalse( isObjectLike(null) ); });
    it('isObjectLike(NaN)   === false' , () => { assert.isFalse( isObjectLike(NaN) ); });
    it('isObjectLike(undefined)   === false' , () => { assert.isFalse( isObjectLike(undefined) ); });
});
