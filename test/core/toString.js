'use strict'

import toString from '../../src/toString'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.toString' , () =>
{
    it('toString( null ) === ""' , () =>
    {
        assert.equal( toString( null ) , '' );
    });

    it('toString( -0 ) === "-0"' , () =>
    {
        assert.equal( toString( -0 ) , '-0' );
    });

    it('toString( [1, 2, 3] ) === "1,2,3' , () =>
    {
        assert.equal( toString( [1, 2, 3] ) , '1,2,3' );
    });
});
