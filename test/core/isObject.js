/*jshint -W053 */
'use strict'

import isObject from '../../src/isObject'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.isObject' , () =>
{
    it('isObject(0)     === false' , () => { assert.isFalse( isObject(0) ); });
    it('isObject(true)  === false' , () => { assert.isFalse( isObject(true) ); });
    it('isObject("foo") === false' , () => { assert.isFalse( isObject("foo") ); });
    it('isObject(null)  === false' , () => { assert.isFalse( isObject(null) ); });
    it('isObject(NaN)   === false' , () => { assert.isFalse( isObject(NaN) ); });
    
    it('isObject(new String("foo")) === true' , () => { assert.isTrue( isObject(new String("foo")) ); });
    it('isObject(new Boolean(true)) === true' , () => { assert.isTrue( isObject(new Boolean(true)) ); });
    it('isObject(new Number(1)) === true' , () => { assert.isTrue( isObject(new Number(1)) ); });
    it('isObject([])) === true' , () => { assert.isTrue( isObject([]) ); });
    it('isObject({})) === true' , () => { assert.isTrue( isObject({}) ); });
    it('isObject(()=>{})) === true' , () => { assert.isTrue( isObject(()=>{}) ); });
});
