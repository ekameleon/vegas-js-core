'use strict'

import MONTH_PER_DECADE from '../../../src/date/MONTH_PER_DECADE'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MONTH_PER_DECADE' , () =>
{
    it( 'MONTH_PER_DECADE === 120' , () =>
    {
        assert.equal( MONTH_PER_DECADE , 120 );
    });
});
