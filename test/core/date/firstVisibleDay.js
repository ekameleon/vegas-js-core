'use strict'

import firstVisibleDay from '../../../src/date/firstVisibleDay'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.firstVisibleDay' , () =>
{
    let date1 = new Date(2020,3,14) ;
    let date2 = new Date(2020,2,30) ;
    let date3 = firstVisibleDay(date1,1) ;
    it( 'firstVisibleDay(new Date(2020,3,14),1) === new Date(2020,2,30)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; });
});
