'use strict'

import MS_PER_HOUR from '../../../src/date/MS_PER_HOUR'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MS_PER_HOUR' , () =>
{
    it( 'MS_PER_MINUTE === 60 * 60 * 1000' , () =>
    {
        assert.equal( MS_PER_HOUR , 60 * 60 * 1000 );
    });
});
