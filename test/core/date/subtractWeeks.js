'use strict'

import MS_PER_WEEK from "../../../src/date/MS_PER_WEEK"

import subtractWeeks from '../../../src/date/subtractWeeks'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractWeeks' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = subtractWeeks(date1,2) ;
    it('subtractWeeks(new Date(2014,10,2),2).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() - 2 * MS_PER_WEEK ); } ) ;
});
