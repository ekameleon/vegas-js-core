'use strict'

import subtractMS from '../../../src/date/subtractMS'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractMS' , () =>
{
    const date1 = new Date(2014,10,2,16,25,50,100) ;
    const date2 = new Date(2014,10,2,16,25,50,50) ;
    const date3 = subtractMS( date1 , 50 ) ;
    it('subtractMS(new Date(2014,10,2,16,25,50,100),50) === new Date(2014,10,2,16,25,50,50)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
