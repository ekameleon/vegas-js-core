'use strict'

import addMS from '../../../src/date/addMS'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addMS' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addMS( date1 , 30000 ) ;
    it('addMS(new Date(2014,10,2),30000).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 30000 ); } ) ;
});
