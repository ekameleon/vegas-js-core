'use strict'

import MS_PER_MINUTE from "../../../src/date/MS_PER_MINUTE"

import addMinutes from '../../../src/date/addMinutes'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addMinutes' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addMinutes( date1 , 30 ) ;
    it('addMinutes(new Date(2014,10,2),30).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 30 * MS_PER_MINUTE ); } ) ;
});
