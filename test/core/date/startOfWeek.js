'use strict'

import startOfWeek from '../../../src/date/startOfWeek'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.startOfWeek' , () =>
{
    let date1 = new Date(2020,3,14) ;
    let date2 = new Date(2020,3,13) ;
    let date3 = startOfWeek(date1,1) ;
    
    it( 'startOfWeek(new Date(2020,3,14),1) === new Date(2020,3,13)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; });
});
