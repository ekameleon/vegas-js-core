'use strict'

import subtractYears from '../../../src/date/subtractYears'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractYears' , () =>
{
    const date1 = new Date(2014,3,2) ;
    const date2 = new Date(2011,3,2) ;
    const date3 = subtractYears(date1,3) ;

    it('subtractYears(new Date(2014,3,2),3) === new Date(2011,3,2)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; } ) ;
});
