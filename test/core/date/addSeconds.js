'use strict'

import MS_PER_SECOND from "../../../src/date/MS_PER_SECOND"

import addSeconds from '../../../src/date/addSeconds'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addSeconds' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addSeconds( date1 , 30 ) ;
    it('addSeconds(new Date(2014,10,2),30).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 30 * MS_PER_SECOND ); } ) ;
});
