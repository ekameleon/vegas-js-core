'use strict'

import subtractMinutes from '../../../src/date/subtractMinutes'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.subtractMinutes' , () =>
{
    const date1 = new Date(2014,10,2,16,25) ;
    const date2 = new Date(2014,10,2,16,15) ;
    const date3 = subtractMinutes( date1 , 10 ) ;
    it('subtractMinutes(new Date(2014,10,2,16,25),10) ===  new Date(2014,10,2,16,15)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
