'use strict'

import MS_PER_400_YEARS from '../../../src/date/MS_PER_400_YEARS'
import MS_PER_HOUR      from '../../../src/date/MS_PER_HOUR'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.MS_PER_400_YEARS' , () =>
{
    it( 'MS_PER_400_YEARS === 60 * 1000' , () =>
    {
        assert.equal( MS_PER_400_YEARS , (365 * 400 + 97) * 24 * MS_PER_HOUR );
    });
});
