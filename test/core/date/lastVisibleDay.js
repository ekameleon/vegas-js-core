'use strict'

import lastVisibleDay from '../../../src/date/lastVisibleDay'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.lastVisibleDay' , () =>
{
    let date1 = new Date(2020,3,14) ;
    let date2 = new Date(2020,4,3,23, 59, 59, 999) ;
    let date3 = lastVisibleDay(date1,1) ;
    it( 'lastVisibleDay(new Date(2020,3,14),1) === new Date(2020,4,3,23, 59, 59, 999)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; });
});
