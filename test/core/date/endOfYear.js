'use strict'

import endOfYear from '../../../src/date/endOfYear'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.endOfYear' , () =>
{
    let date  = new Date(2016,3,12) ;
    let start = endOfYear(date) ;
    
    it( 'endOfYear(new Date(2016,3,12)) instanceof Date', () =>
    {
        assert.instanceOf( start , Date ) ;
    });

    it( 'endOfYear(new Date(2016,3,12)).getFullYear() === date.getFullYear()', () =>
    {
        assert.equal( start.getFullYear(), date.getFullYear() );
    });

    it( 'endOfYear(new Date(2016,3,12)).getMonth() === 11 (December)', () =>
    {
        assert.equal( start.getMonth(), 11 );
    });

    it( 'endOfYear(new Date(2016,3,12)).getDay() == 6 (Saturday)', () =>
    {
        assert.equal( start.getDay() , 6 );
    });
});
