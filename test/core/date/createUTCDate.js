'use strict'

import createUTCDate from '../../../src/date/createUTCDate'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.createUTCDate' , () =>
{
    const date1 = createUTCDate( 2016, 0 , 1 ) ;
    const date2 = new Date(Date.UTC( 2016, 0 , 1 )) ;
    it('createUTCDate(2016,0,1).valueOf()', () => { assert.equal( date1.valueOf() , date2.valueOf() ); } ) ;
});
