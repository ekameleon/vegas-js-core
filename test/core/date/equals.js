'use strict'

import equals from '../../../src/date/equals'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.equals' , () =>
{
    it('equals(new Date(2016,1,1),new Date(2016,1,0)) === false)', () =>
    {
        assert.isFalse( equals(new Date(2016,1,1),new Date(2016,1,0)) );
    });
    it('equals(new Date(2016,1,1),new Date(2016,1,1)) === true)', () =>
    {
        assert.isTrue( equals(new Date(2016,1,1),new Date(2016,1,1)) );
    });
    it('equals(new Date(2015,1,1),new Date(2016,1,1)) === false)', () =>
    {
        assert.isFalse( equals(new Date(2015,1,1),new Date(2016,1,1)) );
    });

    it('equals() throws TypeError', () =>
    {
        assert.throws( () => { equals() ; } , TypeError );
    });

    it('equals("foo",new Date()) throws TypeError', () =>
    {
        assert.throws( function(){ equals( "foo" , new Date() ) } , TypeError ) ;
    });

    it('equals(new Date(),"foo") throws TypeError', () =>
    {
        assert.throws( function(){ equals( new Date() , "foo" ) } , TypeError ) ;
    });
});
