'use strict'

import startOfDay from '../../../src/date/startOfDay'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.startOfDay' , () =>
{
    let date1 = new Date(2020,3,15,16,15,30,500 ) ;
    let date2 = new Date(2020,3,15,0) ;
    let date3 = startOfDay(date1) ;
    it( 'startOfDay(new Date(2020,3,15,16,15,30,500)) === new Date(2020,3,15,0)', () => { assert.equal( date2.valueOf(), date3.valueOf() );});
});
