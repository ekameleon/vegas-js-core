'use strict'

import subtractDays from '../../../src/date/subtractDays'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractDays' , () =>
{
    const date1 = new Date(2014,10,25) ;
    const date2 = new Date(2014,10,15) ;
    const date3 = subtractDays( date1,10) ;
    it('subtractDays(new Date(2014,10,25),10) === new Date(2014,10,15)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
