'use strict'

import round from '../../../src/maths/round'
import daysToMonths from '../../../src/date/daysToMonths'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.daysToMonths' , () =>
{
    it('daysToMonths(1)  === 0.033' , () => { assert.equal( round( daysToMonths(  1 ) , 3 ) , 0.033 ); });
    it('daysToMonths(5)  === 0.164' , () => { assert.equal( round( daysToMonths(  5 ) , 3 ) , 0.164 ); });
    it('daysToMonths(30) === 0.986' , () => { assert.equal( round( daysToMonths( 30 ) , 3 ) , 0.986 ); });
    it('daysToMonths(31) === 1.019' , () => { assert.equal( round( daysToMonths( 31 ) , 3 ) , 1.019 ); });
});
