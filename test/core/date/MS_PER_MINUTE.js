'use strict'

import MS_PER_MINUTE from '../../../src/date/MS_PER_MINUTE'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MS_PER_MINUTE' , () =>
{
    it( 'MS_PER_MINUTE === 60 * 1000' , () =>
    {
        assert.equal( MS_PER_MINUTE , 60 * 1000 );
    });
});
