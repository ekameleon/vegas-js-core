'use strict'

import createDate from '../../../src/date/createDate'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.createDate' , () =>
{
    const date1 = createDate( 2016 , 1 , 12 , 15 , 16 , 12 , 150 ) ;
    const date2 = new Date( 2016 , 1 , 12 , 15 , 16 , 12 , 150 ) ;
    it('createDate(2016,1,12,15,16,12,150).valueOf()', () => { assert.equal( date1.valueOf() , date2.valueOf() ); } ) ;
});
