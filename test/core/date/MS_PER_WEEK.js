'use strict'

import MS_PER_WEEK from '../../../src/date/MS_PER_WEEK'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MS_PER_WEEK' , () =>
{
    it('MS_PER_WEEK === 604800000' , () =>
    {
        assert.equal( MS_PER_WEEK , 604800000 );
    });
});
