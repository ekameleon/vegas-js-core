'use strict'

import addMonths from '../../../src/date/addMonths'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addMonths' , () =>
{
    const date1 = new Date(2014,3,2) ;
    const date2 = new Date(2014,6,2) ;
    const date3 = addMonths(date1,3) ;

    it('addMonths(new Date(2014,3,2),3) === new Date(2014,6,2)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; } ) ;
});
