'use strict'


import visibleDays from '../../../src/date/visibleDays'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.visibleDays' , () =>
{
    const date = new Date(2019,3,14 ) ;

    const first = new Date('2019-03-30T23:00:00.000Z' ) ;
    const last  = new Date('2019-05-03T22:00:00.000Z' ) ;

    let calendar = visibleDays( date , 0 ) ;
    let length   = calendar.length ;

    it('visibleDays(new Date(2020,3,16)).length === 35', () => { assert.equal( length , 35 ); } ) ;
    it('visibleDays(new Date(2020,3,16))[0] ===  new Date(\'2019-03-30T23:00:00.000Z\' )', () => { assert.equal( calendar[0].valueOf() , first.valueOf() ); } ) ;
    it('visibleDays(new Date(2020,3,16))[34] === new Date(\'2019-05-03T22:00:00.000Z\' ) ', () => { assert.equal( calendar[length-1].valueOf() , last.valueOf() ); } ) ;
});
