'use strict'

import startOfYear from '../../../src/date/startOfYear'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.startOfYear' , () =>
{
    let date  = new Date(2016,3,12) ;
    let start = startOfYear(date) ;
    
    it( 'startOfYear(new Date(2016,3,12)) instanceof Date', () =>
    {
        assert.instanceOf( start , Date ) ;
    });

    it( 'startOfYear(new Date(2016,3,12)).getFullYear() === date.getFullYear()', () =>
    {
        assert.equal( start.getFullYear(), date.getFullYear() );
    });

    it( 'startOfYear(new Date(2016,3,12)).getMonth() === 0 (January)', () =>
    {
        assert.equal( start.getMonth(), 0 );
    });

    it( 'startOfYear(new Date(2016,3,12)).getDay() == 5 (Friday)', () =>
    {
        assert.equal( start.getDay() , 5 );
    });
});
