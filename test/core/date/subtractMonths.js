'use strict'

import subtractMonths from '../../../src/date/subtractMonths'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.subtractMonths' , () =>
{
    const date1 = new Date(2014,10) ;
    const date2 = new Date(2014,5) ;
    const date3 = subtractMonths( date1 , 5 ) ;
    it('subtractMonths(new Date(2014,10),5) ===  new Date(2014,5)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
