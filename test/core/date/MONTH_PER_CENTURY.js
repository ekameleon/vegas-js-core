'use strict'

import MONTH_PER_CENTURY from '../../../src/date/MONTH_PER_CENTURY'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MONTH_PER_CENTURY' , () =>
{
    it( 'MONTH_PER_CENTURY === 1200' , () =>
    {
        assert.equal( MONTH_PER_CENTURY , 1200 );
    });
});
