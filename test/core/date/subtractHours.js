'use strict'

import MS_PER_HOUR from "../../../src/date/MS_PER_HOUR"

import subtractHours from '../../../src/date/subtractHours'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractHours' , () =>
{
    const date1 = new Date(2014,10,2 , 16 ) ;
    const date2 = new Date(2014,10,2 , 6 ) ;
    const date3 = subtractHours(date1,10) ;
    it('subtractHours(new Date(2014,10,2,16),10) === new Date(2014,10,2,6)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
