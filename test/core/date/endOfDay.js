'use strict'

import endOfDay from '../../../src/date/endOfDay'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.endOfDay' , () =>
{
    let date1 = new Date(2020,3,15,16,15,30,500) ;
    let date2 = new Date(2020,3,15,23,59,59,999) ;
    let date3 = endOfDay(date1) ;
    it( 'endOfDay(new Date(2020,3,15,16,15,30,500)) === new Date(2020,3,15,23,59,59,999)', () => { assert.equal( date2.valueOf(), date3.valueOf() );});
});
