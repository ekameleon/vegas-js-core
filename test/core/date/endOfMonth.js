'use strict'

import endOfMonth from '../../../src/date/endOfMonth'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.endOfMonth' , () =>
{
    let date  = new Date(2016,3,12) ;
    let start = endOfMonth(date) ;
    
    it( 'endOfMonth(new Date(2016,3,12)) instanceof Date', () =>
    {
        assert.instanceOf( start , Date ) ;
    });

    it( 'endOfMonth(new Date(2016,3,12)).getFullYear() === date.getFullYear()', () =>
    {
        assert.equal( start.getFullYear(), date.getFullYear() );
    });

    it( 'endOfMonth(new Date(2016,3,12)).getMonth() === 3 (April)', () =>
    {
        assert.equal( start.getMonth(), 3 );
    });

    it( 'endOfMonth(new Date(2016,3,12)).getDay() == 6 (Saturday)', () =>
    {
        assert.equal( start.getDay() , 6 );
    });
});
