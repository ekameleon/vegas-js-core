'use strict'

import MS_PER_HOUR from "../../../src/date/MS_PER_HOUR"

import addHours from '../../../src/date/addHours'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addHours' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addHours(date1,30) ;
    it('addHours(new Date(2014,10,2),30).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 30 * MS_PER_HOUR ); } ) ;
});
