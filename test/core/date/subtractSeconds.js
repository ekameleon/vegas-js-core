'use strict'

import subtractSeconds from '../../../src/date/subtractSeconds'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.subtractSeconds' , () =>
{
    const date1 = new Date(2014,10,2,16,25,50) ;
    const date2 = new Date(2014,10,2,16,25,40) ;
    const date3 = subtractSeconds( date1 , 10 ) ;
    it('subtractMS(new Date(2014,10,2,16,25,50),10) === new Date(2014,10,2,16,25,40)', () => { assert.equal( date2.valueOf() , date3.valueOf() ); } ) ;
});
