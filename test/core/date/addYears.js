'use strict'

import addYears from '../../../src/date/addYears'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addYears' , () =>
{
    const date1 = new Date(2014,3,2) ;
    const date2 = new Date(2017,3,2) ;
    const date3 = addYears(date1,3) ;

    it('addYears(new Date(2014,3,2),3) === new Date(2017,3,2)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; } ) ;
});
