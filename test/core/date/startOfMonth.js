'use strict'

import startOfMonth from '../../../src/date/startOfMonth'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.startOfMonth' , () =>
{
    let date  = new Date(2016,3,12) ;
    let start = startOfMonth(date) ;
    
    it( 'startOfMonth(new Date(2016,3,12)) instanceof Date', () =>
    {
        assert.instanceOf( start , Date ) ;
    });

    it( 'startOfMonth(new Date(2016,3,12)).getFullYear() === date.getFullYear()', () =>
    {
        assert.equal( start.getFullYear(), date.getFullYear() );
    });

    it( 'startOfMonth(new Date(2016,3,12)).getMonth() === 3 (April)', () =>
    {
        assert.equal( start.getMonth(), 3 );
    });

    it( 'startOfMonth(new Date(2016,3,12)).getDay() == 5 (Friday)', () =>
    {
        assert.equal( start.getDay() , 5 );
    });
});
