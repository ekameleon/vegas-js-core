'use strict'

import round from '../../../src/maths/round'
import monthsToDays from '../../../src/date/monthsToDays'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.monthsToDays' , () =>
{
    it('monthsToDays(1)===  31' , () => { assert.equal( round( monthsToDays( 1 ) ) , 30 ); });
    it('monthsToDays(5)=== 152' , () => { assert.equal( round( monthsToDays( 5 ) ) , 152 ); });
});
