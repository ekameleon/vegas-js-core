'use strict'

import daysInFebruary from '../../../src/date/daysInFebruary'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.daysInFebruary' , () =>
{
    it('daysInFebruary(2016) === 29', () => { assert.equal( daysInFebruary(2016) , 29 ); });
    it('daysInFebruary(2017) === 28', () => { assert.equal( daysInFebruary(2017) , 28 ); });
});
