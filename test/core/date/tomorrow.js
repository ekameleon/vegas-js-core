'use strict'

import tomorrow from '../../../src/date/tomorrow'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.tomorrow' , () =>
{
    let date = new Date(2016,1,2) ;
    let next = tomorrow(date) ;
    let now  = new Date() ;

    it( 'tomorrow(new Date(2016,1,2)) instanceof Date', () =>
    {
        assert.instanceOf( next , Date ) ;
    });

    it( 'tomorrow(new Date(2016,1,2)).valueOf() === new Date(2016,1,3).valueOf()', () =>
    {
        assert.equal( next.valueOf() , new Date(2016,1,3).valueOf() );
    });

    it( 'tomorrow() instanceof Date', () =>
    {
        assert.instanceOf( tomorrow() , Date ) ;
    });

    it( 'tomorrow().getFullYear() === now.getFullYear()', () =>
    {
        assert.equal( tomorrow().getFullYear(), now.getFullYear() );
    });

    it( 'tomorrow().getDay() === now.getDay()+1', () =>
    {
        next = tomorrow() ;
        let day = now.getDay() + 1 ;
        if( day > 6 )
        {
            day = 0 ;
        }
        assert.equal( next.getDay() , day );
    });
});
