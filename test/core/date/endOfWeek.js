'use strict'

import endOfWeek from '../../../src/date/endOfWeek'

import chai from 'chai'

const { assert } = chai ;

describe( 'core.date.endOfWeek' , () =>
{
    let date1 = new Date(2020,3,14) ;
    let date2 = new Date(2020,3,19,23, 59, 59, 999) ;
    let date3 = endOfWeek(date1,1) ;
    it( 'endOfWeek(new Date(2020,3,14),1) === new Date(2020,3,19,23,59,59,999)', () => { assert.equal( date3.valueOf() , date2.valueOf() ) ; });
});
