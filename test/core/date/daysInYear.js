'use strict'

import daysInYear from '../../../src/date/daysInYear'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.daysInYear' , () =>
{
    it('daysInYear(2016) === 366', () => { assert.equal( daysInYear(2016) , 366 ); });
    it('daysInYear(2017) === 365', () => { assert.equal( daysInYear(2017) , 365 ); });
});
