'use strict'

import MS_PER_DAY from '../../../src/date/MS_PER_DAY'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MS_PER_DAY' , () =>
{
    it( 'MS_PER_DAY === 864000000' , () =>
    {
        assert.equal( MS_PER_DAY , 86400000 );
    });
});
