'use strict'

import MS_PER_WEEK from "../../../src/date/MS_PER_WEEK"

import addWeeks from '../../../src/date/addWeeks'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addWeeks' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addWeeks(date1,1) ;

    it('addWeeks(new Date(2014,10,2),1).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 1 * MS_PER_WEEK ); } ) ;
});
