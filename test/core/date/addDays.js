'use strict'

import MS_PER_DAY from "../../../src/date/MS_PER_DAY"

import addDays from '../../../src/date/addDays'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.addDays' , () =>
{
    const date1 = new Date(2014,10,2) ;
    const date2 = addDays(date1,25) ;
    it('addDays(new Date(2014,10,2),25).valueOf()', () => { assert.equal( date2.valueOf() , date1.valueOf() + 25 * MS_PER_DAY ); } ) ;
});
