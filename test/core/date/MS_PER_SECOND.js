'use strict'

import MS_PER_SECOND from '../../../src/date/MS_PER_SECOND'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MS_PER_SECOND' , () =>
{
    it('MS_PER_SECOND === 1000' , () =>
    {
        assert.equal( MS_PER_SECOND , 1000 );
    });
});
