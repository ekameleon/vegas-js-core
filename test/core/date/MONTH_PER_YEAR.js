'use strict'

import MONTH_PER_YEAR from '../../../src/date/MONTH_PER_YEAR'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.date.MONTH_PER_YEAR' , () =>
{
    it( 'MONTH_PER_YEAR === 12' , () =>
    {
        assert.equal( MONTH_PER_YEAR , 12 );
    });
});
