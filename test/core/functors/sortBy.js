'use strict'

import sortBy from '../../../src/functors/sortBy'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.functors.sortBy' , () =>
{
    describe( 'Sorting with functions' , () =>
    {
        let cities;

        beforeEach( () =>
        {
            cities =
            [
                { id:  7 , name: "Amsterdam"  , population: 750000  , country: "Netherlands" },
                { id: 12 , name: "The Hague"  , population: 450000  , country: "Netherlands" },
                { id: 43 , name: "Rotterdam"  , population: 600000  , country: "Netherlands" },
                { id:  5 , name: "Berlin"     , population: 3000000 , country: "Germany"     },
                { id: 42 , name: "Düsseldorf" , population: 550000  , country: "Germany"     },
                { id: 44 , name: "Stuttgard"  , population: 600000  , country: "Germany"     }
            ] ;
        });

        it( 'Sort by Country, then by Population', () =>
        {
            const compare = sortBy ( ( v1 , v2 ) => ( v1.country < v2.country ) ? -1 : ( v1.country > v2.country ? 1 : 0 ) )
                           .thenBy( ( v1 , v2 ) => v1.population - v2.population ) ;

            cities.sort( compare ) ;

            assert.equal( "Amsterdam"  , cities[5].name ) ;
            assert.equal( "Düsseldorf" , cities[0].name ) ;
        });

        it( 'Sort by Country, then by Population, using unary functions', () =>
        {
            const compare = sortBy( v => v.country    )
                           .thenBy( v => v.population );

            cities.sort( compare ) ;

            assert.equal( "Amsterdam"  , cities[5].name ) ;
            assert.equal( "Düsseldorf" , cities[0].name ) ;
        });

        it( 'Sort by length of name, then by population, then by ID', () =>
        {
            const compare = sortBy( ( v1 , v2 ) => v1.name.length - v2.name.length )
                           .thenBy( ( v1 , v2 ) => v1.population - v2.population   )
                           .thenBy( ( v1 , v2 ) => v1.id - v2.id                   );

            cities.sort( compare ) ;

            assert.equal( "Berlin"     , cities[0].name ) ; // shortest name
            assert.equal( "Düsseldorf" , cities[5].name ) ; // longest name

            // expect Stuttgard just after Rotterdam, same name length, same population, higher ID
            assert.equal("Rotterdam", cities[2].name);
            assert.equal("Stuttgard", cities[3].name);
        });

        it( 'Sort by length of name, then by population, then by ID, using unary functions', () =>
        {
            const compare = sortBy( v => v.name.length )
                           .thenBy( v => v.population  )
                           .thenBy( v => v.id          );

            cities.sort( compare ) ;

            assert.equal( "Berlin"     , cities[0].name ) ; // shortest name
            assert.equal( "Düsseldorf" , cities[5].name ) ; // longest name

            // expect Stuttgard just after Rotterdam, same name length, same population, higher ID
            assert.equal("Rotterdam", cities[2].name);
            assert.equal("Stuttgard", cities[3].name);
        });
    });

    describe( 'Sorting while managing case sensitivity' , () =>
    {
        let cities;

        beforeEach( () =>
        {
            cities =
            [
                { id: 2, name: "Ostrava" , population: 750000  , country: "czech republic" },
                { id: 4, name: "karvina" , population: 450000  , country: "Czech Republic" },
                { id: 6, name: "Brno"    , population: 600000  , country: "czech Republic" },
                { id: 8, name: "prague"  , population: 3000000 , country: "Czech republic" },
            ] ;
        });

        it( 'Sort by Name, using unary function' , () =>
        {
            cities.sort( sortBy( v => v.name ) );

            assert.equal("Brno"    , cities[0].name ) ;
            assert.equal("Ostrava" , cities[1].name ) ;
            assert.equal("karvina" , cities[2].name ) ;
            assert.equal("prague"  , cities[3].name ) ;
        });

        it( 'Sort by Name, ignoring the case, using unary function' , () =>
        {
            cities.sort( sortBy( v => v.name , { ignoreCase:true , direction:1 } ) );

            assert.equal( "Brno"    , cities[0].name ) ;
            assert.equal( "karvina" , cities[1].name ) ;
            assert.equal( "Ostrava" , cities[2].name ) ;
            assert.equal( "prague"  , cities[3].name ) ;
        });

        it( 'Sort by Name desc, ignoring the case, using unary function' , () =>
        {
            cities.sort( sortBy( v => v.name , { ignoreCase:true , direction:'desc' } ) );

            assert.equal( "prague"  , cities[0].name ) ;
            assert.equal( "Ostrava" , cities[1].name ) ;
            assert.equal( "karvina" , cities[2].name ) ;
            assert.equal( "Brno"    , cities[3].name ) ;
        });


        it( 'Sort by Country, then by Name ignoring the case, using property name' , () =>
        {
            cities.sort( sortBy("country").thenBy("name", {ignoreCase:true, direction:1}) );

            //console.log( cities.map( item => item.name ) ) ;

            assert.equal( "karvina" , cities[0].name ) ;
            assert.equal( "prague"  , cities[1].name ) ;
            assert.equal( "Brno"    , cities[2].name ) ;
            assert.equal( "Ostrava" , cities[3].name ) ;
        });

        it( 'Sort by Country ignoring the case using inline .toLowerCase(), then by Name, using function' , () =>
        {
            cities.sort
            (
                 sortBy( ( v1 , v2 ) =>
                 {
                     v1 = v1.country.toLowerCase() ;
                     v2 = v2.country.toLowerCase() ;
                     return v1 < v2 ? -1 : ( v1 > v2 ? 1 : 0 ) ;
                 })
                .thenBy( "name" )
            );

            //console.log( cities.map( item => item.name ) ) ;

            assert.equal( "Brno"    , cities[0].name ) ;
            assert.equal( "Ostrava" , cities[1].name ) ;
            assert.equal( "karvina" , cities[2].name ) ;
            assert.equal( "prague"  , cities[3].name ) ;
        });

        it( 'Sort by Country ignoring the case, then by Name ignoring the case, using unary function' , () =>
        {
            cities.sort
            (
                 sortBy( v => v.country , { ignoreCase:true , direction:1 } )
                .thenBy( v => v.name    , { ignoreCase:true , direction:1 } )
            );

            //console.log( cities.map( item => item.name ) ) ;

            assert.equal( "Brno"    , cities[0].name ) ;
            assert.equal( "karvina" , cities[1].name ) ;
            assert.equal( "Ostrava" , cities[2].name ) ;
            assert.equal( "prague"  , cities[3].name ) ;
        });
    });

    describe( 'Sorting with property names' , () =>
    {
        let cities;

        beforeEach( () =>
        {
            cities =
            [
                { id:  7 , name: "Amsterdam"  , population: 750000  , country: "Netherlands" },
                { id: 12 , name: "The Hague"  , population: 450000  , country: "Netherlands" },
                { id: 43 , name: "Rotterdam"  , population: 600000  , country: "Netherlands" },
                { id:  5 , name: "Berlin"     , population: 3000000 , country: "Germany"     },
                { id: 42 , name: "Düsseldorf" , population: 550000  , country: "Germany"     },
                { id: 44 , name: "Stuttgard"  , population: 600000  , country: "Germany"     }
            ] ;
        });

        it( 'Sort by Country, then by Population' , () =>
        {
            cities.sort( sortBy( 'country' ).thenBy( 'population' ) );

            assert.equal( "Amsterdam"  , cities[5].name ) ;
            assert.equal( "Düsseldorf" , cities[0].name ) ;
        });

        it( 'Sort by Country desc, then by Population' , () =>
        {
            cities.sort( sortBy( 'country' , 'desc' ).thenBy( 'population' ) );

            assert.equal( "Berlin"    , cities[5].name ) ;
            assert.equal( "The Hague" , cities[0].name ) ;
        });

        it( 'Sort by Country asc, then by Population desc' , () =>
        {
            cities.sort( sortBy( 'country' , 'asc' ).thenBy( 'population' , 'desc') );

            assert.equal( "The Hague" , cities[5].name ) ;
            assert.equal( "Berlin"    , cities[0].name ) ;
        });

        it( 'Sort by Country, then by Population desc (using -1)' , () =>
        {
            cities.sort( sortBy( 'country' ).thenBy( 'population' , -1 ) );

            assert.equal( "The Hague" , cities[5].name ) ;
            assert.equal( "Berlin"    , cities[0].name ) ;
        });
    });

    describe( 'Sorting on numerical values' , () =>
    {
        it( 'Sort strings with numbers in them' , () =>
        {
            const values = [ "2" , "20" , "03" , "-2" , "0" , "200" , "2" ];
            const sorted = values.sort( sortBy( Number ) ) ;
            ["-2", "0", "2", "2", "03", "20", "200"].forEach( ( value , index )=>
            {
                assert.equal( sorted[index] , value );
            });
        });

        it( 'Sort strings with numbers in them and normal numbers together' , () =>
        {
            const values = [ "2" , "20" , "03" , "-2" , "0" , 200 , "2" ];

            const sorted = values.sort( sortBy( Number ) ) ;

            ["-2", "0", "2", "2", "03", "20", 200 ].forEach( ( value , index )=>
            {
                assert.equal( sorted[index] , value );
            });
        });

        it( 'Sort strings with numbers in them in properties' , () =>
        {
            const values = [ { a:"2" }, { a:"20" }, { a:"03" }, { a:"-2" }, { a:"0" }, { a:200 }, { a:"2" } ];

            const sorted = values.sort( sortBy( v => Number( v.a ) ) ) ;

            ["-2", "0", "2", "2", "03", "20", 200 ].forEach( ( value , index )=>
            {
                assert.equal( sorted[index].a , value );
            });
        });
    });

    describe( 'Sorting with unary function and custom compare', () =>
    {
        const suits = ['C', 'D', 'H', 'S'];
        const cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

        const suitCompare = ( s1, s2 ) => suits.indexOf( s1 ) - suits.indexOf( s2 );
        const cardCompare = ( c1, c2 ) => cards.indexOf( c1 ) - cards.indexOf( c2 );

        const handOfCards =
            [
                { id: 7, suit: "C", card: "A" },
                { id: 8, suit: "H", card: "10" },
                { id: 9, suit: "S", card: "10" },
                { id: 10, suit: "D", card: "10" },
                { id: 11, suit: "S", card: "9" }
            ];

        it( 'Sort by value, then by suit', () =>
        {
            const compare = sortBy( "card", { compare : cardCompare, direction: -1 } )
                           .thenBy( "suit", { compare : suitCompare, direction: -1 } );

            handOfCards.sort( compare );
            assert.equal( 7, handOfCards[0].id );
            assert.equal( 9, handOfCards[1].id );
            assert.equal( 8, handOfCards[2].id );
            assert.equal( 10, handOfCards[3].id );
            assert.equal( 11, handOfCards[4].id );
        } );

        it( 'using Intl.Collator', () =>
        {
            const compare = (new Intl.Collator( 'en' )).compare;
            const data = [{ a: "aäb" }, { a: "aãb" }, { a: "aab" }, { a: "abb" }, { a: "Aäb" }];
            data.sort( sortBy( "a", { compare } ) );
            assert.equal( "aab", data[0].a );
            assert.equal( "aäb", data[1].a );
            assert.equal( "Aäb", data[2].a );
            assert.equal( "aãb", data[3].a );
            assert.equal( "abb", data[4].a );
        } );

        it( 'using Intl.Collator in complex object', () =>
        {
            const compare = (new Intl.Collator( 'en' )).compare;
            const data = [{ a : { b : "aäb" } }, { a : { b : "aãb" } }, { a : { b : "aab" } }, { a : { b : "abb" } } , { a : { b : "Aäb" } } ];
            data.sort( sortBy( "a.b", { compare } ) );
            assert.equal( "aab", data[0].a.b );
            assert.equal( "aäb", data[1].a.b );
            assert.equal( "Aäb", data[2].a.b );
            assert.equal( "aãb", data[3].a.b );
            assert.equal( "abb", data[4].a.b );
        } );
    });
});
