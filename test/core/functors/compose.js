'use strict'

import compose from '../../../src/functors/compose'

import chai from 'chai' ;

const { assert } = chai ;

const a = value => value + 1 ;
const b = value => value + 2 ;
const c = value => value + 3 ;

describe( 'core.functors.compose' , () =>
{
    it('compose(a,b)(1) === 4', () =>
    {
        assert.equal( compose(a,b)(1) , 4 ) ;
    });

    it('compose(a,\'coucou\',null,4,b)(1) === 4', () =>
    {
        assert.equal( compose(a,'coucou',null,4,b)(1) , 4 ) ; // test non functions
    });

    const flag = true ;

    it('compose(a,flag && b,c)(1) === 7', () =>
    {
        assert.equal( compose(c,flag && b,a)(1) , 7 ) ;
    });

    it('compose(a,b,c)(1) === 5', () =>
    {
        assert.equal( compose(a,!flag && b,c)(1) , 5 ) ;
    });
});
