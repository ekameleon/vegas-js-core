'use strict'

import memoize from '../../../src/functors/memoize'

import chai from 'chai' ;

const { assert } = chai ;

const add = (n) => (n + 10) ;

const cache = memoize(add);

describe( 'core.functors.memoize' , () =>
{
    it('memoize(add(3)) === 13', () =>
    {
        assert.equal( cache(3) , 13 ) ; // calculated
        assert.equal( cache(3) , 13 ) ; // cached
    });
    it('memoize(add(4)) === 14', () =>
    {
        assert.equal( cache(4) , 14 ) ; // calculated
        assert.equal( cache(4) , 14 ) ; // cached
    });
});
