'use strict'

import negate from '../../../src/functors/negate'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.functors.negate' , () =>
{
    it('filter([1,2,3,4,5,6] , negate( isEven ) ) ) === [1,3,5]', () =>
    {
        const isEven = n => n%2 === 0 ;
        const result = [1,2,3,4,5,6].filter( negate( isEven ) ) ;
        assert.isArray( result ) ;
        assert.equal( result.length , 3 ) ;
        assert.equal( result[0] , 1 ) ;
        assert.equal( result[1] , 3 ) ;
        assert.equal( result[2] , 5 ) ;
    });
});
