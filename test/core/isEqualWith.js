'use strict'

import isEqualWith from '../../src/isEqualWith'

import chai from 'chai' ;

const assert = chai.assert ;

const isGreeting = value => /^h(?:i|ello)$/.test(value) ;

const customizer = ( objValue , othValue) => isGreeting(objValue) && isGreeting(othValue)  || undefined ;

describe( 'core.isEqualWith' , () =>
{
    it('isEqualWith( { a:1 , b:2 } , { a:1 , b:2 } ) === true' , () =>
    {
        assert.isTrue( isEqualWith({ a:1 , b:2 } , { a:1 , b:2 } ) ); }
    );

    it('isEqualWith( [ \'hello\' , \'goodbye\' ] , [ \'hi\' , \'goodbye\' ] , customizer ) === true' , () =>
    {
        const array = [ 'hello' , 'goodbye' ] ;
        const other = [ 'hi'    , 'goodbye' ] ;

        assert.isTrue( isEqualWith( array , other , customizer ) ) ;
    });

});
