'use strict'

import isBuffer from '../../src/isBuffer'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.isBuffer' , () =>
{
    it('isBuffer(new Buffer(2)) === true'     , () => { assert.isTrue( isBuffer(new Buffer(2)) ); });
});
