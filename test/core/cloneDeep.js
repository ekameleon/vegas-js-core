'use strict'

import cloneDeep from '../../src/cloneDeep'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.cloneDeep' , () =>
{
    it("cloneDeep([{ 'a': 1 }, { 'b': 2 }])"  , () =>
    {
        const object = [{ 'a': 1 }, { 'b': 2 }]  ;
        const deep = cloneDeep( object )  ;
        assert.isFalse( deep[0] === object[0] );
        assert.equal( JSON.stringify( deep ) , JSON.stringify( object ) );
    });
});
