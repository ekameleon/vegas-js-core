'use strict'

import isPlainObject from '../../src/isPlainObject'

import chai from 'chai'

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isPlainObject' , () =>
{
    it('isPlainObject({}) === true' , () => { isTrue( isPlainObject({}) ); });
    it('isPlainObject({foo:"bar"}) === true' , () => { isTrue( isPlainObject({foo:'bar'}) ); });
    it('isPlainObject(Object.create({})) === true' , () => { isTrue( isPlainObject(Object.create({})) ); });
    it('isPlainObject(Object.create(Object.prototype)) === true' , () => { isTrue( isPlainObject(Object.create(Object.prototype)) ); });
    
    it('isPlainObject(null) === false' , () => { isFalse( isPlainObject(null) ); });
    it('isPlainObject(1) === false' , () => { isFalse( isPlainObject(1) ); });
    it('isPlainObject([]) === false' , () => { isFalse( isPlainObject([]) ); });
    it('isPlainObject(new Date()) === false' , () => { isFalse(isPlainObject(new Date())); });
    it('isPlainObject(Object.create(null)) === false' , () => { isFalse(isPlainObject(Object.create(null))); });
});
