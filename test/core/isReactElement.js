/*jshint -W053 */
'use strict'

import React, { Component } from 'react'

import isReactElement from '../../src/isReactElement'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isReactElement' , () =>
{
    it('isReactElement(null)      === true' , () => { isFalse( isReactElement(null) ); });
    it('isReactElement(undefined) === true' , () => { isFalse( isReactElement(undefined) ); });
    
    it('isReactElement(undefined) === true' , () => { isTrue( isReactElement(<Component/>) ); });
});
