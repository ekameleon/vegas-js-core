'use strict' ;

import './functors/aop'
import './functors/compose'
import './functors/memoize'
import './functors/negate'
import './functors/sortBy'
