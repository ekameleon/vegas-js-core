'use strict'

import isEqual from '../../src/isEqual'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.isEqual' , () =>
{
    it('isEqual( { a:1 , b:2 } , { a:1 , b:2 } ) === true' , () =>
    {
        assert.isTrue( isEqual({ a:1 , b:2 } , { a:1 , b:2 } ) ); }
    );

    it('isEqual( true , true ) === true' , () =>
    {
        assert.isTrue( isEqual(true , true ) ) ;
        assert.isTrue( isEqual(Boolean(true) , Boolean(true) ) ) ;

        assert.isFalse( isEqual(true , false ) ) ;
        assert.isFalse( isEqual(Boolean(true) , Boolean(false) ) ) ;
    });

    it('isEqual( new Date() , new Date() ) === true' , () =>
    {
        assert.isTrue( isEqual( new Date() , new Date() ) ) ;
        assert.isTrue( isEqual( new Date(2024,8 , 5 ) , new Date(2024,8 , 5 ) ) ) ;
    });

    it('isEqual( "hello" , String("hello") ) === true' , () =>
    {
        assert.isTrue( isEqual( "hello" , String("hello") ) ) ;
    });
});
