'use strict'

import MILE_TO_METER from '../../../src/maths/MILE_TO_METER'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.maths.MILE_TO_METER' , () =>
{
    it('MILE_TO_METER === 1609' , () =>
    {
        assert.equal( MILE_TO_METER , 1609 );
    });
});
