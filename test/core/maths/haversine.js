'use strict'

import haversine from '../../../src/maths/haversine'

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.maths.haversine' , () =>
{
    let position1 = { x : 37.422045 , y : -122.084347 } ; // Google HQ
    let position2 = { x : 37.77493  , y : -122.419416 } ; // San Francisco, CA
    it('haversine(' + position1.x + ',' + position1.y + ',' + position2.x + ',' + position2.y + ') ===  49103.007 m (on Earth)' , () =>
    {
        assert.equal( haversine(position1.x,position1.y,position2.x,position2.y ) , 49103.007 );
    });

    it('haversine(' + position1.x + ',' + position1.y + ',' + position2.x + ',' + position2.y + ',3390) ===  26.128 m (on Mars)' , () =>
    {
        assert.equal( haversine(position1.x,position1.y,position2.x,position2.y,3390),26.128 );
    });
});
