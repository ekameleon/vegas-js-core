'use strict'

import floorMod from '../../../src/maths/floorMod'

import chai from 'chai' ;

const { assert } = chai ;

describe( 'core.maths.floorMod' , () =>
{
    it('floorMod(25,5) === 0' , () => { assert.equal( floorMod(25,5) , 0 ); });
    it('floorMod(123,50) === 23' , () => { assert.equal( floorMod(123,50) , 23 ); });
    it('floorMod(123,-50) === -27' , () => { assert.equal( floorMod(123,-50) , -27 ); });
    it('floorMod(-123,50) === 27' , () => { assert.equal( floorMod(-123,50) , 27 ); });
});
