/*jshint -W053 */
'use strict'

import isMergeableObject from '../../src/isMergeableObject'

import chai from 'chai'

const { assert } = chai ;

const {
    isTrue,
    isFalse
} = assert ;

describe( 'core.isMergeableObject' , () =>
{
    it('isMergeableObject({}) === true' , () => { isTrue( isMergeableObject({}) ); });
    it('isMergeableObject([]) === true' , () => { isTrue( isMergeableObject([]) ); });
    
    it('isMergeableObject(null) === false' , () => { isFalse( isMergeableObject(null) ); });
    it('isMergeableObject(undefined) === new Date()' , () => { isFalse( isMergeableObject(undefined) ); });
    it('isMergeableObject(null) === new Date()' , () => { isFalse( isMergeableObject(new Date()) ); });
});
