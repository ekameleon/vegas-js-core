'use strict'

import isWeakMap from '../../src/isWeakMap'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isWeakMap' , () =>
{
    it('isWeakMap(new Map()) === false' , () => { isFalse( isWeakMap(new Map()) ); });
    it('isWeakMap(new WeakMap()) === true' , () => { isTrue( isWeakMap(new WeakMap()) ); });
});
