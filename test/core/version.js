'use strict'

import version from '../../src/version.js' ;

import chai from 'chai' ;

const assert = chai.assert ;

describe( 'core.version' , () =>
{
    let v0 = new version() ;
    let v1 = new version(1,2,3,4) ;
    
    it('version().toString(0) === 0' , () => { assert.equal( v0.toString(0) , '0' ); });
    it('version().toString(1) === 0' , () => { assert.equal( v0.toString(1) , '0' ); });
    it('version().toString(2) === 0.0' , () => { assert.equal( v0.toString(2) , '0.0' ); });
    it('version().toString(3) === 0.0.0' , () => { assert.equal( v0.toString(3) , '0.0.0' ); });
    it('version().toString(4) === 0.0.0.0' , () => { assert.equal( v0.toString(4) , '0.0.0.0' ); });
    
    it('version().valueOf() === 0' , () => { assert.equal( v0.valueOf() , 0 ); });
    
    it('version(1,2,3,4).toString(0) === 1.2.3.4' , () => { assert.equal( v1.toString(0) , '1.2.3.4' ); });
    it('version(1,2,3,4).toString(1) === 1' , () => { assert.equal( v1.toString(1) , '1' ); });
    it('version(1,2,3,4).toString(2) === 1.2' , () => { assert.equal( v1.toString(2) , '1.2' ); });
    it('version(1,2,3,4).toString(3) === 1.2.3' , () => { assert.equal( v1.toString(3) , '1.2.3' ); });
    it('version(1,2,3,4).toString(4) === 1.2.3.4' , () => { assert.equal( v1.toString(4) , '1.2.3.4' ); });
    
    it('version(1,2,3,4).valueOf() === 302186500' , () => { assert.equal( v1.valueOf() , 302186500 ); });
    
    describe( 'core.version.fromString()' , () =>
    {
        let v0 = version.fromString() ;
        let v1 = version.fromString('1.2.3.4') ;
        
        it('version.fromString().toString(0) === 0' , () => { assert.equal( v0.toString(0) , '0' ); });
        it('version.fromString().toString(1) === 0' , () => { assert.equal( v0.toString(1) , '0' ); });
        it('version.fromString().toString(2) === 0.0' , () => { assert.equal( v0.toString(2) , '0.0' ); });
        it('version.fromString().toString(3) === 0.0.0' , () => { assert.equal( v0.toString(3) , '0.0.0' ); });
        it('version.fromString().toString(4) === 0.0.0.0' , () => { assert.equal( v0.toString(4) , '0.0.0.0' ); });
        
        it('version.fromString().valueOf() === 0' , () => { assert.equal( v0.valueOf() , 0 ); });
        
        it('version.fromString(1,2,3,4).toString(0) === 1.2.3.4' , () => { assert.equal( v1.toString(0) , '1.2.3.4' ); });
        it('version.fromString(1,2,3,4).toString(1) === 1' , () => { assert.equal( v1.toString(1) , '1' ); });
        it('version.fromString(1,2,3,4).toString(2) === 1.2' , () => { assert.equal( v1.toString(2) , '1.2' ); });
        it('version.fromString(1,2,3,4).toString(3) === 1.2.3' , () => { assert.equal( v1.toString(3) , '1.2.3' ); });
        it('version.fromString(1,2,3,4).toString(4) === 1.2.3.4' , () => { assert.equal( v1.toString(4) , '1.2.3.4' ); });
        
        it('version.fromString(1,2,3,4).valueOf() === 302186500' , () => { assert.equal( v1.valueOf() , 302186500 ); });
    });
    
    
    describe( 'core.version.valueOf()' , () =>
    {
        let v0 = version.fromNumber(0) ;
        let v1 = version.fromNumber(302186500) ;
        
        it('version.fromNumber(0).toString(0) === 0' , () => { assert.equal( v0.toString(0) , '0' ); });
        it('version.fromNumber(0).toString(1) === 0' , () => { assert.equal( v0.toString(1) , '0' ); });
        it('version.fromNumber(0).toString(2) === 0.0' , () => { assert.equal( v0.toString(2) , '0.0' ); });
        it('version.fromNumber(0).toString(3) === 0.0.0' , () => { assert.equal( v0.toString(3) , '0.0.0' ); });
        it('version.fromNumber(0).toString(4) === 0.0.0.0' , () => { assert.equal( v0.toString(4) , '0.0.0.0' ); });
        
        it('version.fromNumber(0).valueOf() === 0' , () => { assert.equal( v0.valueOf() , 0 ); });
        
        it('version.fromNumber(302186500).toString(0) === 1.2.3.4' , () => { assert.equal( v1.toString(0) , '1.2.3.4' ); });
        it('version.fromNumber(302186500).toString(1) === 1' , () => { assert.equal( v1.toString(1) , '1' ); });
        it('version.fromNumber(302186500).toString(2) === 1.2' , () => { assert.equal( v1.toString(2) , '1.2' ); });
        it('version.fromNumber(302186500).toString(3) === 1.2.3' , () => { assert.equal( v1.toString(3) , '1.2.3' ); });
        it('version.fromNumber(302186500).toString(4) === 1.2.3.4' , () => { assert.equal( v1.toString(4) , '1.2.3.4' ); });
        
        it('version.fromNumber(302186500).valueOf() === 302186500' , () => { assert.equal( v1.valueOf() , 302186500 ); });
    });
});
