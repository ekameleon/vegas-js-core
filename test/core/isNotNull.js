'use strict'

import isNotNull from '../../src/isNotNull'

import chai from 'chai' ;

const { assert } = chai ;

const { isFalse, isTrue } = assert ;

describe( 'core.isNotNull' , () =>
{
    it('isNotNullObject(null) === false' , () => { isFalse( isNotNull(null) ); });

    it('isNotNullObject([]) === true' , () => { isTrue( isNotNull([]) ); });
    it('isNotNullObject({}) === true' , () => { isTrue( isNotNull({}) ); });

    it('isNotNullObject(0) === true'     , () => { isTrue( isNotNull(0) ); });
    it('isNotNullObject(1) === true'     , () => { isTrue( isNotNull(1) ); });
    it('isNotNullObject("") === true'    , () => { isTrue( isNotNull("") ); });
    it('isNotNullObject(true) === true'  , () => { isTrue( isNotNull(true) ); });
    it('isNotNullObject(false) === true' , () => { isTrue( isNotNull(false) ); });
});
