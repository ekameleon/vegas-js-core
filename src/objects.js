'use strict'

import at             from './objects/at'
import castPath       from './objects/castPath'
import clean          from './objects/clean'
import forEach        from './objects/forEach'
import fuse           from './objects/fuse'
import get            from './objects/get'
import getTag         from './objects/getTag'
import has            from './objects/has'
import hasIn          from './objects/hasIn'
import hasPath        from './objects/hasPath'
import hasPathIn      from './objects/hasPathIn'
import isEmpty        from './objects/isEmpty'
import isKey          from './objects/isKey'
import keys           from './objects/keys'
import keysIn         from './objects/keysIn'
import map            from './objects/map'
import members        from './objects/members'
import merge          from './objects/merge'
import mergeDeep      from './objects/mergeDeep'
import omit          from './objects/omit'
import pairs         from './objects/pairs'
import pick           from './objects/pick'
import pickBy         from './objects/pickBy'
import removeProperty from './objects/removeProperty'
import renameProperty from './objects/renameProperty'
import set            from './objects/set'
import stub           from './objects/stub'
import toKey          from './objects/toKey'
import unset          from './objects/unset'

/**
 * The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
 * @summary The {@link core.objects} package is a modular <b>JavaScript</b> library that provides extra <code>Object</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.objects
 * @memberof core
 */
const objects =
{
    at,
    castPath,
    clean,
    forEach,
    fuse,
    get,
    getTag,
    has,
    hasIn,
    hasPath,
    hasPathIn,
    isEmpty,
    isKey,
    keys,
    keysIn,
    map,
    members,
    merge,
    mergeDeep,
    omit,
    pairs,
    pick,
    pickBy,
    removeProperty,
    renameProperty,
    set,
    stub,
    toKey,
    unset,
} ;

export default objects ;