'use strict'

/**
 * Slice an array with a specific number of elements from the beginning
 * @name take
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array reference to slice.
 * @param {int} [count=1] - The number of elements to slice from the beginning.
 * @return {Array} The slice array representation.
 * @example
 * console.log( take( [1,2,3]     ) ) ; // [1]
 * console.log( take( [1,2,3] , 2 ) ) ; // [1,2]
 * console.log( take( [1,2,3] , 8 ) ) ; // [1,2,3]
 * console.log( take( [1,2,3] , 0 ) ) ; // []
 */
const take = ( array , count = 1 ) =>
{
    if( array instanceof Array && array.length > 0 )
    {
        return array.slice( 0 , count < 0 ? 0 : count ) ;
    }
    return [] ;
};

export default take ;