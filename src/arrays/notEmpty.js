'use strict'

/**
 * Indicates if the passed-in value is not an empty array.
 * @name notEmpty
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The value to evaluate.
 * @return {boolean} <code>true</code> if the object is not an empty array.
 * @example
 * console.log( notEmpty( [] ) ) ; // false
 * console.log( notEmpty( [1,2,3] ) ) ; // true
 * console.log( notEmpty(0) ) ; // false
 * console.log( notEmpty(true) ) ; // false
 * console.log( notEmpty(null) ) ; // false
 * console.log( notEmpty('hello') ) ; // false
 * console.log( notEmpty(null) ) ; // false
 */
const notEmpty = array => ( array instanceof Array) && ( array.length > 0 ) ;

export default notEmpty ;