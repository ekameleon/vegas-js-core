'use strict'

/**
 * Slice an array with a specific number of elements from the end
 * @name takeRight
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array reference to slice.
 * @param {int} [count=1] - The number of elements to slice from the end.
 * @return {Array} The slice array representation.
 * @example
 * console.log( takeRight( [1,2,3]     ) ) ; // [3]
 * console.log( takeRight( [1,2,3] , 2 ) ) ; // [2,3]
 * console.log( takeRight( [1,2,3] , 8 ) ) ; // [1,2,3]
 * console.log( takeRight( [1,2,3] , 0 ) ) ; // []
 */
const takeRight = ( array , count = 1 ) =>
{
    if( array instanceof Array && array.length > 0 )
    {
        const len = array.length ;
        count = len - count ;
        return array.slice( count < 0 ? 0 : count , len ) ;
    }
    return [] ;
};

export default takeRight ;