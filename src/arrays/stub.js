'use strict'

/**
 * This function return an empty array.
 * @name stub
 * @memberof core.arrays
 * @function
 * @instance
 * @since 1.0.36
 * @returns {Array} An empty Array
 * @example
 * const array = stub() ;
 * console.log( array ) ; // []
 */
const stub = () => [] ;

export default stub ;