'use strict'

/**
 * Copies the values of `source` to `array`.
 * @name copy
 * @memberof core.arrays
 * @since 1.0.34
 * @function
 * @instance
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns `array`.
 */
const copy = ( source , array ) =>
{
    const length= source.length ;
    let index   = -1 ;

    array = array ?? Array(length) ;

    while ( ++index < length )
    {
        array[index] = source[index] ;
    }

    return array ;
}

export default copy ;