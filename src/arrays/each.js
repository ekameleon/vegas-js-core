'use strict'

/**
 * Executes a provided function once for each array element.
 * @name each
 * @memberof core.arrays
 * @since 1.0.36
 * @function
 * @instance
 * @param {Array} array The array to iterate over.
 * @param {Function} callback The function invoked per iteration.
 * @returns {Array} The array reference.
 * @example
 * each( [1,2,3] , ( value , index , array ) =>
 * {
 *     console.log( value , index , array ) ;
 * } ) ;
 */
const each = ( array , callback ) =>
{
    const length = array?.length ?? 0 ;
    if( length > 0 && callback instanceof Function )
    {
        let index = -1 ;
        while ( ++index < length )
        {
            if ( callback( array[index] , index , array ) === false )
            {
                break ;
            }
        }
    }
    return array ;
}

export default each ;