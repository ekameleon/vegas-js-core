'use strict'

/**
 * Creates an array of values by running each element of `array` through `callback`. The callback function is invoked with three arguments: (value, index, array).
 * @name map
 * @memberof core.arrays
 * @function
 * @instance
 * @since 1.0.38
 * @param {array} array - The Array reference to transform.
 * @param {Function} callback - The position of the element to move
 * @return {array} Returns the new mapped array.
 * @example
 * const square = n => n * n ;
 * console.log( map([4, 8], square) ; // [ 16 , 64 ]
 */
const map = ( array , callback ) =>
{
    if( array?.length > 0 )
    {
        const length = array.length;
        const result = new Array(length);
        let index = -1 ;
        while ( ++index < length )
        {
            result[index] = callback( array[index] , index , array ) ;
        }
    }
    return [];
}

export default map ;