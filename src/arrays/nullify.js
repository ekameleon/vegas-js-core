'use strict'

/**
 * Returns <pre>null</pre> if the passed-in argument is not an Array and does not contain elements.
 * @name nullify
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array to check.
 * @return The array representation or null if the array is empty.
 * @example
 * let ar =  ;
 * console.log( nullify( [2, 3, 4] ) ) ; // [2, 3, 4]
 * console.log( nullify( [] ) ) ; // null
 */
const nullify = array => ( array instanceof Array ) && array.length > 0 ? array : null ;

export default nullify ;