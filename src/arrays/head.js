'use strict'

/**
 * Returns the first element of the array.
 * @name head
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array reference.
 * @return {*} The first element of the array.
 * @example
 * console.log( head( [2, 3, 4] ) ) ; // 2
 */
const head = array=> ( Array.isArray( array ) && array.length ) ? array[0] : undefined;

export default head ;