'use strict'

/**
 * Insert an element between all the items in an Array object.
 * @name inBetween
 * @memberof core.arrays
 * @version 1.0.19
 * @since 1.0.19
 * @function
 * @instance
 * @param {Array} source - The source Array.
 * @param {*} element - The element to insert between all the items in the array.
 * @return A new array representation of the original array.
 * @throws TypeError if the passed-in source parameter must be an Array instance.
 * @example
 * console.log( inBetween( ['a', 'b', 'c'], '/') ); // ["a", "/", "b", "/", "c"]
 */
const inBetween = ( source, element = null ) => 
{
    if( !Array.isArray( source ) )
    {
        throw new TypeError( 'inBetween failed, the passed-in source parameter must be an Array.' );
    }
    const len = source.length ;
    if (len > 1)
    {
        return source.flatMap((value, index) => (index === 0 || index === len) ? [value] : [element, value]);
    }
    return source;
}

export default inBetween ;