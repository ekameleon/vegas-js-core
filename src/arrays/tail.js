'use strict'

/**
 * Returns the elements of the array without the first in a new array representation.
 * @name tail
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array reference.
 * @return {Array} A new array representation without the first element.
 * @example
 * console.log( tail( [2, 3, 4] ) ) ; // [3,4]
 */
const tail = ( array ) =>
{
    const num = (array instanceof Array) ? array.length : 0;
    if (!num)
    {
        return [];
    }
    const [ , ...rest] = array ;
    return rest;
};

export default tail ;