'use strict'

/**
 * Indicates if the specified value exist in the array with a specific index.
 * @name distinct
 * @memberof core.arrays
 * @since 1.0.13
 * @function
 * @instance
 * @param {*} value - The object to find in the array.
 * @param {number} index - The index to find the value in the array.
 * @param {Array} array - The search Array.
 * @return {Boolean} `true` if the specified value exist in the array with a specific index.
 * @example
 * console.log( distinct( 3 , 0 , [1,2,3] ) ) ; // false
 * console.log( distinct( 3 , 1 , [1,2,3] ) ) ; // false
 * console.log( distinct( 3 , 2 , [1,2,3] ) ) ; // true
 */
const distinct = ( value , index , array ) => (array instanceof Array) ? (array.indexOf(value) === index) : false ;

export default distinct ;