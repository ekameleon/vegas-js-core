'use strict'

/**
 * A function to execute for each element in the array. It should return a truthy value to indicate the element passes the test, and a falsy value otherwise.
 * If such an element is found, some() immediately returns true and stops iterating through the array.
 * The callback function is invoked with three arguments: value, index, array.
 * @name copy
 * @memberof core.arrays
 * @since 1.0.37
 * @function
 * @instance
 * @param {Array} array The array to iterate over.
 * @param {Function} callback The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check, else `false`.
 * @example
 * console.log( some([null, 0, 'yes', false], Boolean) ) ; // true
 */
const some = ( array, callback ) =>
{
    if ( Array.isArray( array ) )
    {
        let index = -1 ;

        const length = array === null || array === undefined ? 0 : array.length;

        while ( ++index < length )
        {
            if ( callback( array[index] , index, array ) )
            {
                return true ;
            }
        }
    }

    return false;
}

export default some ;