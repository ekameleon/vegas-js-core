'use strict'

/**
 * Flatten a multidimensional array.
 * @name contains
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The Array to transform.
 * @return The value <code>true</code> if the specified object exists as an element in the array ; otherwise, <code>false</code>.
 * @example
 * let array = [1, [ 2, [ 3, [ 4 , 5 ] ] ] ] ;
 * console.log( flatten( array ) ) ; // [ 1 , 2 , 3 , 4 , 5 ]
 */
const flatten = array =>
{
    if( !Array.isArray( array ) )
    {
        throw new TypeError( 'flatten failed, the passed-in source parameter must be an Array.' );
    }
    return array.flat(Infinity) ;
}

export default flatten ;