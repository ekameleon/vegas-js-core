'use strict'

import isNil from '../isNil'

/**
 * A function to create flexibly-numbered lists of integers, handy for each and map loops. Start, if omitted, defaults to 0 ;
 * step defaults to 1 if start is before stop, otherwise -1. Returns a list of integers from start (inclusive) to stop (exclusive), incremented (or decremented) by step.
 * @name contains
 * @memberof core.arrays
 * @since 1.0.39
 * @function
 * @instance
 * @param {number|null} start - The start value of the range.
 * @param {number|null} stop - The stop value of the range.
 * @param {number} [step=1] - The stop value of the range.
 * @return {Array} The array representation of the range.
 * @example
 * let ar = [2, 3, 4] ;
 * console.log( range( 10 ) ) ; // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
 * console.log( range( 1 , 11 ) ) ; // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 * console.log( range( 0 , 30 , 5 ) ) ; // [ 0, 5, 10, 15, 20, 25 ]
 * console.log( range( 0, -10, -1 ) ) ; // [ 0, -1, -2, -3, -4, -5, -6, -7, -8, -9 ]
 */
const range = ( start, stop= null , step = 1 ) =>
{
    if ( isNil( stop ) )
    {
        stop  = start ?? 0 ;
        start = 0 ;
    }

    if (!step)
    {
        step = stop < start ? -1 : 1 ;
    }

    const length = Math.max( Math.ceil(( stop - start ) / step ) , 0 ) ;
    const  range = Array( length ) ;

    for( let id = 0; id < length; id++, start += step )
    {
        range[ id ] = start ;
    }

    return range;
}

export default range ;