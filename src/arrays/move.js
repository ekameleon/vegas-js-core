'use strict'

/**
 * Moves an element in the passed-in array to a new position, and then returns the array or a new array if the <pre>clone</pre> parameter is true.
 * @name move
 * @memberof core.arrays
 * @function
 * @instance
 * @version 1.0.24
 * @since 1.0.24
 * @param {array} array - The Array reference to transform.
 * @param {number} from - The position of the element to move
 * @param {number} to - The position of the element to move
 * @param {boolean} [clone=false] - [clone=false] Returns a new modified clone of the passed-in array.
 * @return {array} The modified array representation or new array if the <pre>clone</pre> is true.
 * @example
 * let ar = [1, 2, 3, 4] ;
 * console.log( move( ar , 1 , 2 ) ) ; // 1,3,2,4
 * console.log( move( ar , 1 , -1 ) ) ; // 1,3,4,2
 */
const move = ( array , from , to , clone = false ) =>
{
    if ( array instanceof Array )
    {
        if( clone )
        {
            array = [ ...array ] ;
        }

        if( array.length > 1 )
        {
            if( from < 0 )
            {
                from += array.length ;
                if( from < 0 )
                {
                    from = 0 ;
                }
            }

            if( to < 0 )
            {
                to += array.length ;
                if( to < 0 )
                {
                    to = 0 ;
                }
            }

            array.splice( to , 0 , array.splice( from , 1 )[0] ) ;
        }

        return array ;
    }
    else
    {
        throw new TypeError( 'move failed, the passed-in array argument is not an Array.')
    }
}

export default move ;