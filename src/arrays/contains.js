'use strict'

/**
 * Determines whether the specified object exists as an element in an Array object.
 * @name contains
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The search Array.
 * @param {*} value - The object to find in the array.
 * @return The value <code>true</code> if the specified object exists as an element in the array ; otherwise, <code>false</code>.
 * @example
 * let ar = [2, 3, 4] ;
 * console.log( contains( ar , 3 ) ) ; // true
 * console.log( contains( ar , 5 ) ) ; // false
 */
const contains = ( array  , value ) => Array.isArray(array) ? array.includes(value) : false ;

export default contains ;