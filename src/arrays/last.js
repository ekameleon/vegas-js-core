'use strict'

/**
 * Returns the last element of `array`.
 * @name last
 * @memberof core.arrays
 * @function
 * @instance
 * @param {Array} array - The array to evaluate.
 * @return {*} The last element of the array
 * @example
 * console.log( last( [1,2,3] ) ) ; // 3
 */
const last = array =>
{
    const length = array === null ? 0 : array.length ;
    return length ? array[length - 1] : undefined ;
}

export default last ;