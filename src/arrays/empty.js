'use strict'

/**
 * Indicates if the passed-in value is an empty array.
 * @name empty
 * @memberof core.arrays
 * @function
 * @instance
 * @param {*} array - The value to evaluate.
 * @return {boolean} <code>true</code> if the object is an empty array.
 * @example
 * console.log( empty( [] ) ) ; // true
 * console.log( empty( [1,2,3] ) ) ; // false
 * console.log( empty(0) ) ; // false
 * console.log( empty(true) ) ; // false
 * console.log( empty(null) ) ; // false
 * console.log( empty('hello') ) ; // false
 * console.log( empty(null) ) ; // false
 */
const empty = array => ( array instanceof Array ) && ( array.length === 0 ) ;

export default empty ;