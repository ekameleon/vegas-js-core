'use strict'

import gcd   from '../maths/gcd'
import floor from '../maths/floor'

/**
 * Resize a specific area with a specific new height component value and keep the original aspect ratio.
 * @name resizeHeightAndKeepAspectRatio
 * @memberof core.graphics
 * @function
 * @instance
 * @param {number} height - The new height value to transform the specific area size.
 * @param {number} originalWidth - The original width value to transform.
 * @param {number} originalHeight - The original height value to transform.
 * @return An object with a new width/height values.
 */
const resizeHeightAndKeepAspectRatio = ( height, originalWidth, originalHeight ) =>
{
    const _gcd = gcd( originalWidth , originalHeight ) ;

    let _aspW = floor( originalWidth  / _gcd , 0 ) ;
    let _aspH = floor( originalHeight / _gcd , 0 ) ;

    if( isNaN(_aspW) )
    {
        _aspW = 0 ;
    }

    if( isNaN(_aspH) )
    {
        _aspH = 0 ;
    }

    return {
        width  : floor(height * _aspW / _aspH , 0 ) ,
        height : floor(height , 0)
    };
};

export default resizeHeightAndKeepAspectRatio ;
