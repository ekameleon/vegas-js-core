'use strict'

import gcd   from '../maths/gcd'
import floor from '../maths/floor'

/**
 * Resize a specific area with a specific new width component value and keep the original aspect ratio.
 * @name resizeWidthAndKeepAspectRatio
 * @memberof core.graphics
 * @function
 * @instance
 * @param {number} width - The new width value to transform the specific area size.
 * @param {number} originalWidth - The original width value to transform.
 * @param {number} originalHeight - The original height value to transform.
 * @return An object with a new width/height component values.
 */
const resizeWidthAndKeepAspectRatio = ( width, originalWidth, originalHeight ) =>
{
    const _gcd = gcd( originalWidth , originalHeight ) ;

    let _aspW = floor( originalWidth  / _gcd , 0 ) ;
    let _aspH = floor( originalHeight / _gcd , 0 ) ;

    if( isNaN(_aspW) )
    {
        _aspW = 0 ;
    }

    if( isNaN(_aspH) )
    {
        _aspH = 0 ;
    }

    return {
        width  : floor(width , 0) ,
        height : floor(width * _aspH / _aspW , 0 )
    };
};

export default resizeWidthAndKeepAspectRatio ;
