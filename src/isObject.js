'use strict'

/**
 * Indicates if the specific value is an Object.
 * (e.g. objects, arrays, functions, regexes, `new Number(0)`, and `new String('')`)
 * @name isObject
 * @memberof core
 * @function
 * @instance
 * @param {Object} value - The value to check.
 * @return {boolean} <code>true</code> if the object is an Object.
 * @example
 * console.log( isObject({}) ) ; // true
 * console.log( isObject([1, 2, 3]) ) ; // true
 * console.log( isObject(Function) ) ; // true
 * console.log( isObject(0) ) ; // false
 * console.log( isObject(true) ) ; // false
 * console.log( isObject(null) ) ; // false
 * console.log( isObject(undefined) ) ; // false
 * console.log( isObject('hello') ) ; // false
 */
const isObject = ( value ) =>
{
    const type = typeof(value);
    return value !== null && value !== undefined && (type === 'object' || type === 'function') ;
};

export default isObject ;