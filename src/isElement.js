'use strict'

import isObjectLike from './isObjectLike.js'
import isPlainObject from './isPlainObject.js'

/**
 * Checks if `value` is likely a DOM element.
 * @static
 * @memberOf core
 * @since 1.0.36
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a DOM element, else `false`.
 * @example
 * console.log( isElement( document.body ) ) ; // true
 * console.log( isElement('<body>') ) ; // false
 */
const isElement = value => isObjectLike(value) && value.nodeType === 1 && !isPlainObject( value );

export default isElement;