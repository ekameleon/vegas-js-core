'use strict'

import acosD                   from './maths/acosD'
import acosHm                  from './maths/acosHm'
import acosHp                  from './maths/acosHp'
import angleOfLine             from './maths/angleOfLine'
import asinD                   from './maths/asinD'
import asinH                   from './maths/asinH'
import atan2D                  from './maths/atan2D'
import atanD                   from './maths/atanD'
import atanH                   from './maths/atanH'
import bearing                 from './maths/bearing'
import berp                    from './maths/berp'
import bounce                  from './maths/bounce'
import cartesianToPolar        from './maths/cartesianToPolar'
import ceil                    from './maths/ceil'
import clamp                   from './maths/clamp'
import clerp                   from './maths/clerp'
import cosD                    from './maths/cosD'
import coserp                  from './maths/coserp'
import cosH                    from './maths/cosH'
import DEG2RAD                 from './maths/DEG2RAD'
import degreesToRadians        from './maths/degreesToRadians'
import distance                from './maths/distance'
import distanceByObject        from './maths/distanceByObject'
import EARTH_RADIUS_IN_METERS  from './maths/EARTH_RADIUS_IN_METERS'
import EPSILON                 from './maths/EPSILON'
import factorial               from './maths/factorial'
import fibonacci               from './maths/fibonacci'
import finalBearing            from './maths/finalBearing'
import fixAngle                from './maths/fixAngle'
import floor                   from './maths/floor'
import floorMod                from './maths/floorMod'
import gcd                     from './maths/gcd'
import haversine               from './maths/haversine'
import hermite                 from './maths/hermite'
import hypothenuse             from './maths/hypothenuse'
import interpolate             from './maths/interpolate'
import isEven                  from './maths/isEven'
import isOdd                   from './maths/isOdd'
import LAMBDA                  from './maths/LAMBDA'
import lerp                    from './maths/lerp'
import littleEndian            from './maths/littleEndian'
import log10                   from './maths/log10'
import logN                    from './maths/logN'
import map                     from './maths/map'
import midPoint                from './maths/midPoint'
import MILE_TO_METER           from './maths/MILE_TO_METER'
import modulo                  from './maths/modulo'
import nearlyEquals            from './maths/nearlyEquals'
import normalize               from './maths/normalize'
import percentage              from './maths/percentage'
import PHI                     from './maths/PHI'
import PI2                     from './maths/PI2'
import polarToCartesian        from './maths/polarToCartesian'
import RAD2DEG                 from './maths/RAD2DEG'
import replaceNaN              from './maths/replaceNaN'
import round                   from './maths/round'
import sharpen                 from './maths/sharpen'
import sign                    from './maths/sign'
import sinD                    from './maths/sinD'
import sinerp                  from './maths/sinerp'
import sinH                    from './maths/sinH'
import tanD                    from './maths/tanD'
import tanH                    from './maths/tanH'
import vincenty                from './maths/vincenty'
import wrap                    from './maths/wrap'

/**
 * The {@link core.maths} package is a modular <b>JavaScript</b> library that provides extra <code>mathematics</code> methods and implementations.
 * @summary The {@link core.maths} package is a modular <b>JavaScript</b> library that provides extra <code>mathematics</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.maths
 * @memberof core
 */
const maths =
{
    acosD,
    acosHm,
    acosHp,
    angleOfLine,
    asinD,
    asinH,
    atan2D,
    atanD,
    atanH,
    bearing,
    berp,
    bounce,
    cartesianToPolar,
    ceil,
    clamp,
    clerp,
    cosD,
    coserp,
    cosH,
    DEG2RAD,
    degreesToRadians,
    distance,
    distanceByObject,
    EARTH_RADIUS_IN_METERS,
    EPSILON,
    factorial,
    fibonacci,
    finalBearing,
    fixAngle,
    floor,
    floorMod,
    gcd,
    haversine,
    hermite,
    hypothenuse,
    interpolate,
    isEven,
    isOdd,
    LAMBDA,
    lerp,
    littleEndian,
    log10,
    logN,
    map,
    midPoint,
    MILE_TO_METER,
    modulo,
    nearlyEquals,
    normalize,
    percentage,
    PHI,
    PI2,
    polarToCartesian,
    RAD2DEG,
    replaceNaN,
    round,
    sharpen,
    sign,
    sinD,
    sinerp,
    sinH,
    tanD,
    tanH,
    vincenty,
    wrap
} ;

export default maths ;
