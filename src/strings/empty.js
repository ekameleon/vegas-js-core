'use strict'

/**
 * Indicates if the passed-in value is an empty string.
 * @name empty
 * @memberof core.strings
 * @function
 * @instance
 * @param {*} value - The value to evaluate.
 * @return {boolean} <code>true</code> if the object is string and is not empty.
 * @example
 * console.log( empty(0) ) ; // false
 * console.log( empty(true) ) ; // false
 * console.log( empty(null) ) ; // false
 * console.log( empty('hello') ) ; // false
 * console.log( empty('') ) ; // true
 */
const empty = ( value ) =>  ((typeof(value) === 'string') || (value instanceof String )) && (value === '') ;

export default empty ;