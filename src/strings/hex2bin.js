'use strict'

import substr from './substr'

/**
 * Indicates if the passed-in value is an empty string.
 * @name hex2Bin
 * @memberof core.strings
 * @since 1.0.44
 * @function
 * @instance
 * @param {string} value - The string value to transform.
 * @return {string|boolean} The binary representation of the passed-in hex string expression.
 * @example
 */
const hex2Bin = value =>
{
    let i = 0 ;
    let l ;

    value += ''

    const ret = []
    for ( l = value.length ; i < l ; i += 2 )
    {
        const c = parseInt( substr( value , i , 1 )    , 16 ) ;
        const k = parseInt( substr( value , i + 1 , 1) , 16 ) ;

        if ( isNaN(c) || isNaN(k) )
        {
            return false ;
        }

        ret.push( ( c << 4 ) | k ) ;
    }

    return String.fromCharCode.apply( String , ret ) ;
}

export default hex2Bin ;