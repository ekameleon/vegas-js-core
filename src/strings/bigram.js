'use strict'

import nGram from './nGram'

/**
 * Creates the bigram representation from a given value.
 * @name bigram
 * @memberof core.strings
 * @param {string} value - The expression to evaluates.
 * @function
 * @instance
 * @since 1.0.37
 * @example
 * console.log( bigram('n-gram') ) ; // ['n-', '-g', 'gr', 'ra', 'am']
 * console.log( bigram(['alpha', 'bravo', 'charlie']) ) ; // [['alpha', 'bravo'], ['bravo', 'charlie']]
 */
const bigram = nGram(2 ) ;

export default bigram ;