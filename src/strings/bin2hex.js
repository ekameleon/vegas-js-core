'use strict'

/**
 * Convert binary data into hexadecimal representation
 * @name bin2hex
 * @memberof core.strings
 * @since 1.0.44
 * @function
 * @instance
 * @param {string} value - The string value to transform.
 * @return {string} Returns the hexadecimal representation of the given string.
 * @example
 * const hex = bin2hex('Hello world!');
 * console.log( hex ) ; // 48656c6c6f20776f726c6421
 * console.log( hex2bin( hex ) ) ; // Hello world!
 */
const bin2hex = value =>
{
    const encoder = new TextEncoder() ;
    const bytes = encoder.encode( value ) ;
    let hex = '' ;
    for ( const byte of bytes )
    {
        hex += byte.toString(16).padStart( 2, '0' ) ;
    }
    return hex ;
}

export default bin2hex ;