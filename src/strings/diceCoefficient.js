'use strict'

import bigram from './bigram'

/**
 * Returns the difference value according to Dice-Sørensen coefficient. That means it gets two strings (typically words), and turns it into a number between 0 (completely different) and 1 (exactly the same).
 * You can pass bigrams (from [`n-gram`][n-gram]) too, which will improve performance when you are comparing the same values multiple times.
 * @param {string|Array<string>} value - The primary value to evaluates
 * @param {string|Array<string>} other - The second value to evaluates.
 * @return {number} The difference value according to Dice-Sørensen coefficient. The result is normalized to a number between `0` (completely different and `1` (exactly the same).
 * @see https://en.wikipedia.org/wiki/Dice-S%C3%B8rensen_coefficient
 * @since 1.0.37
 */
const diceCoefficient = ( value , other ) =>
{
    const left  = toPairs( value ) ;
    const right = toPairs( other ) ;

    let index = -1 ;
    let intersections = 0 ;

    while ( ++index < left.length )
    {
        const leftPair = left[index] ;

        let offset = -1 ;
        while ( ++offset < right.length )
        {
            const rightPair = right[offset] ;

            if (leftPair === rightPair)
            {
                intersections++ ;
                right[offset] = ''  ; // Make sure this pair never matches again.
                break ;
            }
        }
    }

    return ( 2 * intersections) / ( left.length + right.length ) ;
}

export default diceCoefficient ;

/**
 * @private
 * @param {string|Array<string>} value
 * @returns {Array<string>}
 */
const toPairs = value =>
{
    if ( Array.isArray(value) )
    {
        return value.map( bigram => normalize(bigram) ) ;
    }

    const normal = normalize(value) ;
    return normal.length === 1 ? [normal] : bigram(normal) ;
}

/**
 * @private
 * @param {string} value
 * @returns {string}
 */
const normalize = value => String( value ).replace( /\s+/g , '' ).toLowerCase() ;