'use strict'

/**
 * Converts a value string to n-grams.
 * An n-gram is a sequence of n adjacent symbols in particular order.
 * @name nGram
 * @memberof core.strings
 * @function
 * @instance
 * @since 1.0.37
 * @param {number} n The number of adjacent symbols.
 * @return The function that creates n-grams from a given value.
 * @see https://en.wikipedia.org/wiki/N-gram
 * @example
 * console.log( nGram(2)('n-gram') ) ; // ['n-', '-g', 'gr', 'ra', 'am']
 * console.log( nGram(6)('n-gram') ) ; // ['n-gram']
 */
const nGram = n =>
{
    if ( typeof n !== 'number' || Number.isNaN(n) || n < 1 || n === Number.POSITIVE_INFINITY )
    {
        throw new Error( '`' + n + '` is not a valid argument for `n-gram`' )
    }

    return value =>
    {
        const nGrams = [] ;

        if ( value === null || value === undefined )
        {
            return nGrams
        }

        const source = typeof value.slice === 'function' ? value : String(value) ;
        let index = source.length - n + 1 ;

        if ( index < 1 )
        {
            return nGrams
        }

        while (index--)
        {
            nGrams[index] = source.slice(index, index + n)
        }

        return nGrams ;
    }
}

export default nGram ;