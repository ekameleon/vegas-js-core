'use strict'

import toIntegerOrInfinity from '../numbers/toIntegerOrInfinity'

/**
 * Returns a portion of the passed-in string, starting at the specified index and extending for a given number of characters afterward.
 * @name substr
 * @memberof core.strings
 * @since 1.0.44
 * @function
 * @instance
 * @param {string} string - The string value to evaluates.
 * @param {int} start - The index of the first character to include in the returned substring.
 * @param {int} length - The number of characters to extract.
 * @return {string} A new string containing the specified part of the given string.
 * @example
 * const str = 'Mozilla';
 * console.log( substr(str, 1, 2) ); // "oz"
 * console.log( substr(str, 2) ); // "zilla"
 */
const substr = ( string , start , length ) =>
{
    let size = string.length;

    start = toIntegerOrInfinity( start ) ;
    if (start === Infinity)
    {
        start = 0 ;
    }
    else if ( start < 0 )
    {
        start = Math.max(size + start , 0 ) ;
    }

    length = length === undefined ? size : toIntegerOrInfinity(length);
    if  (length <= 0 || length === Infinity)
    {
        return '' ;
    }

    let end = Math.min(start + length , size ) ;
    return start >= end ? '' : string.slice( start , end ) ;
}

export default substr ;