'use strict'

import memoizeBounded from '../functors/memoizeBounded'

const charCodeOfDot = '.'.charCodeAt(0) ;
const escapeChar    = /\\(\\)?/g ;
const propName      = RegExp
(

     '[^.[\\]]+' + '|' // Match anything that isn't a dot or bracket.
   + '\\[(?:' // Or match property names within brackets.
   + '([^"\'][^[]*)' + '|' // Match a non-string expression.
   + '(["\'])((?:(?!\\2)[^\\\\]|\\\\.)*?)\\2' // Or match strings (supports escaping characters).
   + ')\\]'+ '|' // Or match "" as the space between consecutive dots or empty brackets.
   + '(?=(?:\\.|\\[\\])(?:\\.|\\[\\]|$))'
   , 'g'
);

/**
 * Converts `string` to a property path array.
 * @private
 * @param {string} source - The string to convert.
 * @returns {Array} Returns the property path array.
 */
const stringToPath = memoizeBounded( source =>
{
    const result = [] ;
    if ( source.charCodeAt(0) === charCodeOfDot )
    {
        result.push( '' ) ;
    }

    source.replace( propName, ( match , expression , quote , subString) =>
    {
        let key = match ;
        if ( quote )
        {
            key = subString.replace( escapeChar , '$1' ) ;
        }
        else if ( expression )
        {
            key = expression.trim() ;
        }
        result.push( key ) ;
    });

    return result ;
});

export default stringToPath ;