'use strict'

import deburr from './deburr.js'
import words  from './words.js'

const transform = (result, word, index) => ( result + (index ? '_' : '') + word.toLowerCase()) ;

const rsApos = "['\u2019]" ;
const reApos = RegExp(rsApos, 'g');

/**
 * Converts `string` to a [snake case](https://en.wikipedia.org/wiki/Snake_case) expression.
 * @name snakeCase
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} [source=''] - The string to transform.
 * @return {String} The [snake case](https://en.wikipedia.org/wiki/Snake_case) representation.
 * @example
 * console.log(snakeCase('Foo Bar')) ; // 'foo_bar'
 * console.log(snakeCase('fooBar')) ; // 'foo_bar'
 * console.log(snakeCase('--FOO-BAR--')); // 'foo_bar'
 * @since 1.0.2
 */
export default function snakeCase( source = null )
{
    if( !(source instanceof String || typeof(source) === 'string') )
    {
        return null ;
    }
    return words(deburr(source).replace(reApos, '')).reduce( transform , '' ) ;
}