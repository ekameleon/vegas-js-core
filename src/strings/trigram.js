'use strict'

import nGram from './nGram'

/**
 * Creates the trigram representation from a given value.
 * @name trigram
 * @memberof core.strings
 * @param {string} value - The expression to evaluates.
 * @function
 * @instance
 * @since 1.0.37
 * @example
 * console.log( trigram('n-gram') ) ; // ['n-g', '-gr', 'gra', 'ram']
 */
const trigram = nGram(3 ) ;

export default trigram ;