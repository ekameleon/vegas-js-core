'use strict'

/**
 * Capitalize each word in a string.
 * @name ucWords
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} source - The string reference to transform.
 * @param {string} [separator=' '] - The optional separator expression.
 * @return The new string expression with each word capitalized.
 * @example
 * console.log( ucWords("hello world")) ; // Hello World
 * console.log( ucWords("hello-world","-")) ; // Hello-World
 */
export default function ucWords( source , separator = " " )
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return '' ;
    }
    let ar = source.split( separator ) ;
    let l  = ar.length ;
    while(--l > -1)
    {
        ar[l] = ar[l].charAt(0).toUpperCase() + ar[l].substring(1) ;
    }
    return ar.join( separator ) ;
}
