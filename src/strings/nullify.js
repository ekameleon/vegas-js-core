'use strict'

import empty from './empty'

/**
 * Nullify the string value if empty.
 * @name nullify
 * @memberof core.arrays
 * @since 1.0.36
 * @function
 * @instance
 * @param {string} value - The object to find in the array.
 * @return {string|null} The value or null if the string is empty.
 * @example
 * console.log( nullify('') ) ; // null
 * console.log( nullify('foo') ) ; // foo
 */
const nullify = value => empty(value) ? null : value ;

export default nullify ;