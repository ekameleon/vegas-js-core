'use strict'

/**
 * Capitalize the first letter of a string.
 * @name ucFirst
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} source - The string reference to transform.
 * @return The capitalized first expression.
 * @example
 * console.log( ucFirst("hello world")) ; // Hello world
 */
export default function ucFirst( source )
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return '' ;
    }
    return source.charAt(0).toUpperCase() + source.substring(1) ;
}