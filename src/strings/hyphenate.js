'use strict'

/**
 * Converts a camel cased string to a hyphenated string.
 * @name hyphenate
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} source - The string reference to hyphenate.
 * @return The hyphenated string.
 * @example
 * console.log( hyphenate( "helloWorld" ) ) ; //"hello-world"
 */
const hyphenate = source =>
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return '' ;
    }
    return source.replace
    (
        /[A-Z]/g, ( match ) => '-' + match.charAt(0).toLowerCase()
    );
}

export default hyphenate ;