'use strict'

/**
 * Quick and fast format of a string using indexed parameters only.
 * <p><strong>Usage :</strong>
 * <ul>
 * <li><code>fastformat( pattern:String, ...args:Array ):String</code></li>
 * <li><code>fastformat( pattern:String, [arg0:*,arg1:*,arg2:*, ...] ):String</code></li>
 * </ul>
 * </p>
 * @name fastformat
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} pattern - The String pattern expression to format.
 * @param {...string} args - A serie of strings values or of arrays of strings to fill the pattern expression.
 * @return The formatted expression.
 * @example
 * console.log( fastformat( "hello {0}", "world" ) ); // "hello world"
 * console.log( fastformat( "hello {0} {1} {2}", [ "the", "big", "world" ] ) ); // "hello the big world"
 * console.log( fastformat( "hello {0} {1} {2}", [ "the", "big" ] , "world" ) ); // "hello the big world"
 */
const fastFormat = ( pattern , ...args ) =>
{
    if( (pattern === null) || !(pattern instanceof String || typeof(pattern) === 'string' ) )
    {
        return "" ;
    }

    if( args.length > 0 )
    {
        args = [].concat.apply( [] , args )
        let len  = args.length ;
        for( let i = 0 ; i < len ; i++ )
        {
            pattern = pattern.replace( new RegExp( "\\{" + i + "\\}" , "g" ), args[i] );
        }
    }

    return pattern;
}

export default fastFormat ;