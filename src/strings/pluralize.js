'use strict'

const defaultOptions =
{
    format   : null ,
    singular : '' ,
    plural   : 's'
}

/**
 * Quick and fast format of a string to pluralize or singular it.
 * @name pluralize
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} expression - The expression to format.
 * @param {string} count - The number of elements.
 * @param {object} options - The generic object to set the formatter.
 * @param {object} [options.plural='s'] - The plural expression
 * @param {object} [options.singular=''] - The singular expression
 * @param {function} [options.format=null] - A custom function to format the expression.
 * @return <code>true</code> if the value is find in first.
 * @example <caption>Basic usage</caption>
 * console.log( 'my ' + pluralize( "cat" , 0 ) ); // my cat
 * console.log( 'my ' + pluralize( "cat" , 1 ) ); // my cat
 * console.log( 'my ' + pluralize( "cat" , 2 ) ); // my cats
 *
 * const options =
 * {
 *     format : ( { expression , count , plural , singular } = {} ) =>
 *     {
 *         const params = count > 1 ? plural : singular ;
 *         const { one , two } = params || {} ;
 *         return fastformat( expression , one , two ) ;
 *     },
 *     plural : { one:'mes' , two : 'x' } ,
 *     singular : { one : 'mon' , two : '' }
 * };
 *
 * console.log( pluralize( '{0} bijou{1}' , 1 , options ) ) ;
 * console.log( pluralize( '{0} bijou{1}' , 2 , options ) ) ;
 */

const pluralize = ( expression , count , options = null ) =>
{
    options = { ...defaultOptions , ...options } ;

    const { format , plural , singular } = options ;

    if( format instanceof Function )
    {
        return format({ expression , count , plural , singular }) ;
    }

    return expression + ( count > 1 ? plural : singular ) ;
}

export default pluralize ;