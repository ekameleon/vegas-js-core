'use strict'

import unicodeWords from './unicodeWords.js'

const reAsciiWord      = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g;
const reHasUnicodeWord = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;

const asciiWords     = string => string.match(reAsciiWord) || [];
const hasUnicodeWord = string => reHasUnicodeWord.test( string ) ;

/**
 * Splits `string` into an array of its words.
 * @name words
 * @memberof core.strings
 * @function
 * @instance
 * @param source {String} The string to inspect.
 * @param pattern {RegExp|string} The pattern to match words.
 * @param guard {Boolean} The optional flag to enforce the pattern to be undefined.
 * @return {Array} An array of its words.
 * @since 1.0.2
 * @example
 * console.log(words('vegas, neo, & inu')) ;
 * // ['vegas', 'neo', 'inu']
 *
 * console.log(words('vegas, neo, & inu', /[^, ]+/g));
 * // ['vegas', 'neo', '&', 'inu']
 */
const words = ( source , pattern = undefined , guard = false ) =>
{
    pattern = guard ? undefined : pattern ;
    if ( pattern === undefined )
    {
        return hasUnicodeWord(source) ? unicodeWords(source) : asciiWords(source);
    }
    return source.match(pattern) || [] ;
};

export default words ;