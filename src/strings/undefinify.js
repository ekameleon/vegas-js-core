'use strict'

import empty from './empty'

const undefinify = value => empty(value) ? undefined : value ;

export default undefinify ;