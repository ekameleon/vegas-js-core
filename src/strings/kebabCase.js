'use strict'

import words from './words.js'
import toString from '../toString.js'

/**
 * Converts a string to a kebabCase string.
 * @name kebabCase
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} source - The string reference to kebabCase.
 * @return The kebabCase string.
 * @see https://en.wikipedia.org/wiki/Letter_case#Kebab_case
 * @version 1.0.37
 * @example
 * console.log( kebabCase( "helloWorld" ) ) ; // "hello-world"
 * console.log( kebabCase( "Hello World" ) ) ; // "hello-world"
 * console.log( kebabCase( "__HELLO__WORLD__" ) ) ; // "hello-world"
 * console.log( kebabCase( 1 ) ) ; // ""
 * console.log( kebabCase( true ) ) ; // ""
 */
const kebabCase = source =>
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return '' ;
    }
    return words
    (
        toString( source ).replace(/['\u2019]/g, '')
    )
    .reduce( ( result , word, index) => result + (index ? '-' : '') + word.toLowerCase() , '' , );
}

export default kebabCase ;