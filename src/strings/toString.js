'use strict'

/**
 * Returns the string representation of the passed-in value.
 * @name toString
 * @memberof core.strings
 * @function
 * @instance
 * @param {*} value - The value to transform.
 * @param {*} [defaultValue=''] The default value to return if the value is null or undefined.
 * @return {*} The default representation of passed-in value. Returns `defaultValue` if `value` is `null` or `undefined`.
 * @example
 * let value ;
 * console.log( toString(value,"unknow") ) ; // unknow
 */
const toString = ( value , defaultValue = '' ) =>
{
    if( value === null || value === undefined )
    {
        return defaultValue ;
    }
    
    if( value instanceof String || typeof(value) === 'string' )
    {
        return value ;
    }
    
    return String( value ) ;
};

export default toString ;