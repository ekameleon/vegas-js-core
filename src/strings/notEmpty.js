'use strict'

/**
 * Indicates if the passed-in value is not an empty string.
 * @name notEmpty
 * @memberof core.strings
 * @function
 * @instance
 * @param {*} value - The value to evaluate.
 * @return {boolean} <code>true</code> if the object is string and is not empty.
 * @example
 * console.log( notEmpty(0) ) ; // false
 * console.log( notEmpty(true) ) ; // false
 * console.log( notEmpty(null) ) ; // false
 * console.log( notEmpty('hello') ) ; // true
 * console.log( notEmpty('') ) ; // false
 */
const notEmpty = value => ((typeof(value) === 'string') || (value instanceof String )) && (value !== '') ;

export default notEmpty ;