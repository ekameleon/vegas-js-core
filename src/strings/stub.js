'use strict'

/**
 * This function return an empty string.
 * @name stub
 * @memberof core.strings
 * @function
 * @instance
 * @since 1.0.36
 * @returns {string}
 * @example
 * const string = stub() ;
 * console.log( string ) ; // ''
 */
const stub = () => '' ;

export default stub ;