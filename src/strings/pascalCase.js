'use strict'

import capitalize from './capitalize.js'
import words      from './words.js'

/**
 * Converts a string to a Pascal Case string.
 * @name pascalCase
 * @memberof core.strings
 * @function
 * @instance
 * @param {string} source - The string reference to kebabCase.
 * @return The kebabCase string.
 * @see https://en.wikipedia.org/wiki/Letter_case#Pascal_case
 * @version 1.0.40
 * @example
 * console.log( kebabCase( "pascalCase" ) ) ; // "PascalCase"
 */
const pascalCase = source =>
{
    if( !( source instanceof String || typeof(source) === 'string' ) || source === '' )
    {
        return '' ;
    }

    return words( source ).map(  word => capitalize( word ) ).join('') ;
}

export default pascalCase ;