'use strict'

import baseIsTypedArray from './_/baseIsTypedArray.js'
import baseUnary        from './_/baseUnary.js'
import nodeUtil         from './_/nodeUtil.js'

/* Node.js helper references. */
const nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if the passed-in `value` is classified as a typed array.
 * @static
 * @function
 * @memberOf core
 * @since 1.0.36
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 * isTypedArray(new Uint8Array()); // true
 * isTypedArray([]); false
 */
const isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

export default isTypedArray;