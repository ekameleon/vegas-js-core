'use strict'

/**
 * Checks whether a value is a JavaScript primitive, include null, undefined, strings, numbers, booleans, symbols, and bigints.
 * @memberOf core
 * @static
 * @since 1.0.39
 * @param {*} value The value to evaluates.
 * @returns {boolean} Returns `true` if the value is a primitive.
 * @example
 * isPrimitive(null); // true
 * isPrimitive(undefined); // true
 * isPrimitive('123'); // true
 * isPrimitive(false); // true
 * isPrimitive(true); // true
 * isPrimitive(Symbol('a')); // true
 * isPrimitive(123n); // true
 * isPrimitive({}); // false
 * isPrimitive(new Date()); // false
 * isPrimitive(new Map()); // false
 * isPrimitive(new Set()); // false
 * isPrimitive([1, 2, 3]); // false
 */
const isPrimitive = value => value === null || (typeof value !== 'object' && typeof value !== 'function') ;

export default isPrimitive ;