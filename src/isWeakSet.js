'use strict'

import getTag       from './objects/getTag.js'
import isObjectLike from './isObjectLike.js'

import { weakSetTag } from './_/tags'

/**
 * Indicates if the specific object is a WeakSet instance.
 * @name isWeakMap
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @param {*} value - The value to check.
 * @return {boolean} Returns `true` if `value` is a weak set, else `false`.
 * @example
 * console.log( isWeakSet( new Set() ) // false ;
 * console.log( isWeakSet( new WeakSet() ) // true ;
 */
const isWeakSet = value => isObjectLike(value) && getTag(value) === weakSetTag ;

export default isWeakSet ;