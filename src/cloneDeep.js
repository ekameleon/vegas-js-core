'use strict'

import baseClone from './_/baseClone.js';

export const CLONE_DEEP_FLAG    = 1 ;
export const CLONE_SYMBOLS_FLAG = 4 ;

/**
 * Clone all object values recursively`.
 * @static
 * @memberOf core
 * @since 1.0.26
 * @category Lang
 * @param {*} value The value to recursively clone.
 * @returns {*} Returns the deep cloned value.
 * @example
 * let objects = [{ 'a': 1 }, { 'b': 2 }];
 * let deep = cloneDeep(objects);
 * console.log(deep[0] === objects[0]); // => false
 */
const cloneDeep = value => baseClone( value , CLONE_DEEP_FLAG | CLONE_SYMBOLS_FLAG ) ;

export default cloneDeep;