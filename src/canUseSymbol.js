'use strict'

/**
 * This constant is `true` if the version of javascript is compatible with the Symbol element.
 * @name canUseSymbol
 * @memberof core
 * @since 1.0.14
 * @instance
 */
const canUseSymbol = typeof Symbol === 'function' && Symbol.for ;

export default canUseSymbol ;