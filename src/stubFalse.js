'use strict'

/**
 * This function return always false.
 * @name stub
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @returns {boolean}
 * @example
 * const bool = stub() ;
 * console.log( bool ) ; // false
 */
const stubFalse = () => false ;

export default stubFalse ;