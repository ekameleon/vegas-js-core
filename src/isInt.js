'use strict'

/**
 * Indicates if an value is an integer.
 * @name isInt
 * @memberof core
 * @function
 * @instance
 * @param {number} value - The value to evaluates.
 * @return <code>true</code> if the passed-in value is an integer.
 * @example
 * console.log( isInt(-1) ) ; // true
 * console.log( isInt(0) ) ; // true
 * console.log( isInt(0.5) ) ; // false
 * console.log( isInt(1) ) ; // true
 */
const isInt = value => Number(value) === value && (value%1 === 0) ;

export default isInt ;