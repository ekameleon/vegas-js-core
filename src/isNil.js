'use strict'

/**
 * Indicates if a value is 'null' or 'undefined'.
 * @name isNil
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` is the passed-in value is `null` or `undefined`, `false` otherwise
 * @example
 * console.log( isNil( null ) ; // true
 * console.log( isNil( undefined ) ; // true
 * console.log( isNil( 1 ) ; // false
 */
const isNil = value => value === undefined || value === null ;

export default isNil ;