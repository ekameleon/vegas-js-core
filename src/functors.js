'use strict'

import aop            from './functors/aop'
import callOrReturn   from './functors/callOrReturn'
import compose        from './functors/compose'
import debounce       from './functors/debounce'
import empty          from './functors/empty'
import maybeCall      from './functors/maybeCall'
import memoize        from './functors/memoize'
import memoizeBounded from './functors/memoizeBounded'
import negate         from './functors/negate'
import sortBy         from './functors/sortBy'
import throttle       from './functors/throttle'

/**
 * The {@link core.functors} package is a modular <b>JavaScript</b> library that provides extra <code>Function</code> methods.
 * @summary The {@link core.functors} package is a modular <b>JavaScript</b> library that provides extra <code>Function</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.functors
 * @memberof core
 */
const functors =
{
    aop,
    callOrReturn,
    compose,
    debounce,
    empty,
    maybeCall,
    memoize,
    memoizeBounded,
    negate,
    sortBy ,
    throttle
} ;

export default functors ;