'use strict'

import baseGetTag from './_/baseGetTag.js'
import isObject from './isObject.js'

import {
    asyncTag ,
    funcTag ,
    genTag ,
    proxyTag ,
}
from './_/tags'

/**
 * Indicates if the specific value is a Function.
 * @since 1.0.10
 * @version 1.0.37
 * @name isFunction
 * @memberof core
 * @function
 * @instance
 * @param {Object} value - The value to evaluates.
 * @return {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 * console.log( isFunction(() => {} ) ) ; // true
 * console.log( isFunction(Array.prototype.push) ) ; // true
 */
const isFunction = value =>
{
    // const type = typeof(value);
    // return value !== null && (type === 'object' || type === 'function') ;

    if ( !isObject( value ) )
    {
        return false ;
    }
    // The use of `Object#toString` avoids issues with the `typeof` operator in Safari 9 which returns 'object' for typed arrays and other constructors.
    const tag = baseGetTag( value ) ;

    return tag === funcTag || tag === genTag || tag === asyncTag || tag === proxyTag;
};

export default isFunction ;