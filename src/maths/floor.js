'use strict'

/**
 * Rounds and returns a number by a count of floating points.
 * @name floor
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} n - The number to round.
 * @param {number} [floatCount=0] the count of number after the point.
 * @return the floor value of a number by a count of floating points.
 * @example
 * console.log(floor(4.572525153, 2)) ; // 4.57
 * console.log(floor(4.572525153, -1)) ; // 4
 */
const floor = ( n , floatCount = 0 ) =>
{
    if (isNaN( n ))
    {
        return NaN ;
    }
    let r = 1 ;
    let i = - 1 ;
    while (++ i < floatCount)
    {
        r *= 10 ;
    }
    return Math.floor( n * r ) / r  ;
}

export default floor ;