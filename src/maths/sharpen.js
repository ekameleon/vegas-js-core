'use strict'

/**
 * Rounded a decimal value to
 * @param value The value to rounded
 * @param to The fraction value to apply to round the numeric value.
 * @returns {number} The new value.
 * @example
 * console.log( sharpen( 0.61 ) ) ; // 0.5
 * console.log( sharpen( 1.2 ) ) ; // 1
 * console.log( sharpen( 1.45 ))  ; // 1.5
 */
const sharpen = ( value , to = 0.5 ) =>
{
    const rounded = Math.round(value / to ) * to ;
    const precision = `${to}`.split('.')[1]?.length || 0;
    return Number( rounded.toFixed( precision ) );
}

export default sharpen ;