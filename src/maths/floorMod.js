'use strict'

/**
 * A math function which returns the floor modulus of the integer arguments passed to it. Therefore, floor modulus is (a – (floorDiv(a, b) * b)), has the same sign as the divisor b, and is in the range of -abs(b) < t < +abs(b).
 * @param {number} x - Refers to the dividend value.
 * @param {number} n - Refers to the divisor value.
 * @return the floor modulus of the integer arguments passed to it
 * @name floorMod
 * @memberof core.maths
 * @function
 * @instance
 * @example
 * console.log(floorMod(25,5)) ; // 0
 * console.log(floorMod(123,50)) ; // 23
 * console.log(floorMod(123,-50)) ; // -27
 * console.log(floorMod(-123,50)) ; // 27
 */
const floorMod = ( x , n ) => x - n * Math.floor(x / n) ;

export default floorMod ;
