'use strict'

/**
 * Anti-hyperbolic cosine : <code>acoshm = ln(x-√(x^2-1))</code>
 * @name acosHm
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} x - A value to calculate the Anti-hyperbolic cosine.
 */
const acosHm = ( x ) => Math.log( x - Math.sqrt( x * x - 1 ) );

export default acosHm;