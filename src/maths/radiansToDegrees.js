'use strict'

import RAD2DEG from './RAD2DEG'

/**
 * Converts radians to degrees.
 * @name radiansToDegrees
 * @memberof core.maths
 * @param angle {number} Value, in radians, to convert to degrees.
 * @function
 * @instance
 * @return an angle in degrees.
 */
const radiansToDegrees = ( angle ) => angle * RAD2DEG ;

export default radiansToDegrees ;
