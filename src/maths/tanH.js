'use strict'

import cosH from './cosH'
import sinH from './sinH'

/**
 * Calculates the Hyperbolic tangent.
 * @name tanH
 * @memberof core.maths
 * @function
 * @instance
 * @param {number} x - A value to calculates.
 * @return The Hyperbolic tangent of the specified value.
 */
const tanH = ( x ) => sinH(x) / cosH(x) ;

export default tanH;
