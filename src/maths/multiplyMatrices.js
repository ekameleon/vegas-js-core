'use strict'

export const multiplyMatrices = ( A , B ) =>
[
    A[0]*B[0] + A[1]*B[1] + A[2]*B[2],
    A[3]*B[0] + A[4]*B[1] + A[5]*B[2],
    A[6]*B[0] + A[7]*B[1] + A[8]*B[2]
];

export default multiplyMatrices ;