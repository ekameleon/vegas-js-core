'use strict'

/**
 * The <b>golden ratio</b> (phi) : <code>( 1 + Math.sqrt(5) ) / 2</code>.
 * @name PHI
 * @memberof core.maths
 * @const
 * @instance
 */
const PHI = 1.61803398874989 ;

export default PHI ;
