'use strict'

/**
 * This constant change degrees to radians : <code>Math.PI/180</code>.
 * @name DEG2RAD
 * @memberof core.maths
 * @instance
 * @const
 */
const DEG2RAD = Math.PI / 180 ;

export default DEG2RAD ;
