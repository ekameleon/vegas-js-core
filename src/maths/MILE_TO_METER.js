'use strict'

/**
 * This constant change mile distance to meter : <code>1 mile = 1609 m</code>.
 * @name MILE_TO_METER
 * @memberof core.maths
 * @const
 * @instance
 */
const MILE_TO_METER = 1609 ;

export default MILE_TO_METER;
