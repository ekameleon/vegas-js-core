'use strict'

/* jshint -W089 */

/**
 * Converts `value` to a plain object flattening inherited enumerable string keyed properties of `value` to own properties of the plain object.
 * @name toPlainObject
 * @memberof core
 * @since 1.0.39
 * @param {*} value The value to convert.
 * @returns {Object} Returns the converted plain object.
 * @example
 * const Foo = () => { this.b = 2 ; }
 * Foo.prototype.c = 3 ;
 *
 * Object.assign( { 'a': 1 } , new Foo() ) ; // { 'a': 1, 'b': 2 }
 * Object.assign( { 'a': 1 }, toPlainObject( new Foo() ) ) ; // { 'a': 1, 'b': 2, 'c': 3 }
 */
const toPlainObject = value =>
{
    value = Object( value ) ;

    const result = {} ;

    for ( const key in value )
    {
        result[ key ] = value[ key ] ;
    }

    return result ;
}

export default toPlainObject;