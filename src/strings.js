'use strict'

import between         from './strings/between'
import bigram          from './strings/bigram'
import bin2hex         from './strings/bin2hex'
import camelCase       from './strings/camelCase'
import capitalize      from './strings/capitalize'
import center          from './strings/center'
import clean           from './strings/clean'
import compare         from './strings/compare'
import deburr          from './strings/deburr'
import diceCoefficient from './strings/diceCoefficient'
import empty           from './strings/empty'
import endsWith        from './strings/endsWith'
import fastformat      from './strings/fastformat'
import fastformatDate  from './strings/fastformatDate'
import format          from './strings/format'
import hex2bin         from './strings/hex2bin'
import hyphenate       from './strings/hyphenate'
import indexOfAny      from './strings/indexOfAny'
import insert          from './strings/insert'
import kebabCase       from './strings/kebabCase'
import lastIndexOfAny  from './strings/lastIndexOfAny'
import nGram           from './strings/nGram'
import notEmpty        from './strings/notEmpty'
import pad             from './strings/pad'
import pascalCase      from './strings/pascalCase'
import pluralize       from './strings/pluralize'
import repeat          from './strings/repeat'
import snakeCase       from './strings/snakeCase'
import startsWith      from './strings/startsWith'
import stringToPath    from './strings/stringToPath'
import stub            from './strings/stub'
import substr          from './strings/substr'
import toString        from './strings/toString'
import trigram         from './strings/trigram'
import trim            from './strings/trim'
import trimEnd         from './strings/trimEnd'
import trimStart       from './strings/trimStart'
import truncate        from './strings/truncate'
import ucFirst         from './strings/ucFirst'
import ucWords         from './strings/ucWords'
import validateUUID    from './strings/validateUUID'
import versionUUID     from './strings/versionUUID'
import words           from './strings/words'

// import latinize from './strings/latinize'

/**
 * The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
 * @summary The {@link core.strings} package is a modular <b>JavaScript</b> library that provides extra <code>String</code> methods.
 * @namespace core.strings
 * @memberof core
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @since 1.0.0
 */
const strings = {
    between,
    bigram,
    bin2hex,
    camelCase,
    capitalize,
    center,
    clean,
    compare,
    deburr,
    diceCoefficient,
    empty,
    endsWith,
    fastformat,
    fastformatDate,
    format,
    hex2bin,
    hyphenate,
    indexOfAny,
    insert,
    kebabCase,
    lastIndexOfAny,
    //latinize ,
    nGram,
    notEmpty,
    pad ,
    pascalCase,
    pluralize,
    repeat,
    snakeCase,
    startsWith,
    stringToPath,
    stub,
    substr,
    toString,
    trigram,
    trim,
    trimEnd,
    trimStart,
    truncate,
    ucFirst,
    ucWords,
    validateUUID,
    versionUUID,
    words
} ;

export default strings ;