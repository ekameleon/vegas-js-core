'use strict'

/**
 * Used as references for various `Number` constants.
 * @type {number}
 */
const MAX_SAFE_INTEGER = 9007199254740991 ;

export default MAX_SAFE_INTEGER ;