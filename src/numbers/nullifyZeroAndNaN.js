'use strict'

import nullify from './nullify'

const nullifyZeroAndNaN = value => value === 0 ? undefined : nullify( value ) ;

export default nullifyZeroAndNaN ;