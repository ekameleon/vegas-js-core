'use strict'

import sum from './sum'

/**
 * Creates an average for the specified range.
 * @name avg
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.43
 * @param {Array.<number>} array - The Array to evaluates.
 * @param {Number} index - Index of element being calculated
 * @param {Number} range - Size of range to calculate.
 * @return The average for the specified range.
 * @throws TypeError Thrown if this method is invoked on an object that is not an Array.
 * @example
 * console.log( avg( [1, 2, 3, 4, 5, 6, 7, 8, 9], 5, 4 ) ) ; // 3.5
 */
const avg = ( array , index , range ) => sum( array.slice( index  - range , index ) ) / range ;

export default avg ;