'use strict'

import baseTrim from '../_/baseTrim.js'
import isObject from '../isObject.js'
import isSymbol from '../isSymbol.js'

/** Used as references for various `Number` constants. */
const NAN = 0 / 0;

/** Used to detect bad signed hexadecimal string values. */
const reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
const reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
const reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
const freeParseInt = parseInt;

/**
 * Returns the number representation of the passed-in value.
 * @name toNumber
 * @memberof core.numbers
 * @function
 * @instance
 * @param {*} value - The value to transform.
 * @param {*} [defaultValue=''] The default value to return if the value is null or undefined.
 * @return {number} The number representation of passed-in value. Returns `defaultValue` if `value` is `null` or `undefined`.
 * @example
 * let value ;
 * trace( toNumber(value,12) ) ; // 12
 */
const toNumber = ( value , defaultValue = 0 ) =>
{
    if( value === null || value === undefined )
    {
        return defaultValue ;
    }
    
    if( typeof(value) === 'number' )
    {
        return value ;
    }

    if ( isSymbol(value) )
    {
        return NAN;
    }

    if ( isObject(value) )
    {
        const other = typeof value?.valueOf === 'function' ? value.valueOf() : value;
        value = isObject(other) ? (other + '') : other ;
    }

    if (typeof value !== 'string')
    {
        return value === 0 ? value : +value;
    }

    value = baseTrim(value) ;
    const isBinary = reIsBinary.test(value);

    return (isBinary || reIsOctal.test(value)) ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
                                                : (reIsBadHex.test(value) ? NAN : +value) ;
};

export default toNumber ;