'use strict'

import isNumber from '../isNumber'

/**
 * Indicates if the given number is odd.
 * @name isOdd
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.44
 * @param {number} value - The value to evaluates.
 * @return {boolean} Return true if the given number is even.
 * @throws TypeError Thrown if this method is invoked with a parameter that is not a number.
 * @throws Error Thrown if this method is invoked with a parameter that is not a safe integer.
 * @example
 * console.log( isOdd(0) ) ; // false
 * console.log( isOdd(1) ) ; // true
 * console.log( isOdd(2) ) ; // false
 * console.log( isOdd(3) ) ; // true
 * console.log( isOdd(4) ) ; // false
 */
const isOdd = value =>
{
    if ( !isNumber(value) )
    {
        throw new TypeError( 'isOdd failed, expected a number' ) ;
    }

    if ( !Number.isInteger( value ) )
    {
        throw new Error('isOdd failed, expected an integer');
    }

    if ( !Number.isSafeInteger( value ) )
    {
        throw new Error('isOdd failed, value exceeds maximum safe integer');
    }

    return value%2 === 1 ;
}

export default isOdd ;