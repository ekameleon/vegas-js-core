'use strict'

import avg from './avg'

const toFixed = n => n.toFixed(2) ;

/**
 * Calculate the simple moving average of an array.
 * @name sma
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.44
 * @param {Array} array - Array of numbers to calculate.
 * @param {number} range - Size of the window to use to when calculating the average for each range. Defaults to array length.
 * @param {function} format - Custom format function called on each calculated average. Defaults to n.toFixed(2).
 * @return Returns the resulting array of averages.
 * @example
 * ```
 * console.log( sma( [1, 2, 3, 4, 5, 6, 7, 8, 9] , 4 ) ) ;
 * // [ '2.50', '3.50', '4.50', '5.50', '6.50', '7.50' ]
 * //   │       │       │       │       │       └─(6+7+8+9)/4
 * //   │       │       │       │       └─(5+6+7+8)/4
 * //   │       │       │       └─(4+5+6+7)/4
 * //   │       │       └─(3+4+5+6)/4
 * //   │       └─(2+3+4+5)/4
 * //   └─(1+2+3+4)/4
 * ```
 */
const sma = ( array , range , format = null ) =>
{
    if (!Array.isArray(array))
    {
        throw TypeError( 'sma failed, expected first argument to be an array' ) ;
    }

    const fn = typeof format === 'function' ? format : toFixed;

    const result = [];

    const num = range ?? array.length;
    const len = array.length + 1;

    let index = num - 1;

    while (++index < len)
    {
        result.push( fn( avg( array , index , num ) ) ) ;
    }

    return result ;
}

export default sma ;