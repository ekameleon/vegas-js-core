'use strict'

/**
 * Returns <pre>null</pre> if the passed-in argument is not a number.
 * @name nullify
 * @memberof core.arrays
 * @function
 * @instance
 * @param {number} value - The value to check.
 * @return The number representation or null if the array is empty.
 */
const nullify = value => isNaN(value) ? null : value ;

export default nullify ;