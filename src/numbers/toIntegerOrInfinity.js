'use strict'

/**
 * Returns either a normal completion containing either an integer, +∞, or -∞, or a throw completion.
 * @param value
 * @return {number}
 */
const toIntegerOrInfinity = value =>
{
    const number = +value ;
    return number !== number || number === 0 ? 0 : Math.trunc( number ) ;
}

export default toIntegerOrInfinity;