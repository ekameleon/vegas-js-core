'use strict'

import isNumber from '../isNumber'

/**
 * Indicates if the given number is even.
 * @name isEven
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.43
 * @param {number} value - The value to evaluates.
 * @return {boolean} Return true if the given number is even.
 * @throws TypeError Thrown if this method is invoked with a parameter that is not a number.
 * @throws Error Thrown if this method is invoked with a parameter that is not a safe integer.
 * @example
 * console.log( isEven(0) ) ; // true
 * console.log( isEven(1) ) ; // false
 * console.log( isEven(2) ) ; // true
 * console.log( isEven(3) ) ; // false
 * console.log( isEven(4) ) ; // true
 */
const isEven = value =>
{
    if ( !isNumber(value) )
    {
        throw new TypeError( 'isEven failed, expected a number' ) ;
    }

    if ( !Number.isInteger( value ) )
    {
        throw new Error('isEven failed, expected an integer');
    }

    if ( !Number.isSafeInteger( value ) )
    {
        throw new Error('isEven failed, value exceeds maximum safe integer');
    }

    return value%2 === 0 ;
}

export default isEven ;