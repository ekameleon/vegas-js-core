'use strict'

/**
 * Computes the sum of the values in array.
 * @name sum
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.44
 * @param {Array.<number>} array - The search Array.
 * @return The the sum of the values in array.
 * @throws TypeError Thrown if this method is invoked on an object that is not an Array.
 * @example
 * console.log( sum( [ 1 , 2 , 3 ] ) ) ; // 6
 */
const sum = array =>
{
    if ( !Array.isArray( array ) )
    {
        throw new TypeError( 'sum failed, expected an array' ) ;
    }
    let len = array.length ;
    let num = 0 ;
    while (len--)
    {
        num += Number( array[len] );
    }
    return num;
}

export default sum ;