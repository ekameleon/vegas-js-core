'use strict'

/**
 * Returns a string representing this number using fixed-point notation with the specified number of decimal places.
 * This method encapsulate the number.toFixed method.
 * @name toFixed
 * @memberof core.numbers
 * @function
 * @instance
 * @since 1.0.44
 * @param {number} number - The number to fix.
 * @param {number} [digits=0] - The number of digits to appear after the decimal point; should be a value between 0 and 100, inclusive. If this argument is omitted, it is treated as 0.
 * @return {string} Returns A string representing the given number using fixed-point notation.
 * @throws RangeError Thrown if digits is not between 0 and 100 (inclusive).
 * @throws TypeError Thrown if this method is invoked on an object that is not a Number.
 */
const toFixed = ( number, digits = 0 ) => number.toFixed( digits ) ;

export default toFixed ;