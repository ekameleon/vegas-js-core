'use strict'

const padTo2Digits = number => `${number}`.padStart(2,0) ;

export default padTo2Digits  ;