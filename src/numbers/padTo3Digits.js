'use strict'

export const padTo3Digits = number => `${number}`.padStart(3,'0') ;

export default padTo3Digits ;