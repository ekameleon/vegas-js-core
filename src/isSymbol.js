'use strict'

import getTag from './objects/getTag'

import { symbolTag } from './_/tags'

/**
 * Evaluates if the passed-in value is classified as a `Symbol` primitive or object.
 * @since 1.0.37
 * @name isSymbol
 * @memberof core
 * @function
 * @instance
 * @param {*} object - The object to evaluate.
 * @return {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 * console.log( isSymbol(Symbol.iterator) ) ; // true
 * console.log( isSymbol('abc') ) ; // false
 */
const isSymbol = object =>
{
    const type = typeof object ;
    return type === 'symbol' || (type === 'object' && object !== null && getTag( object ) === symbolTag ) ;
}

export default isSymbol ;