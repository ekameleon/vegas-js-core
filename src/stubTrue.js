'use strict'

/**
 * This function return always true.
 * @name stub
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @returns {boolean}
 * @example
 * const bool = stub() ;
 * console.log( bool ) ; // true
 */
const stubTrue = () => true ;

export default stubTrue ;