'use strict'

/**
 * Indicates if the specific object is a Boolean.
 * @name isBoolean
 * @memberof core
 * @function
 * @instance
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a Boolean.
 * @example
 * console.log( isBoolean(0) ) ; // false
 * console.log( isBoolean(true) ) ; // true
 * console.log( isBoolean(false) ) ; // true
 * console.log( isBoolean(3>2) ) ; // true
 * console.log( isBoolean(null) ) ; // false
 */
const isBoolean = ( object ) => (typeof(object) === 'boolean') || (object instanceof Boolean ) ;

export default isBoolean ;