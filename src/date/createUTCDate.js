/*jshint node: true*/
'use strict'

/**
 * Create an UTC date object.
 * @param {Number} year - The year component value.
 * @param {...*} rest - The rest of the arguments to creates the new date.
 * @return {Date} The new Date reference.
 * @name createUTCDate
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( createUTCDate(2016,0,1) ) ;
 */
const createUTCDate = ( year , ...rest ) =>
{
    let date;
    let args = [ year , ...rest ] ;
    if ( year < 100 && year >= 0) // the Date.UTC function remaps years 0-99 to 1900-1999
    {
        args[0] = year + 400;
        date = new Date( Date.UTC.apply(null, args) );
        if ( isFinite( date.getUTCFullYear() ) )
        {
            date.setUTCFullYear( year );
        }
    }
    else
    {
        date = new Date( Date.UTC.apply(null, args) );
    }

    return date;
};

export default createUTCDate ;
