'use strict'

import MS_PER_MINUTE from "./MS_PER_MINUTE"

/**
 * Solve the daylight saving time gap of two dates.
 * @param {Date} current - The current date reference.
 * @param {Date} next - The next date reference.
 * @return The new solved date reference fixed by the daylight saving time gap.
 * @name solveDST
 * @memberof core.date
 * @function
 * @instance
 */
const solveDST = ( current , next) =>
{
    let currentOffset = current.getTimezoneOffset() ;
    let nextOffset    = next.getTimezoneOffset()

    // if is DST, add the difference in minutes
    // else the difference is zero
    let diffMinutes = (nextOffset - currentOffset) ;

    return new Date(+(next) + diffMinutes * MS_PER_MINUTE ) ;
}

export default solveDST ;
