'use strict'

import leapYear from "./leapYear"

/**
 * Returns the numbers of days in a specified year.
 * @param {Number} year - The year integer to search the number of days in a year.
 * @return The numbers of days in a specified year.
 * @name daysInYear
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( daysInYear( 2016 ) ; // 366
 * console.log( daysInYear( 2017 ) ; // 365
 */
const daysInYear = year => leapYear(year) ? 366 : 365;

export default daysInYear ;
