'use strict'

import MONTH_PER_YEAR from './MONTH_PER_YEAR'

import addMonths from './addMonths'

/**
 * Add years to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of years to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addYears
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addYears(new Date(2014,03,2), 2) ) ;
 */
const addYears = ( date , value ) => addMonths( date , value * MONTH_PER_YEAR ) ;

export default addYears ;
