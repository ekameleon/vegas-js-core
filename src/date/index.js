'use strict'

import ONE_DAY_MS       from './ONE_DAY_MS'
import MS_PER_400_YEARS from './MS_PER_400_YEARS'
import MS_PER_DAY       from './MS_PER_DAY'
import MS_PER_HOUR      from './MS_PER_HOUR'
import MS_PER_MINUTE    from './MS_PER_MINUTE'
import MS_PER_SECOND    from './MS_PER_SECOND'
import MS_PER_WEEK      from './MS_PER_WEEK'

import MONTH_PER_CENTURY from './MONTH_PER_CENTURY'
import MONTH_PER_DECADE  from './MONTH_PER_DECADE'
import MONTH_PER_YEAR    from './MONTH_PER_YEAR'

import addDays         from './addDays'
import addHours        from './addHours'
import addMinutes      from './addMinutes'
import addMonths       from './addMonths'
import addMS           from './addMS'
import addSeconds      from './addSeconds'
import addWeeks        from './addWeeks'
import addYears        from './addYears'
import after           from './after'
import before          from './before'
import createDate      from './createDate'
import createUTCDate   from './createUTCDate'
import daysInFebruary  from './daysInFebruary'
import daysInMonth     from './daysInMonth'
import daysInYear      from './daysInYear'
import daysOf          from './daysOf'
import daysToMonths    from './daysToMonths'
import equals          from './equals'
import endOfDay        from './endOfDay'
import endOfMonth      from './endOfMonth'
import endOfWeek       from './endOfWeek'
import endOfYear       from './endOfYear'
import firstVisibleDay from './firstVisibleDay'
import lastVisibleDay  from './lastVisibleDay'
import leapYear        from './leapYear'
import monthsToDays    from './monthsToDays'
import startOfDay      from './startOfDay'
import startOfMonth    from './startOfMonth'
import startOfWeek     from './startOfWeek'
import startOfYear     from './startOfYear'
import subtractDays    from './subtractDays'
import subtractHours   from './subtractHours'
import subtractMinutes from './subtractMinutes'
import subtractMonths  from './subtractMonths'
import subtractMS      from './subtractMS'
import subtractSeconds from './subtractSeconds'
import tomorrow        from './tomorrow'
import visibleDays     from './visibleDays'
import yesterday       from './yesterday'

/**
 * The {@link core.date} package is a modular <b>JavaScript</b> library that provides extra <code>Date</code> methods.
 * @summary The {@link core.date} package is a modular <b>JavaScript</b> library that provides extra <code>Date</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.date
 * @memberof core
 */
const date =
{
    ONE_DAY_MS,
    MONTH_PER_CENTURY,
    MONTH_PER_DECADE,
    MONTH_PER_YEAR,
    MS_PER_400_YEARS,
    MS_PER_DAY,
    MS_PER_HOUR,
    MS_PER_MINUTE,
    MS_PER_SECOND,
    MS_PER_WEEK,

    addDays,
    addHours,
    addMinutes,
    addMonths,
    addMS,
    addSeconds,
    addWeeks,
    addYears,
    after,
    before,
    createUTCDate,
    daysInFebruary,
    daysInMonth,
    daysInYear,
    daysToMonths,
    daysOf,
    createDate,
    equals,
    endOfDay,
    endOfMonth,
    endOfWeek,
    endOfYear,
    firstVisibleDay,
    lastVisibleDay,
    leapYear,
    monthsToDays,
    startOfDay,
    startOfMonth,
    startOfWeek,
    startOfYear,
    subtractDays,
    subtractHours,
    subtractMinutes,
    subtractMonths,
    subtractMS,
    subtractSeconds,
    tomorrow,
    visibleDays,
    yesterday
} ;

export default date ;
