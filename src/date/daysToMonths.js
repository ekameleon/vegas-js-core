'use strict'

// 400 years have 146097 days (taking into account leap year rules)
// 400 years have 12 months === 4800
const CONSTANT = 4800 / 146097 ;

/**
 * Returns the numbers of month based on a numbers of days.
 * @param {Number} days - The numbers of days to convert in months.
 * @return The numbers of month based on a numbers of days.
 * @name daysToMonths
 * @memberof core.date
 * @function
 * @instance
 * @example
 * import { daysToMonths } from 'vegas-js-core/src/date/daysToMonths'
 * import { round } from 'vegas-js-core/src/maths/round'
 * console.log( round( daysToMonths( 31 ) , 3 ) ) ; //  1.019
 */
const daysToMonths = days => days * CONSTANT ;

export default daysToMonths ;
