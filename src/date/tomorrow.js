'use strict'

import ONE_DAY_MS from './ONE_DAY_MS'

/**
 * The <code>tomorrow</code> new Date object.
 * @param {Date} [date] - The date to evaluates. If this argument is ommited the current Date is used.
 * @return The <code>tomorrow</code> new Date object.
 * @name tomorrow
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( tomorrow(new Date(2016,1,2)) ) ;
 */
const tomorrow = ( date = null ) =>
{
    let d = (date instanceof Date) ? date : new Date() ;
    return new Date( d.valueOf() + ONE_DAY_MS ) ;
};

export default tomorrow;