'use strict'

/**
 * Constant field representing milliseconds per hour.
 * @since 1.0.10
 * @name MS_PER_HOUR
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 3600000
 */
const MS_PER_HOUR = 60 * 60 * 1000 ;

export default MS_PER_HOUR;
