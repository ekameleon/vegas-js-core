'use strict'

import MS_PER_WEEK from "./MS_PER_WEEK"

import addMS from './addMS'

/**
 * Add weeks to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of weeks to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addWeeks
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addWeeks(new Date(2014,10,2), 1) ) ;
 */
const addWeeks = ( date , value ) => addMS(date,value*MS_PER_WEEK) ;

export default addWeeks ;
