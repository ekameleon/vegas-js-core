'use strict'

import solveDST from "./solveDST"

/**
 * Add milliseconds to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of milliseconds to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addMS
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addMS(new Date(2014,10,2), 30000) ) ;
 */
const addMS = ( date , value ) =>
{
    if( !(date instanceof Date) )
    {
        date = new Date() ;
    }
    return solveDST( date , new Date(+(date) + value ) ) ;
}

export default addMS ;
