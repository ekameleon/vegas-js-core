'use strict'

import MS_PER_SECOND from "./MS_PER_SECOND"

/**
 * Add seconds to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} seconds - The number of seconds to add to the specific date.
 * @return The new Date reference.
 * @name addSeconds
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addSeconds(new Date(2014,10,2), 30) ) ;
 */
const addSeconds = ( date , seconds ) => new Date( date.getTime() + seconds * MS_PER_SECOND ) ;

export default addSeconds ;
