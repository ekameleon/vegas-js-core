'use strict'

const getLength = method =>
{
    switch( method )
    {
        case 'Milliseconds':
        {
            return 3600000 ;
        }
        case 'Seconds' :
        {
            return 3600;
        }
        case 'Minutes' :
        {
            return 60;
        }
        case 'Hours' :
        {
            return 1 ;
        }
        default :
        {
            return null;
        }
    }
}

const createAccessors = method =>
{
    const length = getLength(method) ;
    return ( date, value ) =>
    {
        if ( value === undefined )
        {
            return date['get' + method]() ;
        }

        let dateOut = new Date(date) ;

        let getter = dateOut['get' + method] ;
        let setter = dateOut['set' + method] ;

        if( setter instanceof Function && getter instanceof Function )
        {
            dateOut['set' + method](value) ;
            if(
                length && ( dateOut['get' + method]() !== value ) &&
                ( method === 'Hours' || value >=length && ( dateOut.getHours()-date.getHours() < Math.floor(value/length) ) )
            )
            {

                dateOut['set' + method](value+length); //Skip DST hour, if it occurs
            }
        }
        return dateOut ;
    }
}

export const date         = createAccessors('Date') ;
export const day          = createAccessors('Day') ;
export const hours        = createAccessors('Hours') ;
export const milliseconds = createAccessors('Milliseconds') ;
export const minutes      = createAccessors('Minutes') ;
export const month        = createAccessors('Month') ;
export const seconds      = createAccessors('Seconds') ;
export const year         = createAccessors('FullYear') ;

export default {
    date ,
    day,
    hours,
    milliseconds,
    minutes,
    seconds,
    year
}