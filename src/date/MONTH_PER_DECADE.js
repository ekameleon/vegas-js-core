'use strict'

/**
 * Constant field representing months per decade.
 * @since 1.0.22
 * @name MONTH_PER_DECADE
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 120
 */
const MONTH_PER_DECADE = 12 * 10 ;

export default MONTH_PER_DECADE;
