'use strict'

import localStartOfDate from './localStartOfDate'
import utcStartOfDate   from './utcStartOfDate'

/**
 * The start of year.
 * @since 1.0.10
 * @param {Date} [date=null] - The date to evaluates. If this argument is omitted the current Date is used.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return The start of year.
 * @name startOfYear
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( startOfYear(new Date(2016,3,12)) ) ;
 */
const startOfYear = ( date = null , isUTC = false ) =>
{
    if( !(date instanceof Date) )
    {
        date = new Date() ;
    }
    let startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
    return new Date(startOfDate( date.getFullYear() , 0 , 1 )) ;
};

export default startOfYear;