'use strict'

/**
 * Actual modulo handles negative numbers (for dates before 1970).
 * @param {number} dividend - The dividend value.
 * @param {number} divisor - The second date to evaluates.
 * @return The actual modulo handles negative numbers (for dates before 1970).
 * @name mod
 * @memberof core.date
 * @function
 * @instance
 */
const mod = ( dividend, divisor ) => (dividend % divisor + divisor) % divisor;

export default mod ;