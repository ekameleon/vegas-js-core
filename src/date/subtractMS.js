'use strict'

import addMS from './addMS'

/**
 * Subtract milliseconds to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of milliseconds to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractMS
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractMS(new Date(2014,10,2), 30000) ) ;
 */
const subtractMS = ( date , value ) => addMS(date,-(value)) ;

export default subtractMS ;
