/*jshint node: true*/
'use strict'

/**
 * Indicates if the first date <b>equals</b> the second date.
 * @param {Date} date1 - The first date to evaluates.
 * @param {Date} date2 - The second date to evaluates.
 * @return The value <code>true</code> if the first date equals the second date.
 * @name equals
 * @memberof core.date
 * @function
 * @instance
 * @throws TypeError The two date arguments must be a valid Date object.
 * @example
 * console.log( equals(new Date(2016,0,1),new Date(2016,0,1)) ) ; // true
 * console.log( equals(new Date(2016,0,1),new Date(2017,0,1)) ) ; // false
 */
const equals = ( date1 , date2 ) =>
{
    if( !(date1 instanceof Date && date2 instanceof Date) )
    {
        throw new TypeError( 'equals() failed, the passed-in date arguments must be valid Date objects.' ) ;
    }
    return date1.valueOf() === date2.valueOf() ;
};

export default equals ;