'use strict'

/**
 * Constant field representing milliseconds per week.
 * @since 1.0.22
 * @name MS_PER_DAY
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 604 800 000
 */
const MS_PER_WEEK = 7 * 24 * 60 * 60 * 1000  ;

export default MS_PER_WEEK;
