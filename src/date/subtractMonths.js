'use strict'

import addMonths from "./addMonths"

/**
 * Remove months to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of month to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractMonths
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractMonths(new Date(2014,10,2), 5) ) ;
 */
const subtractMonths = ( date , value ) => addMonths(date,-(value)) ;

export default subtractMonths ;
