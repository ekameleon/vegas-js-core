'use strict'

import addSeconds from "./addSeconds"

/**
 * Subtract seconds to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of seconds to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractSeconds
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractSeconds(new Date(2014,10,2), 30) ) ;
 */
const subtractSeconds = ( date , value ) => addSeconds(date,-(value)) ;

export default subtractSeconds ;
