'use strict'

import leapYear from "./leapYear"

/**
 * Returns the numbers of february days with a specified year.
 * @param {Number} year - The year integer to search the number of days in a year.
 * @return The numbers of february days in a specified year.
 * @name daysInFebruary
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( daysInFebruary( 2016 ) ; // 29
 * console.log( daysInFebruary( 2017 ) ; // 28
 */
const daysInFebruary = year => leapYear(year) ? 29 : 28 ;

export default daysInFebruary ;
