'use strict'

import { lte } from './days'

import addDays from './addDays'

import firstVisibleDay from './firstVisibleDay'
import lastVisibleDay from './lastVisibleDay'

/**
 * Returns an array representation of all days in a full month. The array can begin with null values if the first day in the first week are previous days of the previous month.
 * @param {Date} date - The specified <code>Date</code> to return the full month calendar. The default value is the current <code>Date</code> object.
 * @param {number} [firstOfWeek=0] - Indicates the first day offset of the week > Sunday - Saturday : 0 - 6.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return An array representation of all visible days of the current month.
 * @name visibleDays
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( visibleDays( new Date(2020,04,16) , 1 ) ) ;
 */
const visibleDays = ( date , firstOfWeek = 0 , isUTC = false ) =>
{
    date = new Date(date) ;

    let current = firstVisibleDay(date, firstOfWeek, isUTC) ;
    let last    = lastVisibleDay(date, firstOfWeek, isUTC) ;

    let days = []

    while( lte( current , last ) )
    {
        days.push( current )
        current = addDays(current,1) ;
    }

    return days
};

export default visibleDays ;
