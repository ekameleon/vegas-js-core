'use strict'

/**
 * Indicates if the specified date is a <b>leap year</b> (also known as an intercalary year or a bissextile year) .
 * @param {Date|number} date - The date to evaluates or a number representing the year integer value representing the year (Value from 0 to 99 map to the years 1900 to 1999).
 * @return {Boolean} <code>true</code> if the specified date is a <b>leap year</b> .
 * @name leapYear
 * @memberof core.date
 * @function
 * @instance
 * @throws TypeError The date argument must be a valid Date object.
 * @example
 * console.log( leapYear(2016) ) ; // true
 * console.log( leapYear(new Date(2016,0,1)) ) ; // true
 * console.log( leapYear(2017) ) ; // false
 * console.log( leapYear(new Date(2017,0,1)) ) ; // false
 */
const leapYear = date =>
{
    let year ;
    if( date instanceof Date )
    {
        year = date.getFullYear() ;
    }
    else if( typeof(date) === 'number' || date instanceof Number )
    {
        year = date ;
    }
    else
    {
        throw new TypeError( 'leapYear failed, the passed-in date argument must be a valid Date object or an integer representing the year to evaluates.' ) ;
    }
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
};


export default leapYear ;
