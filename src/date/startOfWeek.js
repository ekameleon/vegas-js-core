'use strict'

import accessors from './accessors'
import addDays   from "./addDays"

const { day, hours } = accessors ;

const weekday = ( date , value, firstDay ) =>
{
    let w = (day(date) + 7 - (firstDay || 0) ) % 7;
    return value === undefined ? w : addDays( date , value - w ) ;
}

/**
 * The start of week.
 * @since 1.0.22
 * @param {Date} [date=null] - The date reference. If this argument is omitted the current Date is used.
 * @param {number} [firstOfWeek=0] - Indicates the first day offset of the week > Sunday - Saturday : 0 - 6.
 * @return The start of year.
 * @name startOfWeek
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( startOfWeek(new Date(2020,3,14),1) ) ; // firstOfWeek === monday
 */
const startOfWeek = ( date = null , firstOfWeek = 0 ) =>
{
    date = new Date(date) ;
    date = hours( date , 0 ) ;
    date = weekday( date , 0, firstOfWeek);
    return date ;
};

export default startOfWeek;