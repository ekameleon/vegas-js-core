'use strict'

import addYears from "./addYears"

/**
 * Remove years to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of years to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractYears
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractYears(new Date(2014,10,2), 5) ) ;
 */
const subtractYears = ( date , value ) => addYears(date,-(value)) ;

export default subtractYears ;
