'use strict'

/**
 * Constant field representing milliseconds per second.
 * @since 1.0.10
 * @name MS_PER_SECOND
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 1000
 */
const MS_PER_SECOND = 1000 ;

export default MS_PER_SECOND;
