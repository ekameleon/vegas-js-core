'use strict'

const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] ;

/**
 * Returns the numbers of days in a specified month.
 * @return The numbers of days in a specified month.
 * @name daysInMonth
 * @memberof core.date
 * @function
 * @instance
 * @param {Date|Number} [year=null] - The year value or a date (or now) to search the number of days in a month.
 * @param {Number} [month=null] - The optional month value.
 * @example
 * console.log( daysInMonth(new Date(2017,1)) ) ; // 28
 * console.log( daysInMonth(new Date(2016,1)) ) ; // 29
 *
 * console.log( daysInMonth(new Date(2016,0,1)) ) ; // 31
 * console.log( daysInMonth(new Date(2016,1,1)) ) ; // 29
 * console.log( daysInMonth(new Date(2017,0,1)) ) ; // 31
 */
const daysInMonth = ( year = null , month = null ) =>
{
    let y, m ;

    if( year instanceof Date )
    {
        y = year.getFullYear() ;
        m = year.getMonth() ;
    }
    else if(
        ((typeof(year) === 'number') || (year instanceof Number ))
        && ((typeof(month) === 'number') || (month instanceof Number )) )
    {
        y = year ;
        m = month ;
    }
    else
    {
        const now = new Date();
        y = now.getFullYear() ;
        m = now.getMonth() ;
    }

    if( m === 1 )
    {
        return ((y % 4 === 0) && (y % 100 !== 0)) || (y % 400 === 0) ? 29 : 28 ;
    }

    return monthDays[m] ;
};

export default daysInMonth ;
