'use strict'

/**
 * Constant field representing milliseconds per minute.
 * @since 1.0.10
 * @name MS_PER_MINUTE
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 60000
 */
const MS_PER_MINUTE = 60 * 1000 ;

export default MS_PER_MINUTE;
