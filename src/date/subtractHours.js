'use strict'

import addHours from './addHours'

/**
 * Subtract hours to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of hours to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractHours
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractHours( new Date(2014,10,2) , 30 ) ) ;
 */
const subtractHours = ( date , value ) => addHours( date , -(value) ) ;

export default subtractHours ;
