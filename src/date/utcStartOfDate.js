'use strict'

import MS_PER_400_YEARS from './MS_PER_400_YEARS'

/**
 * Date.UTC remaps years 0-99 to 1900-1999
 * @private
 */
const utcStartOfDate = ( y, m, d ) =>
{
    if ( y < 100 && y >= 0 )
    {
        // preserve leap years using a full 400 year cycle, then reset
        return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
    }
    else
    {
        return Date.UTC(y, m, d);
    }
};

export default utcStartOfDate ;