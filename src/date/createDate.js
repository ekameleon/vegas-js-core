/*jshint node: true*/
'use strict'

/**
 * Create a date object.
 * @param year The full year designation is required for cross-century date accuracy. If year is between 0 and 99 is used, then year is assumed to be 1900 + year.
 * @param month The month as an number between 0 and 11 (January to December).
 * @param date The date as an number between 1 and 31.
 * @param hours Must be supplied if minutes is supplied. An number from 0 to 23 (midnight to 11pm) that specifies the hour.
 * @param minutes Must be supplied if seconds is supplied. An number from 0 to 59 that specifies the minutes.
 * @param seconds Must be supplied if milliseconds is supplied. An number from 0 to 59 that specifies the seconds.
 * @param ms An number from 0 to 999 that specifies the milliseconds.
 * @return {Date} The new Date reference.
 * @name createDate
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( createDate(2016,0,1) ) ;
 */
const createDate = ( year , month = 0 , day = 1 , hours = 0 , minutes = 0 , seconds = 0 , ms = 0 ) =>
{
    let date ;
    if ( year < 100 && year >= 0) // the date constructor remaps years 0-99 to 1900-1999
    {
        // preserve leap years using a full 400 year cycle, then reset
        date = new Date( year + 400, month, day, hours, minutes, seconds, ms );
        if ( isFinite( date.getFullYear() ) )
        {
            date.setFullYear(year) ;
        }
    }
    else
    {
        date = new Date( year , month , day , hours , minutes , seconds , ms );
    }
    return date;
};

export default createDate ;
