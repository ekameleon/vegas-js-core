'use strict'

import accessors from './accessors'

const { milliseconds, minutes, hours, seconds } = accessors ;

/**
 * The start of the day.
 * @since 1.0.22
 * @param {Date} [date=null] - The date to evaluates. If this argument is omitted the current Date is used.
 * @return The start of day.
 * @name startOfDay
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( startOfDay(new Date(2016,3,12)) ) ;
 */
const startOfDay = ( date = null ) =>
{
    date = new Date( date ) ;
    date = hours( date  , 0 ) ;
    date = minutes( date  , 0 ) ;
    date = seconds( date  , 0 ) ;
    date = milliseconds( date  , 0 ) ;
    return date ;
}

export default startOfDay;