'use strict'

import startOfDay from './startOfDay'

const createComparer = operator => (a , b ) => operator(+startOfDay(a),+startOfDay(b)) ;

export const eq  = createComparer((a, b) => a === b ) ;
export const neq = createComparer((a, b) => a !== b ) ;
export const gt  = createComparer((a, b) => a > b   ) ;
export const gte = createComparer((a, b) => a >= b  ) ;
export const lt  = createComparer((a, b) => a < b   ) ;
export const lte = createComparer((a, b) => a <= b  ) ;

export default
{
    eq,
    neq,
    gt,
    gte,
    lt,
    lte
}