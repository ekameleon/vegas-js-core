'use strict'

import daysOf from './daysOf'

/**
 * Add weeks to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of weeks to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addMonths
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addMonths(new Date(2014,03,2), 2) ) ;
 */
const addMonths = ( date , value ) =>
{
    if( !(date instanceof Date) )
    {
        date = new Date() ;
    }

    let day   = date.getDate() ;
    let year  = date.getFullYear() ;
    let month = date.getMonth() ;

    let totalMonths = year * 12 + month + value ;
    let nextYear    = Math.trunc(totalMonths / 12 ) ;
    let nextMonth   = totalMonths % 12 ;
    let nextDay     = Math.min(day, daysOf(nextYear)[nextMonth]) ;

    let next = new Date( date ) ;
    next.setFullYear( nextYear ) ;

    next.setDate(1) // To avoid a bug when sets the Feb month with a date > 28 or date > 29 (leap year)

    next.setMonth(nextMonth)
    next.setDate(nextDay)

    return next
}

export default addMonths ;
