'use strict'

import MS_PER_MINUTE from './MS_PER_MINUTE'

import addMS from './addMS'

/**
 * Add minutes to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of minutes to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addMinutes
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addMinutes(new Date(2014,10,2), 30) ) ;
 */
const addMinutes = ( date , value ) => addMS(date,value*MS_PER_MINUTE) ;

export default addMinutes ;
