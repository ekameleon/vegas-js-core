'use strict'

import localStartOfDate from './localStartOfDate'
import utcStartOfDate   from './utcStartOfDate'

/**
 * The end of year.
 * @since 1.0.10
 * @param {Date} [date=null] - The date to evaluates. If this argument is omitted the current Date is used.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return The end of year.
 * @name endOfYear
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( endOfYear(new Date(2016,3,12)) ) ;
 */
const endOfYear = ( date = null , isUTC = false ) =>
{
    if( !(date instanceof Date) )
    {
        date = new Date() ;
    }
    let startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
    return new Date( startOfDate( date.getFullYear() + 1, 0, 1 ) - 1 ) ;
};

export default endOfYear;