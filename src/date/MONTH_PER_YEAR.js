'use strict'

/**
 * Constant field representing months per year.
 * @since 1.0.22
 * @name MONTH_PER_YEAR
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 12
 */
const MONTH_PER_YEAR = 12 ;

export default MONTH_PER_YEAR;
