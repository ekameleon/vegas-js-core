'use strict'

import MS_PER_400_YEARS from './MS_PER_400_YEARS';

/**
 * The date constructor remaps years 0-99 to 1900-1999.
 * @private
 */
const localStartOfDate = ( y, m, d ) =>
{
    if ( y < 100 && y >= 0 )
    {
        // preserve leap years using a full 400 year cycle, then reset
        return new Date(y + 400, m, d) - MS_PER_400_YEARS;
    }
    else
    {
        return new Date(y, m, d).valueOf();
    }
};

export default localStartOfDate ;