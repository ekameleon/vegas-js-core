'use strict'

/**
 * The end of the day.
 * @since 1.0.22
 * @param {Date} [date=null] - The date to evaluates. If this argument is omitted the current Date is used.
 * @return The end of day.
 * @name endOfDay
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( endOfDay(new Date(2016,3,12)) ) ;
 */
const endOfDay = ( date = null ) =>
{
    date = new Date( date ) ;
    date.setHours(23, 59, 59, 999) ;
    return date ;
}

export default endOfDay;