'use strict'

/**
 * Constant field representing milliseconds per day.
 * @since 1.0.10
 * @name MS_PER_DAY
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 86400000
 */
const MS_PER_DAY = 24 * 60 * 60 * 1000 ;

export default MS_PER_DAY;
