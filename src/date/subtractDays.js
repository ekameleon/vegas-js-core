'use strict'

import addDays from './addDays'

/**
 * Subtract days to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of days to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractDays
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractDays( new Date(2014,10,2) , 25 ) ) ;
 */
const subtractDays = ( date , value ) => addDays(date,-(value)) ;

export default subtractDays ;
