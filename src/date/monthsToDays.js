'use strict'

// 400 years have 146097 days (taking into account leap year rules)
// 400 years have 12 months === 4800
const CONSTANT = 146097 / 4800 ;

/**
 * Returns the numbers of days based on a numbers of months.
 * @param {Number} months - The numbers of months to convert in days.
 * @return The numbers of days based on a numbers of months.
 * @name monthsToDays
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( round( monthsToDays( 1 ) ) ) ; // 30
 */
const monthsToDays = months => months * CONSTANT ;

export default monthsToDays ;
