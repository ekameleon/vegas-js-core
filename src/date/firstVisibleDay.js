'use strict'

import startOfMonth from './startOfMonth'
import startOfWeek  from './startOfWeek'

/**
 * Returns the first visible day of the current month calendar who includes the specific date.
 * @param {Date} date - The date to evaluate. If this argument is omitted the current Date is used.
 * @param {number} [firstOfWeek=0] - Indicates the first day offset of the week > Sunday - Saturday : 0 - 6.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return {Date} The first visible day of the current month calendar who includes the specific date.
 * @name firstVisibleDay
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( firstVisibleDay( new Date(2020,3,14) , 1 ) ) ; // new Date(2020,2,30)
 */
const firstVisibleDay = ( date , firstOfWeek = 0 , isUTC = false ) => startOfWeek( startOfMonth(date,isUTC) , firstOfWeek) ;

export default firstVisibleDay ;
