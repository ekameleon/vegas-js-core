'use strict'

import addMinutes from "./addMinutes"

/**
 * Remove minutes to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of minutes to remove to the specific date.
 * @return {Date} The new Date reference.
 * @name subtractMinutes
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractMinutes(new Date(2014,10,2), 30) ) ;
 */
const subtractMinutes = ( date , value ) => addMinutes(date,-(value)) ;

export default subtractMinutes ;
