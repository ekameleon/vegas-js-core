'use strict'

/**
 * Constant field representing months per century.
 * @since 1.0.22
 * @name MONTH_PER_CENTURY
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 1200
 */
const MONTH_PER_CENTURY = 12 * 100 ;

export default MONTH_PER_CENTURY;
