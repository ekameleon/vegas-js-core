'use strict'

import addWeek      from './addWeeks'
import startOfWeek  from "./startOfWeek"
import subtractDays from './subtractDays'

/**
 * The end of week.
 * @since 1.0.22
 * @param {Date} date - The date reference. If this argument is omitted the current Date is used.
 * @param {number} [firstOfWeek=0] - Indicates the first day offset of the week > Sunday - Saturday : 0 - 6.
 * @return {Date} The new Date reference.
 * @name endOfWeek
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( endOfWeek(new Date(2014,10,2), 1) ) ;
 */
const endOfWeek =(date = null , firstOfWeek = 0 ) =>
{
    date = new Date(date) ;
    date = startOfWeek( date , firstOfWeek ) ;
    date = addWeek(date, 1) ;
    date = subtractDays(date, 1) ;
    date.setHours(23, 59, 59, 999 ) ;
    return date ;
}

export default endOfWeek ;
