'use strict'

import MS_PER_HOUR from './MS_PER_HOUR'

import addMS from './addMS'

/**
 * Add hours to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of hours to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addHours
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addHours( new Date(2014,10,2) , 30 ) ) ;
 */
const addHours = ( date , value ) => addMS( date ,value * MS_PER_HOUR ) ;

export default addHours ;
