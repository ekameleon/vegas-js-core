'use strict'

/**
 * Constant field representing one day, in milliseconds.
 * @since 1.0.10
 * @name ONE_DAY_MS
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 86400000
 */
const ONE_DAY_MS = 86400000 ; // 1000*60*60*24

export default ONE_DAY_MS;
