'use strict'

import endOfWeek  from './endOfWeek'
import endOfMonth from './endOfMonth'

/**
 * Returns the last visible day of the current month calendar who includes the specific date.
 * @param {Date} date - The date to evaluate. If this argument is omitted the current Date is used.
 * @param {number} [firstOfWeek=0] - Indicates the first day offset of the week > Sunday - Saturday : 0 - 6.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return {Date} The last visible day of the current month calendar who includes the specific date.
 * @name lastVisibleDay
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( lastVisibleDay( new Date(2020,3,14) , 1 ) ) ; // new Date(2020,4,3)
 */
const lastVisibleDay = ( date , firstOfWeek = 0 , isUTC = false ) => endOfWeek( endOfMonth(date,isUTC) , firstOfWeek) ;

export default lastVisibleDay ;
