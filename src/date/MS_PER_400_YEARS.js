'use strict'

import MS_PER_HOUR from './MS_PER_HOUR'

/**
 * Constant field representing milliseconds per 400 years.
 * @since 1.0.10
 * @name MS_PER_HOUR
 * @memberof core.date
 * @instance
 * @const
 * @type number
 * @default 12622780800000
 */
const MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR ;

export default MS_PER_400_YEARS;
