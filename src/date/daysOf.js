'use strict'

import daysInFebruary from './daysInFebruary'

/**
 * Returns the array representation of all month's days in a specific year.
 * @name daysInMonth
 * @memberof core.date
 * @param {Date|Number} [year=null] - The year value or a date (or now) to search the number of days in a month.
 * @return the array representation of all month's days in a specific year.
 * @function
 * @instance
 */
const daysOf = ( year) =>[31, daysInFebruary(year), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

export default daysOf ;
