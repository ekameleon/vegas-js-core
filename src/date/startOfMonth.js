'use strict'

import localStartOfDate from './localStartOfDate'
import utcStartOfDate   from './utcStartOfDate'

/**
 * The start of the month.
 * @since 1.0.10
 * @param {Date} [date=null] - The date to evaluates. If this argument is omitted the current Date is used.
 * @param {boolean} [isUTC=false] - Indicates if the date is an UTC date.
 * @return The start of month.
 * @name startOfMonth
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( startOfMonth(new Date(2016,3,12)) ) ;
 */
const startOfMonth = ( date = null , isUTC = false ) =>
{
    if( !(date instanceof Date) )
    {
        date = new Date() ;
    }
    let startOfDate = isUTC ? utcStartOfDate : localStartOfDate;
    return new Date( startOfDate( date.getFullYear() , date.getMonth() , 1 )) ;
};

export default startOfMonth;