'use strict'

import MS_PER_WEEK from "./MS_PER_WEEK"

/**
 * Subtract weeks to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} weeks - The number of weeks to remove to the specific date.
 * @return The new Date reference.
 * @name subtractWeeks
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( subtractDays( new Date(2014,10,2) , 25 ) ) ;
 */
const subtractWeeks = (date , weeks ) => new Date( date.getTime() - weeks * MS_PER_WEEK ) ;

export default subtractWeeks ;
