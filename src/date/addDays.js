'use strict'

import MS_PER_DAY from './MS_PER_DAY'

import addMS from './addMS'

/**
 * Add days to a specific Date reference.
 * @param {Date} date - The date to transform. If this argument is omitted the current Date is used.
 * @param {Number} value - The number of days to add to the specific date.
 * @return {Date} The new Date reference.
 * @name addDays
 * @memberof core.date
 * @function
 * @instance
 * @example
 * console.log( addDays( new Date(2014,10,2) , 25 ) ) ;
 */
const addDays = ( date , value ) => addMS(date,value*MS_PER_DAY) ;

export default addDays ;
