/* jshint strict: false */
/* jshint -W069 */

import isString from '../isString'

import get from '../objects/get'
import hasPath from '../objects/hasPath'

/**
 * Helps to sorting arrays on multiple keys. It allows you to use the native Array.sort() javascript method,
 * but pass in multiple functions to sort that are chained with sortBy().thenBy().thenBy() pattern.
 * This function is a fork of the <a href="https://github.com/Teun/thenBy.js">thenBy.JS</a> library, but full ES6 and use the the vegas-core-js library.
 * @name sortBy
 * @memberof core.functors
 * @function
 * @instance
 * @version 1.0.43
 * @param {Function|String} definition - The string reference to change.
 * @param {Object|'desc'} options - Can be the 'desc' expression or a complex object to configure the helper
 * @param {'desc'|'asc'} options.direction - The direction of the sorting algorithm.
 * @param {function} options.compare - The custom compare function.
 * @param {boolean} options.ignoreCase - Indicates if the sort process ignore case.
 * @return Returns the chained sorting function.
 * @example
 * ```
 * let cities =
 * [
 *     { id:  7 , name: "Amsterdam"  , population: 750000  , country: "Netherlands" },
 *     { id: 12 , name: "The Hague"  , population: 450000  , country: "Netherlands" },
 *     { id: 43 , name: "Rotterdam"  , population: 600000  , country: "Netherlands" },
 *     { id:  5 , name: "Berlin"     , population: 3000000 , country: "Germany"     },
 *     { id: 42 , name: "Düsseldorf" , population: 550000  , country: "Germany"     },
 *     { id: 44 , name: "Stuttgard"  , population: 600000  , country: "Germany"     }
 * ] ;
 * const compare = sortBy( ( v1 , v2 ) => ( v1.country < v2.country ) ? -1 : ( v1.country > v2.country ? 1 : 0 ) )
 *                .thenBy( ( v1 , v2 ) => v1.population - v2.population ) ;
 *
 * cities.sort( compare ) ;
 *
 * console.log( cities ) ;
 * ```
 * A collection of objects and a path to find the properties to compare :
 * ```
 * const compare = (new Intl.Collator( 'en' )).compare ;
 * const data = [{ doc : { text : "aäb" } }, { doc : { text : "aãb" } }, { doc : { text : "aab" } }, { doc : { text : "abb" } } , { doc : { text : "Aäb" } } ];
 * data.sort( sortBy( "doc.text", { compare } ) );
 * * ```
 */
function sortBy( definition , options = null )
{
    let x = (typeof(this) === "function" && !this['firstBy'] ) ? this : false;
    let y = makeCompare( definition , options );
    const func = x ? (a, b) => x(a,b) || y(a,b) : y ;
    Object.defineProperty( func , 'thenBy' , { enumerable:true , value: sortBy } ) ;
    return func ;
}

export default sortBy ;

// ---------- Private

const identity = v => v ;

const ignoreCase = value => isString(value) ? value.toLowerCase() : value ;

const defaultCompare = ( v1 , v2 ) =>
{
    if ( v1 > v2 )
    {
        return v1 < v2 ? -1 : 1;
    }
    else
    {
        return v1 < v2 ? -1 : 0;
    }
}

const makeCompare = ( definition , options ) =>
{
    options = typeof(options) === "object" ? options : { direction : options };

    let {
        direction ,
        compare,
        ignoreCase:ic
    }
    = options || {} ;

    if( isString( definition ) )
    {
        const prop = definition ;
        definition = object => hasPath( object , prop ) ? get( object , prop ) : '' ;
    }

    compare = compare instanceof Function ? compare : defaultCompare ;

    if( definition.length === 1 )
    {
        let uf = definition ;
        let pr = ic ? ignoreCase : identity ;
        definition = ( v1 , v2 ) => compare( pr( uf(v1) ) , pr( uf(v2) ) ) ;
    }

    return ( direction === -1 || direction === 'desc' ) ? ( ( v1 , v2 ) => -definition( v1 , v2 ) ) : definition ;
}
