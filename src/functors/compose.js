'use strict'

/**
 * Composes single-argument functions from right to left.
 * The rightmost function can take multiple arguments as it provides the signature for the resulting composite function.
 * @name compose
 * @memberof core.functors
 * @function
 * @instance
 * @param {...Function} functions - The functions to compose.
 * @return {Function} A function obtained by composing the argument functions from right to left.
 * For example, compose(f, g, h) is identical to doing (...args) => f(g(h(...args))).
 */
const compose = ( ...functions ) =>
{
    if ( functions.length === 0 )
    {
        return arg => arg ;
    }

    functions = functions.filter( func => func instanceof Function ) ;

    if ( functions.length === 1 )
    {
        return functions[0] ;
    }

    return functions.reduce( ( a , b ) => ( ...args ) => a( b( ...args ) ) ) ;
};

export default compose  ;
