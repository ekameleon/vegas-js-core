'use strict'

import isFunction from '../isFunction'

import debounce from './debounce'

/**
 * Creates a throttled function that only invokes `func` at most once per
 * every `wait` milliseconds (or once per browser frame). The throttled function
 * comes with a `cancel` method to cancel delayed `func` invocations and a
 * `flush` method to immediately invoke them. Provide `options` to indicate
 * whether `func` should be invoked on the leading and/or trailing edge of the
 * `wait` timeout. The `func` is invoked with the last arguments provided to the
 * throttled function. Subsequent calls to the throttled function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the throttled function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred until the next tick,
 * similar to `setTimeout` with a timeout of `0`.
 *
 * If `wait` is omitted in an environment with `requestAnimationFrame`, `func`invocation will be deferred until
 * the next frame is drawn (typically about 16ms).
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/) for details over the differences between `throttle` and `debounce`.
 * This implementation is inspired by the lodash throttle implementation.
 * @since 1.0.7
 * @name throttle
 * @memberof core.functors
 * @function
 * @instance
 * @param {Function} func - The function to throttle.
 * @param {number} wait - The number of milliseconds to delay the throttle invokation; if omitted, `requestAnimationFrame` is used.
 * @param {Object} [options] The optional object to set the throttle function.
 * @param {boolean} [options.leading=true] - Specify invoking on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true] - Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the throttled function.
 */
const throttle = ( func, wait , options = {} ) =>
{
    if ( !isFunction(func) )
    {
        throw new TypeError('Expected a function')
    }

    const { leading = true , trailing = true } = options || {} ;

    return debounce( func , wait, { leading , maxWait:wait, trailing } );
};

export default throttle;