'use strict'

import isFunction from '../isFunction'

/**
 * If `func` is a function, call the function with the passed-in arguments. Otherwise, return `false`.
 * @name maybeCall
 * @memberof core.functors
 * @param {*} func The function to invoke or the value to return if not a Function.
 * @param args The optional arguments to passed-in the function.
 * @return false or the result of the passed-in function.
 * @function
 * @instance
 */
const maybeCall = ( func , ...args ) => isFunction( func ) && func( ...args ) ;

export default maybeCall ;