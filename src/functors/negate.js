/* jshint strict: false */

/**
 * Generates a function that negates the result of the given predicate function.
 * @name negate
 * @memberof core.functors
 * @function
 * @instance
 * @since 1.0.43
 * @param {function} func - The functions to compose.
 * @return {Function} Returns the new negated function.
 * @example
 * ```
 *  const isEven = n => n%2 === 0 ;
 *  console.log( filter([1,2,3,4,5,6] , negate( isEven ) ) ) ; // [1,3,5]
 * ```
 */
const negate = func =>
{
    if ( typeof func !== 'function' )
    {
        throw new TypeError( 'negate failed, expected a function' ) ;
    }
    return ( ...args ) => !func( ...args );
};

export default negate  ;
