'use strict'

import isFunction from '../isFunction'

/**
 * If `func` is a function, call the function with the passed-in arguments. Otherwise, return `func`.
 * @name callOrReturn
 * @param {*} func The function to invoke or the value to return if not a Function.
 * @param args The optional arguments to passed-in the function.
 * @memberof core.functors
 * @function
 * @instance
 * @return {Function} The function return or the func value (if not a Function)
 */
const callOrReturn = ( func , ...args ) => isFunction( func ) ? func( ...args ) : func ;

export default callOrReturn ;