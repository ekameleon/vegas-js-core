'use strict'

/**
 * An empty method to mock some default variables.
 * @name empty
 * @memberof core.functors
 * @function
 * @instance
 * @return {Function} The function reference.
 */
const empty = () => {} ;

export default empty ;