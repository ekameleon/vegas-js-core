'use strict'

import isObject from '../isObject'

import cancelAnimationFrame  from '../cancelAnimationFrame'
import requestAnimationFrame from '../requestAnimationFrame'

/**
 * Creates a debounced function that delays invoking `func` until after `wait`milliseconds have elapsed since the
 * last time the debounced function was invoked, or until the next browser frame is drawn.
 * The debounced function comes with a `cancel` method to cancel delayed `func` invocations and a
 * `flush` method to immediately invoke them. Provide `options` to indicate
 * whether `func` should be invoked on the leading and/or trailing edge of the
 * `wait` timeout. The `func` is invoked with the last arguments provided to the
 * debounced function. Subsequent calls to the debounced function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * If `wait` is omitted in an environment with `requestAnimationFrame`, `func`
 * invocation will be deferred until the next frame is drawn (typically about
 * 16ms).
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `debounce` and `throttle`.
 * This implementation is inspired by the lodash throttle implementation.
 * @since 1.0.7
 * @name debounce
 * @memberof core.functors
 * @function
 * @instance
 * @param {Function} func - The function to debounce.
 * @param {number|null} wait - The number of milliseconds to delay invokation; if omitted, `requestAnimationFrame` is used.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false] - Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait] - The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true] - Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the debounced function.
 * @example
 * window.onload = function()
 * {
 *     const debounce = core.functors.debounce;
 *
 *     let w = document.querySelector("#width") ;
 *     let h = document.querySelector("#height") ;
 *     let c = document.querySelector("#calls");
 *
 *     let calls = 0;
 *
 *     function getDimensions()
 *     {
 *         console.log( 'getDimensions' ) ;
 *         w.innerHTML = window.innerWidth;
 *         h.innerHTML = window.innerHeight;
 *         c.innerHTML = ++calls ;
 *     }
 *
 *     window.addEventListener('resize', debounce( getDimensions, 200 ) );
 * };
 */
const debounce = ( func, wait, options) =>
{
    let lastArgs,
        lastCallTime,
        lastThis,
        maxWait,
        result,
        timerId;
  
    let lastInvokeTime = 0 ;
    let leading        = false ;
    let maxing         = false ;
    let trailing       = true ;

    const framed = (!wait && wait !== 0 && typeof requestAnimationFrame === 'function')

    if (typeof(func) !== 'function')
    {
        throw new TypeError('Expected a function')
    }
  
    wait = +wait || 0 ;
  
    if ( isObject(options) )
    {
        leading  = options.leading;
        maxing   = options.hasOwnProperty('maxWait') ;
        maxWait  = maxing ? Math.max(+options.maxWait || 0, wait) : maxWait ;
        trailing = options.hasOwnProperty('trailing') ? options.trailing : trailing ;
    }

    const invokeFunc = ( time ) =>
    {
        const args    = lastArgs;
        const thisArg = lastThis;

        lastArgs       = lastThis = undefined;
        lastInvokeTime = time ;
        result = func.apply(thisArg, args);
        return result
    };

    const startTimer = (pendingFunc, wait) =>
    {
        if (framed)
        {
            return requestAnimationFrame(pendingFunc);
        }
        return setTimeout(pendingFunc, wait);
    };

    const stopTimer = (id) =>
    {
        if (framed)
        {
            return cancelAnimationFrame(id);
        }
        clearTimeout(id);
    };

    const leadingEdge = (time) =>
    {
        lastInvokeTime = time;
        timerId = startTimer(timerExpired, wait);
        return leading ? invokeFunc(time) : result;
    };

    const remainingWait = (time) =>
    {
        const timeSinceLastCall   = time - lastCallTime;
        const timeSinceLastInvoke = time - lastInvokeTime;
        const timeWaiting         = wait - timeSinceLastCall;

        return maxing ? Math.min(timeWaiting, maxWait - timeSinceLastInvoke)
                      : timeWaiting ;
    };

    const shouldInvoke = (time) =>
    {
        const timeSinceLastCall = time - lastCallTime;
        const timeSinceLastInvoke = time - lastInvokeTime;
        return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
               (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
    };

    const timerExpired = () =>
    {
        const time = Date.now();
        if (shouldInvoke(time))
        {
            return trailingEdge(time);
        }
        timerId = startTimer(timerExpired, remainingWait(time));
    };

    const trailingEdge = (time) =>
    {
        timerId = undefined;

        if (trailing && lastArgs)
        {
            return invokeFunc(time);
        }
        lastArgs = lastThis = undefined;
        return result;
    };

    const cancel = () =>
    {
        if (timerId !== undefined)
        {
            stopTimer(timerId);
        }
        lastInvokeTime = 0 ;
        lastArgs = lastCallTime = lastThis = timerId = undefined;
    };

    const flush = () =>
    {
        return timerId === undefined ? result : trailingEdge(Date.now());
    };

    const pending = () =>
    {
        return timerId !== undefined;
    };

    function debounced(...args)
    {
        const time = Date.now();
        const isInvoking = shouldInvoke(time);

        lastArgs     = args;
        lastThis     = this;
        lastCallTime = time;

        if (isInvoking)
        {
            if (timerId === undefined)
            {
                return leadingEdge(lastCallTime);
            }
            if (maxing)
            {
                timerId = startTimer(timerExpired, wait);
                return invokeFunc(lastCallTime);
            }
        }
        if (timerId === undefined)
        {
            timerId = startTimer(timerExpired, wait);
        }
        return result;
    }
    
    debounced.cancel  = cancel;
    debounced.flush   = flush;
    debounced.pending = pending;
    
    return debounced;
};

export default debounce;