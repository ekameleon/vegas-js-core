'use strict'

/**
 * Checks if the passed-in `value` is likely a prototype object.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
const isPrototype = value =>
{
    const Constructor = value && value.constructor ;
    const proto = (typeof Constructor === 'function' && Constructor.prototype) || Object.prototype ;
    return value === proto ;
}

export default isPrototype;