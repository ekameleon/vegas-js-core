'use strict'

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
const toString = value => Object.prototype.toString.call( value ) ;

export default toString ;