'use strict'

import isNotNullObject from './isNotNullObject'
import isReactElement  from './isReactElement'

import { dateTag , regexpTag} from './_/tags'

const isSpecial = value =>
{
    value = Object.prototype.toString.call(value) ;
    return value === regexpTag || value === dateTag || isReactElement( value ) ;
};

/**
 * Returns `true` if the given value is a mergeable object.
 * @name isMergeableObject
 * @memberof core
 * @since 1.0.14
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is a mergeable object.
 * @example
 * console.log(isMergeableObject(undefined)) ; // false
 * console.log(isMergeableObject(null)) ; // false
 * console.log(isMergeableObject(new RegExp('hello'))) ; // false
 * console.log(isMergeableObject(new Date())) ; // false
 * console.log(isMergeableObject(reactElement)) ; // false
 *
 * console.log(isMergeableObject({})) ; // true
 */
const isMergeableObject = value => isNotNullObject(value) && !isSpecial(value) ;

export default isMergeableObject ;