'use strict'

import Uint8Array from './Uint8Array.js'

/**
 * Creates a clone of `arrayBuffer`.
 * @private
 * @param {*} arrayBuffer The array buffer to clone.
 * @returns {ArrayBuffer} Returns the cloned array buffer.
 */
const cloneArrayBuffer = arrayBuffer =>
{
    const result = new arrayBuffer.constructor( arrayBuffer.byteLength ) ;
    new Uint8Array( result ).set( new Uint8Array( arrayBuffer ) );
    return result ;
}

export default cloneArrayBuffer;