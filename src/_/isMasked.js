'use strict'

import coreJsData from './coreJsData.js'

const apply = () =>
{
    const uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys?.IE_PROTO || '' ) ;
    return uid ? ('Symbol(src)_1.' + uid) : '';
}

const maskSrcKey = apply();

/**
 * Checks if `func` has its source masked.
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
const isMasked = func => !!maskSrcKey && (maskSrcKey in func) ;

export default isMasked;