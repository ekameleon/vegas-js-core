'use strict'

import eq from '../eq.js'
import baseAssignValue from './baseAssignValue.js'

/**
 * This function is like `assignValue` except that it doesn't assign `undefined` values.
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
const assignMergeValue = ( object , key , value ) =>
{
    if ( (value !== undefined && !eq( object[key], value ) ) || ( value === undefined && !(key in object ) ) )
    {
        baseAssignValue(object, key, value) ;
    }
}

export default assignMergeValue