'use strict'

import getMapData from './getMapData.js';

import Hash      from './Hash.js'
import ListCache from './ListCache.js'
import Map       from './Map.js'

class MapCache
{
    /**
     * Creates a new ListCache instance.
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    constructor( entries)
    {
        this.clear();
        let index = -1 ;
        let length = entries == null ? 0 : entries.length ;
        while (++index < length)
        {
            let entry = entries[index] ;
            this.set( entry[0] , entry[1] ) ;
        }
    }

    /**
     * Removes all key-value entries from the map.
     * @name clear
     * @memberOf MapCache
     */
    clear()
    {
        this.size = 0;
        const MapConstructor = Map ?? ListCache ;
        this.__data__ =
        {
            'hash'   : new Hash(),
            'map'    : new MapConstructor() ,
            'string' : new Hash()
        };
    }

    /**
     * Removes `key` and its value from the map.
     * @name delete
     * @memberOf MapCache
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    delete( key )
    {
        const result = getMapData( this , key )[ 'delete' ]( key );
        this.size -= result ? 1 : 0;
        return result;
    }

    /**
     * Checks if a map value for `key` exists.
     * @name has
     * @memberOf MapCache
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    has( key )
    {
        return getMapData( this , key ).has( key ) ;
    }

    /**
     * Gets the map value for `key`.
     * @name get
     * @memberOf MapCache
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    get( key )
    {
        return getMapData( this , key ).get( key );
    }

    /**
     * Sets the map `key` to `value`.
     * @name set
     * @memberOf MapCache
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the map cache instance.
     */
    set( key , value )
    {
        const data = getMapData( this , key ) ;
        const size = data.size;
        data.set( key , value );
        this.size += data.size === size ? 0 : 1;
        return this;
    }
}

export default MapCache ;