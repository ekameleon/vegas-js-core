'use strict'

/**
 * Adds the key-value `pair` to `map`.
 * @private
 * @param {Object} map The map to modify.
 * @param {Array} pair The key-value pair to add.
 * @returns {Object} Returns `map`.
 */
const addMapEntry = ( map, pair) =>
{
    map.set( pair[0] , pair[1] ) ; // Don't return `map.set` because it's not chainable in IE 11.
    return map;
}

export default addMapEntry;