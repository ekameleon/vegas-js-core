'use strict'

import baseCreate from './baseCreate.js';
import isObject   from '../isObject.js'

/**
 * Creates a function that produces an instance of `Constructor` regardless of whether it was invoked
 * as part of a `new` expression or by `call` or `apply`.
 * @private
 * @param {Function} Constructor The constructor to wrap.
 * @returns {Function} Returns the new wrapped function.
 */
const createConstructor = Constructor => ( ...args ) =>
{
    switch ( args.length )
    {
        case 0 : return new Constructor();
        case 1 : return new Constructor( args[0] );
        case 2 : return new Constructor( args[0], args[1] );
        case 3 : return new Constructor( args[0], args[1], args[2] );
        case 4 : return new Constructor( args[0], args[1], args[2], args[3] );
        case 5 : return new Constructor( args[0], args[1], args[2], args[3], args[4] );
        case 6 : return new Constructor( args[0], args[1], args[2], args[3], args[4], args[5] );
        case 7 : return new Constructor( args[0], args[1], args[2], args[3], args[4], args[5], args[6] );
    }
    const thisBinding = baseCreate(Constructor.prototype) ;
    const result      = Constructor.apply( thisBinding , args ) ;

    // Mimic the constructor's `return` behavior.
    // See https://es5.github.io/#x13.2.2 for more details.
    return isObject(result) ? result : thisBinding;
}

export default createConstructor ;