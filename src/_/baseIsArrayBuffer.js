'use strict'

import getTag       from '../objects/getTag.js'
import isObjectLike from '../isObjectLike.js'

import { arrayBufferTag } from './tags.js'

/**
 * The base implementation of `isArrayBuffer` without Node.js optimizations.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array buffer , else `false`.
 */
const baseIsArrayBuffer = value => isObjectLike(value) && getTag(value) === arrayBufferTag ;

export default baseIsArrayBuffer;