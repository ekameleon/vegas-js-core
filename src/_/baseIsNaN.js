'use strict'

/**
 * The base implementation of `isNaN` without support for number objects.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is 'NaN', else `false`.
 */
const baseIsNaN = value => value !== value ;

export default baseIsNaN;