'use strict'

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function|ObjectConstructor} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
const overArg = ( func , transform ) => arg => func( transform( arg ) ) ;

export default overArg;