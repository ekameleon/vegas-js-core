'use strict'

import root from './root'
import moduleExports from './moduleExports'

const Buffer = moduleExports ? root.Buffer : undefined ;

const allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined ;

export default allocUnsafe ;