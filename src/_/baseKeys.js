'use strict'

import isPrototype from '../reflect/isPrototype.js'
import nativeKeys  from './nativeKeys.js'

/**
 * The base implementation of `keys` which doesn't treat sparse arrays as dense.
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
const baseKeys = object =>
{
    if ( !isPrototype(object) )
    {
        return nativeKeys(object) ;
    }
    const result = [] ;
    for ( let key in Object(object) )
    {
        if ( Object.prototype.hasOwnProperty.call(object, key) && key !== 'constructor' )
        {
            result.push(key);
        }
    }
    return result ;
}

export default baseKeys ;