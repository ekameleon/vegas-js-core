'use strict'

import baseCreate   from './baseCreate.js';
import getPrototype from './getPrototype.js';
import isPrototype  from '../reflect/isPrototype.js'

/**
 * Initializes an object clone.
 * @private
 * @param {Object} object The object to clone.
 * @returns {Object} Returns the initialized clone.
 */
const initCloneObject = object => ( typeof object.constructor === 'function' && !isPrototype(object) ) ? baseCreate( getPrototype(object) ) : {} ;

export default initCloneObject ;