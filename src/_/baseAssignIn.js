'use strict'

import copy   from '../objects/copy.js'
import keysIn from '../objects/keysIn.js'

/**
 * The base implementation of `assignIn` without support for multiple sources or `customizer` functions.
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
const baseAssignIn = ( object, source) => object && copy(source, keysIn(source), object) ;

export default baseAssignIn;