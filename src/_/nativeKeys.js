'use strict'

import overArg from './overArg.js'

const nativeKeys = overArg( Object.keys , Object ) ;

export default nativeKeys ;