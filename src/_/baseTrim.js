'use strict'

import trimmedEndIndex from './trimmedEndIndex.js';

/** Used to match leading whitespace. */
var reTrimStart = /^\s+/;

/**
 * The base implementation of `_.trim`.
 *
 * @private
 * @param {string} string The string to trim.
 * @returns {string} Returns the trimmed string.
 */
const baseTrim = string => string ? string.slice(0, trimmedEndIndex(string) + 1).replace(reTrimStart, '')
                                          : string ;

export default baseTrim;