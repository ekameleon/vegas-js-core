'use strict'

import _Symbol from './Symbol.js'

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */

/** Built-in value references. */
const symToStringTag = _Symbol?.toStringTag ?? undefined ;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
const getRawTag = value =>
{
    const isOwn = Object.prototype.hasOwnProperty.call( value , symToStringTag ) ;
    const tag = value[ symToStringTag ] ;

    let unmasked ;

    try {
        value[symToStringTag] = undefined;
        unmasked              = true ;
    }
    catch ( doNothing )
    {

    }

    const result = Object.prototype.toString.call(value);
    if ( unmasked )
    {
        if (isOwn)
        {
            value[symToStringTag] = tag ;
        }
        else
        {
            delete value[symToStringTag] ;
        }
    }
    return result;
}

export default getRawTag;