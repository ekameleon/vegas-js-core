'use strict'

import getTag       from '../objects/getTag.js'
import isObjectLike from '../isObjectLike.js'

import { setTag } from './tags'

/**
 * The base implementation of `isSet` without Node.js optimizations.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a set, else `false`.
 */
const baseIsSet = value => isObjectLike(value) && getTag(value) === setTag ;

export default baseIsSet;