'use strict'

export const nullTag      = '[object Null]' ;
export const undefinedTag = '[object Undefined]' ;

export const argsTag    = '[object Arguments]' ;
export const arrayTag   = '[object Array]' ;
export const asyncTag   = '[object AsyncFunction]' ;
export const boolTag    = '[object Boolean]' ;
export const dateTag    = '[object Date]' ;
export const errorTag   = '[object Error]' ;
export const funcTag    = '[object Function]' ;
export const genTag     = '[object GeneratorFunction]' ;
export const mapTag     = '[object Map]' ;
export const numberTag  = '[object Number]' ;
export const objectTag  = '[object Object]' ;
export const proxyTag   = '[object Proxy]' ;
export const regexpTag  = '[object RegExp]' ;
export const setTag     = '[object Set]' ;
export const stringTag  = '[object String]' ;
export const symbolTag  = '[object Symbol]' ;
export const weakSetTag = '[object WeakSet]' ;
export const weakMapTag = '[object WeakMap]' ;

export const arrayBufferTag  = '[object ArrayBuffer]' ;
export const dataViewTag     = '[object DataView]' ;
export const float32Tag      = '[object Float32Array]' ;
export const float64Tag      = '[object Float64Array]' ;
export const int8Tag         = '[object Int8Array]' ;
export const int16Tag        = '[object Int16Array]' ;
export const int32Tag        = '[object Int32Array]' ;
export const uint8Tag        = '[object Uint8Array]' ;
export const uint8ClampedTag = '[object Uint8ClampedArray]' ;
export const uint16Tag       = '[object Uint16Array]' ;
export const uint32Tag       = '[object Uint32Array]' ;

export const cloneableTags =
{
    [ argsTag         ]  : true ,
    [ arrayTag        ]  : true ,
    [ arrayBufferTag  ]  : true ,
    [ dataViewTag     ]  : true ,
    [ boolTag         ]  : true ,
    [ dateTag         ]  : true ,
    [ float32Tag      ]  : true ,
    [ float64Tag      ]  : true ,
    [ int8Tag         ]  : true ,
    [ int16Tag        ]  : true ,
    [ int32Tag        ]  : true ,
    [ mapTag          ]  : true ,
    [ numberTag       ]  : true ,
    [ objectTag       ]  : true ,
    [ regexpTag       ]  : true ,
    [ setTag          ]  : true ,
    [ stringTag       ]  : true ,
    [ symbolTag       ]  : true ,
    [ uint8Tag        ]  : true ,
    [ uint8ClampedTag ]  : true ,
    [ uint16Tag       ]  : true ,
    [ uint32Tag       ]  : true ,

    [ errorTag   ] : false ,
    [ funcTag    ] : false ,
    [ weakMapTag ] : false ,
} ;

export const typedArrayTags =
{
    float32Tag      : true ,
    float64Tag      : true ,
    int8Tag         : true ,
    int16Tag        : true ,
    int32Tag        : true ,
    uint8Tag        : true ,
    uint8ClampedTag : true ,
    uint16Tag       : true ,
    uint32Tag       : true ,

    argsTag         : false ,
    arrayTag        : false ,
    arrayBufferTag  : false ,
    boolTag         : false ,
    dataViewTag     : false ,
    dateTag         : false ,
    errorTag        : false ,
    funcTag         : false ,
    mapTag          : false ,
    numberTag       : false ,
    objectTag       : false ,
    regexpTag       : false ,
    setTag          : false ,
    stringTag       : false ,
    weakMapTag      : false ,
};

