'use strict'

import cloneArrayBuffer from './cloneArrayBuffer.js'
import cloneDataView    from './cloneDataView.js'
import cloneRegExp      from './cloneRegExp.js'
import cloneSymbol      from './cloneSymbol.js'
import cloneTypedArray  from './cloneTypedArray.js'

import {
    boolTag ,
    dateTag ,
    mapTag ,
    numberTag ,
    regexpTag ,
    setTag ,
    stringTag ,
    symbolTag ,

    arrayBufferTag ,
    dataViewTag ,
    float32Tag ,
    float64Tag ,
    int8Tag ,
    int16Tag ,
    int32Tag ,
    uint8Tag ,
    uint8ClampedTag ,
    uint16Tag ,
    uint32Tag ,
}
from './tags'

/**
 * Initializes an object clone based on its `toStringTag`.
 * **Note:** This function only supports cloning values with tags of
 * `Boolean`, `Date`, `Error`, `Map`, `Number`, `RegExp`, `Set`, or `String`.
 * @private
 * @param {Object} object The object to clone.
 * @param {string} tag The `toStringTag` of the object to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the initialized clone.
 */
const initCloneByTag = ( object , tag , isDeep ) =>
{
    const Ctor = object.constructor;
    switch ( tag )
    {
        case arrayBufferTag:
        {
            return cloneArrayBuffer( object ) ;
        }

        case boolTag :
        case dateTag :
        {
            return new Ctor(+object) ;
        }

        case dataViewTag :
        {
            return cloneDataView(object, isDeep);
        }

        case float32Tag      :
        case float64Tag      :
        case int8Tag         :
        case int16Tag        :
        case int32Tag        :
        case uint8Tag        :
        case uint8ClampedTag :
        case uint16Tag       :
        case uint32Tag       :
        {
            return cloneTypedArray(object, isDeep);
        }

        case setTag :
        case mapTag :
        {
            return new Ctor() ;
        }

        case numberTag :
        case stringTag :
        {
            return new Ctor( object ) ;
        }

        case regexpTag:
        {
            return cloneRegExp( object );
        }

        case symbolTag :
        {
            return cloneSymbol(object) ;
        }
    }
}

export default initCloneByTag ;