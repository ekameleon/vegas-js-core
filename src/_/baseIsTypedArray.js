'use strict'

import baseGetTag   from './baseGetTag.js';
import isLength     from '../isLength.js';
import isObjectLike from '../isObjectLike.js';

import { typedArrayTags } from './tags'

/**
 * The base implementation of `isTypedArray` without Node.js optimizations.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
const baseIsTypedArray = value => isObjectLike(value) && isLength(value.length) && !!typedArrayTags[baseGetTag(value)];

export default baseIsTypedArray;