'use strict'

import getNative from './getNative.js'
import root      from './root.js'

export default getNative( root , 'Map' ) ;