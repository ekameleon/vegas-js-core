'use strict'

/**
 * The base implementation of `unary` without support for storing metadata.
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
const baseUnary = func => value => func(value) ;

export default baseUnary;