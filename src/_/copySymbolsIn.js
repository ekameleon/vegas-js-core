'use strict'

import copyObject from '../objects/copy.js';
import getSymbolsIn from './getSymbolsIn.js';

/**
 * Copies own symbols of `source` to `object`.
 * @private
 * @param {Object} source The object to copy symbols from.
 * @param {Object} [object={}] The object to copy symbols to.
 * @returns {Object} Returns `object`.
 */
const copySymbolsIn = ( source , object ) => copyObject( source , getSymbolsIn(source) , object ) ;

export default copySymbolsIn;