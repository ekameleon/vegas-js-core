'use strict'

import copyObject from '../objects/copy.js'
import keys       from '../objects/keys.js'

/**
 * The base implementation of `assign` without support for multiple sources or `customizer` functions.
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
const baseAssign = ( object , source ) =>object && copyObject(source, keys(source), object) ;

export default baseAssign ;