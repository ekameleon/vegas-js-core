'use strict'

import isFunction from '../isFunction.js'
import isMasked   from './isMasked.js'
import isObject   from '../isObject.js'
import toSource   from './toSource.js'

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
const reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
const reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used to detect if a method is native. */
const reIsNative = RegExp('^' +
    Function.prototype.toString.call( Object.prototype.hasOwnProperty )
                .replace( reRegExpChar, '\\$&' )
                .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
const baseIsNative = value =>
{
    if ( !isObject(value) || isMasked(value) )
    {
        return false;
    }
    const pattern = isFunction(value) ? reIsNative : reIsHostCtor ;
    return pattern.test(toSource(value));
}

export default baseIsNative;