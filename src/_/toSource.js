'use strict'

/**
 * Converts `func` to its source code.
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
const toSource = func =>
{
    if ( func !== null )
    {
        try
        {
            return Function.prototype.toString.call( func ) ;
        }
        catch ( doNothing )
        {

        }

        try
        {
            return ( func + '' ) ;
        }
        catch ( doNothing )
        {

        }
    }
    return '' ;
}

export default toSource;