'use strict'

/* jshint -W016 */
/* jshint -W040 */

import root from './root.js'
import createConstructor from './createConstructor.js'

/** Used to compose bitmasks for function metadata. */
const WRAP_BIND_FLAG = 1;

/**
 * Creates a function that wraps `func` to invoke it with the optional `this`
 * binding of `thisArg`.
 * @private
 * @param {Function} func The function to wrap.
 * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
 * @param {*} [thisArg] The `this` binding of `func`.
 * @returns {Function} Returns the new wrapped function.
 */
const createBind = ( func , bitmask, thisArg ) =>
{
    const isBind = bitmask & WRAP_BIND_FLAG ;
    const Constructor = createConstructor( func );

    function wrapper()
    {
        const fn = ( this && this !== root && this instanceof wrapper ) ? Constructor : func;
        return fn.apply(isBind ? thisArg : this, arguments);
    }

    return wrapper;
}

export default createBind;