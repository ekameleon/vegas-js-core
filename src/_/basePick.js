'use strict'

import hasIn from '../objects/hasIn.js'
import basePickBy from './basePickBy.js'
/**
 * The base implementation of `pick` without support for individual property identifiers.
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @returns {Object} Returns the new object.
 */
const basePick = ( object , paths ) => basePickBy( object , paths , ( value , path ) => hasIn( object , path ) )  ;

export default basePick