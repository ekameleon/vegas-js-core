'use strict'

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
const getValue = (object, key) => object === null || object === undefined ? undefined : object[key] ;

export default getValue ;