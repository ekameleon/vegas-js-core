'use strict'

/**
 * Appends the elements of `values` to `array`.
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
const arrayPush = ( array , values ) =>
{
    let index = -1 ;
    let length = values.length ;
    let offset = array.length;

    while (++index < length)
    {
        array[offset + index] = values[index];
    }

    return array;
}

export default arrayPush ;