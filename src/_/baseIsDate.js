'use strict'

import getTag       from '../objects/getTag.js'
import isObjectLike from '../isObjectLike.js'

import { dateTag } from './tags'

/**
 * The base implementation of `isDate` without Node.js optimizations.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a Date, else `false`.
 */
const baseIsDate = value => isObjectLike(value) && getTag(value) === dateTag ;

export default baseIsDate;