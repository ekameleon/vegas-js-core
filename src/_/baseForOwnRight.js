'use strict'

import baseFor from './baseFor.js'
import keys    from '../objects/keys.js'

/**
 * The base implementation of `forOwnRight`.
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
const baseForOwnRight = ( object , iteratee ) => object && baseFor( object , iteratee , keys ) ;

export default baseForOwnRight