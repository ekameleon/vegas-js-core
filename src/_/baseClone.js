'use strict'

/* jshint -W016 */

import Stack from './Stack.js';

import arrayEach       from '../arrays/each.js'
import assignValue     from '../objects/assignValue.js'
import baseAssign      from './baseAssign.js';
import baseAssignIn    from './baseAssignIn.js';
import cloneBuffer     from './cloneBuffer.js';
import copyArray       from '../arrays/copy.js';
import copySymbols     from './copySymbols.js';
import copySymbolsIn   from './copySymbolsIn.js';
import getAllKeys      from './getAllKeys.js';
import getAllKeysIn    from './getAllKeysIn.js';
import getTag          from '../objects/getTag.js';
import initCloneArray  from './initCloneArray.js';
import initCloneByTag  from './initCloneByTag.js';
import initCloneObject from './initCloneObject.js';
import isArray         from '../isArray.js';
import isBuffer        from '../isBuffer.js';
import isMap           from '../isMap.js';
import isObject        from '../isObject.js';
import isSet           from '../isSet.js';
import keys            from '../objects/keys.js';
import keysIn          from '../objects/keysIn.js';

import {
    argsTag ,
    cloneableTags ,
    funcTag ,
    genTag  ,
    objectTag ,
}
from './tags'

const CLONE_DEEP_FLAG    = 1 ;
const CLONE_FLAT_FLAG    = 2 ;
const CLONE_SYMBOLS_FLAG = 4 ;

/**
 * The base implementation of `_.clone` and `_.cloneDeep` which tracks
 * traversed objects.
 * @private
 * @param {*} value The value to clone.
 * @param {*} bitmask The bitmask flags.
 *  1 - Deep clone
 *  2 - Flatten inherited properties
 *  4 - Clone symbols
 * @param {Function} [customizer] The function to customize cloning.
 * @param {string} [key] The key of `value`.
 * @param {Object} [object] The parent object of `value`.
 * @param {Object} [stack] Tracks traversed objects and their clone counterparts.
 * @returns {*} Returns the cloned value.
 */
const baseClone = ( value , bitmask , customizer , key, object, stack ) =>
{
    let result ;

    const isDeep = bitmask & CLONE_DEEP_FLAG ;
    const isFlat = bitmask & CLONE_FLAT_FLAG ;
    const isFull = bitmask & CLONE_SYMBOLS_FLAG ;

    if ( customizer )
    {
        result = object ? customizer( value , key , object , stack ) : customizer( value ) ;
    }

    if ( result !== undefined )
    {
        return result;
    }

    if ( !isObject( value ) )
    {
        return value;
    }

    const isArr = isArray( value ) ;

    if ( isArr )
    {
        result = initCloneArray( value );
        if ( !isDeep )
        {
            return copyArray(value, result);
        }
    }
    else
    {
        const tag    = getTag( value ) ;
        const isFunc = tag === funcTag || tag === genTag ;

        if ( isBuffer( value ) )
        {
            return cloneBuffer(value, isDeep);
        }

        if ( tag === objectTag || tag === argsTag || (isFunc && !object) )
        {
            result = (isFlat || isFunc) ? {} : initCloneObject(value) ;
            if (!isDeep)
            {
                return isFlat ? copySymbolsIn( value , baseAssignIn( result , value ) ) : copySymbols( value , baseAssign( result , value ) );
            }
        }
        else
        {
            if ( !cloneableTags[tag] )
            {
                return object ? value : {};
            }
            result = initCloneByTag(value, tag, isDeep);
        }
    }

    // Check for circular references and return its corresponding clone.

    stack = stack ?? new Stack() ;
    const stacked = stack.get( value ) ;
    if ( stacked )
    {
        return stacked;
    }
    stack.set( value , result ) ;

    if ( isSet( value ) )
    {
        value.forEach( subValue =>
        {
            result.add( baseClone(subValue, bitmask, customizer, subValue, value, stack ) );
        });
    }
    else if ( isMap(value) )
    {
        value.forEach( ( subValue , key ) =>
        {
            result.set(key, baseClone(subValue, bitmask, customizer, key, value, stack));
        });
    }

    const keysFunc = isFull ? (isFlat ? getAllKeysIn : getAllKeys) : (isFlat ? keysIn : keys) ;

    const props = isArr ? undefined : keysFunc(value) ;
    arrayEach(props ?? value , ( subValue , key ) =>
    {
        if ( props )
        {
            key      = subValue ;
            subValue = value[key] ;
        }
        assignValue( result , key , baseClone( subValue , bitmask , customizer , key , value , stack ) ); // Recursively populate clone (susceptible to call stack limits).
    });

    return result;
}

export default baseClone;