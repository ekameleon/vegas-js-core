'use strict'

/**
 * Adds the key-value `pair` to `set`.
 * @private
 * @param {Object} set The set to modify.
 * @param {*} value The value to add.
 * @returns {Object} Returns `set`.
 */
const addSetEntry = ( set,  value ) =>
{
    set.add(value) ; // Don't return `set.add` because it's not chainable in IE 11.
    return set;
}

export default addSetEntry;