'use strict'

import isObject from '../isObject.js'

const create = Object?.create ;

const build = () =>
{
    const Constructor = () => {} ;

    return proto =>
    {
        if ( !isObject( proto ) )
        {
            return {} ;
        }

        if ( create )
        {
            return create( proto );
        }

        Constructor.prototype = proto;

        const result = new Constructor();

        Constructor.prototype = undefined ;

        return result;
    };
}

/**
 * The base implementation of `create` without support for assigning properties to the created object.
 * @private
 * @param {Object} proto The object to inherit from.
 * @returns {Object} Returns the new object.
 */
const baseCreate = build() ;

export default baseCreate;