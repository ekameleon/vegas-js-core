'use strict'

/**
 * Copies the values of the `source` to `array`.
 * @private
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns the `array` reference or a new shallow copy of the array.
 */
const copyArray = ( source , array ) =>
{
    const length = source.length ;

    array = array ?? new Array( length ) ;

    let index = -1 ;

    while ( ++index < length )
    {
        array[index] = source[index] ;
    }

    return array ;
}

export default copyArray ;