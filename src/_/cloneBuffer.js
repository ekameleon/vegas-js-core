'use strict'

import allocUnsafe from './allocUnsafeBuffer'

/**
 * Creates a clone of  `buffer`.
 * @private
 * @param {Buffer} buffer The buffer to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Buffer} Returns the cloned buffer.
 */
const cloneBuffer = ( buffer , isDeep ) =>
{
    if ( isDeep )
    {
        return buffer.subarray();
    }
    const length = buffer.length ;
    const result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length) ;
    buffer.copy(result);
    return result;
}

export default cloneBuffer;