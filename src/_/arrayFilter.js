'use strict'

/**
 * A specialized version of `filter` for arrays without support for iteratee shorthands.
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
const arrayFilter = ( array , predicate ) =>
{
    let index = -1 ;
    let resIndex = 0 ;

    const length = array?.length ?? 0 ;
    const result = [] ;

    while (++index < length)
    {
        const value = array[index];
        if ( predicate( value, index, array ) )
        {
            result[resIndex++] = value ;
        }
    }

    return result;
}

export default arrayFilter ;