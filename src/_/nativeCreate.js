'use strict'

import getNative from './getNative.js';

export default getNative( Object , 'create' ) ;
