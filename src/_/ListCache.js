'use strict'

import assocIndexOf from './assocIndexOf.js'

class ListCache
{
    /**
     * Creates a new ListCache instance.
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    constructor( entries)
    {
        this.clear() ;
        let index = -1 ;
        const length = entries?.length ?? 0 ;
        while (++index < length)
        {
            let entry = entries[index] ;
            this.set(entry[0], entry[1]);
        }
    }

    /**
     * Removes all key-value entries from the list cache.
     * @name clear
     * @memberOf ListCache
     */
    clear()
    {
        this.__data__ = [];
        this.size = 0;
    }

    /**
     * Removes `key` and its value from the list cache.
     * @name delete
     * @memberOf ListCache
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    delete( key )
    {
        const data = this.__data__ ;

        const index = assocIndexOf(data, key) ;
        if (index < 0)
        {
            return false ;
        }

        const lastIndex = data.length - 1 ;
        if (index === lastIndex)
        {
            data.pop();
        }
        else
        {
            Array.prototype.splice.call( data , index , 1 ) ;
        }
        --this.size ;
        return true ;
    }

    /**
     * Checks if a list cache value for `key` exists.
     * @name has
     * @memberOf ListCache
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    has( key )
    {
        return assocIndexOf(this.__data__, key) > -1;
    }

    /**
     * Gets the list cache value for `key`.
     * @name get
     * @memberOf ListCache
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    get( key )
    {
        const data = this.__data__ ;
        const index = assocIndexOf(data, key) ;
        return index < 0 ? undefined : data[index][1];
    }

    /**
     * Sets the hash `key` to `value`.
     * @name set
     * @memberOf Hash
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the hash instance.
     */
    set( key , value )
    {
        const data = this.__data__ ;
        const index = assocIndexOf(data, key) ;
        if ( index < 0 )
        {
            ++this.size;
            data.push( [key, value] ) ;
        }
        else
        {
            data[index][1] = value ;
        }
        return this ;
    }
}

export default ListCache ;