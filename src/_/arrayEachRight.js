'use strict'

/**
 * A specialized version of `forEachRight` for arrays.
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns `array`.
 */
const arrayEachRight = ( array, iteratee ) =>
{
    let length = ( array === null || array === undefined ) ? 0 : array.length ;

    while ( length-- )
    {
        if ( iteratee( array[length] , length , array ) === false )
        {
            break ;
        }
    }

    return array ;
}

export default arrayEachRight ;