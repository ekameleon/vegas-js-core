'use strict'

import baseGetAllKeys from './baseGetAllKeys.js';
import getSymbolsIn   from './getSymbolsIn.js';
import keysIn         from '../objects/keysIn.js';

/**
 * Creates an array of own and inherited enumerable property names and symbols of `object`.
 * @private
 * @param {Object} object The object to query.
 * @returns {string[]} Returns the array of property names and symbols.
 */
const getAllKeysIn = object => baseGetAllKeys( object , keysIn , getSymbolsIn ) ;

export default getAllKeysIn;