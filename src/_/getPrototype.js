'use strict'

import overArg from './overArg.js'

const getPrototype = overArg( Object.getPrototypeOf , Object ) ;

export default getPrototype ;