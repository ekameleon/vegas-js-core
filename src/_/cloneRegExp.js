'use strict'

const reFlags = /\w*$/;

/**
 * Creates a clone of `regexp`.
 * @private
 * @param {*} regexp The regexp to clone.
 * @returns {Object} Returns the cloned regexp.
 */
const cloneRegExp = regexp =>
{
    const result = new regexp.constructor( regexp.source , reFlags.exec( regexp ) );
    result.lastIndex = regexp.lastIndex;
    return result;
}

export default cloneRegExp;