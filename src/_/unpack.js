'use strict'

import isPlainObject from '../isPlainObject'

const unpack = ( args , key = null ) =>
{
    if ( args.length >= 3 )
    {
        return Array.prototype.slice.call( args ) ;
    }

    // if the first argument is an object
    if ( isPlainObject(args[0] ) && key )
    {
        return key.split('').filter( k => args[0][ k ] !== undefined ).map( k => args[0][k] );
    }

    // else the first argument is an array
    return args?.[0].slice(0) ;
};

export default unpack ;