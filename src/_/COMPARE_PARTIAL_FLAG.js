'use strict'

/** Used to compose bitmasks for value comparisons. */
const COMPARE_PARTIAL_FLAG = 1 ;

export default COMPARE_PARTIAL_FLAG ;