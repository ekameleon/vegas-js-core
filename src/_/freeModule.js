'use strict'

import freeExports from './freeExports'

const freeModule = freeExports && typeof module === 'object' && module && !module.nodeType && module;

export default freeModule ;