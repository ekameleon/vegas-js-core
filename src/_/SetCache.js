'use strict'

import MapCache from './MapCache.js';

const HASH_UNDEFINED = '__hash_undefined__' ;

class SetCache
{
    /**
     * Creates an array cache object to store unique values.
     * @constructor
     * @param {Array} [values] The values to cache.
     */
    constructor( values)
    {
        let index = -1 ;
        const length = values === null || values === undefined ? 0 : values?.length ;

        this.__data__ = new MapCache() ;

        while (++index < length)
        {
            this.add(values[index])
        }
    }

    /**
     * Adds `value` to the array cache.
     * @memberOf SetCache
     * @alias push
     * @param {*} value The value to cache.
     * @returns {Object} Returns the cache instance.
     */
    add( value )
    {
        this.__data__.set( value , HASH_UNDEFINED ) ;
        return this
    }

    /**
     * Checks if `value` is in the array cache.
     * @memberOf SetCache
     * @param {*} value The value to search for.
     * @returns {boolean} Returns `true` if `value` is found, else `false`.
     */
    has( value )
    {
        return this.__data__.has( value ) ;
    }


    /**
     * Push `value` to the array cache.
     * @memberOf SetCache
     * @alias add
     * @param {*} value The value to cache.
     * @returns {Object} Returns the cache instance.
     */
    push( value )
    {
        this.add(value ) ;
        return this ;
    }
}

export default SetCache ;