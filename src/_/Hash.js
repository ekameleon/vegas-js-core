'use strict'

import nativeCreate from './nativeCreate.js';

export const HASH_UNDEFINED = '__hash_undefined__' ;

class Hash
{
    /**
     * Creates a new Hash instance.
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    constructor( entries)
    {
        this.clear() ;
        let index = -1 ;
        const length = entries?.length ?? 0 ;
        while (++index < length)
        {
            let entry = entries[index] ;
            this.set(entry[0], entry[1]);
        }
    }

    /**
     * Removes all key-value entries from the hash.
     * @name clear
     * @memberOf Hash
     */
    clear()
    {
        this.__data__ = nativeCreate ? nativeCreate( null ) : {} ;
        this.size = 0 ;
    }

    /**
     * Removes `key` and its value from the hash.
     * @name delete
     * @memberOf Hash
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    delete( key )
    {
        const result = this.has(key) && delete this.__data__[key] ;
        this.size -= result ? 1 : 0 ;
        return result;
    }

    /**
     * Checks if a hash value for `key` exists.
     * @name has
     * @memberOf Hash
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    has( key )
    {
        const data = this.__data__ ;
        return nativeCreate ? (data[key] !== undefined) : Object.prototype.hasOwnProperty.call(data, key) ;
    }

    /**
     * Gets the hash value for `key`.
     * @name get
     * @memberOf Hash
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    get( key )
    {
        const data = this.__data__ ;
        if ( nativeCreate )
        {
            const result = data[key];
            return result === HASH_UNDEFINED ? undefined : result ;
        }
        return Object.prototype.hasOwnProperty.call(data, key) ? data[key] : undefined;
    }

    /**
     * Sets the hash `key` to `value`.
     * @name set
     * @memberOf Hash
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the hash instance.
     */
    set( key , value )
    {
        const data = this.__data__ ;
        this.size += this.has(key) ? 0 : 1;
        data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
        return this ;
    }
}

export default Hash ;