'use strict'

/**
 * Initializes an array clone.
 * @private
 * @param {Array} array The array to clone.
 * @returns {Array} Returns the initialized clone.
 */
const initCloneArray = array =>
{
    const length = array.length ;
    const result = new array.constructor( length ) ;

    // Add properties assigned by `RegExp#exec`.
    if (length && typeof array[0] === 'string' && Object.prototype.hasOwnProperty.call(array , 'index' ) )
    {
        result.index = array.index;
        result.input = array.input;
    }

    return result;
}

export default initCloneArray;