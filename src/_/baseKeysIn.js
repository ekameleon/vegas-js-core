'use strict'

/* jshint -W089 */

import isObject     from '../isObject.js'
import isPrototype  from '../reflect/isPrototype.js'
import nativeKeysIn from './nativeKeysIn.js'

/**
 * The base implementation of `keysIn` which doesn't treat sparse arrays as dense.
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
const baseKeysIn = object =>
{
    if ( !isObject( object ) )
    {
        return nativeKeysIn(object) ;
    }

    const isProto = isPrototype(object) ;

    const result = [] ;

    for ( let key in object)
    {
        if (!(key === 'constructor' && (isProto || !Object.prototype.hasOwnProperty.call(object, key))))
        {
            result.push(key);
        }
    }
    return result;
}

export default baseKeysIn;