/* jshint -W061 */
/* jshint -W117 */

'use strict'

import freeGlobal from './freeGlobal'

/** Detect free variable `self`. */
const freeSelf = typeof self === 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
const root = freeGlobal ?? freeSelf ?? Function('return this' )() ;

export default root ;