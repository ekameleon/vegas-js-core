'use strict'

import baseGetAllKeys from './baseGetAllKeys.js';
import getSymbols     from './getSymbols.js';
import keys           from '../objects/keys.js';

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
const getAllKeys = object => baseGetAllKeys(object , keys , getSymbols ) ;

export default getAllKeys;