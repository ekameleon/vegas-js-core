'use strict'

/* jshint -W035 */

/** Used to match a single whitespace character. */
const whitespace = /\s/;

/**
 * Used by `_.trim` and `_.trimEnd` to get the index of the last non-whitespace
 * character of `string`.
 *
 * @private
 * @param {string} string The string to inspect.
 * @returns {number} Returns the index of the last non-whitespace character.
 */
const trimmedEndIndex = string =>
{
    let index = string.length;
    while ( index-- && whitespace.test(string.charAt(index)) )
    {
        // do nothing
    }
    return index;
}

export default trimmedEndIndex;