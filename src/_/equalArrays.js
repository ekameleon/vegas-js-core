'use strict'

/* jshint -W014 */

import SetCache from './SetCache.js'
import some     from '../arrays/some.js'

import COMPARE_PARTIAL_FLAG   from './COMPARE_PARTIAL_FLAG'
import COMPARE_UNORDERED_FLAG from './COMPARE_UNORDERED_FLAG'

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for partial deep comparisons.
 * @private
 * @param {*} array The array to compare.
 * @param {*} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
const equalArrays = ( array, other, bitmask, customizer , equalFunc , stack ) =>
{
    if( !Array.isArray( array ) || !Array.isArray( other ) )
    {
        return false ;
    }

    const isPartial = bitmask & COMPARE_PARTIAL_FLAG ;

    const arrLength = array.length ;
    const othLength = other.length ;

    if ( arrLength !== othLength && !( isPartial && othLength > arrLength ) )
    {
        return false ;
    }

    const stacked = stack.get( array ) ;
    if ( stacked && stack.get( other ) )
    {
        return stacked === other ;
    }

    let index = -1 ;
    let result = true ;

    const seen = ( bitmask & COMPARE_UNORDERED_FLAG ) ? new SetCache() : undefined ;

    stack.set( array , other ) ;
    stack.set( other , array ) ;

    const compareArrays = arrValue => ( othValue , othIndex ) =>
    {
        if ( !seen.has( othIndex ) && ( arrValue === othValue || equalFunc( arrValue , othValue, bitmask, customizer, stack) ) )
        {
            return seen.push( othIndex ) ;
        }
    }

    while ( ++index < arrLength ) // Ignore non-index properties.
    {
        let compared
        const arrValue = array[index] ;
        const othValue = other[index] ;

        if ( customizer )
        {
            compared = isPartial
                ? customizer( othValue, arrValue, index, other, array, stack )
                : customizer( arrValue, othValue, index, array, other, stack ) ;
        }

        if ( compared !== undefined )
        {
            if ( compared )
            {
                continue ;
            }
            result = false ;
            break ;
        }

        if ( seen )
        {
            if ( !some( other, compareArrays( arrValue ) ) )
            {
                result = false ;
                break ;
            }
        }
        else if (!( arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack) ) )
        {
            result = false ;
            break ;
        }
    }

    stack['delete']( array ) ;
    stack['delete']( other ) ;

    return result ;
}

export default equalArrays