'use strict'

import freeExports from './freeExports'
import freeModule  from './freeModule'

const moduleExports = freeModule && freeModule.exports === freeExports;

export default moduleExports ;