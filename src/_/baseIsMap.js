'use strict'

import getTag       from '../objects/getTag.js'
import isObjectLike from '../isObjectLike.js'

import { mapTag } from './tags'

/**
 * The base implementation of `isMap` without Node.js optimizations.
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a map, else `false`.
 */
const baseIsMap = value => isObjectLike( value ) && getTag( value ) === mapTag ;

export default baseIsMap ;