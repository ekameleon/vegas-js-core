'use strict'

import _Symbol from './Symbol.js'
import getRawTag from './getRawTag.js'
import toString from '../reflect/toString.js'

import {
    nullTag ,
    undefinedTag ,
}
from './tags'

/** Built-in value references. */
const symToStringTag = _Symbol?.toStringTag ?? undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
const baseGetTag = value =>
{
    if ( value === null )
    {
        return nullTag ;
    }
    else if( value === undefined )
    {
        return undefinedTag ;
    }

    return ( symToStringTag && symToStringTag in Object(value) ) ? getRawTag(value) : toString(value) ;
}

export default baseGetTag;