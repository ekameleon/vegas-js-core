'use strict'

const freeExports = typeof exports === 'object' && exports && !exports.nodeType && exports ;

export default freeExports ;