'use strict'

/**
 * The base implementation of `times` without support for iteratee shorthands or max array length checks.
 * @private
 * @param {number} count The number of times to invoke `callback`.
 * @param {Function} callback The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
const baseTimes = ( count , callback ) =>
{
    const result = Array( count );

    let index = -1 ;

    while (++index < count )
    {
        result[index] = callback(index) ;
    }

    return result;
}

export default baseTimes ;