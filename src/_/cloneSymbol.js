'use strict'

import Symbol from './Symbol.js'

/** Used to convert symbols to primitives and strings. */
const symbolProto = Symbol ? Symbol.prototype : undefined ;
const symbolValueOf = symbolProto ? symbolProto.valueOf : undefined ;

/**
 * Creates a clone of the `symbol` object.
 * @private
 * @param {Object} symbol The symbol object to clone.
 * @returns {Object} Returns the cloned symbol object.
 */
const cloneSymbol = symbol => symbolValueOf ? Object( symbolValueOf.call( symbol ) ) : {} ;

export default cloneSymbol;