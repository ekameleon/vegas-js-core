'use strict'

import castPath from '../objects/castPath.js'

import { baseGet } from '../objects/get.js'
import { baseSet } from '../objects/set.js'

/**
 * The base implementation of `pickBy`.
 * @private
 * @param {Object} object The source object.
 * @param {Array} paths The property paths to pick.
 * @param {Function} predicate The function invoked per property.
 * @returns {Object} Returns the new object.
 */
const basePickBy = ( object , paths , predicate ) =>
{
    const result = {} ;

    const length = paths.length ;

    let index = -1 ;
    while (++index < length)
    {
        const path = paths[index] ;
        const value = baseGet(object, path) ;
        if ( predicate( value , path ) )
        {
            baseSet( result , castPath(path, object) , value ) ;
        }
    }

    return result ;
}

export default basePickBy