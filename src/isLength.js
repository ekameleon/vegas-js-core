'use strict'

import MAX_SAFE_INTEGER from './numbers/MAX_SAFE_INTEGER'

/**
 * Checks if `value` is a valid array-like length.
 * @memberof core
 * @function
 * @instance
 * @since 1.0.35
 * @param {*} value - The value to evaluates.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 */
const isLength = value => typeof value === 'number' && value > -1 && value % 1 === 0 && value <= MAX_SAFE_INTEGER ;

export default isLength ;