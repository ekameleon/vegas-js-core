'use strict'

import distance     from './colors/distance'
import equals       from './colors/equals'
import fade         from './colors/fade'
import fromARGB     from './colors/fromARGB'
import getAlpha     from './colors/getAlpha'
import getBlue      from './colors/getBlue'
import getGreen     from './colors/getGreen'
import getRed       from './colors/getRed'
import hex          from './colors/hex'
import hexIsDark    from './colors/hexIsDark'
import hexIsLight   from './colors/hexIsLight'
import hexToInt     from './colors/hexToInt'
import hexToRgb     from './colors/hexToRgb'
import hexToYIQ     from './colors/hexToYIQ'
import htmlColors   from './colors/htmlColors'
import parse        from './colors/parse'
import percentToHex from './colors/percentToHex'
import rgbIsDark    from './colors/rgbIsDark'
import rgbIsLight   from './colors/rgbIsLight'
import rgbToHex     from './colors/rgbToHex'
import rgbToYIQ     from './colors/rgbToYIQ'
import isUnique     from './colors/isUnique'
import toHex        from './colors/toHex'
import uniques      from './colors/uniques'

/**
 * The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
 * @summary The {@link core.colors} package is a modular <b>JavaScript</b> library that provides extra <b>rgb color</b> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.colors
 * @memberof core
 */
const colors =
{
    distance ,
    equals   ,
    fade     ,
    fromARGB ,
    getAlpha ,
    getBlue  ,
    getGreen ,
    getRed   ,
    hex      ,
    hexIsDark ,
    hexToInt ,
    hexIsLight,
    hexToRgb ,
    hexToYIQ ,
    htmlColors ,
    isUnique ,
    parse ,
    percentToHex ,
    rgbIsDark ,
    rgbIsLight ,
    rgbToHex ,
    rgbToYIQ ,
    toHex    ,
    uniques
};

export default colors ;