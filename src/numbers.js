'use strict'

import avg from './numbers/avg'
import clip from './numbers/clip'
import isEven from './numbers/isEven'
import isOdd from './numbers/isOdd'
import leading from './numbers/leading'
import nullify from './numbers/nullify'
import nullifyZeroAndNaN from './numbers/nullifyZeroAndNaN'
import padTo2Digits from './numbers/padTo2Digits'
import padTo3Digits from './numbers/padTo3Digits'
import sma from './numbers/sma'
import sum from './numbers/sum'
import toFinite from './numbers/toFinite'
import toFixed from './numbers/toFixed'
import toInt from './numbers/toInt'
import toInteger from './numbers/toInteger'
import toNumber from './numbers/toNumber'
import toUint from './numbers/toUint'
import toIntegerOrInfinity from './numbers/toIntegerOrInfinity'
import toUnicodeNotation from './numbers/toUnicodeNotation'

/**
 * The {@link core.numbers} package is a modular <b>JavaScript</b> library that provides extra <code>Number</code> methods and implementations.
 * @summary The {@link core.numbers} package is a modular <b>JavaScript</b> library that provides extra <code>Number</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.numbers
 * @memberof core
 */
const numbers =
{
    avg,
    clip,
    isEven,
    isOdd,
    leading,
    nullify,
    nullifyZeroAndNaN,
    padTo2Digits,
    padTo3Digits,
    sma,
    sum,
    toFinite,
    toFixed,
    toInt,
    toInteger,
    toNumber,
    toUint,
    toIntegerOrInfinity,
    toUnicodeNotation,
} ;

export default numbers ;