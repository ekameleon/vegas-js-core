'use strict'

import baseGetTag from './_/baseGetTag.js'
import isObject from './isObject.js'

import { asyncTag } from './_/tags'

/**
 * Indicates if the specific value is an async function.
 * @since 1.0.44
 * @name isAsyncFunction
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to evaluates.
 * @return {boolean} Returns `true` if `value` is an async function, else `false`.
 * @example
 * console.log( isAsyncFunction( () => {} ) ) ; // false
 * console.log( isAsyncFunction( async () => {}) ) ; // true
 */
const isAsyncFunction = value =>
{
    if ( !isObject( value ) )
    {
        return false ;
    }

    const tag = baseGetTag( value ) ;

    return tag === asyncTag ;
};

export default isAsyncFunction ;