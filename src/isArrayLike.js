'use strict'

import isFunction from './isFunction.js'
import isLength   from './isLength.js'

/**
 * Checks if `value` is classified as an `Array` object. Use the native Array.isArray function (alias).
 * @name isArrayLike
 * @since 1.0.36
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to check.
 * @return {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 * console.log( isArrayLike([1, 2, 3]) ) ; // true
 * console.log( isArrayLike(document.body.children) ) ; // true
 * console.log( isArrayLike('123') ) ; // true
 */
const isArrayLike = value => value !== null && isLength( value.length ) && !isFunction( value ) ;

export default isArrayLike;