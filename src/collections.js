'use strict'

import partition from './collections/partition'
import reduce    from './collections/reduce'

/**
 * The {@link core.collections} package is a modular <b>JavaScript</b> library that provides extra <code>collections</code> methods. A collection is an array of objects.
 * @summary The {@link core.collections} package is a modular <b>JavaScript</b> library that provides extra <code>collection</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.collections
 * @memberof core
 * @since 1.0.37
 */
const collections =
{
    partition ,
    reduce ,
} ;

export default collections ;