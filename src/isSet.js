'use strict'

import baseIsSet from './_/baseIsSet.js'
import baseUnary from './_/baseUnary.js'
import nodeUtil from './_/nodeUtil.js'

const nodeIsSet = nodeUtil && nodeUtil.isSet ;

/**
 * Indicates if the specific object is a Set.
 * @name isSet
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a String.
 * @example
 * console.log( isMap( new Set() ) // true ;
 * console.log( isMap( new WeakSet() ) // false ;
 */
const isSet = nodeIsSet ? baseUnary(nodeIsSet) : baseIsSet ;

export default isSet ;