'use strict'

import isSymbol from './isSymbol.js'

/** Used as references for various `Number` constants. */
export const INFINITY = 1 / 0 ;

/**
 * Converts `value` to a string. An empty string is returned for `null` and `undefined` values. The sign of `-0` is preserved.
 * @since 1.0.37
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 * toString(null) // ''
 * toString(-0) // '-0'
 * toString([1, 2, 3]) // '1,2,3'
 */
const toString = value =>
{
    if ( value === null || value === undefined )
    {
        return '';
    }

    if (  (typeof(value) === 'string') || (value instanceof String ) )
    {
        return value;
    }

    if ( Array.isArray( value ) )
    {
        return `${value.map((other) => (other === null ? other : toString(other)))}` ; // Recursively convert values (susceptible to call stack limits).
    }

    if ( isSymbol( value ) )
    {
        return value.toString();
    }

    const result = `${value}` ;

    return result === '0' && 1 / value === -INFINITY ? '-0' : result ;
}

export default toString;