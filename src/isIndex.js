'use strict'

import MAX_SAFE_INTEGER from './numbers/MAX_SAFE_INTEGER'

const reIsUint = /^(?:0|[1-9]\d*)$/ ; // Detect unsigned integer values.

/**
 * Checks if `value` is a valid array-like index.
 * @since 1.0.36
 * @param {*} value - The value to evaluates.
 * @param {number|null} [length=MAX_SAFE_INTEGER] - The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
const isIndex = ( value , length = MAX_SAFE_INTEGER ) =>
{
    const type = typeof value ;
    return !!length && ( type === 'number' || (type !=='symbol' && reIsUint.test(value))) && (value > -1 && value % 1 === 0 && value < length) ;
};

export default isIndex ;