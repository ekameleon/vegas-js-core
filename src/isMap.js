'use strict'

import baseIsMap from './_/baseIsMap.js'
import baseUnary from './_/baseUnary.js'
import nodeUtil from './_/nodeUtil.js'

const nodeIsMap = nodeUtil && nodeUtil.isMap ;

/**
 * Indicates if the specific object is a Map.
 * @name isString
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a String.
 * @example
 * console.log( isMap( new Map() ) ) // true ;
 * console.log( isMap( new WeakMap() ) ) // false ;
 */
const isMap = nodeIsMap ? baseUnary(nodeIsMap) : baseIsMap ;

export default isMap ;