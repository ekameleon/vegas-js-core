'use strict'

/**
 * Indicates if the specific object is a Number.
 * @name isNumber
 * @memberof core
 * @function
 * @instance
 * @param {Object} object - The object to check.
 * @return {boolean} <code>true</code> if the object is a Number.
 * @example
 * console.log( isNumber(0) ) ; // true
 * console.log( isNumber(0.5) ) ; // true
 * console.log( isNumber(true) ) ; // true
 * console.log( isNumber(null) ) ; // false
 * console.log( isNumber(NaN) ) ; // true
 */
const isNumber = ( object ) => (typeof(object) === 'number') || (object instanceof Number ) ;

export default isNumber ;