'use strict'

/**
 * Checks if `value` is classified as an `Array` object. Use the native Array.isArray function (alias).
 * @name isArray
 * @since 1.0.336
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to check.
 * @return {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 * console.log( isArray([1, 2, 3]) ) ; // true
 * console.log( isArray(document.body.children) ) ; // false
 * console.log( isArray('123') ) ; // false
 */
const isArray = Array.isArray;

export default isArray ;