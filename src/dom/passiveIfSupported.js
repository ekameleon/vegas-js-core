'use strict'

/**
 * Indicates if the passive property is supported in the EventTarget.addEventListener method.
 * @name passiveIsSupported
 * @memberof core.dom
 * @instance
 * @instance
 * @return The value <code>true</code> if the device accept touch events.
 */
let supported = false ;

try
{
    window.addEventListener
    (
        'check' ,
        null ,
        Object.defineProperty( {} , 'passive' , {
            get: () =>
            {
                supported = { passive: true };
            }
        }
    )
  );
}
catch( error )
{
    // do nothing
}

const passiveIfSupported = supported ;

export default passiveIfSupported ;