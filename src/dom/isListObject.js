'use strict'

/**
 * Indicates if a value is a NodeList object.
 * @name isListObject
 * @memberof core.dom
 * @function
 * @instance
 * @param {Object} value - The value to check.
 * @return The value <code>true</code> if the passed-in value is a <code>NodeList</code> object.
 * @example
 * let list = document.getElementsByTagName("p") ;
 * for ( let i = 0; i < list.length; i++)
 * {
 *     console.log( isListObject( list[i] ) ) ;
 * }
 */
const isListObject = ( value ) =>
{
    if( !value )
    {
        return false ;
    }
    try
    {
        return value instanceof NodeList ;
    }
    catch (e)
    {
        return false ;
    }
};

export default isListObject;