'use strict'

import getTag       from './objects/getTag.js'
import isObjectLike from './isObjectLike.js'

import { weakMapTag } from './_/tags'

/**
 * Indicates if the specific object is a WeakMap instance.
 * @name isWeakMap
 * @memberof core
 * @function
 * @instance
 * @since 1.0.36
 * @param {*} value - The object to check.
 * @return {boolean} Returns `true` if `value` is a weak map, else `false`.
 * @example
 * console.log( isWeakMap( new Set() ) // false ;
 * console.log( isWeakMap( new WeakMap() ) // true ;
 */
const isWeakMap = value => isObjectLike(value) && getTag(value) === weakMapTag ;

export default isWeakMap ;