'use strict'

import baseIsEqual from './_/baseIsEqual'

/**
 * This method is like `core.isEqual` except that it accepts a `customizer` function to compare values.
 * If `customizer` returns `undefined`, comparisons are handled by the method instead.
 * The `customizer` is invoked with up to six arguments: ( objValue , othValue [, index|key, object, other, stack]).
 * @memberOf core
 * @since 1.0.37
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * const isGreeting = value => /^h(?:i|ello)$/.test(value) ;
 *
 * const customizer = ( objValue , othValue )  =>
 * {
 *     if( isGreeting(objValue) && isGreeting(othValue) )
 *     {
 *         return true ;
 *     }
 *     return undefined ; // if not equals returns undefined !important
 * };
 *
 * const array = [ 'hello' , 'goodbye' ] ;
 * const other = [ 'hi'    , 'goodbye' ] ;
 *
 * console.log( isEqualWith(array, other, customizer ) ) ; // true
 */
const isEqualWith = ( value , other , customizer ) =>
{
    customizer = typeof customizer === 'function' ? customizer : undefined;
    const result = customizer ? customizer( value, other ) : undefined ;
    return result === undefined ? baseIsEqual( value, other, undefined, customizer ) : !!result;
}

export default isEqualWith ;