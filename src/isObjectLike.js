'use strict'

/**
 * Indicates if an value is a float number.
 * @name isObjectLike
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to evaluates.
 * @return <code>true</code> if the passed-in value is object-like, else `false`.
 * @example
 * console.log( isObjectLike({}) ) ; // true
 * console.log( isObjectLike([1, 2, 3]) ) ; // true
 * console.log( isObjectLike(Function) ) ; // false
 * console.log( isObjectLike(null) ) ; // false
 */
const isObjectLike = value => typeof value === 'object' && value !== null && value !== undefined ;

export default isObjectLike ;