'use strict'

/**
 * Returns `true` if the given value is not null.
 * @name isNotNull
 * @memberof core
 * @since 1.0.16
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given value is not null.
 * @example
 * console.log(isNotNull(null)); // false
 *
 * console.log(isNotNull([])); // true
 * console.log(isNotNull({})); // true
 *
 * console.log(isNotNull('')); // true
 * console.log(isNotNull(0)); // true
 * console.log(isNotNull(1)); // true
 * console.log(isNotNull(true)); // true
 * console.log(isNotNull(false)); // true
 */
const isNotNull = value => value !== null ;

export default isNotNull ;
