'use strict'

import isArrayLike  from './isArrayLike.js';
import isObjectLike from './isObjectLike.js';

/**
 * This method is like `isArrayLike` except that it also checks if `value` is an Object.
 * @name isArrayLikeObject
 * @since 1.0.39
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to check.
 * @return {boolean} Returns `true` if `value` is an array-like object, else `false`.
 * @example
 * console.log( isArrayLike([1, 2, 3]) ) ; // true
 * console.log( isArrayLike(document.body.children) ) ; // true
 * console.log( isArrayLike('123') ) ; // false
 * console.log( isArrayLike(Function) ) ; // false
 */
const isArrayLikeObject = value => isObjectLike( value ) && isArrayLike( value ) ;

export default isArrayLikeObject ;