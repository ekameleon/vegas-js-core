'use strict'

import contains    from './arrays/contains'
import distinct    from './arrays/distinct'
import each        from './arrays/each'
import empty       from './arrays/empty'
import head        from './arrays/head'
import inBetween   from './arrays/inBetween'
import initialize  from './arrays/initialize'
import last        from './arrays/last'
import map         from './arrays/map'
import move        from './arrays/move'
import notEmpty    from './arrays/notEmpty'
import nullify     from './arrays/nullify'
import partition   from './arrays/partition'
import pierce      from './arrays/pierce'
import range       from './arrays/range'
import repeat      from './arrays/repeat'
import rotate      from './arrays/rotate'
import shuffle     from './arrays/shuffle'
import sortOn      from './arrays/sortOn'
import spliceInto  from './arrays/spliceInto'
import some        from './arrays/some'
import stub        from './arrays/stub'
import swap        from './arrays/swap'
import tail        from './arrays/tail'
import take        from './arrays/take'
import takeRight   from './arrays/takeRight'
import toLength    from './arrays/toLength'
import unique      from './arrays/unique'

/**
 * The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @summary The {@link core.arrays} package is a modular <b>JavaScript</b> library that provides extra <code>Array</code> methods.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.arrays
 * @memberof core
 * @since 1.0.0
 */
const arrays =
{
    contains ,
    distinct ,
    each ,
    empty ,
    head ,
    inBetween ,
    initialize ,
    last ,
    map ,
    move ,
    notEmpty ,
    nullify ,
    partition ,
    pierce ,
    range ,
    repeat ,
    rotate ,
    shuffle ,
    sortOn ,
    spliceInto ,
    some ,
    stub ,
    swap ,
    tail ,
    take ,
    takeRight ,
    toLength ,
    unique ,
} ;

export default arrays ;