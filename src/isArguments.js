'use strict'

import getTag       from './objects/getTag.js'
import isObjectLike from './isObjectLike.js'

import { argsTag } from './_/tags'

/**
 * Checks if `value` is likely an `arguments` object.
 * @name isArguments
 * @since 1.0.35
 * @memberof core
 * @function
 * @instance
 * @param {*} value - The value to check.
 * @return {boolean} `true` if `value` is an `arguments` object, else `false`.
 * @example
 * console.log( isArguments(function() { return arguments }() ) ) ; // true
 * console.log( isArguments( [1, 2, 3] ) ) ; // true
 */
const isArguments = value => isObjectLike(value) && getTag(value) === argsTag ;

export default isArguments ;