'use strict' ;

/**
 * A basic <code>trace()</code> function based on the console.log method.
 * @name trace
 * @memberOf core
 * @instance
 * @function
 * @example
 * trace( 'hello world' ) ;
 */
const trace = ( context ) =>
{
    if( console )
    {
        console.log( context ) ;
    }
};

export default trace ;