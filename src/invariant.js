'use strict'

import format from './strings/fastformat'

const defaultMessage = 'Invariant failed' ;

/**
 * This global flag set if the method is invoked in production mode.
 * ```js
 * isProduction = process.env.NODE_ENV === 'production' ;
 * ```
 * @type {boolean}
 */
let isProduction = false ;

/**
 * Sets the internal production indicator.
 * @function
 * @instance
 * @memberof core
 * @since 1.0.43
 * @param {bool|function} condition - Defines the internal isProduction flag with a boolean or a function to invoke.
 */
export const setIsProduction = condition =>
{
    isProduction = typeof condition === 'function' ? condition() : condition ;
}

/**
 * Used to assert that the `condition` is [truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy). This function `throw` an `Error` if the `condition` is [falsey](https://developer.mozilla.org/en-US/docs/Glossary/Falsy).
 * @name invariant
 * @memberof core
 * @function
 * @instance
 * @since 1.0.43
 * @param {*} condition - The object to evaluates.
 * @param {string|function} message - Can provide a string, or a function that returns a string for cases where the message takes a fair amount of effort to compute.
 * @param {...*} args - The optional values to passed-in to format the message.
 * @example
 * ```js
 * const user = { name: 'Bob' };
 * invariant( user , 'Expected value to be a valid user' ) ;
 * ```
 */
const invariant = ( condition , message, ...args ) =>
{
    if ( condition )
    {
        return;
    }

    if ( isProduction )
    {
        throw new Error( defaultMessage ) ;
    }

    const provided = typeof message === 'function' ? message( ...args ) : format( message , ...args ) ;

    throw new Error( provided ?? defaultMessage ) ;
}

export default invariant ;