'use strict'

import arrayEachRight from './_/arrayEachRight.js'
import baseEachRight  from './_/baseEachRight.js'

/**
 * This method is like `forEach` except that it iterates over elements of collection` from right to left.
 * @since 1.0.37
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 * @example
 * forEachRight( [1, 2] , value => console.log(value) ) ;
 */
const forEachRight = ( collection , iteratee ) =>
{
    const func = Array.isArray( collection ) ? arrayEachRight : baseEachRight ;
    return func( collection , iteratee ) ;
}

export default forEachRight ;