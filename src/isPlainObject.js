'use strict'

import isObject from './isObject'

import { objectTag } from './_/tags'

const isPureObject = value => isObject(value) === true && Object.prototype.toString.call(value) === objectTag ;

/**
 * Returns `true` if the given object was created by the Object constructor.
 * @name isPlainObject
 * @memberof core
 * @since 1.0.14
 * @function
 * @instance
 * @param {*} value - The object to evaluate.
 * @return {boolean} `true` if the given object was created by the Object constructor.
 * @example
 * console.log(isPlainObject({}); // true
 * console.log(isPlainObject([]); // true
 * console.log(isPlainObject(Object.create({})); // true
 * console.log(isPlainObject(Object.create(Object.prototype)); // true
 */
const isPlainObject = ( value ) =>
{
    let ctor, proto ;
    
    if( isPureObject( value ) === false )
    {
        return false ;
    }
    
    ctor = value.constructor;
    if( typeof ctor !== 'function' )
    {
        return false ;
    }
    
    proto = ctor.prototype;
    if( isPureObject(proto) === false || proto.hasOwnProperty('isPrototypeOf') === false )
    {
        return false;
    }
    
    return true;
};

export default isPlainObject ;