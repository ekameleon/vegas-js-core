'use strict'

import backIn         from './easings/backIn'
import backInOut      from './easings/backInOut'
import backOut        from './easings/backOut'
import bounceIn       from './easings/bounceIn'
import bounceInOut    from './easings/bounceInOut'
import bounceOut      from './easings/bounceOut'
import circularIn     from './easings/circularIn'
import circularInOut  from './easings/circularInOut'
import circularOut    from './easings/circularOut'
import cubicIn        from './easings/cubicIn'
import cubicInOut     from './easings/cubicInOut'
import cubicOut       from './easings/cubicOut'
import elasticIn      from './easings/elasticIn'
import elasticInOut   from './easings/elasticInOut'
import elasticOut     from './easings/elasticOut'
import expoIn         from './easings/expoIn'
import expoInOut      from './easings/expoInOut'
import expoOut        from './easings/expoOut'
import linear         from './easings/linear'
import quarticIn      from './easings/quarticIn'
import quarticInOut   from './easings/quarticInOut'
import quarticOut     from './easings/quarticOut'
import quinticIn      from './easings/quinticIn'
import quinticInOut   from './easings/quinticInOut'
import quinticOut     from './easings/quinticOut'
import regularIn      from './easings/regularIn'
import regularInOut   from './easings/regularInOut'
import regularOut     from './easings/regularOut'
import sineIn         from './easings/sineIn'
import sineInOut      from './easings/sineInOut'
import sineOut        from './easings/sineOut'

/**
 * The {@link system.transitions} package use the {@link core.easings} library who contains all the easing functions to create the specific <b>tweening</b> effects.
 * <p>These easings functions provide different flavors of math-based motion under a consistent API.</p>
 *
 * |  easing   |                         description                         |  in  | out  | inout  |
 * |:--------: |:----------------------------------------------------------: |:---: |:---: |:-----: |
 * |  linear   | simple linear tweening : no easing, no acceleration         |  -   |  -   |   -    |
 * |   back    | back easing : overshooting cubic easing: (s+1)*t^3 - s*t^2  | yes  | yes  |  yes   |
 * |  bounce   | bounce easing : exponentially decaying parabolic bounce     | yes  | yes  |  yes   |
 * | circular  | circular easing : sqrt(1-t^2)                               | yes  | yes  |  yes   |
 * |   cubic   | cubic easing : t^3                                          | yes  | yes  |  yes   |
 * |  elastic  | elastic easing : exponentially decaying sine wave           | yes  | yes  |  yes   |
 * |   expo    | exponential easing : 2^t                                    | yes  | yes  |  yes   |
 * |   quad    | quadratic easing : t^2                                      | yes  | yes  | yes    |
 * |  quartic  | quartic easing : t^4                                        | yes  | yes  |  yes   |
 * |  quintic  | quintic easing : t^5                                        | yes  | yes  |  yes   |
 * |  regular  | regular easing                                              | yes  | yes  |  yes   |
 * |   sine    | sinusoidal easing : sin(t)                                  | yes  | yes  |  yes   |
 * @summary The {@link core.easings} library contains all the easing functions to create the specific tweening effects.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @namespace core.easings
 * @memberof core
 * @since 1.0.0
 */
const easings =
{
    backIn        ,
    backInOut     ,
    backOut       ,
    bounceIn      ,
    bounceInOut   ,
    bounceOut     ,
    circularIn    ,
    circularInOut ,
    circularOut   ,
    cubicIn       ,
    cubicInOut    ,
    cubicOut      ,
    elasticIn     ,
    elasticInOut  ,
    elasticOut    ,
    expoIn        ,
    expoInOut     ,
    expoOut       ,
    linear        ,
    quarticIn     ,
    quarticInOut  ,
    quarticOut    ,
    quinticIn     ,
    quinticInOut  ,
    quinticOut    ,
    regularIn     ,
    regularInOut  ,
    regularOut    ,
    sineIn        ,
    sineInOut     ,
    sineOut
} ;

export default easings ;