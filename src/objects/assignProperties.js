'use strict'

import clean from './clean'

/**
 * Assigns properties to a new object with a specific object definition.
 * @version 1.0.26
 * @since 1.0.26
 * @name assignProperties
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} definition - The object to populate a new object.
 * @param {Array} [properties=null] - The collection of all the properties to keep in the new object.
 * @param {boolean} [cleanable=false] - Indicates if the final object must remove all undefined and null properties.
 * @example
 * console.log( assignProperties( { a:1, b:2, c:3 }, ["a"] ) ) // { a:1 }
 * console.log( assignProperties( { a:1, b:2, c:3 }, ["a","c"] ) ) // { a:1 , c:3 }
 * console.log( assignProperties( { a:1, b:2, c:null }, ["a","c"] , true ) ) // { a:1 }
 */
export const assignProperties = ( definition , properties = null , cleanable = false ) =>
{
    let object = {} ;
    if( properties instanceof Array && properties.length > 0 )
    {
        for( const name of properties )
        {
            if( definition.hasOwnProperty( name ) )
            {
                object[ name ] = definition[ name ] ;
            }
        }
    }

    if( cleanable )
    {
        object = clean( object );
    }

    return object ;
}

export default assignProperties ;