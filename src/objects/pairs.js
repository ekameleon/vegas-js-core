'use strict'

import keys from './keys';

/**
 * Convert an object into a array list of [key, value] pairs.
 * @static
 * @name pairs
 * @since 1.0.42
 * @memberOf core.objects
 * @param {Object} object The object to query.
 * @returns {Array} Returns the list of [key, value] pairs.
 * @example
 * console.log( pairs({ one: 1, two: 2, three: 3 } ) ) ; //  [["one", 1], ["two", 2], ["three", 3]]
 */
const pairs = object =>
{
    const _keys  = keys( object ) ;
    const length = _keys.length ;
    const pairs  = Array( length ) ;
    for ( let i  = 0; i < length; i++)
    {
        pairs[i] = [ _keys[i] , object[ _keys[i] ] ] ;
    }
    return pairs ;
}

export default pairs;