'use strict'

import arrayLikeKeys from '../_/arrayLikeKeys.js'
import baseKeysIn    from '../_/baseKeysIn.js'
import isArrayLike   from '../isArrayLike.js'

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 * **Note:** Non-object values are coerced to objects.
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @memberof core.objects
 * @since 1.0.36
 * @static
 * @example
 * function Foo()
 * {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * keysIn(new Foo()); // ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
const keysIn = object => isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object) ;

export default keysIn;