'use strict'

import assignValue from './assignValue'
import baseAssignValue from '../_/baseAssignValue'

/**
 * Copies the properties of `source` to `object`.
 * @name copy
 * @memberof core.objects
 * @since 1.0.34
 * @function
 * @instance
 * @param {Object} source The array to copy values from.
 * @param {Array} props The property identifiers to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @param {Function} [customizer] The function to customize copied values.
 * @returns {Object} Returns `object`.
 */
const copy = ( source, props , object= null , customizer = null ) =>
{
    const isNew = !object;

    object = object ?? {} ;

    const length = props?.length ?? 0 ;

    let index = -1 ;

    while (++index < length)
    {
        let key = props[index];

        let newValue = customizer ? customizer(object[key], source[key], key, object, source) : undefined ;

        if ( newValue === undefined )
        {
            newValue = source[key] ;
        }

        if ( isNew )
        {
            baseAssignValue( object , key, newValue );
        }
        else
        {
            assignValue( object , key , newValue ) ;
        }
    }

    return object;
}

export default copy ;