'use strict'

import assignValue from './assignValue'
import castPath    from './castPath'
import isIndex     from '../isIndex'
import isObject    from '../isObject'
import toKey       from './toKey'

/**
 * Sets the value at `path` of `object`. If a portion of `path` doesn't exist, it's created.
 * Arrays are created for missing index properties while objects are created for all other missing properties.
 * Use `setWith` to customize `path` creation.
 * **Note:** This method mutates `object`.
 * @since 1.0.10
 * @name get
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The object to change.
 * @param {Array|string} path - The path of the property to set.
 * @param {*} value - The value to set.
 * @returns {Object} Returns the `object` reference.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * set(object, 'a[0].b.c', 4)
 * console.log(object.a[0].b.c) ; // 4
 *
 * set(object, ['x', '0', 'y', 'z'], 5)
 * console.log(object.x[0].y.z); // 5
 */
const set = ( object , path , value ) => object === null || object === undefined ? object : baseSet( object , path , value ) ;

export default set ;

/**
 * The base implementation of `set`.
 * @private
 * @param {Object} object - The object to change.
 * @param {Array|string} path - The path of the property to set.
 * @param {*} value - The value to set.
 * @param {Function} [customizer=null] The function to customize path creation.
 * @returns {Object} Returns the `object` reference.
 */
export const baseSet = ( object, path, value , customizer = null ) =>
{
    if (!isObject(object))
    {
        return object ;
    }

    path = castPath( path , object ) ;

    const length = path.length;
    const lastIndex = length - 1;

    let index = -1;
    let nested = object;

    while ( nested !== null && ++index < length )
    {
        const key = toKey( path[index] ) ;
        let newValue = value;
        if ( index !== lastIndex )
        {
            const objValue = nested[key];
            newValue = customizer ? customizer(objValue, key, nested) : undefined;
            if (newValue === undefined)
            {
                if( isObject(objValue) )
                {
                    newValue = objValue ;
                }
                else
                {
                    newValue = isIndex( path[index + 1] ) ? [] : {} ;
                }
            }
        }

        assignValue(nested, key, newValue);

        nested = nested[key];
    }
    return object
};