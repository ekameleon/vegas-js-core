'use strict'

/**
 * Creates an object that excludes the properties corresponding to the specified keys.
 * @since 1.0.42
 * @name omit
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The source object.
 * @param {string[]} [keys] - An array of properties to be omitted from the object.
 * @returns {Object} Returns the new object.
 * @returns {*} Returns the resolved value.
 * @example
 * let source = { a:1 , b:2 , c:3 } ;
 * console.log( omit( source , ['a','c'] ) ) ; // { b:2 }
 */
const omit = ( object, keys ) =>
{
    const result = { ...object } ;
    const length = keys.length ;
    for ( let i = 0; i < length; i++ )
    {
        delete result[ keys[i] ] ;
    }
    return result ;
}

export default omit ;