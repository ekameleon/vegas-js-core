'use strict'

/**
 * Returns all the public members of an object, either by key or by value.
 * @name members
 * @memberof core.objects
 * @function
 * @instance
 * @param {object} object The target object to enumerate.
 * @param {boolean} [byValue=false] The optional flag indicates if the function return an Array of strings (keys) or of values.
 * @return {Array} An array containing all the string key names or values (if the #byValue argument is true). The method returns null if no members are finding.
 * @example
 * var o = { a : 5 , b : 6 } ;
 * let( dump( members( o ) ) ) ; // [a,b]
 * let( dump( members( o , true ) ) ) ; // [5,6]
 */
const members = ( object , byValue = false ) => byValue ? Object.values( object ) : Object.keys( object ) ;

export default members ;