'use strict'

/**
 * Creates an helper to rename a property in the objects.
 * @since 1.0.13
 * @name renameProperty
 * @memberof core.objects
 * @function
 * @instance
 * @param {string} prop - The name of the property to rename.
 * @param {string} name - The new name of the property
 * @return {Function} A new function used to rename a property in a specific object reference.
 * @example
 * const user = { id:1, name: 'John Doe', password: 'PWD' } ;
 * const renameID = renameProperty('id','ID');
 * console.log( renameID(user) ) ; // { ID:1, name: 'John Doe' }
 */
const renameProperty = ( prop , name ) => ({ [prop] : value, ...rest }) => ({ [name] : value , ...rest }) ;

export default renameProperty ;