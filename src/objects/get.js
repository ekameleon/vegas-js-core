'use strict'

import castPath from './castPath'
import toKey    from './toKey'

/**
 * Gets the value at `path` of `object`.
 * If the resolved value is `undefined`, the `defaultValue` is returned in its place.
 * @since 1.0.10
 * @name get
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object The object to query.
 * @param {Array|string} path - The path of the property to get.
 * @param {*} [defaultValue=null] - The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * get(object, 'a[0].b.c')
 * // => 3
 *
 * get(object, ['a', '0', 'b', 'c'])
 * // => 3
 *
 * get(object, 'a.b.c', 'default')
 * // => 'default'
 */
const get = ( object , path , defaultValue = null ) =>
{
    const result = object === null ? undefined : baseGet(object, path) ;
    return result === undefined ? defaultValue : result
};

export default get ;

/**
 * The base implementation of `get` without support for default values.
 * @private
 * @param {Object} object - The object to query.
 * @param {Array|string} path - The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
export const baseGet = ( object , path ) =>
{
    path = castPath(path, object) ;

    let index = 0 ;
    const length = path.length ;

    while ( object !== null && object !== undefined && index < length )
    {
        object = object[ toKey( path[index++] ) ] ;
    }

    return (index && index === length) ? object : undefined ;
};