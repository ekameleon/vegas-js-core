'use strict'

/**
 * Checks if `path` is a direct or inherited property of `object`.
 * @since 1.0.38
 * @name hasIn
 * @memberof core.object
 * @function
 * @instance
 * @param {Object} object The object to query.
 * @param {string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 * @example
 * const object = create({ 'a': create({ 'b': 2 }) })
 * console.log( hasIn(object, 'a') ) ; // true
 * console.log( hasIn(object, 'b') ) ; // false
 */
const hasIn = ( object, key) => object !== null && object !== undefined && key in Object(object);

export default hasIn ;