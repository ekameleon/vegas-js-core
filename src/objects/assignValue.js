'use strict'

import eq from '../eq'
import baseAssignValue from '../_/baseAssignValue'

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent.
 * @private
 * @param {Object} object - The object to populate.
 * @param {string} key - The key of the property to assign.
 * @param {*} value - The value to assign.
 */
const assignValue = ( object , key, value ) =>
{
    const objValue = object[key] ;
    if ( !(Object.prototype.hasOwnProperty.call(object, key) && eq(objValue, value)) || (value === undefined && !(key in object) ) )
    {
        baseAssignValue( object , key , value );
    }
};

export default assignValue ;