'use strict'

import basePick from '../_/basePick.js'

/**
 * Creates an object composed of the picked `object` properties.
 * If the resolved value is `undefined`, the `defaultValue` is returned in its place.
 * @since 1.0.38
 * @name pick
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The source object.
 * @param {string[]} [paths] - The property paths to pick.
 * @returns {Object} Returns the new object.
 * @returns {*} Returns the resolved value.
 * @example
 * let source = { a:1 , b:2 , c:3 } ;
 * console.log( pick( source , ['a','c'] ) ) ; // { a:1 , c:3 }
 */
const pick = ( object, paths ) => object === null || object === undefined ? {} : basePick( object , paths );

export default pick ;