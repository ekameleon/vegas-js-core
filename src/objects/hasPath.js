'use strict'

import castPath    from './castPath.js'
import isArguments from '../isArguments.js'
import isIndex     from '../isIndex.js'
import isLength    from '../isLength.js'
import toKey       from './toKey.js'

import has from './has'

export const factory = ( validator = has ) => ( object , path ) =>
{
    path = castPath( path , object ) ;

    let key ;
    let { length } = path ;
    let result = false ;

    let index  = -1 ;
    while (++index < length)
    {
        key = toKey(path[index]);
        result = validator( object, key ) ;
        if (!result )
        {
            break;
        }
        object = object[key] ;
    }

    if ( result || ( ++index !== length ) )
    {
        return result ;
    }

    length = ( object === null || object === undefined ) ? 0 : object.length ;

    return (
        !!length &&
        isLength(length) &&
        isIndex(key, length) &&
        (Array.isArray(object) || isArguments(object))
    );
}

/**
 * Checks if `path` is a direct property of `object`
 * @name hasPath
 * @memberof core.objects
 * @since 1.0.38
 * @function
 * @instance
 * @example
 * const object = { 'a': { 'b': 2 } } ;
 * console.log( hasPath(object , 'a.b') ) ; // true
 * console.log( hasPath(object , ['a', 'b']) ) ; // true
 */
const hasPath = factory()  ;

export default hasPath ;