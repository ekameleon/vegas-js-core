'use strict'

import { baseGet } from './get.js'
import { baseSet } from './set.js'

/**
 * This method is like `set` except that it accepts `updater` to produce the
 * value to set. Use `updateWith` to customize `path` creation. The `updater`
 * is invoked with one argument: (value).
 *
 * **Note:** This method mutates `object`.
 *
 * @since 1.0.35
 * @category Object
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {Function} updater The function to produce the updated value.
 * @returns {Object} Returns `object`.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * update( object, 'a[0].b.c', n => n * n ) ;
 * console.log( object.a[0].b.c ) ; // 9
 *
 * update(object, 'x[0].y.z', n => n ? n + 1 : 0 ) ;
 * console.log(object.x[0].y.z ) ; // 0
 */
const update = ( object , path , updater ) =>
{
    return object === null ? object : baseUpdate( object , path , updater ) ;
}

export default update ;

/**
 * The base implementation of `update`.
 * @private
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to update.
 * @param {Function} updater The function to produce the updated value.
 * @param {Function} [customizer] The function to customize path creation.
 * @returns {Object} Returns `object`.
 */
const baseUpdate = ( object , path , updater , customizer ) =>
{
    return baseSet( object , path , updater( baseGet( object , path ) ) , customizer ) ;
}