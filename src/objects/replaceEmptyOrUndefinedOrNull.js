'use strict'

import emptyString from '../strings/empty'
import emptyArray  from '../arrays/empty'
import emptyObject from '../isEmpty'

const removeEmpty = obj => Object.fromEntries(Object.entries(obj).filter(([_, v]) => v != null));

export const replaceEmptyOrUndefinedOrNull = ( key , value ) =>
{
    if
    (
        value === null ||
        value === undefined ||
        emptyString( value ) ||
        emptyArray( value )
    )
    {
        return undefined;
    }

    if( typeof(value) === 'object' )
    {
        value = removeEmpty( value ) ;
        if( emptyObject(value) )
        {
            return undefined ;
        }
    }

    return value ;
};

export default replaceEmptyOrUndefinedOrNull ;