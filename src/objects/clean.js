'use strict'

/**
 * Remove all null or undefined properties.
 * This function is the default replacer of the clean method.
 * @param key
 * @param value
 * @returns {undefined|*}
 */
export const replaceUndefinedOrNull = ( key , value ) =>
{
    if ( value === null || value === undefined )
    {
        return undefined;
    }
    return value ;
};

/**
 * Copy the passed-in object and remove all undefined and null properties.
 * @name clean
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The source to copy and clean.
 * @param {Function} [replacer=null] - The optional replacer function to
 * @return The copy reference.
 * @example
 * let object = { a:1 , b:null , c:3 } ;
 *
 * console.log( clean( object ) ) ; // { a:1 , c:3 }
 *
 * const replacer = ( key , value ) =>
 * {
 *     if ( key === 'c' || value === null || value === undefined )
 *     {
 *         return undefined;
 *     }
 *     return value ;
 * };
 *
 * console.log( clean( object , replacer ) ) ; // { a:1 }
 */
export default function clean( object , replacer = null )
{
    if( object )
    {
        return JSON.parse( JSON.stringify( object , replacer ?? replaceUndefinedOrNull ) );
    }
    return null ;
}