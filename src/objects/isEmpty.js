'use strict'

/**
 * Indicates if the object is empty and not contains enumerable properties.
 * @version 1.0.25
 * @since 1.0.25
 * @name isEmpty
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The object to evaluate.
 * @param {boolean} [checkAll=true] - Indicates if the function test all properties (enumerable or not) or only the enumerable properties.
 * @return {Boolean} True if the object is empty.
 * @example
 * console.log( isEmpty( {} ) ) ; // true
 * console.log( isEmpty( { name: 'John Doe' } ) ) ; // false
 */
const isEmpty = ( object , checkAll = true ) => checkAll ? ( Object.getOwnPropertyNames( object ).length === 0 )
                                                         : ( Object.keys( object ).length === 0 ) ;

export default isEmpty ;