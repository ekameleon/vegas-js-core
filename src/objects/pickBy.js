'use strict'

import basePickBy   from '../_/basePickBy.js'
import getAllKeysIn from '../_/getAllKeysIn.js'

/**
 * Creates an object composed of the `object` properties `predicate` returns truthy for. The predicate is invoked with two arguments: (value, key).
 * @since 1.0.38
 * @name pickBy
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The source object.
 * @param {Function} callback The function invoked per property.
 * @returns {Object} Returns the new object.
 * @returns {*} Returns the resolved value.
 * @example
 * let source = { a:1 , b:"hello", c:3 } ;
 * console.log( pickBy( source , isNumber ) ) ; // { a:1 , c:3 }
 */
const pickBy = ( object, callback ) =>
{
    if (object === null || object === undefined )
    {
        return {} ;
    }

    const props = getAllKeysIn(object).map( prop => [prop] ) ;
    return basePickBy( object , props, ( value , path) => callback( value , path[0] ) );
}

export default pickBy ;