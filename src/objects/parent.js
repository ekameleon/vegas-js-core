'use strict'

import { baseGet } from './get'

/**
 * Gets the parent value at `path` of `object`.
 * @private
 * @param {Object} object - The object to query.
 * @param {Array} path - The array path to get the parent value of.
 * @returns {*} Returns the parent value
 */
const func = ( object , path ) =>
{
    return path.length < 2 ? object : baseGet( object , path.slice( 0, -1 ) ) ;
};

export default func ;