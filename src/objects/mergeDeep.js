'use strict'

import baseMerge      from '../_/baseMerge.js'
import createAssigner from '../_/createAssigner.js'

/**
 * Merging enumerable properties from a specific Object to a target Object.
 * @name merge
 * @memberof core.objects
 * @since 1.0.39
 * @function
 * @instance
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 * const object = { a: [ { b: 2 }, { d: 4 }] } ;
 * const other = { a: [{ c: 3 }, { e: 5 }] } ;
 * merge(object, other) ;
 * console.log( dump( object ) ) ; // { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
 */
const mergeDeep = createAssigner(( object , source , srcIndex ) =>
{
    baseMerge( object , source , srcIndex ) ;
});

export default mergeDeep ;