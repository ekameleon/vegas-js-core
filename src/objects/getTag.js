'use strict'

import {
    nullTag ,
    undefinedTag ,
}
from '../_/tags'

/**
 * Gets the `toStringTag` value of the specific element.
 * @since 1.0.10
 * @name getTag
 * @memberof core.objects
 * @function
 * @instance
 * @param {*} object - The object to evaluate.
 * @return {string} Returns the `toStringTag` value of the object.
 * @example
 * console.log( getTag([2,3] ) ; // [object Array]
 */
const getTag = object =>
{
    if ( object === null )
    {
        return nullTag ;
    }
    else if( object === undefined )
    {
        return undefinedTag ;
    }
    return Object.prototype.toString.call( object ) ;
};

export default getTag ;