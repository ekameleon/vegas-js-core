'use strict'

import isKey from './isKey'
import stringToPath from '../strings/stringToPath'

/**
 * Casts the passed-in `value` to a path array if it's not one.
 * @private
 * @param {*} value - The value to inspect.
 * @param {Object} [object] - The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
const castPath = ( value , object ) =>
{
    if ( value instanceof Array )
    {
        return value ;
    }
    return isKey( value , object ) ? [ value ] : stringToPath( value ) ;
};

export default castPath ;