'use strict'

import arrayLikeKeys from '../_/arrayLikeKeys.js';
import baseKeys      from '../_/baseKeys.js';
import isArrayLike   from '../isArrayLike.js';

/**
 * Creates an array of the own enumerable property names of `object`.
 * @static
 * @since 1.0.36
 * @memberOf core.objects
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 * function Foo()
 * {
 *   this.a = 1;
 *   this.b = 2;
 * }
 * Foo.prototype.c = 3;
 *
 * keys( new Foo() ); // ['a', 'b'] (iteration order is not guaranteed)
 * keys('hi'); // ['0', '1']
 */
const keys = object => isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object) ;

export default keys;