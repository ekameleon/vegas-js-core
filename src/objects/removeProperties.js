'use strict'

/**
 * Remove the properties of the passed-in object.
 * @version 1.0.26
 * @since 1.0.26
 * @name removeProperties
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The object to transform.
 * @param {Array} [properties=null] - The collection of all the properties to remove in the object.
 * @example
 * console.log( removeProperties( { a:1, b:2, c:3 }, ["a"] ) ) // { b:2, c:3 }
 * console.log( removeProperties( { a:1, b:2, c:3 }, ["a","b"] ) ) // { c:3 }
 * console.log( removeProperties( { a:1, b:2, c:3 }, ["a","b","c"] ) ) // {}
 */
export const removeProperties = ( object , properties = null ) =>
{
    if( properties instanceof Array && properties.length > 0 )
    {
        for( const name of properties )
        {
            if( object.hasOwnProperty(name) )
            {
                delete object[name] ;
            }
        }
    }
    return object ;
}

export default removeProperties ;