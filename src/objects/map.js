'use strict'

/**
 * A function to execute for each element in the object. Its return value is added as a single element in the new object.
 * @callback objectMapCallback
 * @param {*} value - The value of the current property.
 * @param {string} key - The current property name.
 * @param {Object} object - The object reference.
 * @param {number} index - The index of the property in the map process.
 */

/**
 * Creates an object  by running each element of `object` through `callback`. The callback function is invoked with three arguments: (value, key , object, index).
 * @name map
 * @memberof core.objects
 * @function
 * @instance
 * @since 1.0.40
 * @param {Object} object - The object reference to transform.
 * @param {objectMapCallback} callback - A function to execute for each element in the object.
 * @return {Object} Returns the new mapped object.
 * @example
 * const square =  ;
 * console.log( map({ a : 2 , b : 4 }), n => n * n )  ); // { a : 4 , b : 16 }
 */
const map = ( object , callback )  => Object.fromEntries( Object.entries( object ).map( ( [ k, v ] , i) => [k, callback(v, k, object , i )] ) ) ;

export default map ;