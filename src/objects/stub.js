'use strict'

/**
 * This function return an empty object.
 * @name stub
 * @memberof core.objects
 * @function
 * @instance
 * @since 1.0.36
 * @returns {object}
 * @example
 * const object = stub() ;
 * console.log( object ) ; // {}
 */
const stub = () => ({}) ;

export default stub ;