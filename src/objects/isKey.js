'use strict'

import isSymbol from '../isSymbol'

const reIsDeepProp  = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/ ;
const reIsPlainProp = /^\w*$/ ;

/**
 * Checks if `value` is a property name and not a property path.
 * @name isKey
 * @memberof core.objects
 * @function
 * @instance
 * @param {*} value - The value to check.
 * @param {*} object - The object to queries key on.
 * @return {boolean} Returns `true` if `value` is a property name, else `false`.
 */
const isKey = ( value, object ) =>
{
    if ( Array.isArray(value) )
    {
        return false ;
    }
    
    const type = typeof value ;
    
    if (type === 'number' || type === 'boolean' || value === null || isSymbol(value) )
    {
        return true
    }
  
    return reIsPlainProp.test(value) || !reIsDeepProp.test(value) || ( object !== null && value in Object( object ) ) ;
};

export default isKey ;