'use strict'

import { factory } from './hasPath'

import hasIn from './hasIn'

/**
 * Checks if `path` is a direct property of `object`.
 * @name hasPathIn
 * @memberof core.objects
 * @since 1.0.38
 * @function
 * @instance
 * @example
 * const object = { 'a': { 'b': 2 } } ;
 * const other = create({ 'a': create({ 'b': 2 }) })
 * console.log( hasPathIn(object , 'a.b') ) ; // true
 * console.log( hasPathIn(object , ['a', 'b']) ) ; // true
 */
const hasPathIn = factory( hasIn ) ;

export default hasPathIn ;