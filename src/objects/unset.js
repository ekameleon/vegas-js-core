'use strict'

import castPath from './castPath'
import last     from '../arrays/last'
import parent   from './parent'
import toKey    from './toKey'

/**
 * Removes the property at `path` of `object`.
 * **Note:** This method mutates `object`.
 * @since 1.0.34
 * @name get
 * @memberof core.objects
 * @function
 * @instance
 * @param {Object} object - The object to change.
 * @param {Array|string} path - The path of the property to unset.
 * @returns {boolean} Returns `true` if the property is deleted, else `false`.
 * @example
 *
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 *
 * console.log( unset(object, 'a[0].b.c' ) ) ; // true
 * console.log( object ) ; // { 'a': [{ 'b': {} }] }
 */
const unset = ( object , path ) => object === null ? true : baseUnset(object, path) ;

export default unset ;

/**
 * The base implementation of `unset`.
 * @private
 * @param {Object} object - The object to query.
 * @param {Array|string} path - The path of the property to unset.
 * @returns {boolean} Returns `true` if the property is deleted, else `false`.
 */
export const baseUnset = ( object , path ) =>
{
    path = castPath( path , object ) ;
    object = parent( object , path ) ;
    return object === null || delete object[ toKey( last(path) ) ] ;
};