'use strict'

/**
 * Checks if `key` is a direct property of `object`.
 * @since 1.0.38
 * @name has
 * @memberof core.object
 * @function
 * @instance
 * @param {Object} object The object to query.
 * @param {string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 * @example
 * const object = { 'a': { 'b': 2 } } ;
 * const other  = create({ 'a': create({ 'b': 2 }) })
 * console.log( has(object, 'a') ) ; // true
 * console.log( has(other, 'b') ) ; // false
 */
const has = ( object, key) => object !== null && object !== undefined && Object.prototype.hasOwnProperty.call( object , key ) ;

export default has ;