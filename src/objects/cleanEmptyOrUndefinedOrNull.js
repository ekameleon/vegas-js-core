'use strict'

import clean from './clean'

import replaceEmptyOrUndefinedOrNull from './replaceEmptyOrUndefinedOrNull'

export const cleanEmptyOrUndefinedOrNull = ( object , replacer = replaceEmptyOrUndefinedOrNull ) => clean( object , replacer ) ;

export default cleanEmptyOrUndefinedOrNull ;