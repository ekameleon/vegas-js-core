'use strict'

import root from './_/root.js'

/** Detect free variable `exports`. */
const freeExports = typeof exports === 'object' && exports && !exports?.nodeType && exports;

/** Detect free variable `module`. */
const freeModule = freeExports && typeof module === 'object' && module && !module?.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
const moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
const Buffer = moduleExports ? root?.Buffer : undefined ;

/**
 * Checks if `value` is a buffer.
 * @memberOf core
 * @since 1.0.36
 * @function
 * @instance
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * isBuffer( new Buffer(2)); // true
 * isBuffer( new Uint8Array(2)); // false
 */
const isBuffer = Buffer?.isBuffer ?? ( () => false ) ;

export default isBuffer;