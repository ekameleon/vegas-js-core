/* jshint -W016 */

'use strict'

import baseClone from './_/baseClone.js';

export const CLONE_DEEP_FLAG    = 1 ;
export const CLONE_SYMBOLS_FLAG = 4 ;

/**
 * Clone all object values recursively`.
 * @static
 * @memberOf core
 * @since 3.0.26
 * @category Lang
 * @param {*} value The value to recursively clone.
 * @param {Function} customizer The function to customize cloning.
 * @returns {*} Returns the deep cloned value.
 * @example
 * const customizer => value =>
 * {
 *   if (isElement(value) )
 *   {
 *      return value.cloneNode(true) ;
 *   }
 * }
 * const el = cloneDeepWith( document.body , customizer ) ;
 * console.log(el === document.body)  ;// false
 * console.log(el.nodeName); // 'BODY'
 * console.log(el.childNodes.length); // 20
 */
const cloneDeepWith = ( value , customizer = null ) => baseClone
(
    value ,
    CLONE_DEEP_FLAG | CLONE_SYMBOLS_FLAG ,
    typeof(customizer) === 'function' ? customizer : undefined
) ;

export default cloneDeepWith ;