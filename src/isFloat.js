'use strict'

/**
 * Indicates if an value is a float number.
 * @name isFloat
 * @memberof core
 * @function
 * @instance
 * @param {number} value - The value to evaluates.
 * @return <code>true</code> if the passed-in value is a float.
 * @example
 * console.log( isFloat(0) ) ; // false
 * console.log( isFloat(0.5) ) ; // true
 * console.log( isFloat(1) ) ; // false
 */
const isFloat = ( value ) => (Number(value) === value) && (value % 1 !== 0);

export default isFloat ;