'use strict'

import reduce from './reduce.js'

/**
 * Creates an array of elements split into two groups, the first of whichcontains elements `predicate` returns truthy for, the second of which
 * contains elements `predicate` returns falsey for. The predicate is invoked with one argument: (value).
 * @since 1.0.37
 * @memberof core.collections
 * @function
 * @instance
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns An array of grouped things.
 * @example
 * const developers =
 * [
 *   { name : 'eka'  , age: 41 , active : false } ,
 *   { name : 'faya' , age: 40 , active : true  } ,
 *   { name : 'bob'  , age: 24 , active : false }
 * ]
 *
 *  partition( developers , ( { active } = {} ) => active ) // [['faya'], ['eka', 'bob']]
 */
const partition = ( collection, predicate ) => reduce
(
    collection,
    ( result , value ) =>
    {
        result[ predicate(value) ? 0 : 1 ].push( value ) ;
        return result ;
    },
    [ [] , [] ] ,
) ;

export default partition;