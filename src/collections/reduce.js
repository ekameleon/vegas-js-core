'use strict'

import arrayReduce from '../_/arrayReduce.js'
import baseEach    from '../_/baseEach.js'
import baseReduce  from '../_/baseReduce.js'

/**
 * Reduces `collection` to a value which is the accumulated result of running each element in `collection`
 * through `iteratee`, where each successive invocation is supplied the return value of the previous.
 * If `accumulator` is not given, the first element of `collection` is used as the initial value.
 * The iteratee is invoked with four arguments: ( accumulator , value , index|key , collection ).
 * @since 1.0.37
 * @memberof core.collections
 * @function
 * @instance
 * @param {Array|Object} collection - The collection to iterate over.
 * @param {Function} iteratee - The function invoked per iteration.
 * @param {*} [accumulator] - The initial value.
 * @returns {*} Returns the accumulated value.
 * @example
 * reduce( [1, 2], (sum, n) => sum + n, 0 ) ; // 3
 *
 * reduce({ 'a': 1, 'b': 2, 'c': 1 }, ( result = [] , value , key ) =>
 * {
 *   result[value] = result[value] ?? [] ;
 *   result[value].push( key ) ;
 *   return result ;
 * }, {})
 * // { '1': ['a', 'c'], '2': ['b'] } (iteration order is not guaranteed)
 */
const reduce = ( collection, iteratee , accumulator ) =>
{
    const func = Array.isArray(collection) ? arrayReduce : baseReduce ;
    const initAccum = arguments.length < 3 ;
    return func( collection , iteratee , accumulator , initAccum , baseEach );
}

export default reduce;