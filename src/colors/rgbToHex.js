'use strict'

/**
 * Transform a rgb component in this hexadecimal expression.
 * @name rgbToHex
 * @memberof core.colors
 * @function
 * @instance
 * @param {Array|number} [r=0] - The red component value or the [r,g,b] array definition.
 * @param {number} [g=0] - The green component value.
 * @param {number} [b=0] - The blue component value.
 * @example
 * console.log( rgbToHex( 0xFF , 0    , 0    ) ) ; // #ff0000
 * console.log( rgbToHex( 0    , 0xFF , 0    ) ) ; // #00ff00
 * console.log( rgbToHex( 0    , 0    , 0xFF ) ) ; // #0000ff
 */
const rgbToHex = ( r= 0 , g= 0 , b= 0 ) =>
{
    if( Array.isArray(r ) && r.length === 3 )
    {
        g = r[1] ;
        b = r[2] ;
        r = r[0] ;
    }
    return '#' + [ r , g , b ].map( x => Number(x).toString(16).padStart(2, '0') ).join('') ;
}

export default rgbToHex ;