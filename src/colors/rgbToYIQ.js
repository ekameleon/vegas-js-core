'use strict'

/**
 * Converts the rgb color space components into YIQ.
 * More information about YIQ : https://en.wikipedia.org/wiki/YIQ
 * @name rgbToYIQ
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} r - The red component value.
 * @param {number} g - The green component value.
 * @param {number} b - The blue component value.
 * @return {number} Returns the YIQ value.
 * @example
 * console.log( rgbToYIQ( 209 ,  83 , 77 ) ) ; // #D1534D => 119.99
 * console.log( rgbToYIQ( 230 , 167 , 75 ) ) ; // #E6A74B => 175.349
 * console.log( rgbToYIQ( 250 , 240 , 94 ) ) ; // #FAF05E => 226.346
 */
const rgbToYIQ = ( r , g , b ) => ( (r*299) + (g*587) + (b*114) ) / 1000 ;

export default rgbToYIQ ;
