'use strict'

import leading from '../numbers/leading'

/**
 * Converts a value between 0 and 100 to an hexadecimal value between ('00' and 'FF').
 * @param {number} value - The value between 0 and 100.
 * @param {boolean} upperCase - Indicates if the result expression is uppercase.
 * @return {string} The hexadecimal string representation of the percentage value.
 * @since 1.0.43
 * @example
 * console.log( percentageToHex( 25 ) ) ;
 */
const percentToHex = ( value , upperCase = false ) =>
{
    const result = leading( Math.round(value / 100 * 255 ).toString(16 ) ) ;
    return upperCase ? result.toUpperCase() : result ;
}

export default percentToHex ;