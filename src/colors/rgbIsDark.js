'use strict'

/**
 * Indicates if the rgb color space components defines a dark color.
 * This function use the YIQ equation : https://en.wikipedia.org/wiki/YIQ
 * @name rgbIsDark
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} r - The red component value.
 * @param {number} g - The green component value.
 * @param {number} b - The blue component value.
 * @return {boolean} Returns <code>true</code> if the rgb color is dark.
 * @example
 * console.log( rgbIsDark( 209 ,  83 , 77 ) ) ; // true
 * console.log( rgbIsDark( 230 , 167 , 75 ) ) ; // false
 * console.log( rgbIsDark( 250 , 240 , 94 ) ) ; // false
 */
const rgbIsDark = ( r , g , b ) => (( (r*299) + (g*587) + (b*114) ) / 1000) < 128 ;

export default rgbIsDark ;
