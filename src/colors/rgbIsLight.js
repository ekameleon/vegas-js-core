'use strict'

/**
 * Indicates if the rgb color space components defines a light color.
 * This function use the YIQ equation : https://en.wikipedia.org/wiki/YIQ
 * @name rgbIsLight
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} r - The red component value.
 * @param {number} g - The green component value.
 * @param {number} b - The blue component value.
 * @return {boolean} Returns <code>true</code> if the rgb color is light.
 * @example
 * console.log( rgbIsLight( 209 ,  83 , 77 ) ) ; // false
 * console.log( rgbIsLight( 230 , 167 , 75 ) ) ; // true
 * console.log( rgbIsLight( 250 , 240 , 94 ) ) ; // true
 */
const rgbIsLight = ( r , g , b ) => (( (r*299) + (g*587) + (b*114) ) / 1000) >= 128 ;

export default rgbIsLight ;
