'use strict'

import hexToRgb from './hexToRgb'

/**
 * Converts the hexadecimal string RGB color space expression into YIQ.
 * More information about YIQ : https://en.wikipedia.org/wiki/YIQ
 * @name hexToYIQ
 * @memberof core.colors
 * @function
 * @instance
 * @param {string} hex - The hexadecimal color string representation.
 * @return {string} Returns the YIQ value.
 * @example
 * console.log( hexToYIQ( '#D1534D' ) ) ; // 119.99
 * console.log( hexToYIQ( '#E6A74B' ) ) ; // 175.349
 * console.log( hexToYIQ( '#FAF05E' ) ) ; // 226.346
 */
const hexToYIQ = hex =>
{
    const [ r , g , b ] = hexToRgb(hex) ;
    return ((r*299)+(g*587)+(b*114))/1000;
};

export default hexToYIQ ;