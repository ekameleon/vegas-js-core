'use strict'

/**
 * Transform an hexadecimal color expression to a rgb object.
 * @name hexToRgb
 * @memberof core.colors
 * @function
 * @instance
 * @param {string} hex - The hexadecimal color string representation.
 * @return {number[]} the hexadecimal color string representation.
 * @example
 * console.log( hexToRgb( '#ff0000' ) ) ; // [255,0,0]
 * console.log( hexToRgb( '#00ff00' ) ) ; // [0,255,0]
 * console.log( hexToRgb( '#0000ff' ) ) ; // [0,0,255]
 */
const hexToRgb = hex =>
    hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i,(m, r, g, b) => '#' + r + r + g + g + b + b)
       .substring(1).match(/.{2}/g)
       .map(x => parseInt(x, 16)) ;

export default hexToRgb ;