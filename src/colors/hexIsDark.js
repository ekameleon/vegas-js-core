'use strict'

import hexToRgb from './hexToRgb'

/**
 * Indicates if the the hexadecimal string RGB color space expression is a dark color.
 * This function use the YIQ equation : https://en.wikipedia.org/wiki/YIQ
 * @name hexIsDark
 * @memberof core.colors
 * @function
 * @instance
 * @param {string} hex - The hexadecimal color string representation.
 * @return {boolean} Returns a boolean to indicates if the color is dark or not.
 * @example
 * console.log( hexIsDark( '#D1534D' ) ) ; // true
 * console.log( hexIsDark( '#E6A74B' ) ) ; // false
 * console.log( hexIsDark( '#FAF05E' ) ) ; // false
 */
const hexIsDark = hex =>
{
    const [ r , g , b ] = hexToRgb(hex) ;
    return (((r*299)+(g*587)+(b*114))/1000) < 128 ;
};

export default hexIsDark ;