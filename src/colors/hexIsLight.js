'use strict'

import hexToRgb from './hexToRgb'

/**
 * Indicates if the the hexadecimal string RGB color space expression is a light color.
 * This function use the YIQ equation : https://en.wikipedia.org/wiki/YIQ
 * @name hexIsLight
 * @memberof core.colors
 * @function
 * @instance
 * @param {string} hex - The hexadecimal color string representation.
 * @return {boolean} Returns a boolean to indicates if the color is light or not.
 * @example
 * console.log( hexIsLight( '#D1534D' ) ) ; // false
 * console.log( hexIsLight( '#E6A74B' ) ) ; // true
 * console.log( hexIsLight( '#FAF05E' ) ) ; // true
 */
const hexIsLight = hex =>
{
    const [ r , g , b ] = hexToRgb(hex) ;
    return (((r*299)+(g*587)+(b*114))/1000) >= 128 ;
};

export default hexIsLight ;