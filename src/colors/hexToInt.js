'use strict'

/**
 * Converts the hexadecimal string expression into this integer representation.
 * @name hexToInt
 * @memberof core.colors
 * @function
 * @instance
 * @param {string} hex - The hexadecimal string expression to transform.
 * @return {number} Returns the integer representation.
 * @example
 * console.log( hexToInt( '#FFFFFF' ) ) ; // 16777215
 * console.log( hexToInt( '#000000' ) ) ; // 0
 * console.log( hexToInt( '#FF0000' ) ) ; // 16711680
 */
const hexToInt = hex => parseInt( hex.replace(/^#/,'') , 16 ) ;

export default hexToInt ;