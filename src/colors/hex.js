/*jshint bitwise:false */
'use strict'

export default function hex( value , upper = true )
{
    let hex = Number(value).toString( 16 ) ;
    hex = hex.length % 2 === 0 ? hex : "0" + hex ;
    return upper ? hex.toUpperCase() : hex ;
}