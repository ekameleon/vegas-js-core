'use strict'

/*jshint bitwise:false*/

/**
 * Creates a new color with the fading between two rgb numeric colors and a specific ratio.
 * @name fade
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} from - The first color to interpolate.
 * @param {number} to - The second color to interpolate.
 * @param ratio The value between 0 and 1 to calculate the fading level.
 * @example <caption>Javascript example</caption>
 * 'use strict'
 * window.onload = function()
 * {
 *     if( !vegas )
 *     {
 *         throw new Error("The VEGAS library is not found.") ;
 *     }
 *
 *     // ----- imports
 *
 *     let global   = vegas.global ; // jshint ignore:line
 *     let core     = vegas.core   ; // jshint ignore:line
 *     let system   = vegas.system ; // jshint ignore:line
 *     let graphics = vegas.graphics ; // jshint ignore:line
 *
 *     let fade  = core.colors.fade ;
 *     let toHex = core.colors.toHex ;
 *     let Point = graphics.geom.Point ;
 *
 *     // ----- initialize
 *
 *     let canvas  = document.getElementById('canvas') ;
 *     let context = canvas.getContext('2d');
 *
 *     canvas.width  = 800;
 *     canvas.height = 640;
 *
 *     let width  = canvas.width ;
 *     let height = canvas.height ;
 *
 *     let size = 40 ;
 *
 *     let ratio ;
 *     let rgb ;
 *     let color ;
 *
 *     let clear = function()
 *     {
 *         context.clearRect(0, 0, width, height);
 *         context.fillStyle = '#333333' ;
 *         context.fillRect(0, 0, width, height );
 *     }
 *
 *     let render = function( position , start , end , rows )
 *     {
 *         for( let i = 0 ; i < rows ; i++ )
 *         {
 *             ratio = i/(rows-1);
 *             color = fade( start , end, ratio ) ;
 *
 *             rgb = toHex( color ) ;
 *
 *             context.fillStyle = rgb ;
 *             context.fillRect( position.x + (i * size) , position.y , size , size );
 *
 *             console.log( ratio + " => value:" + color + " color:" + rgb );
 *         }
 *     }
 *
 *     clear() ;
 *     render( new Point(25, 25) , 0x00FF00 , 0xFF0000 , 12 ) ;
 *     render( new Point(25, 75) , 0xFFFFFF , 0x000000 , 12 ) ;
 *     render( new Point(25,130) , 0x125D7F , 0xFFED5D , 12 ) ;
 *  }
 * @example <caption>HTML page</caption>
 * <!doctype html>
 * <html>
 * <head>
 *     <meta charset="UTF-8">
 *     <title>Test core.colors.fade()</title>
 *     <style>
 *     body
 *     {
 *         margin: 0px;
 *         padding: 0px;
 *     }
 *     </style>
 * </head>
 * <body>
 *     <canvas id="canvas" width="100%" height="100%"></canvas>
 *     <script src="./js/vegas.js"></script>
 *     <script src="./js/fade.js"></script>
 * </body>
 * </html>
 */
const fade = ( from = 0 , to = 0xFFFFFF , ratio = 0 ) =>
{
    if( ratio <= 0 )
    {
        return from ;
    }
    else if( ratio >= 1 )
    {
        return to ;
    }

    let r = from >> 16 ;
    let g = from >> 8 & 0xFF ;
    let b = from & 0xFF ;

    r += ( ( to >> 16       ) -r ) * ratio ;
    g += ( ( to >> 8 & 0xFF ) -g ) * ratio ;
    b += ( ( to      & 0xFF ) -b ) * ratio ;

    return r<<16 | g<<8 | b ;
};

export default fade ;