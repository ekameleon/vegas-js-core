/*jshint bitwise:false*/
'use strict'

/**
 * Extract the blue part of an ARGB color (value between <code>0</code> and <code>255</code>).
 * @return The blue part of an ARGB color (value between 0 and 255).
 * @name getBlue
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} color - The color value to evaluates.
 * @example
 * console.log( getBlue( 0x0000FF ) ) ; // 255
 * console.log( getBlue( 0xFFFF00 ) ) ; // 0
 * console.log( getBlue( 0x000000 ) ) ; // 0
 */
const getBlue = ( color ) =>
{
    return color & 0xFF ;
};

export default getBlue ;