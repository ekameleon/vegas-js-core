'use strict'

/*jshint bitwise:false*/

import distance from './distance'

/**
 * Evaluates if two colors are similar with a specific tolerance ratio between 0 and 1.
 * @name equals
 * @memberof core.colors
 * @function
 * @instance
 * @param {number} color1 - The first color value.
 * @param {number} color2 - The second color value.
 * @param {number} [tolerance=0.01] - The tolerance ratio.
 * @return The value <code>true</code> if the two colors are similar.
 * @example
 * console.log( equals( 0xFFFFFF , 0x000000 ) ) ; // false
 * console.log( equals( 0xFF0000 , 0xFF0000 ) ) ; // true
 * console.log( equals( 0xFFFFFF , 0xFFFFFF ) ) ; // true
 *
 * console.log( equals( 0xFFFFFF , 0xFFEEFF ) ) ; // true
 * console.log( equals( 0xFFFFFF , 0xFFEEFF , 0 ) ) ; // false
 * console.log( equals( 0xFFFFFF , 0xFFEEFF , 1 ) ) ; // true
 */
const equals = ( color1 , color2 , tolerance = 0.01 ) =>
{
    return distance( color1 , color2 ) <= ( tolerance * ( 255 * 255 * 3 ) << 0 ) ;
};

export default equals ;