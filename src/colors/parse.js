'use strict'

import isNumber from '../isNumber'

export const CMYK   = 'cmyk'  ;
export const COLOR  = 'color' ;
export const DEG    = 'deg'   ;
export const GRAD   = 'grad'  ;
export const GRAY   = 'gray'  ;
export const NONE   = 'none'  ;
export const RAD    = 'rad'   ;
export const RGB    = 'rgb'   ;
export const TURN   = 'turn'  ;

/**
 * Base hues
 * http://dev.w3.org/csswg/css-color/#typedef-named-hue
 */
export const baseHues =
{
    red    : 0   ,
    orange : 60  ,
    yellow : 120 ,
    green  : 180 ,
    blue   : 240 ,
    purple : 300
}

/**
 * Parse a color with a string or number expression and returns the color definition.
 * @param {string|number} color - The color expression to parse.
 * @param {Object} names - An enumerations of color definitions, example: { aliceblue: [240, 248, 255], antiquewhite: [250, 235, 215] , ... }
 * @return The color definition.
 */
const parse = ( color , names = null ) =>
{
    let m ,
        alpha = 1 ,
        parts = [] ,
        space ;

    if ( isNumber( color ) )
    {
        return { space: RGB , values: [color >>> 16, (color & 0x00ff00) >>> 8, color & 0x0000ff] , alpha: 1 } ;
    }

    color = ( color + '' ).toLowerCase();

    if ( names?.[color] ) // keywords
    {
        parts = names[color].slice()
        space = RGB
    }
    else if (color === 'transparent') // reserved words
    {
        alpha = 0
        space = 'rgb'
        parts = [ 0 , 0, 0 ] ;
    }
    else if (color[0] === '#') // hexadecimal  
    {
        const base = color.slice(1) ;
        const size = base.length ;
        const isShort = size <= 4 ;
        
        alpha = 1 ;

        if (isShort) 
        {
            parts = 
            [
                parseInt(base[0] + base[0], 16),
                parseInt(base[1] + base[1], 16),
                parseInt(base[2] + base[2], 16)
            ];

            if ( size === 4 )
            {
                alpha = parseInt(base[3] + base[3], 16) / 255
            }
        }
        else
        {
            parts =
            [
                parseInt(base[0] + base[1], 16),
                parseInt(base[2] + base[3], 16),
                parseInt(base[4] + base[5], 16)
            ] ;

            if ( size === 8 )
            {
                alpha = parseInt(base[6] + base[7], 16) / 255
            }
        }

        parts[0] = parts?.[0] ?? 0 ;
        parts[1] = parts?.[1] ?? 0 ;
        parts[2] = parts?.[2] ?? 0 ;

        space = RGB ;
    }
    else if ( m = /^((?:rgba?|hs[lvb]a?|hwba?|cmyk?|xy[zy]|gray|lab|lchu?v?|[ly]uv|lms|oklch|oklab|color))\s*\(([^\)]*)\)/.exec(color) )  // color space
    {
        let name = m[1] ;

        space = name.replace(/a$/, '') ;

        let dims = space === CMYK ? 4 : ( space === GRAY ? 1 : 3 ) ;

        parts = m[2].trim().split(/\s*[,\/]\s*|\s+/) ;

        // color(srgb-linear x x x) -> srgb-linear(x x x)
        if (space === COLOR )
        {
            space = parts.shift() ;
        }

        parts = parts.map( ( x, i) =>
        {
            //<percentage>
            if (x[x.length - 1] === '%')
            {
                x = parseFloat(x) / 100

                // alpha -> 0..1
                if ( i === 3 )
                {
                    return x ;
                }

                // rgb -> 0..255
                if (space === RGB )
                {
                    return x * 255 ;
                }

                // hsl, hwb H -> 0..100
                if (space[0] === 'h')
                {
                    return x * 100 ;
                }

                // lch, lab L -> 0..100
                if (space[0] === 'l' && !i)
                {
                    return x * 100 ;
                }

                // lab A B -> -125..125
                if (space === 'lab')
                {
                    return x * 125 ;
                }

                // lch C -> 0..150, H -> 0..360
                if (space === 'lch')
                {
                    return i < 2 ? x * 150 : x * 360 ;
                }

                // oklch / oklab L -> 0..1
                if (space[0] === 'o' && !i)
                {
                    return x ;
                }

                // oklab A B -> -0.4..0.4
                if (space === 'oklab')
                {
                    return x * 0.4 ;
                }

                // oklch C -> 0..0.4, H -> 0..360
                if (space === 'oklch')
                {
                    return i < 2 ? x * 0.4 : x * 360 ;
                }

                // color(xxx) -> 0..1
                return x ;
            }

            //hue
            if (space[i] === 'h' || (i === 2 && space[space.length - 1] === 'h'))
            {

                if ( baseHues?.[x] !== undefined ) //<base-hue>
                {
                    return baseHues[x] ;
                }

                if ( x.endsWith( DEG ) )
                {
                    return parseFloat(x) ;
                }

                if ( x.endsWith( TURN ) )
                {
                    return parseFloat(x) * 360 ;
                }

                if ( x.endsWith( GRAD ) )
                {
                    return parseFloat(x) * 360 / 400 ;
                }

                if ( x.endsWith( RAD ) )
                {
                    return parseFloat(x) * 180 / Math.PI ;
                }
            }

            if ( x === NONE )
            {
                return 0 ;
            }

            return parseFloat( x ) ;
        });

        alpha = parts.length > dims ? parts.pop() : 1 ;
    }
    else if ( /[0-9](?:\s|\/|,)/.test( color ) ) // named channels case
    {
        parts = color.match(/([0-9]+)/g).map( value => parseFloat(value) ) ;
        space = color.match(/([a-z])/ig)?.join('')?.toLowerCase() || RGB ;
    }

    return { space, values : parts , alpha }
}

export default parse ;