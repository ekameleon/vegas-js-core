'use strict'

/**
 * Formats a color value with the 'oklab(value)' expression.
 * @param value
 * @return {`oklab(${string})`}
 */
const oklabFormatter = value => `oklab(${value})` ;

export default oklabFormatter ;