'use strict'

/**
 * Formats a color value with the 'rgb(value)' expression.
 * @param value
 * @return {`rgb(${string})`}
 */
const rgbFormatter = value => `rgb(${value})` ;

export default rgbFormatter ;