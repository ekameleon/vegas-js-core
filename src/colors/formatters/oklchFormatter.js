'use strict'

/**
 * Formats a color value with the 'oklch(value)' expression.
 * @param value
 * @return {`oklch(${string})`}
 */
const oklchFormatter = value => `oklch(${value})` ;

export default oklchFormatter ;