/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is undefined.") ;
}

window.onload = function()
{
    let dom = core.dom ;

    let div = document.createElement( "div" ) ;
    let svg = document.createElementNS( "http://www.w3.org/2000/svg" , "svg" ) ;

    console.log( dom.isSVGElement( svg ) ) ;
    console.log( dom.isSVGElement( div ) ) ;
    console.log( dom.isSVGElement( {} ) ) ;
    console.log( dom.isSVGElement( null ) ) ;
};