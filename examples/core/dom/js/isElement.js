/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is undefined.") ;
}

window.onload = function()
{
    let div = document.createElement('div') ;
    let dom = core.dom ;

    console.log( dom.isElement( div ) ) ;
    console.log( dom.isElement( {} ) ) ;
    console.log( dom.isElement( null ) ) ;

    console.log( 'isDOMElement ' + dom.isDOMElement( div ) ) ;
    console.log( 'isDOMObject '  + dom.isDOMObject( div ) ) ;
};