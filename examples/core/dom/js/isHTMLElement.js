/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is undefined.") ;
}

window.onload = function()
{
    let div = document.createElement( "div" ) ;
    let svg = document.createElementNS( "http://www.w3.org/2000/svg" , "svg" ) ;

    let dom = core.dom ;

    console.log( dom.isHTMLElement( div  ) ) ; // true
    console.log( dom.isHTMLElement( svg  ) ) ; // false
    console.log( dom.isHTMLElement( null ) ) ; // false
}