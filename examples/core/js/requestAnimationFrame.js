/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is not found.") ;
}

let trace = core.trace  ; // jshint ignore:line
let perf = core.performance ;
let requestAnimationFrame = core.requestAnimationFrame ;

window.onload = function()
{
    let count = 0 ;
    let total = 50 ;
    let cur  = performance.now() ;
    let time = performance.now() ;

    const test = function()
    {
        if( count++ < total )
        {
            cur = perf.now() - cur ;
            console.log("----- " + cur) ;
            requestAnimationFrame( test ) ;
        }
        else
        {
            time = perf.now() - time ;
            console.log( "finish time:" + time ) ;
        }
    };

    test() ;
};