/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is not found.") ;
}

window.onload = function()
{
    const throttle = core.functors.throttle;

    let w = document.querySelector("#width") ;
    let h = document.querySelector("#height") ;
    let c = document.querySelector("#calls");

    let calls = 0;

    function getDimensions()
    {
        console.log( 'getDimensions' ) ;
        w.innerHTML = window.innerWidth;
        h.innerHTML = window.innerHeight;
        c.innerHTML = ++calls ;
    }
    
    window.addEventListener('resize', throttle( getDimensions , 1000 ) );
};