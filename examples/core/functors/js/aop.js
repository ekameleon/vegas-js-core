/* globals vegas */
/*jshint bitwise: false*/
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is not found.") ;
}

window.onload = function()
{
    let trace = core.trace  ; // jshint ignore:line

    let scope = { toString : function() { return "scope" ; } } ;

    let sum = function(x, y)
    {
        console.info( this + " calculating...")
        return x + y;
    };

    function begin()
    {
        trace("--- begin") ;
    };

    function end()
    {
        trace("--- end") ;
    };

    let result = core.functors.aop(sum, begin, end, scope)(3, 5) ;

    console.log( "--- result : " + result ) ;
};