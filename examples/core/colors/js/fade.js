/* globals vegas */
"use strict" ;

window.onload = function()
{
    if( !core )
    {
        throw new Error("The VEGAS JS | CORE library is undefined.") ;
    }

    // ----- imports

    let colors = core.colors  ; // jshint ignore:line
    let trace  = core.trace   ; // jshint ignore:line

    let fade  = colors.fade ;
    let toHex = colors.toHex ;

    let Point = function( x , y )
    {
        this.x = x ;
        this.y = y ;
    };

    // ----- initialize

    let canvas  = document.getElementById('canvas') ;
    let context = canvas.getContext('2d');

    canvas.width  = 800;
    canvas.height = 640;

    let width  = canvas.width ;
    let height = canvas.height ;

    let size = 40 ;

    let ratio ;
    let rgb ;
    let color ;

    let clear = function()
    {
        context.clearRect(0, 0, width, height);
        context.fillStyle = '#333333' ;
        context.fillRect(0, 0, width, height );
    };

    let render = function( position , start , end , rows )
    {
        for( let i = 0 ; i < rows ; i++ )
        {
            ratio = i/(rows-1);
            color = fade( start , end, ratio ) ;

            rgb = toHex( color ) ;

            context.fillStyle = rgb ;
            context.fillRect( position.x + (i * size) , position.y , size , size );

            trace( ratio + " => value:" + color + " color:" + rgb );
        }
    };

    clear() ;
    render( new Point(25, 25) , 0x00FF00 , 0xFF0000 , 12 ) ;
    render( new Point(25, 75) , 0xFFFFFF , 0x000000 , 12 ) ;
    render( new Point(25,130) , 0x125D7F , 0xFFED5D , 12 ) ;
 };
