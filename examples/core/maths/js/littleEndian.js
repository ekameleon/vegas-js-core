/* globals vegas */
"use strict" ;

if( !core )
{
    throw new Error("The VEGAS JS | CORE library is not found.") ;
}

window.onload = function()
{
    let trace = core.trace  ; // jshint ignore:line
    trace( core.maths.littleEndian ) ;
};