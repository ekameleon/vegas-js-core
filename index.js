"use strict"

import global from './src/global.js'

import canUseSymbol from './src/canUseSymbol'
import dump         from './src/dump'
import trace        from './src/trace'
import version      from './src/version'

import cancelAnimationFrame  from './src/cancelAnimationFrame'
import requestAnimationFrame from './src/requestAnimationFrame'

import cloneDeep         from './src/cloneDeep'
import cloneDeepWith     from './src/cloneDeepWith'
import deepMerge         from './src/deepMerge'
import deepMergeAll      from './src/deepMergeAll'
import eq                from './src/eq'
import invariant         from './src/invariant'
import isArray           from './src/isArray'
import isArrayLike       from './src/isArrayLike'
import isArrayLikeObject from './src/isArrayLikeObject'
import isArguments       from './src/isArguments'
import isAsyncFunction   from './src/isAsyncFunction'
import isBoolean         from './src/isBoolean'
import isBuffer          from './src/isBuffer'
import isEmpty           from './src/isEmpty'
import isEqual           from './src/isEqual'
import isEqualWith       from './src/isEqualWith'
import isFloat           from './src/isFloat'
import isFunction        from './src/isFunction'
import isIndex           from './src/isIndex'
import isInt             from './src/isInt'
import isLength          from './src/isLength'
import isMap             from './src/isMap'
import isMergeableObject from './src/isMergeableObject'
import isNil             from './src/isNil'
import isNotEmpty        from './src/isNotEmpty'
import isNotNull         from './src/isNotNull'
import isNotNullObject   from './src/isNotNullObject'
import isNumber          from './src/isNumber'
import isObject          from './src/isObject'
import isObjectLike      from './src/isObjectLike'
import isPlainObject     from './src/isPlainObject'
import isPrimitive       from './src/isPrimitive'
import isReactElement    from './src/isReactElement'
import isSet             from './src/isSet'
import isString          from './src/isString'
import isSymbol          from './src/isSymbol'
import isTypedArray      from './src/isTypedArray'
import isUint            from './src/isUint'
import isWeakMap         from './src/isWeakMap'
import isWeakSet         from './src/isWeakSet'
import stubFalse         from './src/stubFalse'
import stubTrue          from './src/stubTrue'
import toPlainObject     from './src/toPlainObject'

import arrays      from './src/arrays'
import chars       from './src/chars'
import colors      from './src/colors'
import collections from './src/collections'
import date        from './src/date/index.js'
import dom         from './src/dom'
import easings     from './src/easings'
import functors    from './src/functors'
import graphics    from './src/graphics'
import maths       from './src/maths'
import numbers     from './src/numbers'
import objects     from './src/objects'
import random      from './src/random'
import reflect     from './src/reflect'
import strings     from './src/strings'

/**
 * The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies : arrays, strings, chars, objects, numbers, maths, date, colors, etc.
 * <p>You can consider a library as a set of functions organized into classes, here with a <strong>"core"</strong> library in some cases we organize the functions in the package definitions without assembling them into a class.</p>
 * <p>Those functions are allowed to reuse the builtin types (Object, Array, Date, etc.), the Javascript API classes and packages, but nothing else.</p>
 * @summary The {@link core} package is specialized in functions utilities that are highly reusable without creating any dependencies.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/)|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core
 * @version 1.0.3
 * @since 1.0.0
 */
const core =
{
    global ,

    canUseSymbol,
    dump ,
    trace ,
    version ,

    cancelAnimationFrame  ,
    requestAnimationFrame ,

    cloneDeep ,
    cloneDeepWith ,
    deepMerge,
    deepMergeAll,
    eq,
    invariant,
    isArray,
    isArrayLike,
    isArrayLikeObject,
    isArguments,
    isAsyncFunction,
    isBoolean,
    isBuffer,
    isEmpty,
    isEqual ,
    isEqualWith ,
    isFloat,
    isFunction,
    isIndex,
    isInt,
    isLength,
    isMap,
    isMergeableObject,
    isNil,
    isNotEmpty,
    isNotNull,
    isNotNullObject,
    isNumber,
    isObject,
    isObjectLike,
    isPlainObject,
    isPrimitive,
    isReactElement,
    isSet ,
    isString,
    isSymbol,
    isTypedArray,
    isUint,
    isWeakMap ,
    isWeakSet ,
    stubFalse ,
    stubTrue  ,
    toPlainObject ,

    arrays      ,
    chars       ,
    collections ,
    colors      ,
    date        ,
    dom         ,
    easings     ,
    functors    ,
    graphics    ,
    maths       ,
    numbers     ,
    objects     ,
    random      ,
    reflect     ,
    strings     ,
} ;

export default core ;
