# VEGAS JS CORE OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.45] - 2024-11-08
### Fixed
- Adds core.isBuffer
- 
## [1.0.44] - 2024-11-08
### Added
- Adds core.isAsyncFunction
- Adds core.arrays.flatten
- Adds core.functors.callOrReturn
- Adds core.functors.maybeCall
- Adds core.numbers.toIntegerOrInfinity
- Adds core.strings.hex2bin
- Adds core.strings.substr : To fix the deprecated warning and polyfill the function.

## [1.0.43] - 2024-09-30
### Added
- Adds core.functors.negate
- Adds core.functors.noop
- Adds core.numbers.avg
- Adds core.numbers.isEven
- Adds core.numbers.isOdd
- Adds core.numbers.sma
- Adds core.numbers.sum
- Adds core.numbers.toFixed

- Adds core.colors.percentToHex
- Adds core.invariant
### Changed
- Adds core.functors.sortBy : adds the complex properties path to compare a complex member (ex: 'user.dates.birtday' )
- Updates all unit tests

## [1.0.42] - 2024-09-25
### Added
- Adds core.objects.omit 
- Adds core.objects.pair 

## [1.0.41] - 2024-09-24
### Added
- Adds the core.colors.htmlColors enumeration based on https://www.w3.org/TR/SVG11/types.html#ColorKeywords
- Adds the core.colors.parse function

## [1.0.40] - 2024-09-20
### Added
- Adds core.objects.map
- Adds core.strings.pascalCase

## [1.0.39] - 2024-09-13
### Added
- Adds core.arrays.range
- Adds core.objects.mergeDeep
- Adds core.isArrayLikeObject
- Adds core.isPrimitive
- Adds core.toPlainObject

## [1.0.38] - 2024-09-09
### Added
- Adds core.arrays.map
- Adds core.objects.has
- Adds core.objects.hasIn
- Adds core.objects.hasPathIn
- Adds core.objects.pick
- Adds core.objects.pickBy

## [1.0.37] - 2024-09-05
### Added
- Adds core.forEach
- Adds core.forEachRight
- Adds core.isEqual
- Adds core.isEqualWith
- Adds core.toString
- Adds core.arrays.some
- Adds core.collections.partition
- Adds core.collections.reduce
- Adds core.strings.diceCoefficient
- Adds core.strings.kebabCase
- Adds core.strings.nGram
- Adds core.strings.bigram
- Adds core.strings.trigram
- Adds core.objects.at
 
## [1.0.36] - 2024-08-22
### Added
- Adds core.cloneDeep function : big new tool based on loadash library
- Adds core.cloneDeepWith function
- Adds core.arrays.each function
- Adds core.arrays.stub function
- Adds core.arrays.toLength function
- Adds core.numbers.toFinite function
- Adds core.numbers.toInteger function
- Adds core.strings.stub function
- Adds core.objects.keys function
- Adds core.objects.keysIn function
- Adds core.objects.stub function
- Adds core.isArray function
- Adds core.isArrayLike function
- Adds core.isBuffer function
- Adds core.isMap function
- Adds core.isSet function
- Adds core.isTypedArray function
- Adds core.isWeakMap function
- Adds core.isWeakSet function
- Adds core.stubFalse function
- Adds core.stubTrue function

## [1.0.35] - 2024-08-21
### Added
- Adds core.objects.hasPath function
- Adds core.objects.update function
- Adds core.isArguments function
- Adds core.isLength function
- Adds core.isObjectLike function
### Changed
- Move core.objects.isIndex to core.isIndex

## [1.0.34] - 2024-08-20
### Added
- Adds core.objects.unset function
- Adds core.numbers.nullifyZeroAndNaN function
- Adds core.maths.sharpen function

## [1.0.33] - 2024-08-05
### Added
- Adds core.functors.empty function
- Adds core.dom.isTouchDevice function
- Adds core.dom.passiveIfSupported constant
- Adds core.numbers.nullify function
- Adds core.numbers.padTo2Digits function
- Adds core.numbers.padTo3Digits function
- Adds core.objects.cleanEmptyOrUndefinedOrNull
- Adds core.strings.nullify
- Adds core.strings.undefinify
### Changed
- Refactor the core.arrays.swap function, use the [ array[a] , array[b] ] = [ array[b] , array[a] ] ES6 notation.
- Refactoring the core.objects.members function, use the Object.values or Object.keys methods inside now.
- Fix the core.objects.assignValue function to accept the 0 value.

## [1.0.32] - 2021-05-12
### Added
- Adds core.arrays.take function
- Adds core.arrays.takeRight function

## [1.0.31] - 2021-05-11
### Changed
- Fix core.objects.set function
- Fix core.strings.stringToPath 

## [1.0.30] - 2021-05-07
### Added
- Adds core.functors.sortBy function

## [1.0.29] - 2021-05-05
### Changed
- Upgrade core.objects.isEmpty function, use the Object.getOwnPropertyNames. 

## [1.0.28] - 2021-05-05
### Added
- Add core.arrays.nullify function : console.log( nullify( [] ) ) ; // null

## [1.0.27] - 2021-05-03
### Changed
- Adds the replacer:Function argument in the core.objects.clean function

## [1.0.26] - 2021-05-03
### Added
- Add core.object.assignProperties function
- Add core.object.removeProperties function

## [1.0.25] - 2021-05-01
### Added
- Add core.object.isEmpty function

## [1.0.24] - 2021-03-27
### Added
- Add core.arrays.move function
### Changed
- Update the package.json dependencies

## [1.0.23] - 2021-03-27
### Added
- Add core.dom.isTouchDevice function
- Add core.strings.pluralize function

## [1.0.22] - 2020-04-14
### Added
- Add core.date.solveDST method (daylight saving time gap) + implement it in all add/subtract methods.
- Add core.date.MONTH_PER_CENTURY constant
- Add core.date.MONTH_PER_DECADE constant
- Add core.date.MONTH_PER_YEAR constant
- Add core.date.MS_PER_WEEK constant
- Add core.date.daysInFebruary function
- Add core.date.daysOf function
- Add core.date.addWeeks function
- Add core.date.addMonths function
- Add core.date.addYears function
- Add core.date.endOfWeek function
- Add core.date.firstVisibleDay function
- Add core.date.lastVisibleDay function
- Add core.date.startOfWeek function
- Add core.date.subtractMonths function
- Add core.date.subtractWeeks function
- Add core.date.subtractYears function
- Add core.date.visibleDays function

## [1.0.21] - 2020-04-08
### Changed
- Update the core.functors.compose function, accept non function parameter and remove it.

## [1.0.20] - 2020-03-23
### Added
- Add the new core.arrays.empty function
- Add the new core.arrays.notEmpty function

## [1.0.19] - 2020-03-07
### Added
- Add the new core.arrays.inBetween function
- Add the new date.daysInYear function
- Add the new maths.floorMod function
### Changed
- Change the new date.daysInMonth function : the function accept always a Date parameter and now the pair (year,month) integer parameters, ex : daysOfMonth(2016,1)

## [1.0.18] - 2019-09-06
### Added
- Add the new date.MS_PER_DAY constant
- Add the new date.addDays function
- Add the new date.addHours function
- Add the new date.addMinutes function
- Add the new date.addMS function
- Add the new date.addSeconds function
- Add the new date.createDate function
- Add the new date.createUTCDate function
- Add the new date.daysToMonths function
- Add the new date.monthsToDays function
- Add the new date.subtractDays function
- Add the new date.subtractHours function
- Add the new date.subtractMinutes function
- Add the new date.subtractMS function
- Add the new date.subtractSeconds function

## [1.0.17] - 2019-08-16
### Added
- Add the new graphics.resizeHeightAndKeepAspectRatio function
- Add the new graphics.resizeWidthAndKeepAspectRatio function

## [1.0.16] - 2019-08-12
### Added
- Add the new isNotNull function

## [1.0.15] - 2019-04-13
### Added
- Add the new colors.hexToInt function

## [1.0.14] - 2019-04-13
### Added
- Add the new deepMergeAll function
- Add the new deepMerge function
- Add the new isPlainObject function
- Add the new isMergeableObject function
- Add the new isReactElement function (new package.json dev dependencies with react and @babel/preset-react, only for the unit test yet)
- Add the new canUseSymbol constant
- Add the new isNotNull function

## [1.0.13] - 2019-04-07
### Added
- Add the new colors.rgbIsDark function
- Add the new colors.hexIsDark function
- Add the new colors.rgbIsLight function
- Add the new colors.hexIsLight function
- Add the new colors.rgbToRgb function
- Add the new colors.hexToRgb function
- Add the new colors.rgbToHex function
- Add the new arrays.partition function
- Add the new objects.renameProperty function
- Add the new objects.removeProperty function
- Add the new arrays.distinct function 
- Add the new arrays.unique function 

## [1.0.12] - 2019-03-17
### Added
- Add the new isEmpty function 
- Add the new isNotEmpty function 

## [1.0.11] - 2019-02-26
### Changed
- Fix the new functors.memoize function

## [1.0.10] - 2019-02-26
### Added
- Add the new eq function
- Add the new isFunction function
- Add the new isSymbol function
- Add the new date.endOfMonth function
- Add the new date.endOfYear function
- Add the new date.startOfMonth function
- Add the new date.startOfYear function
- Add the new functors.memoize function
- Add the new functors.memoizeBounded function
- Add the new objects.get function
- Add the new objects.set function

## [1.0.9] - 2019-02-04
- Add the new objects.clean function

## [1.0.8] - 2019-01-08
### Added
- Add the new arrays.head function
- Add the new arrays.tail function
- Add the new functors.compose function

## [1.0.7] - 2018-12-16
- Add the new functors.debounce function
- Add the new functors.throttle function

## [1.0.5] - 2018-11-03
### Changed
- Change the /core folder name in /src
### Added
- Add the new isNil function
- Add the new numbers.clip function
- Add the new strings.empty function
- Add the new strings.notEmpty function
- Add the new strings.toString function

## [1.0.4] - 2018-10-28
### Added
- Change the name of the distribution files.

## [1.0.2] - 2018-10-24
### Added
- Add the version method

## [1.0.0] - 2018-03-07
### Added
- First version based on VEGAS JS 1.0.17

